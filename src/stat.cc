/*! \file stat.cc \brief (see stat.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"

// C++ std libraries
using namespace std;
#include <stdlib.h>
#include <iostream>

//______________________________________________________________________________
void find_lcritgal_los(double const &nsubs_m1m2, double par_dpdv[10],
                       double const &psi_los, double const &theta_los,
                       double const &lmin_start, double const &lmax,
                       double par_subs[25], double &m1, double &m2,
                       double const &user_rse, double const &tol,
                       double const &jgal_bkd, double &l_crit, int iter)
{
   //--- Finds recursively the critical distance below which Galactic clumps
   //    must be drawn given
   //     - their mass range [m1-m2];
   //     - a field of integration: LOS (psi_los, theta_los) + gSIM_ALPHAINT;
   //     - their mass and spatial distributions (nsubs_m1m2, par_dpdv, par_subs);
   //     - a given level of galactic continuum (jgal_bkd).
   //    The function stops when
   //        RSE (= sigma(j_sub) / { <j_sub> + jgal_bkd }) = user_rse,
   //    within a tolerance 'tol'.
   //    N.B.: par_subs contains all information to calculate the multi-level
   //    sub-sub-sub... contributions for the galactic clumps (ony if nlevel>1
   //    and ANNIHILATION is chosen).
   //
   //  nsubs_m1m2    Number of subhalos in the Galaxy in [m1,m2]
   //  par_dpdv[0]   dPdV_Gal normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Gal scale radius [kpc]
   //  par_dpdv[2]   dPdV_Gal shape parameter #1
   //  par_dpdv[3]   dPdV_Gal shape parameter #2
   //  par_dpdv[4]   dPdV_Gal shape parameter #3
   //  par_dpdv[5]   dPdV_Gal card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Gal radius [kpc]
   //  par_dpdv[7]   l_GC: distance observer to Gal. centre [kpc]
   //  par_dpdv[8]   psi_GC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_GC: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  lmin_start    First l_crit value tested [kpc]
   //  lmax          Upper value of the l range (edge of the Galactic halo) [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   //  m1            Lower value of mass range on which to find l_crit [Msol]
   //  m2            Upper value of mass range on which to find l_crit [Msol]
   //  user_rse      Targetted RSE [%]
   //  tol           Tolerance for matching user_rse
   //  jgal_bkd      Gal. bkgd in l.o.s. (annihil. [Msol^2/kpc^3] or decay [Msol/kpc^2])
   //  l_crit        Critical distance to find [kpc]
   //  iter          Number of recursive calls [must be 0 for first call]

   const int npar = 25;
   double par_subs_copy[npar];

   for (int n = 0; n < npar; n++)
      par_subs_copy[n] = par_subs[n];

   if (iter == 0) l_crit = lmin_start;

   // Mean and variance of J in the F.O.I
   double j_1cl = mean1cl_jn(par_dpdv, psi_los, theta_los, l_crit, lmax, par_subs_copy, m1, m2, 1);
   double varj_1cl = var1cl_j(par_dpdv, psi_los, theta_los, l_crit, lmax, par_subs_copy, m1, m2);
   if (varj_1cl < 0) {
      l_crit = lmax;
      return;
   }

   // RSE of all clumps in F.O.I
   double rse = sqrt(nsubs_m1m2 * varj_1cl) * 100. / (nsubs_m1m2 * j_1cl + jgal_bkd);

   // if criterion meets tolerance
   if (fabs(rse - user_rse) / user_rse < tol)
      return;
   // If l_crit too close to lmax, stop searching: l_crit = lmax
   else if (l_crit > lmax / 1.1) {
      l_crit = lmax;
      return;
   }

   ++iter;

   // Just for safety, if too many iteration, stop!
   if (iter > 20) {
      l_crit = lmax;
      return;
   }

   // if rse too large, l_crit must be increased
   double log_step = (log10(lmax) - log10(lmin_start)) / pow(2., iter);
   if (rse > user_rse)
      l_crit = pow(10., (log10(l_crit) + log_step));
   // otherwise, l_crit must be decreased
   else
      l_crit = pow(10., (log10(l_crit) - log_step));

   if (l_crit < lmin_start) {
      l_crit = lmin_start;
      return;
   }

   find_lcritgal_los(nsubs_m1m2, par_dpdv, psi_los, theta_los, lmin_start, lmax,
                     par_subs_copy, m1, m2, user_rse, tol, jgal_bkd, l_crit, iter);
}

//______________________________________________________________________________
double find_mthresh_fov(double par_tot[10], double const &frac, double par_dpdv[10],
                        double const &fov_deg, double const &l1, double const &l2,
                        double par_subs[25], double &mmin_subs, double &mmax_subs,
                        double const &user_rse, double const &tol, double const &jgal_bkd)
{
   //--- Finds, for a given host halo (which is not the Galactic halo), the
   //    threshold mass above which the subclumps in the host halo have to be drawn.
   //    N.B.: for this calculation the l.o.s. is always toward the host halo centre.
   //    N.B.: par_subs contains all information to calculate the multi-level
   //    sub-sub-sub... contributions for the galactic clumps (ony if nlevel>1
   //    and ANNIHILATION is chosen).
   //
   //    In practice, we calculate properly the variance of each sub-clump mass
   //    decade and decide to effectively draw the clumps above m_thresh if its
   //    fluctuation is larger than the "user_preset" the clump smooth contribution.
   //    If the option "is_gal_sm" and "is_gal_meansub" are set to true, takes
   //    into account the "diffuse" contribution from the smooth halo (so that it
   //    increases m_thresh in practise - the subcl have less contrast).
   //
   //  par_tot[0]    rho_tot host normalisation [Msol kpc^{-3}]
   //  par_tot[1]    rho_tot host scale radius [kpc]
   //  par_tot[2]    rho_tot host shape parameter #1
   //  par_tot[3]    rho_tot host shape parameter #2
   //  par_tot[4]    rho_tot host shape parameter #3
   //  par_tot[5]    rho_tot host card_profile [gENUM_PROFILE]
   //  par_tot[6]    rho_tot host radius [kpc]
   //  par_tot[7]    l_HC: distance observer to host centre [kpc]
   //  par_tot[8]    psi_HC: longitude of host centre [rad]
   //  par_tot[9]    theta_HC: latitude of host centre [rad]
   //  frac          Fraction of DM in subclumps (in the host halo)
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]
   //  fov_deg       F.O.V region [deg]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   //  mmin_subs     Lower mass of subhalos populating this halo [Msol]
   //  mmax_subs     Upper mass of subhalos populating this halo [Msol]
   //  user_rse      Targetted RSE [%]
   //  tol           Tolerance for matching user_rse
   //  jgal_bkd      Gal. bkgd in FOV (annihil. [Msol^2/kpc^3] or decay [Msol/kpc^2])

   const int npar = 25;
   double par_subs_copy[npar];

   for (int n = 0; n < npar; n++)
      par_subs_copy[n] = par_subs[n];


   double psi_host_ref = par_tot[8];
   double theta_host_ref = par_tot[9];
   par_tot[8] = 0;
   par_tot[9] = 0;
   par_dpdv[8] = 0;
   par_dpdv[9] = 0;

   cout << "    * Calculate from which mass to start drawing clumps:"
        " sig_j/<j> < " << user_rse << "% (within tol=" << tol << ")" << endl;
   cout << "     [uses ten directions away from the halo centre and take the minimal mass]"
        << endl;

   // Determines the lower mass from which to draw clumps, for 10 directions
   // across the clump (dSph or cluster). Note that in the par_dpdv passed in
   // this function, the position of the clump has to be (phi,theta)=(0,0)
   // to avoid funny behaviours at large theta angles (this explains why
   // for any object we only move around (0,0) below).

   double mtot = mass_singlehalo(par_tot, par_subs_copy[5]);
   double mtot_subs = frac * mtot;
   double ntot_subs = nsubtot_from_msubtot(mtot_subs, mmin_subs, mmax_subs,
                                           &par_subs_copy[8], par_subs_copy[5]);
   double m_thresh = 1.e40;

   printf("           Theta   jhost_bkd   jgal_bkd  =>  msubcl_thresh\n");
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      printf("           [deg]       [Msol^2/kpc^5]            Msol\n");
   else
      printf("           [deg]        [Msol/kpc^2]             Msol\n");
   for (int j = 0; j < 10 ; ++j) {
      double psi_obs = j * (fov_deg / sqrt(2.) / 9.) * DEG_to_RAD;
      check_psi(psi_obs);
      double theta_obs = 0.;

      // 1. j smooth for host halo

      double alphaint_orig = gSIM_ALPHAINT;
      if (par_subs[6] > ZMIN_EXTRAGAL) gSIM_ALPHAINT = atan(1. / (1 + par_subs[6]) * MY_TAN(gSIM_ALPHAINT));
      double jhost_bkd = jsmooth_mix(mtot, par_tot, psi_obs, theta_obs,
                                     l1, l2, par_subs_copy[5], frac, par_dpdv);
      gSIM_ALPHAINT = alphaint_orig;

      double m_thresh_current = mmax_subs;


      find_mthresh_los(ntot_subs, par_dpdv, psi_obs, theta_obs, l1, l2,
                       par_subs_copy, mmin_subs, mmax_subs, user_rse, tol,
                       jhost_bkd, jgal_bkd, m_thresh_current);

      printf("           %.2f    %.2le    %.2le        %.2le \n",
             psi_obs * RAD_to_DEG, jhost_bkd, jgal_bkd, m_thresh_current);
      m_thresh = min(m_thresh, m_thresh_current);
   }
   printf("        => min(msubcl_thresh)=%.2le Msol\n\n", m_thresh);

   par_tot[8] = psi_host_ref;
   par_tot[9] = theta_host_ref;
   par_dpdv[8] = psi_host_ref;
   par_dpdv[9] = theta_host_ref;

   return m_thresh;
}

//______________________________________________________________________________
void find_mthresh_los(double const &ntot_subs, double par_dpdv[10],
                      double const &psi_los, double const &theta_los,
                      double const &l1, double const &l2, double par_subs[25],
                      double &mmin_subs, double &mmax_subs, double const &user_rse,
                      double const &tol, double const &jhost_bkd,
                      double const &jgal_bkd, double &m_thresh, int iter)
{
   //--- Finds recursively the threshold mass above which subclumps in a host halo
   //    (which is an 'external' halo, i.e. not the Gal. halo) must be drawn given
   //       - their mass range [mmin_subs-mmax_subs];
   //       - a field of integration: LOS (psi_los, theta_los) + gSIM_ALPHAINT;
   //       - their mass and spatial distributions (ntot_subs, par_dpdv, par_subs);
   //       - a given level of host+galactic continuum (jhost_bkd, jgal_bkd).
   //    The function stops when
   //        RSE (= sigma(j_sub) / { <j_sub> + jhost_bkd + jgal_bkd}) = user_rse,
   //    within a tolerance 'tol'.
   //    N.B.: par_subs contains all information to calculate the multi-level
   //    sub-sub-sub... contributions for the galactic clumps (ony if nlevel>1
   //    and ANNIHILATION is chosen).
   //
   //    As a rule of thumb, if the DM clump size is much smaller than its distance,
   //    the subclumps are all located at the same position, hence all sub-clumps
   //    of a given mass contribute equally to the signal. In that case, the only
   //    reason why J_subcl can fluctuate above its mean value (i.e. should be drawn)
   //    is if they are to few of them. This happens for the "high mass" sub-clumps
   //    (hence a given mass threshold) as the small mass clumps are the most numerous.
   //
   //  ntot_subs     Total number of subhalos in the Galaxy
   //  par_dpdv[0]   dPdV_Host normalisation  [kpc^{-3}]
   //  par_dpdv[1]   dPdV_Host scale radius [kpc]
   //  par_dpdv[2]   dPdV_Host shape parameter #1
   //  par_dpdv[3]   dPdV_Host shape parameter #2
   //  par_dpdv[4]   dPdV_Host shape parameter #3
   //  par_dpdv[5]   dPdV_Host card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV_Host radius [kpc]
   //  par_dpdv[7]   l_HC: distance observer to Host Centre [kpc]
   //  par_dpdv[8]   psi_HC: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_HC: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)     [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl              [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)   [only for ANNIHILATION and nlevel>1]
   //  mmin_subs     Lower mass of subhalos populating this halo [Msol]
   //  mmax_subs     Upper mass of subhalos populating this halo [Msol]
   //  user_rse      Targetted RSE [%]
   //  tol           Tolerance for matching user_rse
   //  jhost_bkd     Host bkgd (annihil. [Msol^2/kpc^3] or decay [Msol/kpc^2])
   //  jgal_bkd      Gal. bkgd for this los (annihil. [Msol^2/kpc^3] or decay [Msol/kpc^2])
   //  m_thresh      Threshold mass to find [Msol]
   //  iter          Number of recursive calls [must be 0 for first call]

   const int npar = 25;
   double par_subs_copy[npar];

   for (int n = 0; n < npar; n++)
      par_subs_copy[n] = par_subs[n];

   if (iter == 0)
      m_thresh = mmax_subs;

   double frac_newmrange = frac_nsubs_in_m1m2(&par_subs_copy[8], mmin_subs, m_thresh, par_subs_copy[5]);
   double nsubs_m1m2 = ntot_subs * frac_newmrange;

   // Mean and variance of J in the F.O.I
   double j_1cl = mean1cl_jn(par_dpdv, psi_los, theta_los, l1, l2, par_subs_copy, mmin_subs, m_thresh, 1);
   double varj_1cl = var1cl_j(par_dpdv, psi_los, theta_los, l1, l2, par_subs_copy, mmin_subs, m_thresh);


   // RSE of all clumps in F.O.I
   double rse = sqrt(nsubs_m1m2 * varj_1cl) * 100. / (nsubs_m1m2 * j_1cl + jhost_bkd + jgal_bkd);

   // if criterion meets tolerance
   if (fabs(rse - user_rse) / user_rse < tol)
      return;
   // If m_thresh too close to mmin, stop searching: m_thresh = mmin
   else if (m_thresh < mmin_subs * 1.1) {
      m_thresh = mmin_subs;
      return;
   }

   ++iter;

   // if rse too large, m_thresh must be decreased
   double log_step = (log10(mmax_subs) - log10(mmin_subs)) / pow(2., iter);
   if (rse > user_rse)
      m_thresh = pow(10., (log10(m_thresh) - log_step));
   // otherwise, m_thresh must be increased
   else
      m_thresh = pow(10., (log10(m_thresh) + log_step));

   if (m_thresh > mmax_subs) {
      m_thresh = mmax_subs;
      return;
   }
   find_mthresh_los(ntot_subs, par_dpdv, psi_los, theta_los, l1, l2,
                    par_subs_copy, mmin_subs, mmax_subs, user_rse, tol,
                    jhost_bkd, jgal_bkd, m_thresh, iter);
}

//______________________________________________________________________________
void integrand_lum_variance(double &m, double par_subsvar[25], double &res)
{
   //--- Integrand (L(M)-<L>)^2 dP/dM for the calculation of the lum variance:
   //       L(M)=<L(M,c)>_c
   //    L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in subhalos).
   //    N.B.: par_subs contains all information to calculate the multi-level
   //    DM halos contribution (ony if nlevel>1 and ANNIHILATION is chosen).
   //
   // INPUTS:
   //  m               Mass of the clump [Msol]
   //  par_subsvar[0]  rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subsvar[1]  rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subsvar[2]  rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subsvar[3]  rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subsvar[4]  rho_cl(r) radius where to stop integration
   //  par_subsvar[5]  eps: relative precision sought L calculation
   //  par_subsvar[6]  z: redshift of the halo hosting the sub-clumps
   //  par_subsvar[7]  m: mass of rho_cl(r)
   //  par_subsvar[8]  dPdM normalisation
   //  par_subsvar[9]  dPdM slope alphaM
   //  par_subsvar[10] dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subsvar[11] dPdc mean concentration <c>
   //  par_subsvar[12] dPdc standard deviation             [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subsvar[13] dPdc card_profile [gENUM_CDELTA_DIST] [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subsvar[14] f: mass fraction of substructures   [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[15] nlevel: sub-sub...halos (1=no sub)  [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[16] dPdV normalisation  [kpc^{-3}]      [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[17] dPdV scale radius [kpc]             [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[18] dPdV shape parameter #1             [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[19] dPdV shape parameter #2             [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[20] dPdV shape parameter #3             [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[21] dPdV card_profile [gENUM_PROFILE]   [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[22] dPdV radius [kpc]                   [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[23] ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subsvar[24] <L>: mean L for the mass range under scrutiny
   // OUTPUT:
   //  res             Value of (L(M)-<L>)^2 dP/dM for this mass [Msol]

   // Get lum for this clump
   par_subsvar[7] = m;
   double lum = mean1cl_lumn_r_m(par_subsvar, 1);

   // Return integrand for this mass
   res = (lum - par_subsvar[24]) * (lum - par_subsvar[24]) * dpdm(&m, &par_subsvar[8]);
}

//______________________________________________________________________________
void integrand_mass_variance(double &m, double par_dpdmvar[3], double &res)
{
   //--- Integrand (M- <M>)^2 dP/dM for the calculation of the mass variance.
   //
   // INPUTS:
   //  m              Mass of the clump [Msol]
   //  par_dpdmvar[0] dPdM normalisation [1/Msol]
   //  par_dpdmvar[1] dPdM slope alphaM
   //  par_dpdmvar[2] <m>: mean mass for the mass range under scrutiny
   // OUTPUT:
   //  res            Value of (M- <M>)^2 dP/dM for this mass

   res = (m - par_dpdmvar[2]) * (m - par_dpdmvar[2]) * dpdm(&m, par_dpdmvar);
}

//______________________________________________________________________________
double mean1cl_jn(double par_dpdv[10], double const &psi_los, double const &theta_los,
                  double const &l1, double const &l2, double par_subs[25],
                  double &m1, double &m2, int n_pow_j, bool is_normalize_dpdm,
                  bool is_verbose)
{
   //--- Returns <J^n> = <L^n(r)/l^2n> = int_DOmega int_los dpdv(r) * L(r) dl dOmega.
   //    N.B.: L quantities are calculated for nlevel of substructures
   //    (described by the concentration dPdc, mass dPdM, and spatial dPdV
   //    distributions), each level having the same properties and a mass
   //    fraction f (in substructures).
   //    N.B.: Mean is not normalized to int_DOmega int_los dpdv(r) dl dOmega!
   //
   //  par_dpdv[0]   UNUSED
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dpdv centre [kpc]
   //  par_dpdv[8]   psi_C: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_C: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  m1            Minimal mass where dPdM applies [Msol]
   //  m2            Minimal mass where dPdM applies [Msol]
   //  n_pow_j       power of J (1 or 2)
   //  is_normalize_dpdm normalize dpdm or not

   int card_cdelta = int(par_subs[10]);

   if (card_cdelta == kPIERI11_AQUARIUS || card_cdelta == kPIERI11_VIALACTEA || card_cdelta == kB01_VIR_RAD || card_cdelta == kMOLINE17_200) {
      // create or check node profile distant-dependent c(M):

      double res, res0;
      double rmin = 1e-8 * par_dpdv[1];
      int n_par_lum = 28;
      int n_parsubs = 25;
      double par_lum[n_par_lum];
      for (int i = 0; i < n_parsubs; ++i)
         par_lum[i] = par_subs[i];
      par_lum[25] = m1;
      par_lum[26] = m2;
      par_lum[27] = is_normalize_dpdm;
      if (n_pow_j == 1) dpdv_lum_r(rmin, par_lum, res0);
      else if (n_pow_j == 2) dpdv_lum2_r(rmin, par_lum, res0);
      else print_error("stat.cc", "mean1cl_jn", "Luminosity power must be 1 or 2");
      double i_halo_select = -1;

      if (gHALO_NODES_X_GRID.size() > 1) {
         for (int i_halo = 1; i_halo < gHALO_NODES_X_GRID.size(); ++i_halo) {
            if (fabs(res0 - gHALO_NODES_INPUT_RHOSCALE[i_halo] * gHALO_NODES_Y_GRID[i_halo][0]) / res0 < SMALL_NUMBER) {
               i_halo_select = i_halo;

               if (is_verbose) {
                  string integrand;
                  if (n_pow_j == 1) integrand = "dP/dV(r)*<Lum>(r)";
                  else if (n_pow_j == 2) integrand = "dP/dV(r)*Lum^2>(r)";
                  cout << "            Found nodes for " << integrand << " numerical l.o.s. integration in mass decade [" << log10(m1) << "," << log10(m2) << "]..." << endl;
               }

               break;
            }
         }

      }

      int n_lookup_max = 1e5;
      if (i_halo_select == -1 or gHALO_NODES_X_GRID.size() > n_lookup_max) {
         // append node profile:
         if (gHALO_NODES_X_GRID.size() > n_lookup_max) i_halo_select = 0; // do not save halo profile, use and overwrite 1st matrix entry.
         else i_halo_select = gHALO_NODES_X_GRID.size();
         int n_nodes = int(100 * pow(par_subs[5], -0.5));

         double rmax = par_dpdv[6] * par_subs[23]; // make grid larger to ease finding r-2
         vector<double> r_vec = make_1D_grid(rmin, rmax, n_nodes, true);
         vector<double> dpdv_lum_r_vec(n_nodes);

         if (is_verbose) {
            string integrand;
            if (n_pow_j == 1) integrand = "dP/dV(r)*<Lum>(r)";
            else if (n_pow_j == 2) integrand = "dP/dV(r)*Lum^2>(r)";
            cout << "            Calculate nodes for " << integrand << " numerical l.o.s. integration in mass decade [" << log10(m1) << "," << log10(m2) << "]..." << endl;
         }

         dpdv_lum_r_vec[0] = res0;
         for (int i = 1; i < n_nodes; ++i) {
            res = 0;
            if (n_pow_j == 1) dpdv_lum_r(r_vec[i], par_lum, res);
            else if (n_pow_j == 2) dpdv_lum2_r(r_vec[i], par_lum, res);
            dpdv_lum_r_vec[i] = res;
         }
         set_rhonodes(r_vec, dpdv_lum_r_vec, i_halo_select, -1, -1, false);
      }

      int n_par_los = 10;
      double par_los[n_par_los];
      par_los[0] = gHALO_NODES_INPUT_RHOSCALE[i_halo_select];
      par_los[1] = gHALO_NODES_INPUT_RSCALE[i_halo_select];
      par_los[2] = i_halo_select; // choose kNODES profile
      par_los[3] = -1; // unused
      par_los[4] = -1; // unused
      par_los[5] = kNODES;
      par_los[6] = par_dpdv[6]; // Radius of rho [kpc]
      par_los[7] = par_dpdv[7]; // l_C: distance observer - profile centre [kpc]
      par_los[8] = par_dpdv[8]; // psi_C: to profile centre [rad]
      par_los[9] = par_dpdv[9]; // theta_C: to profile centre [rad]
      double rho_sat_orig = gDM_RHOSAT;
      gDM_RHOSAT = 1e100;       // switch off gDM_RHOSAT for this integration

      int switch_f;
      if (n_pow_j == 1) switch_f = 0;
      else if (n_pow_j == 2) switch_f = 5;
      // integrate  dpdv(r) * L^n(r) * l^{2*(1-n)} dl dOmega
      res = los_integral(par_los, switch_f, psi_los, theta_los, l1, l2, par_subs[5]);
      gDM_RHOSAT = rho_sat_orig;
      return res;

   } else {
      // no need to integrate over dpdv:

      // int_m1^m2 L^n(M) dP/dM dM
      double lum_n_bar = mean1cl_lumn_r(par_subs, m1, m2, n_pow_j, is_normalize_dpdm);

      // int_dOmega int_l1^l2 dP/dV dldOmega  * l^{2*(1-n)} => switch_f = 0 in los_integral()
      double lpowmin2n_bar = mean1cl_ln(par_dpdv, psi_los, theta_los, l1, l2, par_subs[5], -2 * n_pow_j);

      return lum_n_bar * lpowmin2n_bar;
   }
}

//______________________________________________________________________________
double mean1cl_ln(double par_dpdv[10], double const &psi_los, double const &theta_los,
                  double const &l1, double const &l2, double const &eps, int n_pow_l)
{
   //--- Returns <l^n> = int_dV int_l1^l2 l^n dP/dV dV
   //                  = int_dOmega int_l1^l2 l^n*l^2 dP/dV dl dOmega ,
   //    the mean n-th power of the distance for a clump in the range [l1,l2]
   //    in the direction (psi_los,theta_los).
   //    N.B.: Mean is not normalized to int_DOmega int_los dpdv(r) dl dOmega!

   //
   //  par_dpdv[0]   UNUSED
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dpdv centre [kpc]
   //  par_dpdv[8]   psi_C: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_C: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration
   //  n_pow_l       Select power of n in <l^n> (n=1, 2, -2, -4)

   // Set switch_f in los_integral() depending on n_pow_l value
   // (beware of the extra l^2 because of the dV in integration)
   //    - if <l>      => switch_f=3 integrates f=l^3*dP/dV
   //    - if <l^2>    => switch_f=4 integrates f=l^4*dP/dV
   //    - if <l^{-2}>=> switch_f=0 integrates f=dP/dV
   //    - if <l^{-4}>=> switch_f=5 integrates f=l^{-2}*dP/dV
   int switch_f;
   if (n_pow_l == 0)  switch_f = 2;
   else if (n_pow_l == 1) switch_f = 3;
   else if (n_pow_l == 2) switch_f = 4;
   else if (n_pow_l == -2) switch_f = 0;
   else if (n_pow_l == -4) switch_f = 5;
   else {
      const int buflen = 250;
      char char_tmp[buflen];
      snprintf(char_tmp, buflen, "n_pow_l = %d does not correspond to any option", n_pow_l);
      print_error("stat.cc", "mean1cl_ln()", string(char_tmp));
   }

   // <l^n>_1cl
   double res = los_integral(par_dpdv, switch_f, psi_los, theta_los, l1, l2, eps);

   return res;
}

//______________________________________________________________________________
double mean1cl_lumn_fullhalo(double par_subs[25], double &m1, double &m2,
                             int n_pow_lum, bool is_normalize_dpdm)
{
   //--- Returns <L^n>_{c,m,R}=int_0_Rmax int_m1^m2 (<L^n(R,m)>_c dP/dM) dM dR, the mean luminosity of
   //    a clump in the range [m1,m2], with <L^n(R,m)>_c =int_c1^c2 (L^n.dP/dc) dc,
   //    the mean luminosity of a clump of mass m within the range of concentration [c1, c2]
   //    averaged over the full host halo dpdv.
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  n_pow_lum     Select power of n in Lum^n (n=1, 2,)
   //  m1            Minimal mass for integration [Msol]
   //  m2            Maximal mass for integration [Msol]

   double res = 0.;
   int card_cdelta = int(par_subs[10]);
   if (card_cdelta == kPIERI11_AQUARIUS || card_cdelta == kPIERI11_VIALACTEA || card_cdelta == kB01_VIR_RAD || card_cdelta == kMOLINE17_200) {
      int n_par = 28;
      int n_parsubs = 25;
      double par[n_par];
      for (int i = 0; i < n_parsubs; ++i)
         par[i] = par_subs[i];
      par[25] = m1;
      par[26] = m2;
      par[27] = double(is_normalize_dpdm);

      double rmin = 1e-6 * par_subs[17];
      double rmax = par_subs[4];

      if (n_pow_lum == 1)      simpson_log_adapt(r2dpdv_lum_r, rmin, rmax, par, res, par_subs[5]);
      else if (n_pow_lum == 2) simpson_log_adapt(r2dpdv_lum2_r, rmin, rmax, par, res, par_subs[5]);
      else print_error("stat.cc", "mean1cl_lumn_fullhalo", "Luminosity power must be 1 or 2");
   } else {
      // no need to integrate over dpdv:
      res = mean1cl_lumn_r(par_subs, m1, m2, n_pow_lum, is_normalize_dpdm);
   }

   return res;
}

//______________________________________________________________________________
double mean1cl_lumn_r(double par_subs[25], double &m1, double &m2, int n_pow_lum, bool is_normalize_dpdm)
{
   //--- Returns <L^n(R)>_{c,m}=int_m1^m2 (<L^n(R,m)>_c dP/dM) dM, the mean luminosity of
   //    a clump in the range [m1,m2], with <L^n(R,m)>_c =int_c1^c2 (L^n.dP/dc) dc,
   //    the mean luminosity of a clump of mass m and position R in host within the
   //    range of concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  m1            Minimal mass for integration [Msol]
   //  m2            Maximal mass for integration [Msol]
   //  n_pow_lum     Select power of n in Lum^n (n=1, 2,)
   //  is_normalize_dpdm normalize dpdm or not

   // Store par_subs[8]
   double ref_norm = par_subs[8];

   // Set normalisation to ensure int_m1^m2 dpdm = 1
   if (is_normalize_dpdm) dpdm_setnormprob(&par_subs[8], m1, m2, par_subs[5]);
   // <lum>_1cl
   double res = 0.;
   if (n_pow_lum == 1)      simpson_log_adapt(dpdm_lum_r_m, m1, m2, par_subs, res, par_subs[5]);
   else if (n_pow_lum == 2) simpson_log_adapt(dpdm_lum2_r_m, m1, m2, par_subs, res, par_subs[5]);
   else print_error("stat.cc", "mean1cl_lumn_r", "Luminosity power must be 1 or 2");

   // Reset norm
   if (is_normalize_dpdm) par_subs[8] = ref_norm;

   return res;
}

//______________________________________________________________________________
double mean1cl_lumn_r_m(double par_subs[25], int n_pow_lum)
{
   //--- Returns <L^n(R,m)>_c = int_c1^c2 (L^n(m,c).dP/dc(R)) dc, the mean luminosity of
   //    a clump as function of mass m and position R in host within the range of concentration [c1, c2].
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  n_pow_lum     Select power of n in Lum^n (n=1, 2,)

   double res = 0.;
   double mmax_sub = par_subs[7] * gDM_SUBS_MMAXFRAC;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par_subs[6]);
   double xpos;
   int card_cdelta = (int)par_subs[10];
   if (card_cdelta == kPIERI11_AQUARIUS || card_cdelta == kPIERI11_VIALACTEA || card_cdelta == kB01_VIR_RAD || card_cdelta == kMOLINE17_200) {
      xpos = par_subs[24] / par_subs[4];
   } else xpos = -1;

   // get cmean: Use directly in kDIRAC case, use for integration boundaries in dPdc case
   par_subs[11] = mdelta_to_cdelta(par_subs[7] /*mass*/, Delta_c, card_cdelta,
                                   par_subs, par_subs[6]/*z*/, xpos);

   // If kDIRAC, no need to integrate over dP/dc, get directly L(m, c_mean):
   if (gDM_FLAG_CDELTA_DIST == kDIRAC) {
      const int n_par = 26;
      double par_host[n_par];
      for (int i = 0; i < n_par - 2; ++i)
         par_host[i + 2] = par_subs[i];

      mdelta_to_par(par_host, par_host[9] /*mass*/, Delta_c, (int)par_host[12] /*card cdelta-mdelta*/,
                    &par_host[2] /* shape params */, par_host[7] /*EPS */, par_host[8] /*redshift*/,
                    par_host[13] /*cmean */, xpos);
      // update r_s of dPdV proportional to r_s of host:
      par_host[19] = par_host[25] * par_host[1];

      if (par_host[23] == kDPDV_PIERI11 and par_host[16] > SMALL_NUMBER) {
         // update r_s and shape parameter:
         gsl_function F;
         F.function = &solve_rbias;
         rbias_params_for_rootfinding params = {par_host[9], par_host[6], par_host[0], par_host[1],
                                                par_host[2], par_host[3], par_host[4], int(par_host[5]), par_host[16]
                                               };
         F.params = &params;
         double rmin = 1e-5 - SMALL_NUMBER;
         double rmax = 1e5;
         int return_status = 0;
         double rbias = rootsolver_gsl(gsl_root_fsolver_falsepos, F, rmin, rmax, gSIM_EPS, return_status); // faster than gsl_root_fsolver_brent and gsl_root_fsolver_bisection
         par_host[19] = par_host[1];
         par_host[20] = rbias;
         par_host[21] = 0;
         par_host[22] = 0;
      }

      if (!gPP_DM_IS_ANNIHIL_OR_DECAY || (int)par_host[17] == 1 || mmax_sub < gDM_SUBS_MMIN) {
         res = lum_singlehalo_nosubs(par_host, par_host[7]/*eps*/);
      } else {
         res = lum_singlehalo_subs(par_host);
      }
      return pow(res, n_pow_lum);
   } else {
      // Break integration in two parts (almost symmetric with a max at <c>)
      // and adjust lower and upper integration boundary for optimal results
      double res_lo = 0., res_hi = 0.;
      double cvir_max = 0.;
      double tol = par_subs[5];
      // Sample boundaries
      double cmin = 1.e-2 * par_subs[11];
      double cmax = 1.e3 * par_subs[11];
      // Find max (and associated c) of function
      // N.B.: use only 1 level to find ymax and boundaries (faster, and good approximation)
      int nevel_ref = (int)par_subs[15];
      par_subs[15] = 1;
      double y_max;
      double y_ref;

      if (n_pow_lum == 1) {
         y_max = goldenmin_logstep(cmin, par_subs[11], cmax, dpdc_lum, par_subs,
                                   tol, false, cvir_max);
         // Adjust lower and upper integration boundaries (fr c)
         y_ref = tol * y_max;
         //cout << "<c>=" << par_subs[11] << "  c_max=" << cvir_max << "  ymax=" << y_max << endl;
         find_x_logstep(y_ref, cmin, cmin, cvir_max, dpdc_lum, par_subs, tol, 0., y_max);
         find_x_logstep(y_ref, cmax, cvir_max, cmax, dpdc_lum, par_subs, tol, y_max, 0.);
         // Min/max done: reset level
         par_subs[15] = nevel_ref;

         // Calculate contributions on the two pieces
         simpson_log_adapt(dpdc_lum, cmin, cvir_max, par_subs, res_lo, tol);
         simpson_log_adapt(dpdc_lum, cvir_max, cmax, par_subs, res_hi, tol);
      } else if (n_pow_lum == 2) {
         y_max = goldenmin_logstep(cmin, par_subs[11], cmax, dpdc_lum2, par_subs,
                                   tol, false, cvir_max);
         // Adjust lower and upper integration boundaries (fr c)
         y_ref = tol * y_max;
         //cout << "<c>=" << par_subs[11] << "  c_max=" << cvir_max << "  ymax=" << y_max << endl;
         find_x_logstep(y_ref, cmin, cmin, cvir_max, dpdc_lum2, par_subs, tol, 0., y_max);
         find_x_logstep(y_ref, cmax, cvir_max, cmax, dpdc_lum2, par_subs, tol, y_max, 0.);
         // Min/max done: reset level
         par_subs[15] = nevel_ref;

         // Calculate contributions on the two pieces
         simpson_log_adapt(dpdc_lum2, cmin, cvir_max, par_subs, res_lo, tol);
         simpson_log_adapt(dpdc_lum2, cvir_max, cmax, par_subs, res_hi, tol);
      } else print_error("stat.cc", "mean1cl_lumn_r", "Luminosity power must be 1 or 2");
      return res_lo + res_hi;
   }
   return -1;
}

//__________________________________________________________________________
double mean1cl_mass(double par_dpdm[2], double &m1, double &m2, double const &eps)
{
   //--- Returns <M>=int_m1^m2 (M.dP/dM) dM, the mean mass of a clump
   //    in the range [m1,m2].
   //
   //  par_dpdm[0]   UNUSED
   //  par_dpdm[1]   dPdM slope alphaM
   //  m1            Minimal mass for integration [Msol]
   //  m2            Maximal mass for integration [Msol]
   //  eps           Relative precision sought for integration


   // Store par_dpdm[0]
   double ref_norm = par_dpdm[0];

   // Set normalisation to ensure int_m1^m2 dpdm = 1
   dpdm_setnormprob(par_dpdm, m1, m2, eps);

   // <m>_1cl
   double res = 0.;
   simpson_log_adapt(dpdm_m, m1, m2, par_dpdm, res, eps);

   // Reset norm
   par_dpdm[0] = ref_norm;

   return res;
}

//______________________________________________________________________________
double var1cl_j(double par_dpdv[10], double const &psi_los, double const &theta_los,
                double const &l1, double const &l2, double par_subs[25],
                double &m1, double &m2)
{
   //--- Returns variance(J) = <L^2/l^4> - <J>^2.
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  par_dpdv[0]   UNUSED
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dpdv centre [kpc]
   //  par_dpdv[8]   psi_C: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_C: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]
   //  m1            Minimal mass for integration [Msol]
   //  m2            Maximal mass for integration [Msol]

   double j2_bar = mean1cl_jn(par_dpdv, psi_los, theta_los, l1, l2, par_subs, m1, m2, 2);

   double j_bar = mean1cl_jn(par_dpdv, psi_los, theta_los, l1, l2, par_subs, m1, m2, 1);

   // N.B.: Normalization correction through division by int_DOmega int_los dpdv(r) dl dOmega
   return j2_bar - j_bar * j_bar / mean1cl_ln(par_dpdv, psi_los, theta_los, l1, l2, par_subs[5], 0.);
}

/* UNUSED
//______________________________________________________________________________
double var1cl_l(double par_dpdv[10], double const &psi_los, double const &theta_los,
                double const &l1, double const &l2, double const &eps)
{
   //--- Returns variance(l)=<l^2>-<l>^2 of a clump in the range [l1,l2]
   //    in the direction (psi_los,theta_los).
   //
   //  par_dpdv[0]   UNUSED
   //  par_dpdv[1]   dPdV scale radius [kpc]
   //  par_dpdv[2]   dPdV shape parameter #1
   //  par_dpdv[3]   dPdV shape parameter #2
   //  par_dpdv[4]   dPdV shape parameter #3
   //  par_dpdv[5]   dPdV card_profile [gENUM_PROFILE]
   //  par_dpdv[6]   dPdV radius [kpc]
   //  par_dpdv[7]   l_C: distance observer to dpdv centre [kpc]
   //  par_dpdv[8]   psi_C: longitude of dpdv [rad]
   //  par_dpdv[9]   theta_C: latitude of dpdv [rad]
   //  psi_los       psi_los: longitude for the l.o.s [rad]
   //  theta_los     theta_los: latitude for the l.o.s [rad]
   //  l1            Lower l.o.s. integration boundary [kpc]
   //  l2            Upper l.o.s. integration boundary [kpc]
   //  eps           Relative precision sought for integration

   double lbar = mean1cl_ln(par_dpdv, psi_los, theta_los, l1, l2, eps, 1);
   double lsquared_bar = mean1cl_ln(par_dpdv, psi_los, theta_los, l1, l2, eps, 2);
   return lsquared_bar - lbar * lbar;
}
*/

//______________________________________________________________________________
double var1cl_lum(double par_subs[25], double &m1, double &m2)
{
   //--- Returns variance(L) = int_m1^m2 (L(M,c)-<L>_{M,c})^2 dP/dM for a clump, with
   //       <L>_{M,c} = int_{m1}^{m2} <L(M,c)>_c dP/dM dM
   //    and
   //       L(M,c) = <L(M,c)>_c = L(M,<c>) for gENUM_CDELTA_DIST = kDIRAC
   //    N.B.: L quantities are calculated for nlevel of substructures (described
   //    by the concentration dPdc, mass dPdM, and spatial dPdV distributions),
   //    each level having the same properties and a mass fraction f (in substructures).
   //
   //  par_subs[0]   rho_cl(r) shape parameter #1 (inner profile for all clumps)
   //  par_subs[1]   rho_cl(r) shape parameter #2 (inner profile for all clumps)
   //  par_subs[2]   rho_cl(r) shape parameter #3 (inner profile for all clumps)
   //  par_subs[3]   rho_cl(r) gENUM_PROFILE (inner profile for all clumps)
   //  par_subs[4]   host halo outer radius where to stop integration [kpc]
   //  par_subs[5]   eps: relative precision sought L calculation
   //  par_subs[6]   z: redshift of the halo hosting the sub-clumps
   //  par_subs[7]   mdelta: mass of rho_cl(r)
   //  par_subs[8]   dPdM normalisation [1/Msol]
   //  par_subs[9]   dPdM slope alphaM
   //  par_subs[10]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par_subs[11]  dPdc mean concentration <c>
   //  par_subs[12]  dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[13]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par_subs[14]  f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par_subs[15]  nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par_subs[16]  dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par_subs[17]  dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par_subs[18]  dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par_subs[19]  dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par_subs[20]  dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par_subs[21]  dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par_subs[22]  host halo outer radius (unused)      [only for ANNIHILATION and nlevel>1]
   //  par_subs[23]  ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   //  par_subs[24]  R_host (corresp. to current dPdV)    [only for ANNIHILATION and nlevel>1]


   // Create par_subsvar[25] for variance calculation
   const int n_par = 25;
   double par_subsvar[n_par];
   for (int i = 0; i < n_par - 1 ; ++i)
      par_subsvar[i] = par_subs[i];

   // Set normalisation to ensure int_m1^m2 dpdm = 1
   dpdm_setnormprob(&par_subsvar[8], m1, m2, par_subs[5]);

   // Calculate <L>_M,c
   par_subsvar[n_par - 1] = mean1cl_lumn_fullhalo(par_subs, m1, m2, 1);

   // Integrate integrand between [m1,m2]
   double res = 0.;
   simpson_log_adapt(integrand_lum_variance, m1, m2, par_subsvar, res, par_subs[5]);
   return res;
}

//______________________________________________________________________________
double var1cl_mass(double par_dpdm[2], double &m1, double &m2, double const &eps)
{
   //--- Returns var(M) = int_{m1}^{m2} (M-<M>)^2 dP/dM dM   [Msol^2]
   //    with <M> = int_{m1}^{m2} M dP/dM dM   [Msol].
   //
   //  par_dpdm[0]   UNUSED
   //  par_dpdm[1]   dPdM slope alphaM
   //  m1            Minimal mass [Msol]
   //  m2            Maximal mass [Msol]
   //  eps           Relative precision sought for integration

   // Create par_dpdmvar[3] for variance calculation
   const int n_par = 3;
   double par_dpdmvar[n_par] = {par_dpdm[0], par_dpdm[1], 0.};
   // Set normalisation to ensure int_m1^m2 dpdm = 1
   dpdm_setnormprob(par_dpdmvar, m1, m2, eps);
   par_dpdmvar[2] = mean1cl_mass(par_dpdmvar, m1, m2, eps);

   // Integrate integrand between [m1,m2]
   double res = 0.;
   simpson_log_adapt(integrand_mass_variance, m1, m2, par_dpdmvar, res, eps);
   return res;
}
