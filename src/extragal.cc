/*! \file extragal.cc \brief (see extragal.h) */

// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>
using namespace std;

// ROOT includes
#if IS_ROOT
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TPaveText.h>
#include <TLine.h>
#include <TFile.h>
#include <TH2D.h>
#endif

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/spectra.h"
#include "../include/stat.h"
#include "../include/cosmo.h"
#include "../include/extragal.h"

// GSL includes
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>

// "extragal.cc"-specific precomputed vector the intensitymultiplier to be integrated:
vector<double> vec_intensitymultiplier;
vector<double> vec_z;
vector<double> vec_paramcheck;

//______________________________________________________________________________
void analyse_intensity_mean(double e1_gev, double e2_gev, const int n_e)
{
   //--- Returns differential intensity [cm^2/s/GeV/sr] for annihilation or decay.
   //    Note analogy to fluxes_plot() function in spectra.cc

   // INPUTS:
   //  e1_gev        Minimal energy to display [GeV]
   //  e2_gev        Maximal energy to display [GeV]
   //  n_e           Number of energy bins

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int card_window = gSIM_EXTRAGAL_FLAG_WINDOWFUNC;

   // fill interpolation grid of mass function and distances:
   vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
   vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);

   init_extragal_manipulatedHMF(z_vec, M_vec, gEXTRAGAL_FLAG_MASSFUNCTION, card_window);

   // define DM intensity model:
   double par_intensity_mean[38];
   // par[0-25] = halo parameters
   // par[26-27]= ln Mhmin, ln Mhmax
   // par[28-34]= spectral parameters
   // par[35-36]= zmin, zmax
   // par[37]= intensity multiplication factor

   par_intensity_mean[26] = log(gSIM_EXTRAGAL_MMIN * gCOSMO_M_to_MH); // mass integration range - Mmin
   par_intensity_mean[27] = log(gSIM_EXTRAGAL_MMAX * gCOSMO_M_to_MH); // mass integration range - Mmax

   par_intensity_mean[28] = gPP_DM_MASS_GEV;                          // Mass of DM candidate [GeV]
   par_intensity_mean[29] = gPP_FLAG_SPECTRUMMODEL;                   // Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   par_intensity_mean[30] = gSIM_FLUX_FLAG_FINALSTATE;                // Card for PP final state (gENUM_FINALSTATE)

   par_intensity_mean[33] = 0.;                                       // EBL uncertainty

   par_intensity_mean[34] = 0.;                                       // Energy at which to compute differential intensity (will be updated later)

   par_intensity_mean[35] = gSIM_EXTRAGAL_ZMIN;                       // redshift integration range - zmin
   par_intensity_mean[36] = gSIM_EXTRAGAL_ZMAX - SMALL_NUMBER;        // redshift integration range - zmax

   par_intensity_mean[37] = gSIM_XPOWER;                              // --> E^2 dPhi/dE

   string legend_dphide_atsource = "Spectral shape at source (z = 0, arbitrary flux normalization)";
   string model = gNAMES_PP_SPECTRUMMODEL[(int)gPP_FLAG_SPECTRUMMODEL];
   string final_state = gNAMES_FINALSTATE[(int)gSIM_FLUX_FLAG_FINALSTATE];

   // Calculate integrated intensity:
   printf("\n => Calculating integrated intensity [%le, %le]...\n", e1_gev, e2_gev);
   double intens_integr = dPhidOmega(e1_gev, e2_gev, par_intensity_mean);

   double e_pow = gSIM_XPOWER;

   // Fill vector of energies for diff. intensity calculation
   vector<double> e_gev, e_gev_pow;
   if (e1_gev == 0.) e1_gev = 1.e-5;
   e_gev.push_back(e1_gev);
   double step = pow(10., log10(e2_gev / e1_gev) / (double)(n_e - 1));
   for (int k = 1; k < n_e - 1; ++k)
      e_gev.push_back(e_gev[k - 1]*step);
   e_gev.push_back(e2_gev);
   for (int k = 0; k < n_e; ++k)
      e_gev_pow.push_back(pow(e_gev[k], e_pow));

   // Initialize output array :
   vector< double >  dPhidOmegadE_vec(n_e);

   // If EBL systematic uncertainty is considered, prepare arrays:
   vector< double >  dPhidOmegadE_up_vec;
   vector< double >  dPhidOmegadE_lo_vec;
   if (fabs(gSIM_EXTRAGAL_EBL_UNCERTAINTY) > SMALL_NUMBER) {
      printf("\n -> Will also calculate the spectrum for an EBL uncertainty of tau = +- %d percent...", int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
      if (gSIM_EXTRAGAL_EBL_UNCERTAINTY < 0.) {
         printf("\n====> ERROR: analyse_intensity_mean() in extragal.cc");
         printf("\n             gSIM_EXTRAGAL_EBL_UNCERTAINTY > 0 required.");
         printf("\n             => abort()\n\n");
         abort();
      }
      dPhidOmegadE_up_vec.resize(n_e);
      dPhidOmegadE_lo_vec.resize(n_e);
   }

   // Initialize strings
   char tmp_erange[2000];
   char tmp_pow[20];
   char tmp_pow2[20];
   sprintf(tmp_erange, "Integrated intensity #Phi in [%g, %g] GeV: #Phi = %.*le cm^{-2} s^{-1} sr^{-1}", e1_gev, e2_gev, gSIM_SIGDIGITS, intens_integr);

   string tmp_erange_str(tmp_erange);
   if (fabs(e_pow) > 0.01) {
      sprintf(tmp_pow, "%g", e_pow);
      tmp_erange_str += " #times GeV^{" + (string)tmp_pow + "}";
   }
   sprintf(tmp_erange, "%s", tmp_erange_str.c_str());

   string y_axis = "d#Phi/d#Omega/dE [# cm^{-2} s^{-1} sr^{-1}";
   string y_name_ecsv = "dPhi/dOmega/dE";
   string y_unit_ecsv = "cm^-2 s^-1 sr^-1";

   sprintf(tmp_pow, "%g", e_pow);
   sprintf(tmp_pow2, "%g", e_pow - 1);
   if (fabs(e_pow) < SMALL_NUMBER) {
      y_axis += "  GeV^{-1}]";
      y_unit_ecsv += " GeV^-1";
   } else if (fabs(e_pow - 1) < SMALL_NUMBER) {
      y_axis = "E #times " + y_axis + "]";
      y_name_ecsv = "E" + y_name_ecsv;
   } else if (fabs(e_pow - 2) < SMALL_NUMBER) {
      y_axis = "E^{2} #times " + y_axis + "  GeV]";
      y_name_ecsv = "E^2" + y_name_ecsv;
      y_unit_ecsv += " GeV";
   } else {
      y_axis = "E^{" + (string)tmp_pow + "} #times " + y_axis + " #times GeV^{" + (string)tmp_pow2 + "}]";
      y_name_ecsv = "E^" + (string)tmp_pow + y_name_ecsv,
      y_unit_ecsv += " GeV^" + (string)tmp_pow;
   }

   char tmp[2000];
   sprintf(tmp, "%s_%s_m%d", model.c_str(), final_state.c_str(), (int)gPP_DM_MASS_GEV);

   // ASCII Outputs/prints
   string f_name = gSIM_OUTPUT_DIR + "cosmo.dPhidOmegadE_" + string(tmp) + ".ecsv";
   FILE *fp = NULL;

   if (gSIM_IS_PRINT) {
      // write ASCII file
      fp = fopen(f_name.c_str(), "w");
      fprintf(fp, "# %%ECSV 1.0\n");
      fprintf(fp, "# ---\n");
      fprintf(fp, "# datatype:\n");
      if (fabs(gSIM_EXTRAGAL_EBL_UNCERTAINTY) < SMALL_NUMBER){
          printf(" E [GeV]\t%s\n", y_axis.c_str());
          fprintf(fp, "# - {name: E, unit: GeV, datatype: float64}\n");
          fprintf(fp, "# - {name: %s, unit: %s, datatype: float64}\n", y_name_ecsv.c_str(), y_unit_ecsv.c_str());
          fprintf(fp, "# - {name: spectrum_at_source, unit:, datatype: float64}\n");
          fprintf(fp, "E %s spectrum_at_source\n", y_name_ecsv.c_str());
      }
      else{
          printf(" E [GeV]\t%s\t+%d%% uncertainty\t-%d%% uncertainty\n", y_axis.c_str(), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
          fprintf(fp, "# - {name: E, unit: GeV, datatype: float64}\n");
          fprintf(fp, "# - {name: %s, unit: %s, datatype: float64}\n", y_name_ecsv.c_str(), y_unit_ecsv.c_str());
          fprintf(fp, "# - {name: spectrum_at_source, unit:, datatype: float64}\n");
          fprintf(fp, "# - {name: spectrum_plus_unc_%d%%, unit:, datatype: float64}\n", int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
          fprintf(fp, "# - {name: spectrum_minus_unc_%d%%, unit:, datatype: float64}\n", int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
          fprintf(fp, "E %s spectrum_at_source spectrum_plus_unc_%d%% spectrum_minus_unc_%d%%\n", y_name_ecsv.c_str(), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
//          fprintf(fp, "#  E [GeV]\t%s\tSpectrum at source (same normalization)\t+%d%% uncertainty\t-%d%% uncertainty\n", y_axis.c_str(), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.), int(gSIM_EXTRAGAL_EBL_UNCERTAINTY * 100.));
      }
   }

   // Calculate differential intensities
   double res_tmp;
   double intens_min = 1.e40, intens_max = -1.e40;
   for (int i = 0; i < (int)e_gev.size(); i++) {
      par_intensity_mean[34] = e_gev[i];
      dPhidOmegadE(e_gev[i], par_intensity_mean, res_tmp);
      dPhidOmegadE_vec[i] = res_tmp;
      if (res_tmp < intens_min && res_tmp > 1.e-29)
         intens_min  = res_tmp;
      if (res_tmp > intens_max)
         intens_max  = res_tmp;
      if (fabs(gSIM_EXTRAGAL_EBL_UNCERTAINTY) > SMALL_NUMBER) {
         par_intensity_mean[33] = gSIM_EXTRAGAL_EBL_UNCERTAINTY;
         dPhidOmegadE(e_gev[i], par_intensity_mean, res_tmp);
         dPhidOmegadE_up_vec[i] = res_tmp;
         par_intensity_mean[33] = -gSIM_EXTRAGAL_EBL_UNCERTAINTY;
         dPhidOmegadE(e_gev[i], par_intensity_mean, res_tmp);
         dPhidOmegadE_lo_vec[i] = res_tmp;
         par_intensity_mean[33] = 0.;
      }
      if (gSIM_IS_PRINT) {
         if (fabs(gSIM_EXTRAGAL_EBL_UNCERTAINTY) < SMALL_NUMBER) printf("  %.*le\t%.*le\n", gSIM_SIGDIGITS + 2, e_gev[i], gSIM_SIGDIGITS, dPhidOmegadE_vec[i]);
         else printf("%.*le %.*le %.*le %.*le\n", gSIM_SIGDIGITS + 2, e_gev[i], gSIM_SIGDIGITS, dPhidOmegadE_vec[i], gSIM_SIGDIGITS, dPhidOmegadE_lo_vec[i], gSIM_SIGDIGITS, dPhidOmegadE_up_vec[i]);
      }
   }

   // Calculate reference spectrum at source for plotting:
   par_intensity_mean[31] = 0.; // redshift = 0.
   par_intensity_mean[32] = gSIM_XPOWER;
   vector< double >  dPhidEatsource_vec(n_e);
   for (int i = 0; i < (int)e_gev.size(); i++) {
      dPhidEatsource_vec[i] = flux(&par_intensity_mean[28], e_gev[i], -1, false);
   }
   double max_dphide_egal = find_max_value(dPhidOmegadE_vec);
   double max_dphide_comp = find_max_value(dPhidEatsource_vec);
   for (int i = 0; i < (int)e_gev.size(); i++) {
      dPhidEatsource_vec[i] *= (max_dphide_egal / max_dphide_comp);
   }

   if (gSIM_IS_PRINT) {
      for (int i = 0; i < (int)e_gev.size(); i++) {
         if (fabs(gSIM_EXTRAGAL_EBL_UNCERTAINTY) < SMALL_NUMBER) fprintf(fp, "%.*le %.*le %.*le\n", gSIM_SIGDIGITS + 2, e_gev[i], gSIM_SIGDIGITS, dPhidOmegadE_vec[i], gSIM_SIGDIGITS, dPhidEatsource_vec[i]);
         else fprintf(fp, "%.*le %.*le %.*le %.*le %.*le\n", gSIM_SIGDIGITS + 2, e_gev[i], gSIM_SIGDIGITS, dPhidOmegadE_vec[i], gSIM_SIGDIGITS, dPhidEatsource_vec[i], gSIM_SIGDIGITS, dPhidOmegadE_lo_vec[i], gSIM_SIGDIGITS, dPhidOmegadE_up_vec[i]);
      }
      fclose(fp);
      fp = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
   }

#if IS_ROOT

   string legend_dphide = legend_for_spectrum(&par_intensity_mean[28], true, false);
   string cosmoinfo;
   char tmp_leg[1000];
   sprintf(tmp_leg, "#Deltaz = [%g, %g], ", gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX);
   cosmoinfo += string(tmp_leg);
   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
      sprintf(tmp_leg, "#DeltaM_{%s}  = [%g, %g] M_{#odot}, ", legend_for_delta().c_str(), gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX);
      cosmoinfo += string(tmp_leg);
      sprintf(tmp_leg, "HMF: k%s", gNAMES_MASSFUNCTION[gEXTRAGAL_FLAG_MASSFUNCTION]);
      cosmoinfo += string(tmp_leg);
   }
   if (gSIM_FLUX_FLAG_FINALSTATE == kGAMMA) {
      sprintf(tmp_leg, ", EBL: k%s", gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE]);
      cosmoinfo += string(tmp_leg);
   }
   legend_dphide = string("#splitline{") + legend_dphide + string("}{") + cosmoinfo + string("}");

   // Save .root format?
   string f_root = gSIM_OUTPUT_DIR + "cosmo.dPhidOmegadE_" + string(tmp) + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   // plot and/or write ROOT file:
   TCanvas *c_dphide = NULL;
   TMultiGraph *mg_dphide = NULL;
   TLegend *leg_dphide = NULL;
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      int n_curves = 1;
      // dNdE
      leg_dphide = new TLegend(0.15, 0.12, 0.4, 0.3);
      leg_dphide->SetFillColor(kWhite);
      leg_dphide->SetTextSize(0.03);
      leg_dphide->SetBorderSize(0);
      leg_dphide->SetFillStyle(0);
      mg_dphide = new TMultiGraph();
      mg_dphide->SetName(("mg_dphide_" + string(tmp)).c_str());
      char gr_name[1000];

      // spectrum at source for comparison:
      sprintf(gr_name, "dphide_atsource_%s_%s_m%d", model.c_str(), final_state.c_str(), (int)gPP_DM_MASS_GEV);
      TGraph *gr_dphide_comp = new TGraph(n_e, &e_gev[0], &dPhidEatsource_vec[0]);
      //cout << gr_name << endl;
      gr_dphide_comp->SetName(gr_name);
      gr_dphide_comp->SetTitle("");
      // Tranparency layer enabled with ROOT>=6.0 only
#if IS_ALPHA_METHOD
      gr_dphide_comp->SetLineColorAlpha(rootcolor(1), 0.3);
#else
      gr_dphide_comp->SetLineColor(rootcolor(1));
#endif
      gr_dphide_comp->SetLineWidth(5);
      gr_dphide_comp->SetLineStyle(2);
      gr_dphide_comp->GetXaxis()->SetTitle("E [GeV]");
      gr_dphide_comp->GetYaxis()->SetTitle(y_axis.c_str());
      mg_dphide->Add(gr_dphide_comp, "L");
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gr_dphide_comp->Write();
      }

      for (int i = 0; i < n_curves; ++i) {
         sprintf(gr_name, "dphide_%s_%s_m%d", model.c_str(), final_state.c_str(), (int)gPP_DM_MASS_GEV);
         TGraph *gr_dphide = new TGraph(n_e, &e_gev[0], &dPhidOmegadE_vec[0]);
         //cout << gr_name << endl;
         gr_dphide->SetName(gr_name);
         gr_dphide->SetTitle("");
         gr_dphide->SetLineColor(rootcolor(1));
         gr_dphide->SetLineWidth(3);
         gr_dphide->SetLineStyle(1 % 9);
         gr_dphide->GetXaxis()->SetTitle("E [GeV]");
         gr_dphide->GetYaxis()->SetTitle(y_axis.c_str());
         mg_dphide->Add(gr_dphide, "L");
         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gr_dphide->Write();
         }
         leg_dphide->AddEntry(gr_dphide, legend_dphide.c_str(), "L");
      }
      leg_dphide->AddEntry(gr_dphide_comp, legend_dphide_atsource.c_str(), "L");

      leg_dphide->AddEntry((TObject *)0, tmp_erange, "h");

      // Draw canvas
      c_dphide = new TCanvas("c_dphide", "c_dphide", 0, 0, 650, 500);
      c_dphide->SetLogx(1);
      c_dphide->SetLogy(1);

      // Plot graphs and legends
      mg_dphide->Draw("A");
      gSIM_CLUMPYAD->Draw();
      mg_dphide->GetXaxis()->SetTitle("E [GeV]");
      mg_dphide->GetYaxis()->SetTitle(y_axis.c_str());
      mg_dphide->SetMaximum(1.5 * intens_max);
      mg_dphide->SetMinimum(intens_min);

      leg_dphide->Draw("SAME");
      c_dphide->Update();
      c_dphide->Modified();

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         mg_dphide->Write();
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         cout << " ... output [ROOT]  written in: " << f_root << endl;
      }

      if (gSIM_IS_PRINT) {
         cout << "_______________________" << endl << endl;
      }
      if (gSIM_IS_WRITE_ROOTFILES && !gSIM_IS_PRINT) {
         cout << "_______________________" << endl << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);
   }

   // Free memory
   if (mg_dphide) delete mg_dphide;
   mg_dphide = NULL;
   if (leg_dphide) delete leg_dphide;
   leg_dphide = NULL;
   if (c_dphide) delete c_dphide;
   c_dphide = NULL;
#endif
   return;
}

//______________________________________________________________________________
void analyse_distances(const vector<double> &z_vec)
{
   //--- Prints/Plots the cosmic distance measures.
   //
   //  z_vec         Grid of x-axis values in redshift direction

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   // ASCII Outputs/prints
   string f_name1 = gSIM_OUTPUT_DIR + "cosmo.distances.ecsv";
   string f_name2 = gSIM_OUTPUT_DIR + "cosmo.omegas.ecsv";
   string f_name3 = gSIM_OUTPUT_DIR + "cosmo.hubble.ecsv";
   string f_name4 = gSIM_OUTPUT_DIR + "cosmo.distances_norm.ecsv";
   FILE *fp1 = fopen(f_name1.c_str(), "w");
   FILE *fp2 = fopen(f_name2.c_str(), "w");
   FILE *fp3 = fopen(f_name3.c_str(), "w");
   FILE *fp4 = fopen(f_name4.c_str(), "w");

   int nz = z_vec.size();

   vector <double> dist_trans(nz);
   vector <double> dist_lum(nz);
   vector <double> dist_ang(nz);

   vector <double> dist_trans_norm(nz);
   vector <double> dist_lum_norm(nz);
   vector <double> dist_ang_norm(nz);

   vector <double> OmegaR_z(nz);
   vector <double> OmegaCDM_z(nz);
   vector <double> Omegab_z(nz);
   vector <double> OmegaL_z(nz);
   vector <double> OmegaK_z(nz);

   vector <double> HoverH0(nz);

   string x_name = "redshift_z";
   vector<string> y_quantity;
   vector<string> title;

   y_quantity.push_back("Distance");
   y_quantity.push_back("Distance #times H_{0} /c");
   y_quantity.push_back("#Omega_{x}(z) = #rho_{x}(z) /#rho_{crit}(z)");
   y_quantity.push_back("H(z)/H_{0}");
   title.push_back("Cosmic distances");
   title.push_back("Cosmic distances normalized to Hubble length");
   title.push_back("Omega parameters");
   title.push_back("Hubble constant");

   vector<string> unit;
   unit.push_back("Mpc");
   unit.push_back("");
   unit.push_back("");
   unit.push_back("");

   vector<string> leg_distances;
   vector<string> names_distances;
   leg_distances.push_back(string("Transverse_comoving_distance"));
   names_distances.push_back(string("DistTransCom"));
   leg_distances.push_back(string("Luminosity_distance"));
   names_distances.push_back(string("DistLum"));
   leg_distances.push_back(string("Angular_diameter_distance"));
   names_distances.push_back(string("DistAng"));
   leg_distances.push_back(string("Hubble_constant"));
   names_distances.push_back(string("Hz_over_H0"));

   vector<string> leg_omegas;
   vector<string> names_omegas;
   leg_omegas.push_back(string("#Omega_{r}(z)"));
   names_omegas.push_back(string("OmegaR"));
   leg_omegas.push_back(string("#Omega_{CDM}(z)"));
   names_omegas.push_back(string("OmegaCDM"));
   leg_omegas.push_back(string("#Omega_{b}(z)"));
   names_omegas.push_back(string("Omegab"));
   leg_omegas.push_back(string("#Omega_{#Lambda}(z)"));
   names_omegas.push_back(string("OmegaL"));
   leg_omegas.push_back(string("#Omega_{K}(z)"));
   names_omegas.push_back(string("OmegaK"));

   double *params = NULL;

   // Print header
   if (gSIM_IS_PRINT) {

      for (auto& f : {ref(fp1), ref(fp2), ref(fp3), ref(fp4)}){
          fprintf(f, "# %%ECSV 1.0\n");
          fprintf(f, "# ---\n");
          fprintf(f, "# datatype:\n");
      }
      fprintf(fp1, "# - {name: %s, unit:, datatype: float64}\n", x_name.c_str());
      fprintf(fp1, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[0].c_str(), unit[0].c_str());
      fprintf(fp1, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[1].c_str(), unit[0].c_str());
      fprintf(fp1, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[2].c_str(), unit[0].c_str());
      fprintf(fp1, "%s %s %s %s\n", x_name.c_str(), leg_distances[0].c_str(), leg_distances[1].c_str(), leg_distances[2].c_str());
//      fprintf(fp1, "#  %s    %s   %s\n", unit[0].c_str(), unit[0].c_str(), unit[0].c_str());

      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", x_name.c_str());
      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", names_omegas[0].c_str());
      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", names_omegas[1].c_str());
      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", names_omegas[2].c_str());
      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", names_omegas[3].c_str());
      fprintf(fp2, "# - {name: %s, unit:, datatype: float64}\n", names_omegas[4].c_str());
      fprintf(fp2, "%s %s %s %s %s %s\n", x_name.c_str(), names_omegas[0].c_str(),
              names_omegas[1].c_str(), names_omegas[2].c_str(), names_omegas[3].c_str(), names_omegas[4].c_str());

      fprintf(fp3, "# - {name: %s, unit:, datatype: float64}\n", x_name.c_str());
      fprintf(fp3, "# - {name: %s, unit:, datatype: float64}\n", names_distances[3].c_str());
      fprintf(fp3, "%s %s\n", x_name.c_str(), names_distances[3].c_str());

      fprintf(fp4, "# - {name: %s, unit:, datatype: float64}\n", x_name.c_str());
      fprintf(fp4, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[0].c_str(), unit[0].c_str());
      fprintf(fp4, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[1].c_str(), unit[0].c_str());
      fprintf(fp4, "# - {name: %s, unit: %s, datatype: float64}\n", leg_distances[2].c_str(), unit[0].c_str());
      fprintf(fp4, "%s %s %s %s\n", x_name.c_str(), leg_distances[0].c_str(), leg_distances[1].c_str(), leg_distances[2].c_str());

   }

   for (int i = 0; i < (int)z_vec.size(); i++) {

      dist_trans[i] = dh_trans(z_vec[i]) / gCOSMO_HUBBLE;
      dist_lum[i] = dh_l(z_vec[i]) / gCOSMO_HUBBLE;
      dist_ang[i] = dh_a(z_vec[i]) / gCOSMO_HUBBLE;

      dist_trans_norm[i] = dist_trans[i] / HUBBLE_LENGTHxh_Mpc * gCOSMO_HUBBLE;
      dist_lum_norm[i] = dist_lum[i] / HUBBLE_LENGTHxh_Mpc * gCOSMO_HUBBLE;
      dist_ang_norm[i] = dist_ang[i] / HUBBLE_LENGTHxh_Mpc * gCOSMO_HUBBLE;

      double x = (1 + z_vec[i]);
      OmegaR_z[i] = gCOSMO_OMEGA0_R * pow(H0_over_H(z_vec[i], params), 2.) * pow(x, 4.);
      OmegaCDM_z[i] = (gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B) * pow(H0_over_H(z_vec[i], params), 2.) * pow(x, 3.);
      Omegab_z[i] = gCOSMO_OMEGA0_B * pow(H0_over_H(z_vec[i], params), 2.) * pow(x, 3.);
      OmegaL_z[i] = gCOSMO_OMEGA0_LAMBDA * pow(H0_over_H(z_vec[i], params), 2.);
      OmegaK_z[i] = gCOSMO_OMEGA0_K * pow(H0_over_H(z_vec[i], params), 2.) * pow(x, 2.);

      HoverH0[i] = H_over_H0(z_vec[i]);

      if (gSIM_IS_PRINT) {
         fprintf(fp1, "%.*g ", gSIM_SIGDIGITS + 2, z_vec[i]);
         fprintf(fp2, "%.*g ", gSIM_SIGDIGITS + 2, z_vec[i]);
         fprintf(fp3, "%.*g ", gSIM_SIGDIGITS + 2, z_vec[i]);
         fprintf(fp4, "%.*g ", gSIM_SIGDIGITS + 2, z_vec[i]);

         fprintf(fp1, "%.*le ", gSIM_SIGDIGITS, dist_trans[i]);
         fprintf(fp1, "%.*le ", gSIM_SIGDIGITS, dist_lum[i]);
         fprintf(fp1, "%.*le\n", gSIM_SIGDIGITS, dist_ang[i]);

         fprintf(fp4, "%.*le ", gSIM_SIGDIGITS, dist_trans_norm[i]);
         fprintf(fp4, "%.*le ", gSIM_SIGDIGITS, dist_lum_norm[i]);
         fprintf(fp4, "%.*le\n", gSIM_SIGDIGITS, dist_ang_norm[i]);

         fprintf(fp2, "%.*le ", gSIM_SIGDIGITS, OmegaR_z[i]);
         fprintf(fp2, "%.*le ", gSIM_SIGDIGITS, OmegaCDM_z[i]);
         fprintf(fp2, "%.*le ", gSIM_SIGDIGITS, Omegab_z[i]);
         fprintf(fp2, "%.*le ", gSIM_SIGDIGITS, OmegaL_z[i]);
         fprintf(fp2, "%.*le\n", gSIM_SIGDIGITS, OmegaK_z[i]);

         fprintf(fp3, "%.*le\n", gSIM_SIGDIGITS, HoverH0[i]);
      }
   }
   double z_eq_LambdaM = pow(gCOSMO_OMEGA0_LAMBDA / gCOSMO_OMEGA0_M, 1. / 3.) - 1;
   double z_eq_rM = gCOSMO_OMEGA0_M / gCOSMO_OMEGA0_R - 1;
   cout << " Matter-Dark energy equality at z = " << z_eq_LambdaM << endl;
   cout << " Matter-radiation equality at z = " << round(z_eq_rM) << endl;

   // Close file writing
   if (gSIM_IS_PRINT) {
      fclose(fp1);
      fclose(fp2);
      fclose(fp3);
      fclose(fp4);
      fp1 = NULL;
      fp2 = NULL;
      fp3 = NULL;
      fp4 = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name1 << endl;
      cout << "                                " << f_name4 << endl;
      cout << "                                " << f_name2 << endl;
      cout << "                                " << f_name3 << endl;
   }

#if IS_ROOT
   // Save .root format?
   string f1_root = gSIM_OUTPUT_DIR + "cosmo.distances.root";
   TFile *root_file1 = NULL;
   if (gSIM_IS_WRITE_ROOTFILES) {
      root_file1 = new TFile(f1_root.c_str(), "recreate");
   }
   // plot and/or write ROOT file:
   vector<double> vec_tmp(nz);

   vector<string> cvs_text;
   vector<string> cvs_text_alt;
   char tmp_leg[1000];
   cvs_text.push_back("Cosmology:");
   cvs_text_alt = cvs_text;
   cvs_text.push_back("(#Omega_{#Lambda}, #Omega_{m}, #Omega_{b}, #Omega_{r}, #Omega_{K}, h) =");
   cvs_text_alt.push_back("(#Omega_{#Lambda}, #Omega_{m}, #Omega_{b}, #Omega_{r}, #Omega_{K}) =");
   sprintf(tmp_leg, "(%g, %g, %g, %g, %g, %g)",
           round(gCOSMO_OMEGA0_LAMBDA, 3), round(gCOSMO_OMEGA0_M, 3), round(gCOSMO_OMEGA0_B, 4),
           gCOSMO_OMEGA0_R, round(gCOSMO_OMEGA0_K, 3), round(gCOSMO_HUBBLE, 3));
   cvs_text.push_back(string(tmp_leg));
   sprintf(tmp_leg, "(%g, %g, %g, %g, %g)",
           round(gCOSMO_OMEGA0_LAMBDA, 3), round(gCOSMO_OMEGA0_M, 3), round(gCOSMO_OMEGA0_B, 4),
           gCOSMO_OMEGA0_R, round(gCOSMO_OMEGA0_K, 3));
   cvs_text_alt.push_back(string(tmp_leg));

   const Int_t n_cvs = 4;
   vector<double> ymax(n_cvs);
   vector<double> ymin(n_cvs);
   double x_shift = 0.;

   vector<string> y_name(n_cvs);

   TMultiGraph **multi = new TMultiGraph*[n_cvs];
   TLegend **leg = new TLegend*[n_cvs];
   for (Int_t c = 0; c < n_cvs; ++c) {
      y_name[c] = y_quantity[c] + "  " + unit[c];

      ymax[c] = 1e-40;
      ymin[c] = 1e40;
      double ytmp;

      multi[c] = new TMultiGraph();

      if (c == 2 and !gSIM_EXTRAGAL_IS_ZLOG) x_shift = 0.5;
      else x_shift = 0.;
      double y_leg_min = 0.65;
      if (c == 2) y_leg_min = 0.5;
      leg[c] = new TLegend(0.15 + x_shift, y_leg_min, 0.35 + x_shift, 0.82);
      leg[c]->SetFillColor(kWhite);
      leg[c]->SetTextSize(0.03);
      leg[c]->SetBorderSize(0);
      leg[c]->SetFillStyle(0);

      if (c != 2) {
         for (Int_t i = 0; i < 3; ++i) {

            if (c == 0 and i == 0) vec_tmp = dist_trans;
            else if (c == 0 and i == 1) vec_tmp = dist_lum;
            else if (c == 0 and i == 2) vec_tmp = dist_ang;
            else if (c == 1 and i == 0) vec_tmp = dist_trans_norm;
            else if (c == 1 and i == 1) vec_tmp = dist_lum_norm;
            else if (c == 1 and i == 2) vec_tmp = dist_ang_norm;
            else if (c == 3) vec_tmp = HoverH0;

            ytmp = find_max_value(vec_tmp);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(vec_tmp);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nz, &z_vec[0], &vec_tmp[0]);
            sprintf(tmp_leg, "%s", names_distances[i].c_str());
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name.c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            if (c != 3) leg[c]->AddEntry(gr, leg_distances[i].c_str(), "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file1->cd();
               gr->Write();
            }

         }
      } else {
         for (Int_t i = 0; i < 5; ++i) {

            if (i == 0) vec_tmp =       OmegaR_z;
            else if (i == 1) vec_tmp =  OmegaCDM_z;
            else if (i == 2) vec_tmp =  Omegab_z ;
            else if (i == 3) vec_tmp =  OmegaL_z;
            else if (i == 4) vec_tmp =  OmegaK_z ;

            ytmp = find_max_value(vec_tmp);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(vec_tmp);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nz, &z_vec[0], &vec_tmp[0]);
            sprintf(tmp_leg, "%s", names_omegas[i].c_str());
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name.c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            leg[c]->AddEntry(gr, leg_omegas[i].c_str(), "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file1->cd();
               gr->Write();
            }
         }
      }
   }

   TCanvas **c_mf = NULL;
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      c_mf = new TCanvas*[n_cvs];
      for (Int_t c = 0; c < n_cvs; ++c) {
         char tmp_cvs[1000];
         sprintf(tmp_cvs, "%s", title[c].c_str());
         c_mf[c] = new TCanvas(tmp_cvs, tmp_cvs, c % 3 * 650, c / 3 * 550, 650, 500);
         c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);
         c_mf[c]->SetLogy(0);
         c_mf[c]->SetGridx(0);
         c_mf[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name.c_str());
         multi[c]->GetYaxis()->SetTitle(y_name[c].c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Set y-range:
         if (c != 2) {
            double ytmp = 1.6e4;
            if (c == 1) ytmp *= 2.261e-4;
            if (ymax[c] > ymin[c] + ytmp)
               ymax[c] =  ymin[c] + ytmp;
            multi[c]->SetMaximum(ymax[c]);
            if (c == 0) multi[c]->SetMinimum(1e-1);
            if (c == 1) multi[c]->SetMinimum(1e-1 * 2.261e-4);
         }
         //
         else {
            if (ymin[c] >= 0) {
               ymin[c] = 1e-5;
               ymax[c] = min(1., 1.1 * ymax[c]);
            }
            multi[c]->SetMaximum(ymax[c]);
            multi[c]->SetMinimum(ymin[c]);
         }

         if (c == 2 and z_eq_LambdaM > z_vec[0] and z_eq_LambdaM < z_vec[z_vec.size() - 1]) {
            TLine *line = new TLine(z_eq_LambdaM, ymin[c], z_eq_LambdaM, ymax[c]);
            line->SetLineColor(13);
            line->SetLineStyle(2);
            line->Draw();
            sprintf(tmp_cvs, "#Omega_{CDM} + #Omega_{b} = #Omega_{#Lambda} at z = %g", round(z_eq_LambdaM, 2));
            leg[c]->AddEntry(line, tmp_cvs, "L");
         }

         if (c == 2 and z_eq_rM > z_vec[0] and z_eq_rM < z_vec[z_vec.size() - 1]) {
            TLine *line = new TLine(z_eq_rM, ymin[c], z_eq_rM, ymax[c]);
            line->SetLineColor(13);
            line->SetLineStyle(4);
            line->Draw();
            sprintf(tmp_cvs, "#Omega_{CDM} + #Omega_{b} = #Omega_{r} at z = %g", round(z_eq_rM));
            leg[c]->AddEntry(line, tmp_cvs, "L");
         }

         // Text if required
         if (c == 2 and !gSIM_EXTRAGAL_IS_ZLOG) x_shift = 0.5;
         else x_shift = 0.;
         TPaveText *txt = new TPaveText(0.14 + x_shift, 0.83, 0.5 + x_shift, 0.95, "NDC");
         txt->SetTextSize(0.03);
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         for (int ii = 0; ii < int(cvs_text.size()); ++ii) {
            if (c == 0) txt->AddText(cvs_text[ii].c_str());
            else txt->AddText(cvs_text_alt[ii].c_str());
         }

         txt->Draw();

         c_mf[c]->Update();
         c_mf[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file1->cd();
            gPad->Write();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file1->Close();
         delete root_file1;
         cout << " ... output [use ROOT TBrowser] written in: " << f1_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;


      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      if (c_mf) delete[] c_mf;
   }
#endif
   return;
}

//______________________________________________________________________________
void analyse_ebl(const vector<double> &e_vec, const vector<double> &z_vec, const int card_ebl)
{
   //--- Prints/Plots the EBL absorption coeeficient tau.
   //
   //  e_vec         Grid of axis values in energy [GeV]
   //  z_vec         Grid of axis values in redshift direction

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   // ASCII Outputs/prints
   string f_name1 = gSIM_OUTPUT_DIR + "cosmo.ebl_tau.output";
   string f_name2 = gSIM_OUTPUT_DIR + "cosmo.ebl_exp-tau.output";
   FILE *fp1 = fopen(f_name1.c_str(), "w");
   FILE *fp2 = fopen(f_name2.c_str(), "w");

   int ne = e_vec.size();
   int nz = z_vec.size();

   vector < vector <double> > tau2D(nz, vector<double>(ne));
   vector < vector <double> > expminustau2D(nz, vector<double>(ne));

   string x_name = "#gamma-ray energy E [GeV]";
   vector<string>  y_name;
   y_name.push_back("Absorption exponent #tau(E, z)");
   y_name.push_back("Absorption factor exp(-#tau(E, z))");
   vector<string> title;

   title.push_back("Absorption coefficient tau");
   title.push_back("Absorption factor exp(-tau)");


   // Print header
   if (gSIM_IS_PRINT) {
      fprintf(fp1, "# %s. First row are redshifts, first column are Energies [GeV]\n", title[0].c_str());
      fprintf(fp2, "# %s. First row are redshifts, first column are Energies [GeV]\n", title[1].c_str());
      fprintf(fp1, "\t");
      fprintf(fp2, "\t");
      for (int j = 0; j < nz; j++) {
         fprintf(fp1, "%.*g\t", gSIM_SIGDIGITS + 2, z_vec[j]);
         fprintf(fp2, "%.*g\t", gSIM_SIGDIGITS + 2, z_vec[j]);
      }
      fprintf(fp1, "\n");
      fprintf(fp2, "\n");
   }

   for (int i = 0; i < ne; i++) {
      double e_gev = e_vec[i];
      if (gSIM_IS_PRINT) {
         fprintf(fp1, "%.*g\t", gSIM_SIGDIGITS + 2, e_gev);
         fprintf(fp2, "%.*g\t", gSIM_SIGDIGITS + 2, e_gev);
      }
      for (int j = 0; j < nz; j++) {
         double z = z_vec[j];
         tau2D[j][i] = opt_depth(e_gev, z, card_ebl);
         expminustau2D[j][i] = exp_minus_opt_depth(e_gev, z, card_ebl);
         if (gSIM_IS_PRINT) {
            fprintf(fp1, "%.*g\t", gSIM_SIGDIGITS, tau2D[j][i]);
            fprintf(fp2, "%.*g\t", gSIM_SIGDIGITS, expminustau2D[j][i]);
         }
      }
      if (gSIM_IS_PRINT) {
         fprintf(fp1, "\n");
         fprintf(fp2, "\n");
      }
   }

   // Close file writing
   if (gSIM_IS_PRINT) {
      fclose(fp1);
      fclose(fp2);
      fp1 = NULL;
      fp2 = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name1 << endl;
      cout << " ... output [ASCII] written in: " << f_name2 << endl;
   }

#if IS_ROOT
   // plot and/or write ROOT file:
   string f1_root = gSIM_OUTPUT_DIR + "cosmo.ebl_tau.root";
   TFile *root_file1 = NULL;
   if (gSIM_IS_WRITE_ROOTFILES) {
      root_file1 = new TFile(f1_root.c_str(), "recreate");
   }

   vector<double> vec_tmp(ne);

   vector<string> cvs_text;

   char tmp_leg[1000];
   cvs_text.push_back("Absorption model: " + string(gNAMES_ABSORPTIONPROFILE[card_ebl]));

   // print extrapolation regimes:
   double z_extrapol[2];
   double e_extrapol[2];
   double tau_extrapol[2];
   if (card_ebl == kFRANCESCHINI08) {
      z_extrapol[0] = 0.001;
      z_extrapol[1] = 3;
      e_extrapol[0] = 20;
      e_extrapol[1] = 1.25e5;
      tau_extrapol[0] = 6.4e-4;
      tau_extrapol[1] = 3.2e3;
   } else if (card_ebl == kFRANCESCHINI17) {
      z_extrapol[0] = 0.01;
      z_extrapol[1] = 3;
      e_extrapol[0] = 20;
      e_extrapol[1] = 1.25e5;
      tau_extrapol[0] = 6.4e-4;
      tau_extrapol[1] = 3.2e3;
   } else if (card_ebl == kFINKE10) {
      z_extrapol[0] = 0.003;
      z_extrapol[1] = 4.99;
      e_extrapol[0] = 20;
      e_extrapol[1] = 7.9e4;
      tau_extrapol[0] = 1e-40;
      tau_extrapol[1] = 1e40;
   } else if (card_ebl == kDOMINGUEZ11_REF or card_ebl == kDOMINGUEZ11_LO or card_ebl == kDOMINGUEZ11_UP) {
      z_extrapol[0] = 0.01;
      z_extrapol[1] = 2;
      e_extrapol[0] = 30;
      e_extrapol[1] = 3e4;
      tau_extrapol[0] = 1e-40;
      tau_extrapol[1] = 1e40;
   } else if (card_ebl == kGILMORE12_FIDUCIAL or card_ebl == kGILMORE12_FIXED) {
      z_extrapol[0] = 0.01;
      z_extrapol[1] = 6.5;
      e_extrapol[0] = 10.;
      e_extrapol[1] = 1e5;
      tau_extrapol[0] = 1e-40;
      tau_extrapol[1] = 1e40;
   } else if (card_ebl == kINOUE13_REF or card_ebl == kINOUE13_LO or card_ebl == kINOUE13_UP) {
      z_extrapol[0] = 0.01;
      z_extrapol[1] = 10.;
      e_extrapol[0] = 1.;
      e_extrapol[1] = 45e3;
      tau_extrapol[0] = 1e-40;
      tau_extrapol[1] = 1e40;
   } else if (card_ebl == kNOEBL) {
      z_extrapol[0] = 1e-40;
      z_extrapol[1] = 1e40;
      e_extrapol[0] = 1e-40;
      e_extrapol[1] = 1e40;
      tau_extrapol[0] = 1e-40;
      tau_extrapol[1] = 1e40;
   }


   const Int_t n_cvs = 3;
   vector<double> ymax(n_cvs);
   vector<double> ymin(n_cvs);

   TMultiGraph **multi = new TMultiGraph*[n_cvs - 1];
   TLegend **leg = new TLegend*[n_cvs - 1];

   double y_leg_min;

   for (Int_t c = 0; c < 2; ++c) {

      ymax[c] = 1e-40;
      ymin[c] = 1e40;

      multi[c] = new TMultiGraph();

      y_leg_min = 0.58;
      if (c == 1)  y_leg_min = 0.15;
      leg[c] = new TLegend(0.15, y_leg_min, 0.35, y_leg_min + 0.35);
      leg[c]->SetFillColor(kWhite);
      leg[c]->SetTextSize(0.03);
      leg[c]->SetBorderSize(0);
      leg[c]->SetFillStyle(0);

      if (c == 0) {
         for (Int_t i = 0; i < nz; ++i) {
            double y_tmp = find_max_value(tau2D[i]);
            if (y_tmp > ymax[c]) ymax[c] = y_tmp;
            y_tmp = find_min_value(tau2D[i]);
            if (y_tmp < ymin[c]) ymin[c] = y_tmp;
         }
         if (ymin[c]  < 1e-5) ymin[c] = 1e-5;
      } else if (c == 1) {
         ymax[c] = 1.2;
         ymin[c] = find_min_value(expminustau2D[0]);
         if (ymin[c] / ymax[c] < 1e-7) ymin[c] = 1e-7 * ymax[c];
         if (ymin[c] / ymax[c] > 1e-1) ymin[c] = 1e-1 * ymax[c];
      }

      int nz_plot = 8;
      int i_step = 0;

      for (Int_t i = 0; i < nz_plot; ++i) {

         if (nz != 1) i_step = int(round(i * (nz - 1) / (nz_plot - 1)));

         if (c == 0) vec_tmp = tau2D[i_step];
         else vec_tmp = expminustau2D[i_step];

         TGraph *gr = new TGraph(ne, &e_vec[0], &vec_tmp[0]);

         string leg_extrapol;
         if (z_vec[i_step] < z_extrapol[0] or z_vec[i_step] > z_extrapol[1]) leg_extrapol = "(Extrapolation)";
         if (c == 0) sprintf(tmp_leg, "tau_z=%*.g", gSIM_SIGDIGITS, z_vec[i_step]);
         else sprintf(tmp_leg, "expminustau_z=%*.g", gSIM_SIGDIGITS, z_vec[i_step]);
         gr->SetName(tmp_leg);
         gr->SetLineColor(rootcolor(i));
         gr->SetLineWidth(2);
         gr->SetLineStyle(1);
         gr->SetTitle(tmp_leg);
         gr->GetXaxis()->SetTitle(x_name.c_str());
         gr->GetYaxis()->SetTitle(y_name[c].c_str());
         multi[c]->Add(gr, "L");
         sprintf(tmp_leg, "z = %.*g %s", gSIM_SIGDIGITS, z_vec[i_step], leg_extrapol.c_str());
         leg[c]->AddEntry(gr, tmp_leg, "L");
         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file1->cd();
            gr->Write();
         }
      }
   }

   TH2D **h2d = NULL;
   TCanvas **c_ebl = NULL;

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {

      c_ebl = new TCanvas*[n_cvs];
      for (Int_t c = 0; c < 2; ++c) {
         char tmp_cvs[1000];
         sprintf(tmp_cvs, "%s", title[c].c_str());
         c_ebl[c] = new TCanvas(tmp_cvs, tmp_cvs, c * 650, 0, 650, 500);
         c_ebl[c]->SetLogx(1);
         c_ebl[c]->SetLogy(1);
         c_ebl[c]->SetGridx(0);
         c_ebl[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name.c_str());
         multi[c]->GetYaxis()->SetTitle(y_name[c].c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Set y-range:
         multi[c]->SetMaximum(ymax[c]);
         multi[c]->SetMinimum(ymin[c]);

         y_leg_min = 0.15;
         double x_leg_min = 0.6;
         if (c == 1)  {
            x_leg_min = 0.14;
            y_leg_min = 0.5;
         }

         // Text if required
         TPaveText *txt = new TPaveText(x_leg_min, y_leg_min, x_leg_min + 0.5, y_leg_min + 0.1, "NDC");
         txt->SetTextSize(0.03);
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         for (int ii = 0; ii < int(cvs_text.size()); ++ii) {
            txt->AddText(cvs_text[ii].c_str());
         }

         txt->Draw();

         TLine *line_extrapol = new TLine(1e-40, 1e-40, 1e-40, 1e-40);
         line_extrapol->SetLineColor(13);
         line_extrapol->SetLineStyle(2);

         sprintf(tmp_cvs, "Extrapolation regime");
         bool is_extrapol = false;
         if (e_vec[ne - 1] >  e_extrapol[1]) {
            TLine *line_xmax = new TLine(e_extrapol[1], ymin[c], e_extrapol[1], ymax[c]);
            line_xmax->SetLineColor(13);
            line_xmax->SetLineStyle(2);
            line_xmax->Draw();
            is_extrapol = true;
         }
         if (e_vec[0] < e_extrapol[0]) {
            TLine *line_xmin = new TLine(e_extrapol[0], ymin[c], e_extrapol[0], ymax[c]);
            line_xmin->SetLineColor(13);
            line_xmin->SetLineStyle(2);
            line_xmin->Draw();
            is_extrapol = true;
         }
         if (ymin[c] < tau_extrapol[0]) {
            TLine *line_ymin = new TLine(e_vec[0], tau_extrapol[0], e_vec[ne - 1], tau_extrapol[0]);
            line_ymin->SetLineColor(13);
            line_ymin->SetLineStyle(2);
            line_ymin->Draw();
            is_extrapol = true;
         }
         if (ymax[c] > tau_extrapol[1]) {
            TLine *line_ymax = new TLine(e_vec[0], tau_extrapol[1], e_vec[ne - 1], tau_extrapol[1]);
            line_ymax->SetLineColor(13);
            line_ymax->SetLineStyle(2);
            line_ymax->Draw();
            is_extrapol = true;
         }

         if (is_extrapol) leg[c]->AddEntry(line_extrapol, tmp_cvs, "L");

         c_ebl[c]->Update();
         c_ebl[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file1->cd();
            gPad->Write();
         }
      }


      h2d = new TH2D*[1];
      string y2D_name = "redshift z";
      sprintf(tmp_leg, "tau2D_Canv");

      if (gSIM_EXTRAGAL_IS_ZLOG) {
         y2D_name = "log_{10} z";
         h2d[0] = new TH2D(tmp_leg, "", nz, log10(z_vec[0]), log10(z_vec[nz - 1]), ne, log10(e_vec[0]), log10(e_vec[ne - 1]));
      } else  h2d[0] = new TH2D(tmp_leg, "", nz, z_vec[0], z_vec[nz - 1], ne, log10(e_vec[0]), log10(e_vec[ne - 1]));

      for (int i = 0; i < nz ; ++i) {
         for (int j = 0; j < ne ; ++j) {
            h2d[0]->SetBinContent(i + 1, j + 1, tau2D[i][j]);
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT)  h2d[0]->Write();

      c_ebl[2] = new TCanvas(tmp_leg, y_name[0].c_str(), 2 * 650, 0, 650, 500);
      h2d[0]->GetXaxis()->CenterTitle(1);
      h2d[0]->GetYaxis()->CenterTitle(1);
      h2d[0]->GetZaxis()->CenterTitle(1);
      h2d[0]->GetXaxis()->SetTitleOffset(1.9);
      h2d[0]->GetXaxis()->SetTitleFont(132);
      h2d[0]->GetXaxis()->SetTitleSize(0.05);
      h2d[0]->GetXaxis()->SetLabelFont(132);
      h2d[0]->GetXaxis()->SetLabelSize(0.04);
      h2d[0]->GetYaxis()->SetTitleOffset(1.9);
      h2d[0]->GetYaxis()->SetTitleFont(132);
      h2d[0]->GetYaxis()->SetTitleSize(0.05);
      h2d[0]->GetYaxis()->SetLabelFont(132);
      h2d[0]->GetYaxis()->SetLabelSize(0.04);
      h2d[0]->GetZaxis()->SetTitleOffset(1.2);
      h2d[0]->GetZaxis()->SetTitleFont(132);
      h2d[0]->GetZaxis()->SetTitleSize(0.05);
      h2d[0]->GetZaxis()->SetLabelFont(132);
      h2d[0]->GetZaxis()->SetLabelSize(0.05);
      h2d[0]->GetXaxis()->SetTitle(y2D_name.c_str());
      h2d[0]->GetYaxis()->SetTitle("log_{10}(E_{#gamma}/GeV)");
      h2d[0]->GetZaxis()->SetTitle(y_name[0].c_str());
      if (ymax[0] > 1e4) ymax[0] = 1e4;
      if (ymin[0] < 1e-5) ymin[0] = 1e-5;
      h2d[0]->SetMaximum(ymax[0]);
      h2d[0]->SetMinimum(ymin[0]);
      h2d[0]->SetTitle(y_name[0].c_str());
      h2d[0]->SetTitleFont(132);
      h2d[0]->SetTitleSize(0.02);
      h2d[0]->SetStats(0);
      c_ebl[2]->SetLogx(0);
      c_ebl[2]->SetLogy(0);
      c_ebl[2]->SetLogz(1);
      c_ebl[2]->SetGridx(0);
      c_ebl[2]->SetGridy(0);
      h2d[0]->Draw("surf3");
      gSIM_CLUMPYAD->Draw();
      c_ebl[2]->Modified();
      c_ebl[2]->Update();

      if (gSIM_IS_WRITE_ROOTFILES && gSIM_IS_PRINT) c_ebl[2]->Write();

      if (gSIM_IS_WRITE_ROOTFILES) {
         cout << " ... output [use ROOT TBrowser] written in: " << f1_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;


      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file1->Close();
         delete root_file1;
      }

      // Free memory
      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      delete[] h2d;
      h2d = NULL;
      if (c_ebl) delete[] c_ebl;
   }
#endif

   return;
}

//______________________________________________________________________________
void analyse_intensity_mass_z_decades(const vector<double> &z_vec, const vector<double> &M_vec, const double &e_gev)
{
   //--- Prints/Plots the differential and integral contributions (per mass and z decade)
   //    to the differential intensity at energy e_gev.
   //
   //  M_grid         Grid of x-axis values in mass dimension
   //  z_grid         Grid of x-axis values in redshift direction
   //  e_gev          Energy at which the differential intensity is evaluated [GeV].

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int card_window = gSIM_EXTRAGAL_FLAG_WINDOWFUNC;
   // fill interpolation grid of mass function and distances:
   init_extragal_manipulatedHMF(z_vec, M_vec, gEXTRAGAL_FLAG_MASSFUNCTION, card_window);

   // restore grid boundaries and calculate Mh grid
   int nz = z_vec.size();
   double zmin = z_vec[0];

   int nm = M_vec.size();
   double Mmax = M_vec[nm - 1];

   // initialize arrays:
   vector<double> Mh_var_vec; // units Msol/h
   for (int i = 0; i < nm; i++) {
      Mh_var_vec.push_back(M_vec[i] * gCOSMO_HUBBLE);
   }

   vector< vector< double > > dPhidOmegadE_zM(nz);
   vector< vector< double > > dPhidOmegadEdlnMh_zM(nz);
   vector< vector< double > > dPhidOmegadEdz_zM(nz);


   // ASCII Outputs/prints
   char tmp_leg[1000];
   sprintf(tmp_leg, "%g", e_gev);
   string f_name = gSIM_OUTPUT_DIR + "cosmo.dPhidOmegadE_at" + string(tmp_leg) + "GeV_2D.output";

   FILE *fp = fopen(f_name.c_str(), "w");

   // compute massfunction in mass and z direction

   const int n_cvs = 4;
   vector<string>  x_name(n_cvs);
   vector<string>  y_name(n_cvs);
   vector<string> y_quantity(n_cvs);
   vector<string> title;
   vector<string> unit(n_cvs);
   double y_range_mass;

   title.push_back("dPhi/dOmega/dE in redshift interval (integrated)");
   title.push_back("dPhi/dOmega/dE in mass interval (integrated)");
   title.push_back("dPhi/dOmega/dE/dz per redshift (differential)");
   title.push_back("dPhi/dOmega/dE/dlogMh per mass decade (differential)");

   y_quantity[0] = "E^{2} #times #frac{d#Phi}{d#Omega dE}(#Deltaz)";
   y_quantity[1] = "E^{2} #times #frac{d#Phi}{d#Omega dE}(#DeltaM)";

   unit[0] = "[GeV cm^{-2} s^{-1} sr^{-1}]";
   unit[1] = unit[0];
   unit[2] = unit[0];

   if (fabs(gSIM_XPOWER) < SMALL_NUMBER) {
      //  dnh/dM in units of h^3 Mpc^-3 Msol^-1
      unit[3] = "[GeV cm^{-2} s^{-1} sr^{-1} M_{#odot}^{-1}]";
      y_quantity[2] = "E^{2} #times #frac{d#Phi}{d#Omega dE dz}";
      y_quantity[3] = "E^{2} #times #frac{d#Phi}{d#Omega dE dM_{" + legend_for_delta() + "}}";
      y_range_mass = 1e10;
   } else if (fabs(gSIM_XPOWER - 1) < SMALL_NUMBER) {
      //  dnh/dM in units of h^3 Mpc^-3 Msol^-1
      unit[3] = "[GeV cm^{-2} s^{-1} sr^{-1}]";
      y_quantity[2] = "z #times E^{2} #times #frac{d#Phi}{d#Omega dE dz}";
      y_quantity[3] = "M_{" + legend_for_delta() + "} #times E^{2} #times #frac{d#Phi}{d#Omega dE dM_{" + legend_for_delta() + "}}";
      y_range_mass = 1e5;
   } else if (fabs(gSIM_XPOWER - 2) < SMALL_NUMBER) {
      //  dnh/dM in units of h^3 Mpc^-3 Msol^-1
      unit[3] = "[GeV cm^{-2} s^{-1} sr^{-1} M_{#odot}]";
      y_quantity[2] = "z^{2} #times E^{2} #times #frac{d#Phi}{d#Omega dE dz}";
      y_quantity[3] = "M_{" + legend_for_delta() + "}^{2} #times E^{2} #times #frac{d#Phi}{d#Omega dE dM_{" + legend_for_delta() + "}}";
      y_range_mass = 1e3;
   } else  {
      sprintf(tmp_leg, "%g", gSIM_XPOWER - 1);
      unit[3] = "[GeV cm^{-2} s^{-1} sr^{-1} M_{#odot}^{" + string(tmp_leg) + "}]";
      sprintf(tmp_leg, "%g", gSIM_XPOWER);
      y_quantity[2] = "z^{" + string(tmp_leg) + "} #times E^{2} #times #frac{d#Phi}{d#Omega dE dz}";
      y_quantity[3] = "M_{" + legend_for_delta() + "}^{" + string(tmp_leg) + "} #times E^{2} #times #frac{d#Phi}{d#Omega dE dM_{" + legend_for_delta() + "}}";
      double exp = 12 - 4 * gSIM_XPOWER;
      y_range_mass = pow(10, exp);
   }

   double res_tmp;

   // define DM intensity model:
   double par_intensity_mean[38];
   // par[0-25] = halo parameters
   // par[26-27]= ln Mhmin, ln Mhmax
   // par[28-34]= spectral parameters
   // par[35-36]= zmin, zmax
   // par[37]= intensity multiplication factor

   par_intensity_mean[28] = gPP_DM_MASS_GEV;           // Mass of DM candidate [GeV]
   par_intensity_mean[29] = gPP_FLAG_SPECTRUMMODEL;    // Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   par_intensity_mean[30] = gSIM_FLUX_FLAG_FINALSTATE; // Card for PP final state (gENUM_FINALSTATE)
   par_intensity_mean[33] = 0.;                        // EBL uncertainty

   par_intensity_mean[34] = e_gev;                     // Energy at which to compute differential intensity

   par_intensity_mean[37] = 2.;                        // --> E^2 dPhi/dE


   for (int i = 0; i < (int)z_vec.size(); i++) {
      double z_current = z_vec[i];
      cout << " ... Processing z = " << z_current << endl;

      if (i == int(z_vec.size() - 1)) z_current -= SMALL_NUMBER;

      par_intensity_mean[27] = log(Mmax * gCOSMO_M_to_MH); // mass integration range - Mmax
      par_intensity_mean[35] = zmin; // redshift integration range - zmin
      par_intensity_mean[36] = z_current; // redshift integration range - zmax

      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {

         double lnMh_current = log(Mh_var_vec[j] / gCOSMO_OMEGA0_M);
         // calculate number of halos between zmin and z and Mmin and M:
         par_intensity_mean[26] = lnMh_current; // mass integration range - Mmin

         res_tmp = 0.;
         dPhidOmegadE(par_intensity_mean[34] /* e_gev*/, par_intensity_mean, res_tmp);
         dPhidOmegadE_zM[i].push_back(res_tmp);

         res_tmp = 0.;
         dPhidOmegadEdlnMh(lnMh_current, par_intensity_mean, res_tmp);
         dPhidOmegadEdlnMh_zM[i].push_back(res_tmp);

         res_tmp = 0.;
         dPhidOmegadEdz(z_current, par_intensity_mean, res_tmp);
         dPhidOmegadEdz_zM[i].push_back(res_tmp);
      }

      // scale for plotting:
      double  prefactor_xz = pow(z_vec[i], gSIM_XPOWER);
      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         double  prefactor_xmass = pow(M_vec[j], gSIM_XPOWER - 1);
         dPhidOmegadEdlnMh_zM[i][j] *= prefactor_xmass;
         dPhidOmegadEdz_zM[i][j] *= prefactor_xz;
      }
   }


   // Print to file
   if (gSIM_IS_PRINT) {
      // Print header
      fprintf(fp, "#\t\t");
      for (int i = 0; i < (int)z_vec.size(); i++) {
         fprintf(fp, "z = %.*g\t", gSIM_SIGDIGITS + 2, z_vec[i]);
      }
      fprintf(fp, "\n");
      fprintf(fp, "# M_%s\tdN/dOmega(<M_max, >z_min) (all following columns) \n", legend_for_delta().c_str());
      fprintf(fp, "# [Msol]\t%s (all following columns)\n", unit[1].c_str());


      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         fprintf(fp, "%.*le\t", gSIM_SIGDIGITS + 2, M_vec[j]);
         for (int i = 0; i < (int)z_vec.size(); i++) {
            fprintf(fp, "%.*le\t", gSIM_SIGDIGITS, dPhidOmegadE_zM[i][j]);
         }
         fprintf(fp, "\n");
      }
      fclose(fp);
      fp = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
   }

#if IS_ROOT
   // plot and/or write ROOT file:
   string f_root = gSIM_OUTPUT_DIR + "cosmo.dPhidOmegadE_at" + string(tmp_leg) + "GeV_2D.root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   sprintf(tmp_leg, "Interval #Deltaz = [%g, z]", zmin);
   x_name[0] = string(tmp_leg);
   sprintf(tmp_leg, "Interval #DeltaM  = [M_{%s}, %g M_{#odot}]", legend_for_delta().c_str(), Mmax);
   x_name[1] = string(tmp_leg);
   x_name[2] = "redshift z";
   x_name[3] = "M_{" + legend_for_delta() + "} #times h  [M_{#odot}]";

   vector<string> cvs_text;

   sprintf(tmp_leg, "d#Phi/dE at E = %g GeV,", e_gev);
   cvs_text.push_back(string(tmp_leg));
   string spec_tmp = legend_for_spectrum(&par_intensity_mean[28], true, false);
   string spec_tmptmp = spec_tmp.substr(0, spec_tmp.find_first_of(",") + 1);
   spec_tmp = spec_tmp.substr(spec_tmp.find_first_of(",") + 1, spec_tmp[spec_tmp.size() - 1]);
   cvs_text.push_back(spec_tmptmp + spec_tmp.substr(0, spec_tmp.find_first_of(",") + 1));
   spec_tmp = spec_tmp.substr(spec_tmp.find_first_of(",") + 2, spec_tmp[spec_tmp.size() - 1]);
   cvs_text.push_back(spec_tmp + ",");
   sprintf(tmp_leg, "halo mass function = %s", gNAMES_MASSFUNCTION[gEXTRAGAL_FLAG_MASSFUNCTION]);
   cvs_text.push_back(string(tmp_leg) + ",");
   sprintf(tmp_leg, "EBL absorption profile = %s", gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE]);
   cvs_text.push_back(string(tmp_leg) + ",");
   sprintf(tmp_leg, "c_{#Delta}^{field}(M_{#Delta}) relation = %s", gNAMES_CDELTAMDELTA[gEXTRAGAL_FLAG_CDELTAMDELTA]);
   cvs_text.push_back(string(tmp_leg) + ",");
   sprintf(tmp_leg, "c_{#Delta}^{subs}(M_{#Delta}) relation = %s", gNAMES_CDELTAMDELTA[gHALO_SUBS_FLAG_CDELTAMDELTA[kEXTRAGAL]]);
   cvs_text.push_back(string(tmp_leg));

   if (gSIM_IS_WRITE_ROOTFILES)
      root_file->cd();

   vector<double> ymax(n_cvs);
   vector<double> ymin(n_cvs);
   double ytmp;
   int nz_plot = 5;
   int nm_plot = 5;
   if (nz < nz_plot) nz_plot = nz;
   if (nm < nm_plot) nm_plot = nm;
   int i_step = 0;
   vector<double> dPhidOmegadE_z(nz);
   vector<double> dPhidOmegadE_M(nm);
   vector<double> dPhidOmegadEdz_z(nz);
   vector<double> dPhidOmegadlnMh_Mh(nm);

   TMultiGraph **multi = new TMultiGraph*[n_cvs];
   TLegend **leg = new TLegend*[n_cvs];

   double x_leg_delta;
   for (Int_t c = 0; c < n_cvs; ++c) {
      multi[c] = new TMultiGraph();

      if (c == 0) x_leg_delta = 0.4;
      else x_leg_delta = 0.;

      leg[c] = new TLegend(0.15 + x_leg_delta, 0.15, 0.35 + x_leg_delta, 0.45);
      leg[c]->SetFillColor(kWhite);
      leg[c]->SetTextSize(0.03);
      leg[c]->SetBorderSize(0);
      leg[c]->SetFillStyle(0);

      ymax[c] = 1e-40;
      ymin[c] = 1e40;
      y_name[c] = y_quantity[c] + "  " + unit[c];

      if (c == 0) {
         for (Int_t i = 0; i < nm_plot; ++i) {
            if (nm != 1) i_step = int(round(i * (nm - 1) / nm_plot));  // vec_step[i];// =


            for (int ii = 0; ii < (int)z_vec.size(); ii++) {
               dPhidOmegadE_z[ii] = dPhidOmegadE_zM[ii][i_step];
            }

            ytmp = find_max_value(dPhidOmegadE_z);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dPhidOmegadE_z);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            int nz_tmp = nz;
            int i_start = 0;
            if (gSIM_EXTRAGAL_IS_ZLOG) {
               nz_tmp = nz - 1;
               i_start = 1;
            }
            TGraph *gr = new TGraph(nz_tmp, &z_vec[i_start], &dPhidOmegadE_z[i_start]);
            sprintf(tmp_leg, "n_plot_int_M=%.*gMsol", gSIM_SIGDIGITS, M_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#DeltaM = [%.*g M_{#odot}, %.*g M_{#odot}]",  gSIM_SIGDIGITS, M_vec[i_step], gSIM_SIGDIGITS, Mmax);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      } else if (c == 1) {
         for (Int_t i = 0; i < nz_plot; ++i) {
            if (nz != 1) i_step = int(round((i + 1) * (nz - 1) / nz_plot)); // vec_step[i];// =

            for (int ii = 0; ii < (int)M_vec.size(); ii++) {
               dPhidOmegadE_M[ii] = dPhidOmegadE_zM[i_step][ii];
            }

            ytmp = find_max_value(dPhidOmegadE_M);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dPhidOmegadE_M);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &M_vec[0], &dPhidOmegadE_M[0]);
            sprintf(tmp_leg, "n_plot_int_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + nm_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#Deltaz = [%.*g, %.*g]",  gSIM_SIGDIGITS, zmin, gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      }  else if (c == 2) {
         for (Int_t i = 0; i < nm_plot; ++i) {
            if (nm != 1) i_step = int(round(i * (nm - 1) / nm_plot));  // vec_step[i];// =

            for (int ii = 0; ii < (int)z_vec.size(); ii++) {
               dPhidOmegadEdz_z[ii] = dPhidOmegadEdz_zM[ii][i_step];
            }

            ytmp = find_max_value(dPhidOmegadEdz_z);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dPhidOmegadEdz_z);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nz, &z_vec[0], &dPhidOmegadEdz_z[0]);
            sprintf(tmp_leg, "nd_plot_diff_M=%.*gMsol", gSIM_SIGDIGITS, M_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#DeltaM = [%.*g M_{#odot}, %.*g M_{#odot}]",  gSIM_SIGDIGITS, M_vec[i_step], gSIM_SIGDIGITS, Mmax);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      } else if (c == 3) {

         for (Int_t i = 0; i < nz_plot; ++i) {
            if (nz != 1) i_step = int(round((i + 1) * (nz - 1) / nz_plot)); // vec_step[i];// =

            for (int ii = 0; ii < (int)M_vec.size(); ii++) {
               dPhidOmegadlnMh_Mh[ii] = dPhidOmegadEdlnMh_zM[i_step][ii];
            }

            ytmp = find_max_value(dPhidOmegadlnMh_Mh);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dPhidOmegadlnMh_Mh);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &Mh_var_vec[0], &dPhidOmegadlnMh_Mh[0]);
            sprintf(tmp_leg, "n_plot_diff_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + nm_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#Deltaz = [%.*g, %.*g]",  gSIM_SIGDIGITS, zmin, gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }

         }
      }
   }

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      TCanvas **c_mf = new TCanvas*[n_cvs];
      for (Int_t c = 0; c < n_cvs; ++c) {
         Int_t c_pos = c;
         if (c > 1) c_pos = c + 1;
         c_mf[c] = new TCanvas(title[c].c_str(), title[c].c_str(), c_pos % 3 * 650, c_pos / 3 * 550, 650, 500);
         if (c == 0)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);
         else if (c == 1)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 3)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 2)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);
         c_mf[c]->SetLogy(1);
         c_mf[c]->SetGridx(0);
         c_mf[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name[c].c_str());
         multi[c]->GetYaxis()->SetTitle(y_name[c].c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Set y-range:
         multi[c]->SetMaximum(1.5 * ymax[c]);
         if (c != 3) {
            double y_range = 1e6;
            if (ymin[c] <  ymax[c] / y_range)
               ymin[c] =  ymax[c] / y_range;
         } else {
            if (ymin[c] <  ymax[c] / y_range_mass)
               ymin[c] =  ymax[c] / y_range_mass;
         }
         multi[c]->SetMinimum(0.8 * ymin[c]);

         double x_txt_delta = 0.;
         if (c == 0) x_txt_delta = 0.4;
         TPaveText *txt = new TPaveText(0.14 + x_txt_delta, 0.47, 0.5 + x_txt_delta, 0.75, "NDC");
         txt->SetTextSize(0.03);
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         for (int ii = 0; ii < int(cvs_text.size()); ++ii) {
            txt->AddText(cvs_text[ii].c_str());
         }
         txt->Draw();

         c_mf[c]->Update();
         c_mf[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY) {
         cout << "  Plo      tting mass function at " << nz_plot << " redshift values (otherwise overcrowded figure)." << endl;
         // Text if required
         cout << "  All your demanded redshift slices are saved in the output file, if gSIM_IS_PRINT=1." << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      if (c_mf) delete[] c_mf;
   }
#endif

   return;
}

//______________________________________________________________________________
void analyse_intensitymultiplier(const vector<double> &z_vec, const vector<double> &M_vec)
{
   //--- Prints/Plots the intensity multiplier: Combination of the mass function
   //    with the halo luminosities.
   //
   //  M_grid         Grid of x-axis values in mass dimension
   //  z_grid         Grid of x-axis values in redshift direction

   if (!gPP_DM_IS_ANNIHIL_OR_DECAY) {
      printf("\n====> ERROR: analyse_intensitymultiplier() in extragal.cc");
      printf("\n             This function is only designed to analyze annihilating DM.");
      printf("\n             => abort()\n\n");
      abort();
   }


   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int card_window = gSIM_EXTRAGAL_FLAG_WINDOWFUNC;
   int card_mf = gEXTRAGAL_FLAG_MASSFUNCTION;
   // fill interpolation grid of mass function and distances:
   init_extragal_manipulatedHMF(z_vec, M_vec, card_mf, card_window);

   // restore grid boundaries and calculate Mh grid
   int nz = z_vec.size();
   int nm = M_vec.size();

   // initialize output arrays:
   vector<double> Mh_var_vec; // units Msol/h
   for (int i = 0; i < nm; i++) {
      Mh_var_vec.push_back(M_vec[i] * gCOSMO_HUBBLE);
   }
   vector< vector< double > > ddelta2dlnM(nz);
   vector< vector< double > > ddelta2dlnM_nosubs;
   vector< double > delta2(nz);
   vector< double > delta2_H_tau(nz);
   vector< double > delta2_nosubs;
   vector< double > delta2_H_tau_nosubs;

   if (gDM_SUBS_NUMBEROFLEVELS > 0) {
      ddelta2dlnM_nosubs.resize(nz);
      delta2_nosubs.resize(nz);
      delta2_H_tau_nosubs.resize(nz);
   }

   // multiply with EBL absorption only for gSIM_FLUX_AT_E_GEV > 1 GeV:
   bool is_multiply_absorption = false;
   if (gSIM_FLUX_AT_E_GEV >= 1.) is_multiply_absorption = true;
   else {
      printf("\n====> NOTE: analyse_intensitymultiplier() in extragal.cc");
      printf("\n            You can include EBL absorption in the plotting");
      printf("\n            by setting gSIM_FLUX_AT_E_GEV >= 1 GeV.\n\n");
   }

   char tmp_leg[1000];
   sprintf(tmp_leg, "%g", gSIM_FLUX_AT_E_GEV);
   string e_flux = string(tmp_leg);

   // ASCII Outputs/prints
   string f_name = gSIM_OUTPUT_DIR + "cosmo.d_intensitymultiplier_dlnM_2D.output";
   string f_name2 = gSIM_OUTPUT_DIR + "cosmo.intensitymultiplier.output";
   FILE *fp = fopen(f_name.c_str(), "w");
   FILE *fp2 = fopen(f_name2.c_str(), "w");

   const int n_cvs = 3;
   vector<string>  x_name(n_cvs);
   vector<string>  y_name(n_cvs);
   vector<string> y_quantity(n_cvs);
   vector<string> title;
   vector<string> unit(n_cvs);

   title.push_back("Intensity multiplier (differential)");
   title.push_back("Intensity multiplier (integrated)");
   if (is_multiply_absorption) title.push_back("Intensity multiplier (integrated, divided by H and EBL absorbed)");
   else title.push_back("Intensity multiplier (integrated, divided by H)");

   y_quantity[0] = "M_{" + legend_for_delta() + "} #times d#LT#delta^{2}(M,z)#GT/dM_{" + legend_for_delta() + "}";
   y_quantity[1] = "(1+z)^{3} #times #LT#delta^{2}(z)#GT";
   if (is_multiply_absorption) y_quantity[2] = "Exp(-#tau(z," + e_flux + " GeV))#times  H_{0} / H(z) #times (1+z)^{3} #times #LT#delta^{2}(z)#GT";
   else y_quantity[2] = "H_{0} / H(z) #times (1+z)^{3} #times #LT#delta^{2}(z)#GT";

   if (abs(gSIM_XPOWER) > SMALL_NUMBER) {
      sprintf(tmp_leg, "%g", gSIM_XPOWER);
      if (abs(gSIM_XPOWER - 1) < SMALL_NUMBER) {
         y_quantity[1] = "z #times " + y_quantity[1];
         y_quantity[2] = "z #times " + y_quantity[2];
      } else {
         y_quantity[1] = "z^{" + string(tmp_leg) + "} #times " + y_quantity[1];
         y_quantity[2] = "z^{" + string(tmp_leg) + "} #times " + y_quantity[2];
      }
   }

   unit[0] = "[unitless]";
   unit[1] = unit[0];
   unit[2] = unit[0];

   double par[38];
   par[7] = gSIM_EPS;
   par[26] = log(gSIM_EXTRAGAL_MMIN * gCOSMO_M_to_MH); // mass integration range - Mmin
   par[27] = log(gSIM_EXTRAGAL_MMAX * gCOSMO_M_to_MH); // mass integration range - Mmax

   // par[0-25] = halo parameters
   // par[26-27]= ln Mhmin, ln Mhmax

   double rho_m02 = pow(gCOSMO_RHO0_C * gCOSMO_OMEGA0_M, 2); // [Msol^2 * kpc^-6]
   double res_tmp;
   for (int i = 0; i < (int)z_vec.size(); i++) {
      cout << "  ... processing redshift=" << z_vec[i] << " ..." << endl;

      int nlevels_save = gDM_SUBS_NUMBEROFLEVELS;

      double unit_conversion = KPC3_to_MPC3 * pow(gCOSMO_HUBBLE, 3) / rho_m02; // from [Msol^2 kpc^-3 Mpc^-3 h^3] to no units

      par[8] = z_vec[i]; // Redshift of the emitted particle

      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         double lnMh = log(Mh_var_vec[j] / gCOSMO_OMEGA0_M);
         res_tmp = 0.;
         d_intensitymultiplier_dlnMh(lnMh, par, res_tmp); // [Msol^2 kpc^-3 Mpc^-3 h^3]
         ddelta2dlnM[i].push_back(res_tmp * unit_conversion * pow(1 + z_vec[i], -3.));
         //cout <<  "z = " << z_vec[i] << "\t M = " <<  Mh_var_vec[j] / gCOSMO_HUBBLE << "\t ddelta2dlnM = " << res_tmp * unit_conversion   << endl; abort();

         if (gDM_SUBS_NUMBEROFLEVELS > 0) {
            // do calculation without substructure:
            gDM_SUBS_NUMBEROFLEVELS = 0;
            res_tmp = 0.;
            if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) d_intensitymultiplier_dlnMh(lnMh, par, res_tmp);
            ddelta2dlnM_nosubs[i].push_back(res_tmp * unit_conversion * pow(1 + z_vec[i], -3.));
            gDM_SUBS_NUMBEROFLEVELS = nlevels_save;
         }
      }
      res_tmp = 0.;
      simpson_lin_adapt(d_intensitymultiplier_dlnMh, par[26] /* ln Mh_min */,
                        par[27] /* ln Mh_max */, par, res_tmp, par[7] /* eps*/);  // [Msol^2 kpc^-3 * Mpc^-3 h^3]
      delta2[i] = res_tmp * unit_conversion * pow(z_vec[i], gSIM_XPOWER);
      delta2_H_tau[i] = res_tmp * unit_conversion / H_over_H0(z_vec[i]) * pow(z_vec[i], gSIM_XPOWER);
      if (is_multiply_absorption) delta2_H_tau[i] *= exp_minus_opt_depth(gSIM_FLUX_AT_E_GEV, par[8], gEXTRAGAL_FLAG_ABSORPTIONPROFILE);

      if (gDM_SUBS_NUMBEROFLEVELS > 0) {
         // do calculation without substructure:
         gDM_SUBS_NUMBEROFLEVELS = 0;
         res_tmp = 0.;
         simpson_lin_adapt(d_intensitymultiplier_dlnMh, par[26] /* ln Mh_min */,
                           par[27] /* ln Mh_max */, par, res_tmp, par[7] /* eps*/);  // [Msol^2 kpc^-3 * Mpc^-3 h^3]
         delta2_nosubs[i] = res_tmp * unit_conversion * pow(z_vec[i], gSIM_XPOWER);
         delta2_H_tau_nosubs[i] = res_tmp * unit_conversion / H_over_H0(z_vec[i]) * pow(z_vec[i], gSIM_XPOWER);
         if (is_multiply_absorption) delta2_H_tau_nosubs[i] *= exp_minus_opt_depth(gSIM_FLUX_AT_E_GEV, par[8], gEXTRAGAL_FLAG_ABSORPTIONPROFILE);
         gDM_SUBS_NUMBEROFLEVELS = nlevels_save;
      }
   }

   // Print header
   if (gSIM_IS_PRINT) {
      // Print header
      fprintf(fp, "#\t\t");
      for (int i = 0; i < (int)z_vec.size(); i++) {
         fprintf(fp, "z = %.*g\t", gSIM_SIGDIGITS + 2, z_vec[i]);
      }
      fprintf(fp, "\n");
      fprintf(fp, "# M_%s\t%s %s (all following columns) \n", legend_for_delta().c_str(), title[0].c_str(), y_quantity[0].c_str());
      fprintf(fp2, "# z\t%s\t%s\n", y_quantity[1].c_str(), y_quantity[2].c_str());
      fprintf(fp, "# [Msol]\t%s (all following columns)\n", unit[0].c_str());

      for (int j = 0; j < (int)M_vec.size(); j++) {
         fprintf(fp, "%.*le\t", gSIM_SIGDIGITS + 2, M_vec[j]);
         for (int i = 0; i < (int)z_vec.size(); i++) {
            fprintf(fp, "%.*le\t", gSIM_SIGDIGITS, ddelta2dlnM[i][j]);
         }
         fprintf(fp, "\n");
      }

      for (int i = 0; i < (int)z_vec.size(); i++) {
         fprintf(fp2, "%.*g\t%.*le\t%.*le\n", gSIM_SIGDIGITS + 2, z_vec[i], gSIM_SIGDIGITS, delta2[i], gSIM_SIGDIGITS, delta2_H_tau[i]);
      }

      fclose(fp);
      fclose(fp2);
      fp = NULL;
      fp2 = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
      cout << "                                " << f_name2 << endl;
   }

#if IS_ROOT
   // plot and/or write ROOT file:
   string f_root = gSIM_OUTPUT_DIR + "cosmo.intensitymultiplier_" +  string(gNAMES_MASSFUNCTION[card_mf]) + "_2D.root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");


   x_name[0] = "M_{" + legend_for_delta() + "} [M_{#odot}]";
   x_name[1] = "Redshift z";
   x_name[2] = x_name[1];


   vector<string> cvs_text;
   sprintf(tmp_leg, "c_{#Delta}^{field}(M_{#Delta}) relation = %s", gNAMES_CDELTAMDELTA[gEXTRAGAL_FLAG_CDELTAMDELTA]);
   cvs_text.push_back(string(tmp_leg));
   sprintf(tmp_leg, "c_{#Delta}^{subs}(M_{#Delta}) relation = %s", gNAMES_CDELTAMDELTA[gHALO_SUBS_FLAG_CDELTAMDELTA[kEXTRAGAL]]);
   cvs_text.push_back(string(tmp_leg));
   sprintf(tmp_leg, "halo mass function = %s", gNAMES_MASSFUNCTION[card_mf]);
   cvs_text.push_back(string(tmp_leg));
   sprintf(tmp_leg, "EBL absorption = %s", gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE]);
   if (is_multiply_absorption) cvs_text.push_back(string(tmp_leg));

   if (gSIM_IS_WRITE_ROOTFILES)
      root_file->cd();

   vector<double> ymax(n_cvs);
   vector<double> ymin(n_cvs);
   double ytmp;
   int nz_plot = 5;

   if (nz < nz_plot) nz_plot = nz;

   int i_step = 0;


   TMultiGraph **multi = new TMultiGraph*[n_cvs];
   TLegend **leg = new TLegend*[n_cvs];

   for (Int_t c = 0; c < n_cvs; ++c) {
      multi[c] = new TMultiGraph();
      double x_leg_delta = 0.;
      double y_leg_delta = 0.38;
      if (c == 1) y_leg_delta = 0.12;

      leg[c] = new TLegend(0.15 + x_leg_delta, 0.15, 0.35 + x_leg_delta, 0.15 + y_leg_delta);
      leg[c]->SetFillColor(kWhite);
      leg[c]->SetTextSize(0.03);
      leg[c]->SetBorderSize(0);
      leg[c]->SetFillStyle(0);

      ymax[c] = 1e-40;
      ymin[c] = 1e40;
      y_name[c] = y_quantity[c] + "  " + unit[c];

      if (c == 0) {

         for (Int_t i = 0; i < nz_plot; ++i) {

            if (nz != 1) i_step = int(round(i * (nz - 1) / (nz_plot - 1))); // vec_step[i];// =
            ytmp = find_max_value(ddelta2dlnM[i_step]);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(ddelta2dlnM[i_step]);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &M_vec[0], &ddelta2dlnM[i_step][0]);
            sprintf(tmp_leg, "delta_diff_plot_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(i + 1));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");

            if (gDM_SUBS_NUMBEROFLEVELS > 0) {
               TGraph *gr_nosubs = new TGraph(nm, &M_vec[0], &ddelta2dlnM_nosubs[i_step][0]);
               sprintf(tmp_leg, "delta_diff_nosubs_plot_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
               gr_nosubs->SetName(tmp_leg);
               gr_nosubs->SetLineColor(rootcolor(i + 1));
               gr_nosubs->SetLineWidth(2);
               gr_nosubs->SetLineStyle(2);
               gr_nosubs->SetTitle(tmp_leg);
               gr_nosubs->GetXaxis()->SetTitle(x_name[c].c_str());
               gr_nosubs->GetYaxis()->SetTitle(y_name[c].c_str());
               multi[c]->Add(gr_nosubs, "L");
            }

            sprintf(tmp_leg, "z = %.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      } else if (c == 1 or c == 2) {

         ytmp = find_max_value(delta2);
         if (ytmp > ymax[c]) ymax[c] = ytmp;
         ytmp = find_min_value(delta2);
         if (ytmp < ymin[c]) ymin[c] = ytmp;

         TGraph *gr = new TGraph[1];
         if (c == 1) {
            gr = new TGraph(nz, &z_vec[0], &delta2[0]);
            sprintf(tmp_leg, "delta2_plot");
         } else {
            gr = new TGraph(nz, &z_vec[0], &delta2_H_tau[0]);
            sprintf(tmp_leg, "delta2_H_plot");
         }

         gr->SetName(tmp_leg);
         gr->SetLineColor(rootcolor(1));
         gr->SetLineWidth(2);
         gr->SetLineStyle(1);
         gr->SetTitle(tmp_leg);
         gr->GetXaxis()->SetTitle(x_name[c].c_str());
         gr->GetYaxis()->SetTitle(y_name[c].c_str());
         multi[c]->Add(gr, "L");

         if (gDM_SUBS_NUMBEROFLEVELS > 0) {
            TGraph *gr_nosubs = new TGraph[1];
            if (c == 1) {
               gr_nosubs = new TGraph(nz, &z_vec[0], &delta2_nosubs[0]);
               sprintf(tmp_leg, "delta2_nosubs_plot");
            } else {
               gr_nosubs = new TGraph(nz, &z_vec[0], &delta2_H_tau_nosubs[0]);
               sprintf(tmp_leg, "delta2_H_nosubs_plot");
            }

            gr_nosubs->SetName(tmp_leg);
            gr_nosubs->SetLineColor(rootcolor(1));
            gr_nosubs->SetLineWidth(2);
            gr_nosubs->SetLineStyle(2);
            gr_nosubs->SetTitle(tmp_leg);
            gr_nosubs->GetXaxis()->SetTitle(x_name[c].c_str());
            gr_nosubs->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr_nosubs, "L");

            if (gDM_SUBS_NUMBEROFLEVELS > 0) {
               char tmp_cvs[1000];
               sprintf(tmp_cvs, "With substructure");
               leg[c]->AddEntry(gr, tmp_cvs, "L");
            }
         }

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gr->Write();
         }
      }
   }


   TCanvas **c_mf = NULL;
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      c_mf = new TCanvas*[n_cvs];
      for (Int_t c = 0; c < n_cvs; ++c) {
         Int_t c_pos = c;

         c_mf[c] = new TCanvas(title[c].c_str(), title[c].c_str(), c_pos % 3 * 650, c_pos / 3 * 550, 650, 500);
         if (c == 0)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 1 or c == 2)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);

         c_mf[c]->SetLogy(1);
         c_mf[c]->SetGridx(0);
         c_mf[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name[c].c_str());
         multi[c]->GetYaxis()->SetTitle(y_name[c].c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Set y-range:
         double y_range = 1e7;
         if (ymin[c] <  ymax[c] / y_range) {
            ymin[c] = 0.8 * ymax[c] / y_range;
            multi[c]->SetMaximum(1.5 * ymax[c]);
            multi[c]->SetMinimum(ymin[c]);
         }

         // Text if required
         double y_txt;
         if (c == 0) y_txt = 0.55;
         else y_txt = 0.275;
         double x_txt_delta = 0.;
         TPaveText *txt = new TPaveText(0.14 + x_txt_delta, y_txt, 0.5 + x_txt_delta, y_txt + 0.12, "NDC");
         txt->SetTextSize(0.03);
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         int iimax = int(cvs_text.size());
         if (c != 2 and is_multiply_absorption) iimax -= 1;
         for (int ii = 0; ii < iimax; ++ii) {
            txt->AddText(cvs_text[ii].c_str());
         }
         txt->Draw();

         if (gDM_SUBS_NUMBEROFLEVELS > 0) {
            TLine *line_nonlin = new TLine(1e-40, 1e-40, 1e-40, 1e-40);
            line_nonlin->SetLineColor(13);
            line_nonlin->SetLineWidth(2);
            line_nonlin->SetLineStyle(2);
            char tmp_cvs[1000];
            sprintf(tmp_cvs, "No substructure in cluster halos");
            leg[c]->AddEntry(line_nonlin, tmp_cvs, "L");
         }

         c_mf[c]->Update();
         c_mf[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY) {
         cout << "  Plotting differential intensity multiplier at " << nz_plot << " redshift values (otherwise overcrowded figure)." << endl;
         cout << "  All your demanded redshift slices are saved in the output file, if gSIM_IS_PRINT=1." << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      if (c_mf) delete[] c_mf;
   }
#endif

   return;
}

//______________________________________________________________________________
void analyse_massfunction(const vector<double> &z_vec, const vector<double> &M_vec, const int card_mf)
{
   //--- Prints/Plots the extragalactic mass function.
   //
   //  M_grid         Grid of x-axis values in mass dimension
   //  z_grid         Grid of x-axis values in redshift direction
   //  card_mf        Mass function to evaluate

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int card_window = gSIM_EXTRAGAL_FLAG_WINDOWFUNC;
   // fill interpolation grid of mass function and distances:
   init_extragal_manipulatedHMF(z_vec, M_vec, card_mf, card_window);

   // restore grid boundaries and calculate Mh grid
   int nz = z_vec.size();
   double zmin = z_vec[0];

   int nm = M_vec.size();
   double Mmax = M_vec[nm - 1];

   // initialize arrays:
   vector<double> Mh_var_vec; // units Msol/h
   vector<double> Mh_vec; // units Omega_m0 * Msol/h
   for (int i = 0; i < nm; i++) {
      Mh_var_vec.push_back(M_vec[i] * gCOSMO_HUBBLE);
      Mh_vec.push_back(M_vec[i] * gCOSMO_M_to_MH);
   }
   vector< vector< double > > dNdVhdlnMh(nz);
   vector< vector< double > > dNdVhdlnMh_cutoff(nz);
   vector< vector< double > > dNdlnMh_comp(nz);
   vector< vector< double > > Mh_var_grid_comp(nz); // units Msol/h
   vector< vector< double > > sigma2_Mh_z(nz);
   vector< vector< double > > dlnsigma2dlnMh_Mh_z(nz);

   vector< vector< double > > dNdOmega_zM(nz);
   vector< vector< double > > dNdOmegadlnMh_zM(nz);
   vector< vector< double > > dNdOmegadz_zM(nz);

   vector< vector<double> >  linear_lnk_vecvec(gCOSMO_Z_GRID.size());
   vector< vector<double> >  linear_lnp_vecvec(gCOSMO_Z_GRID.size());
   vector< vector<double> >  nonlinear_lnk_vecvec(gCOSMO_Z_GRID.size());
   vector< vector<double> >  nonlinear_lnp_vecvec(gCOSMO_Z_GRID.size());

   // load precomputed P(k) for plotting:
   if (gSIM_EXTRAGAL_FLAG_GROWTHFACTOR == kPKZ_FROMFILE) {
      for (int i = 0; i < int(gCOSMO_Z_GRID.size()); i++) {
         get_pk(gCOSMO_Z_GRID[i], linear_lnk_vecvec[i], linear_lnp_vecvec[i], false);
         get_pk(gCOSMO_Z_GRID[i], nonlinear_lnk_vecvec[i], nonlinear_lnp_vecvec[i], true);
      }
   } else {
      get_pk(0, linear_lnk_vecvec[0], linear_lnp_vecvec[0], false);
      get_pk(0, nonlinear_lnk_vecvec[0], nonlinear_lnp_vecvec[0], true);
   }

   // Calculate sigma2
   for (int i = 0; i < int(z_vec.size()); i++) {
      vector<vector<double>> sigma2;
      if (gSIM_EXTRAGAL_FLAG_GROWTHFACTOR == kPKZ_FROMFILE)
         sigma2 = sigma2_andderiv(z_vec[i], Mh_vec, linear_lnk_vecvec[i], linear_lnp_vecvec[i], card_window, gSIM_EXTRAGAL_FLAG_GROWTHFACTOR);
      else
         sigma2 = sigma2_andderiv(z_vec[i], Mh_vec, linear_lnk_vecvec[0], linear_lnp_vecvec[0], card_window, gSIM_EXTRAGAL_FLAG_GROWTHFACTOR);
      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         sigma2_Mh_z[i].push_back(sigma2[0][j]);
         dlnsigma2dlnMh_Mh_z[i].push_back(sigma2[1][j]);
      }
   }

   double tmp;
   char tmp_leg[1000];

   // ASCII Outputs/prints
   string f_name = gSIM_OUTPUT_DIR + "cosmo.hmf_" +  string(gNAMES_MASSFUNCTION[card_mf]) + "_2D.output";
   string f_name2 = gSIM_OUTPUT_DIR + "cosmo.dndOmega_" +  string(gNAMES_MASSFUNCTION[card_mf]) + "_2D.output";
   string f_name3 = gSIM_OUTPUT_DIR + "cosmo.sigma2_2D.output";
   FILE *fp = fopen(f_name.c_str(), "w");
   FILE *fp2 = fopen(f_name2.c_str(), "w");
   FILE *fp3 = fopen(f_name3.c_str(), "w");

   // compute massfunction in mass and z direction
   double prefactor0;
   double prefactor3;
   double y_range;
   vector<string> label_literature;
   bool is_cutoff_calculated = false;

   const int n_cvs = 6;
   vector<string>  x_name(n_cvs);
   vector<string>  y_name(n_cvs);
   vector<string> y_quantity(n_cvs);
   vector<string> title;
   vector<string> unit(n_cvs);

   title.push_back("Halo mass function");
   title.push_back("Halos in redshift interval (integrated)");
   title.push_back("Halos in mass interval (integrated)");
   title.push_back("Halos per redshift (differential)");
   title.push_back("Halos per mass decade (differential)");
   title.push_back("Matter power spectrum");
   title.push_back("Sigma^2");

   y_quantity[1] = "dN/d#Omega(#Deltaz)";
   y_quantity[2] = "dN/d#Omega(#DeltaM)";
   y_quantity[3] = "#frac{dN}{dz d#Omega}";
   y_quantity[5] = "P(k) #times k^{3} / (2#pi^{2})";

   unit[1] = "[sr^{-1}]";
   unit[2] = unit[1];
   unit[3] = unit[1];
   unit[5] = "[unitless]";

   if (fabs(gSIM_XPOWER) < SMALL_NUMBER) {
      //  dnh/dM in units of h^4 Mpc^-3 Msol^-1
      unit[0] = "[Mpc^{-3} M_{#odot}^{-1}]";
      unit[4] = "[sr^{-1} M_{#odot}^{-1}]";
      y_quantity[0] = "#frac{dn}{dM_{" + legend_for_delta() + "}} #times h^{-4}";
      y_quantity[4] = "#frac{dN}{dM_{" + legend_for_delta() + "} d#Omega} #times h^{-1}";
      y_range = 1e12;
   } else if (fabs(gSIM_XPOWER - 1) < SMALL_NUMBER) {
      //  M * dnh/dM (or Mh * dnh/dMh)  in units of h^3 Mpc^-3
      unit[0] = "[Mpc^{-3}]";
      unit[4] = "[sr^{-1}]";
      y_quantity[0] = "M_{" + legend_for_delta() + "} #times #frac{dn}{dM_{" + legend_for_delta() + "}} #times h^{-3}";
      y_quantity[4] = "M_{" + legend_for_delta() + "} #times #frac{dN}{dM_{" + legend_for_delta() + "} d#Omega}";
      y_range = 1e8;
   } else if (fabs(gSIM_XPOWER - 2) < SMALL_NUMBER) {
      // unitless, M^2 / rho_mean * dnh/dM
      unit[0] = "[unitless]";
      unit[4] = "[M_{#odot} sr^{-1}]";
      y_quantity[0] = "M_{" + legend_for_delta() + "}^{2} /#rho_{m,0} #times #frac{dn}{dM_{" + legend_for_delta() + "}}";
      y_quantity[4] = "M_{" + legend_for_delta() + "}^{2} #times #frac{dN}{dM_{" + legend_for_delta() + "} d#Omega #times h}";
      y_range = 1e4;
   } else {
      sprintf(tmp_leg, "%g", gSIM_XPOWER - 1);
      unit[0] = "[Mpc^{-3}  M_{#odot}^{" + string(tmp_leg) + "}]";
      unit[4] = "[sr^{-1} M_{#odot}^{" + string(tmp_leg) + "}]";
      sprintf(tmp_leg, "%g", gSIM_XPOWER);
      y_quantity[0] = "M_{" + legend_for_delta() + "}^{" + string(tmp_leg) + "} #times #frac{dn}{dM_{" + legend_for_delta() + "}}";
      y_quantity[4] = "M_{" + legend_for_delta() + "}^{" + string(tmp_leg) + "} #times #frac{dN}{dM_{" + legend_for_delta() + "} d#Omega}";
      sprintf(tmp_leg, "%g", gSIM_XPOWER - 4);
      y_quantity[0] += " #times h^{" + string(tmp_leg) + "}";
      sprintf(tmp_leg, "%g", gSIM_XPOWER - 1);
      y_quantity[4] += " #times h^{" + string(tmp_leg) + "}";
      double exp = 12 - 4 * gSIM_XPOWER;
      y_range = pow(10, exp);
   }


   for (int i = 0; i < (int)z_vec.size(); i++) {
      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         dNdVhdlnMh[i].push_back(dNdVhdlnMh_interpol(z_vec[i], Mh_var_vec[j] / gCOSMO_OMEGA0_M));

         // calculate number of halos between zmin and z and Mmin and M:
         double z_current = z_vec[i];
         dNdOmega_zM[i].push_back(dNdOmega(zmin, z_current,  M_vec[j], Mmax));

         double res_tmp = 0.;
         double par_z[2] = {zmin, z_current};
         double lnMh = log(Mh_var_vec[j] / gCOSMO_OMEGA0_M);
         dNdOmegadlnMh(lnMh, par_z, res_tmp);
         dNdOmegadlnMh_zM[i].push_back(res_tmp);

         res_tmp = 0.;
         double par_Mh[2] = {Mh_var_vec[j] / gCOSMO_OMEGA0_M, Mmax * gCOSMO_M_to_MH};
         dNdOmegadz(z_current, par_Mh, res_tmp);
         dNdOmegadz_zM[i].push_back(res_tmp);
      }

      // cut off mass function at rho_mean:
      dNdVhdlnMh_cutoff[i] = dNdVhdlnMh[i];

      double rhoh_halos = 0;

      double par[3];
      par[0] = z_vec[i];
      par[1] = 1e11; // will be updated later.
      par[2] = gSIM_EXTRAGAL_MF_SIGMA_CUTOFF;
      double mh_low = 1e-2 * gCOSMO_MH_GRID[0];
      simpson_log_adapt(dNdVhdlnMh_integrand_Mh, gCOSMO_MH_GRID[0], gCOSMO_MH_GRID[gCOSMO_MH_GRID.size() - 1], par, rhoh_halos, gSIM_EPS);
      //cout << "z = " << par[0] <<"\trho_halos = " << rhoh_halos  * KPC3_to_MPC3 /  RHO_CRITperh2_MSOLperKPC3  << " rho_m" << endl;

      if (rhoh_halos > gDM_RHOHALOES_TO_RHOMEAN * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3) {
         //cout << rhoh_halos/( gDM_RHOHALOES_TO_RHOMEAN * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3) << endl;
         gsl_function F;
         F.function = &solve_Mhmin;
         Mmin_params_for_rootfinding params = {z_vec[i], par[2]};
         F.params = &params;
         double mh_up = 1e16;
         int return_status = 0;
         double Mhmin = rootsolver_gsl(gsl_root_fsolver_brent, F, mh_low, mh_up, gSIM_EPS, return_status);
         //cout << Mhmin << endl;
         if (Mhmin > Mh_var_vec[0] / gCOSMO_OMEGA0_M) {
            is_cutoff_calculated = true;
            par[1] = Mhmin;
            for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
               double res_tmp = 0.;
               double Mh = Mh_var_vec[j] / gCOSMO_OMEGA0_M;
               dNdVhdlnMh_sigmoid(Mh, par, res_tmp);
               dNdVhdlnMh_cutoff[i][j] = res_tmp;
            }
         }
      }

      // scale for plotting:
      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         if (fabs(gSIM_XPOWER) < SMALL_NUMBER) {
            //  dnh/dM in units of h^4 Mpc^-3 Msol^-1
            prefactor0 = 1. / (M_vec[j] * gCOSMO_HUBBLE);
            prefactor3 = prefactor0;
         } else if (fabs(gSIM_XPOWER - 1) < SMALL_NUMBER) {
            //  M * dnh/dM (or Mh * dnh/dMh)  in units of h^3 Mpc^-3
            prefactor0 = 1.;
            prefactor3 = prefactor0;
         } else if (fabs(gSIM_XPOWER - 2) < SMALL_NUMBER) {
            // unitless, M^2 / rho_mean * dn/dM
            prefactor0 = Mh_var_vec[j] / gCOSMO_OMEGA0_M / RHO_CRITperh2_MSOLperKPC3 * KPC3_to_MPC3;
            prefactor3 = M_vec[j] * gCOSMO_HUBBLE;
         } else {
            prefactor0 = pow(M_vec[j] * gCOSMO_HUBBLE, gSIM_XPOWER - 1);
            prefactor3 = prefactor0;
         }
         dNdVhdlnMh[i][j] *= prefactor0;
         dNdVhdlnMh_cutoff[i][j] *= prefactor0;
         dNdOmegadlnMh_zM[i][j] *= prefactor3;

         // compute reference curves:
         tmp = prefactor0 * dNdVhdlnMh_literature(z_vec[i], Mh_var_vec[j], card_mf, label_literature);
         if (tmp > 1e-40) {
            dNdlnMh_comp[i].push_back(tmp);
            Mh_var_grid_comp[i].push_back(Mh_var_vec[j]);
         }
      }
   }

   // Print header
   if (gSIM_IS_PRINT) {
      // Print header
      fprintf(fp, "#\t\t");
      fprintf(fp2, "#\t\t");
      fprintf(fp3, "#\t\t");
      for (int i = 0; i < (int)z_vec.size(); i++) {
         fprintf(fp, "z = %.*g\t", gSIM_SIGDIGITS + 2, z_vec[i]);
         fprintf(fp2, "z = %.*g\t", gSIM_SIGDIGITS + 2, z_vec[i]);
         fprintf(fp3, "z = %.*g\t", gSIM_SIGDIGITS + 2, z_vec[i]);
      }
      fprintf(fp, "\n");
      fprintf(fp2, "\n");
      fprintf(fp3, "\n");
      fprintf(fp, "# M_%s x h\t%s %s (all following columns) \n", legend_for_delta().c_str(), title[0].c_str(), y_quantity[0].c_str());
      fprintf(fp2, "# M_%s\tdN/dOmega(<M_max, >z_min) (all following columns) \n", legend_for_delta().c_str());
      fprintf(fp3, "# M_%s x h\t%s (all following columns) \n", legend_for_delta().c_str(), title[6].c_str());

      fprintf(fp, "# [Msol]\t%s (all following columns)\n", unit[0].c_str());
      fprintf(fp2, "# [Msol]\t%s (all following columns)\n", unit[1].c_str());
      fprintf(fp3, "# [Msol]\t%s (all following columns)\n", unit[5].c_str());

      for (int j = 0; j < (int)Mh_var_vec.size(); j++) {
         fprintf(fp, "%.*le\t", gSIM_SIGDIGITS + 2, Mh_var_vec[j]);
         fprintf(fp2, "%.*le\t", gSIM_SIGDIGITS + 2, M_vec[j]);
         fprintf(fp3, "%.*le\t", gSIM_SIGDIGITS + 2, Mh_var_vec[j]);
         for (int i = 0; i < (int)z_vec.size(); i++) {
            fprintf(fp, "%.*le\t", gSIM_SIGDIGITS, dNdVhdlnMh[i][j]);
            fprintf(fp2, "%.*le\t", gSIM_SIGDIGITS, dNdOmega_zM[i][j]);
            fprintf(fp3, "%.*le\t", gSIM_SIGDIGITS, sigma2_Mh_z[i][j]);
         }
         fprintf(fp, "\n");
         fprintf(fp2, "\n");
         fprintf(fp3, "\n");
      }
      fclose(fp);
      fclose(fp2);
      fclose(fp3);
      fp = NULL;
      fp2 = NULL;
      fp3 = NULL;
      cout << endl;
      cout << "_______________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
      cout << "                                " << f_name2 << endl;
      cout << "                                " << f_name3 << endl;
   }

#if IS_ROOT
   // plot and/or write ROOT file:
   string f_root = gSIM_OUTPUT_DIR + "cosmo.hmf_dndOmega_" +  string(gNAMES_MASSFUNCTION[card_mf]) + "_2D.root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   x_name[0] = "M_{" + legend_for_delta() + "} #times h  [M_{#odot}]";
   x_name[4] = x_name[0];

   sprintf(tmp_leg, "Interval #Deltaz = [%g, z]", zmin);
   x_name[1] = string(tmp_leg);
   sprintf(tmp_leg, "Interval #DeltaM  = [M_{%s}, %g M_{#odot}]", legend_for_delta().c_str(), Mmax);
   x_name[2] = string(tmp_leg);
   x_name[3] = "redshift z";
   x_name[5] = "wavenumber k/h [Mpc^{-1}]";

   vector<string> cvs_text;
   vector<string> cvs_text_alt;

   cvs_text.push_back("Cosmology:");
   cvs_text.push_back("(#Omega_{#Lambda}, #Omega_{m}, #Omega_{b}, #sigma_{8}, h, n_{s}) =");
   sprintf(tmp_leg, "(%g, %g, %g, %g, %g, %g)",
           round(gCOSMO_OMEGA0_LAMBDA, 3), round(gCOSMO_OMEGA0_M, 3), round(gCOSMO_OMEGA0_B, 4),
           gCOSMO_SIGMA8, round(gCOSMO_HUBBLE, 3), gCOSMO_N_S);
   cvs_text.push_back(string(tmp_leg));
   cvs_text_alt = cvs_text;

   sprintf(tmp_leg, "window = %s, lin. growth = %s", gNAMES_WINDOWFUNC[card_window], gNAMES_GROWTHFACTOR[gSIM_EXTRAGAL_FLAG_GROWTHFACTOR]);
   cvs_text.push_back(string(tmp_leg));
   sprintf(tmp_leg, "HMF = %s,  window fnct. = %s", gNAMES_MASSFUNCTION[card_mf], gNAMES_WINDOWFUNC[card_window]);
   cvs_text_alt.push_back(string(tmp_leg));

   if (gSIM_IS_WRITE_ROOTFILES)
      root_file->cd();

   vector<double> ymax(n_cvs);
   vector<double> ymin(n_cvs);
   double ytmp;
   int nz_plot = 5;
   int nm_plot = 5;
   if (nz < nz_plot) nz_plot = nz;
   if (nm < nm_plot) nm_plot = nm;
   int i_step = 0;
   vector<double> dNdOmega_z(nz);
   vector<double> dNdOmega_M(nm);
   vector<double> dNdOmegadz_z(nz);
   vector<double> dNdOmegadlnMh_Mh(nm);

   TMultiGraph **multi = new TMultiGraph*[n_cvs];
   TLegend **leg = new TLegend*[n_cvs];

   double x_leg_delta;
   double kmax = 1e-40;
   double kmin = 1e40;

   for (Int_t c = 0; c < n_cvs; ++c) {
      multi[c] = new TMultiGraph();

      if (c == 1 or c == 3) x_leg_delta = 0.4;
      else x_leg_delta = 0.;

      leg[c] = new TLegend(0.15 + x_leg_delta, 0.15, 0.35 + x_leg_delta, 0.53);
      leg[c]->SetFillColor(kWhite);
      leg[c]->SetTextSize(0.03);
      leg[c]->SetBorderSize(0);
      leg[c]->SetFillStyle(0);

      ymax[c] = 1e-40;
      ymin[c] = 1e40;
      y_name[c] = y_quantity[c] + "  " + unit[c];

      if (c == 0) {
         int i_label_ref = -1;
         for (Int_t i_ref = 0; i_ref < nz; ++i_ref) {
            if (i_ref != 0 and dNdlnMh_comp[i_ref] == dNdlnMh_comp[i_ref - 1])
               continue;
            else if (Mh_var_grid_comp[i_ref].size() > 0) {
               i_label_ref++;
               TGraph *gr_compare = new TGraph(Mh_var_grid_comp[i_ref].size(), &Mh_var_grid_comp[i_ref][0], &dNdlnMh_comp[i_ref][0]);
               sprintf(tmp_leg, "mf_reference_%d", i_label_ref);
               gr_compare->SetName(tmp_leg);
               int i_label_start = 13;
               if (i_label_ref >= 5) i_label_start = 15;
               gr_compare->SetLineColor(i_label_start + i_label_ref);
               gr_compare->SetLineWidth(5);
               gr_compare->SetLineStyle(1);
               gr_compare->SetTitle(tmp_leg);
               gr_compare->GetXaxis()->SetTitle(x_name[c].c_str());
               gr_compare->GetYaxis()->SetTitle(y_name[c].c_str());
               multi[c]->Add(gr_compare, "L");
               sprintf(tmp_leg, "%s", label_literature[i_label_ref].c_str());
               leg[c]->AddEntry(gr_compare, tmp_leg, "L");
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr_compare->Write();
               ytmp = find_max_value(dNdlnMh_comp[i_ref]);
               if (ytmp > ymax[c]) ymax[c] = ytmp;
               ytmp = find_min_value(dNdlnMh_comp[i_ref]);
               if (ytmp < ymin[c]) ymin[c] = ytmp;
            }
         }

         for (Int_t i = 0; i < nz_plot; ++i) {

            if (nz != 1) i_step = int(round(i * (nz - 1) / (nz_plot - 1))); // vec_step[i];// =
            ytmp = find_max_value(dNdVhdlnMh[i_step]);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dNdVhdlnMh[i_step]);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &Mh_var_vec[0], &dNdVhdlnMh[i_step][0]);
            sprintf(tmp_leg, "hmf_plot_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(i + 1));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "k%s, z = %.*g", gNAMES_MASSFUNCTION[card_mf], gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }

            if (is_cutoff_calculated) {
               TGraph *gr_cutoff = new TGraph(nm, &Mh_var_vec[0], &dNdVhdlnMh_cutoff[i_step][0]);
               sprintf(tmp_leg, "hmf_plot_cutoff_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
               gr_cutoff->SetName(tmp_leg);
               gr_cutoff->SetLineColor(rootcolor(i + 1));
               gr_cutoff->SetLineWidth(2);
               gr_cutoff->SetLineStyle(2);
               gr_cutoff->SetTitle(tmp_leg);
               gr_cutoff->GetXaxis()->SetTitle(x_name[c].c_str());
               gr_cutoff->GetYaxis()->SetTitle(y_name[c].c_str());
               multi[c]->Add(gr_cutoff, "L");
               if (gSIM_IS_WRITE_ROOTFILES) {
                  root_file->cd();
                  gr_cutoff->Write();
               }
            }
         }

         if (is_cutoff_calculated) {
            double dummy[] = {1e-40};
            TGraph *gr_dummy = new TGraph(1, &Mh_var_vec[0], &dummy[0]);
            multi[c]->Add(gr_dummy, "L");
            gr_dummy->SetLineColor(13);
            gr_dummy->SetLineWidth(2);
            gr_dummy->SetLineStyle(2);
            multi[c]->Add(gr_dummy, "L");
            sprintf(tmp_leg, "#rho_{haloes} = %g #rho_{m}, #sigma_{cutoff} = %g", gDM_RHOHALOES_TO_RHOMEAN, gSIM_EXTRAGAL_MF_SIGMA_CUTOFF);
            leg[c]->AddEntry(gr_dummy, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr_dummy->Write();
            }
         }
      } else if (c == 1) {
         for (Int_t i = 0; i < nm_plot; ++i) {
            if (nm != 1) i_step = int(round(i * (nm - 1) / nm_plot));  // vec_step[i];// =


            for (int ii = 0; ii < (int)z_vec.size(); ii++) {
               dNdOmega_z[ii] = dNdOmega_zM[ii][i_step];
            }

            ytmp = find_max_value(dNdOmega_z);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dNdOmega_z);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nz, &z_vec[0], &dNdOmega_z[0]);
            sprintf(tmp_leg, "n_plot_int_M=%.*gMsol", gSIM_SIGDIGITS, M_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#DeltaM = [%.*g M_{#odot}, %.*g M_{#odot}]",  gSIM_SIGDIGITS, M_vec[i_step], gSIM_SIGDIGITS, Mmax);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      } else if (c == 2) {
         for (Int_t i = 0; i < nz_plot; ++i) {
            if (nz != 1) i_step = int(round((i + 1) * (nz - 1) / nz_plot)); // vec_step[i];// =

            for (int ii = 0; ii < (int)M_vec.size(); ii++) {
               dNdOmega_M[ii] = dNdOmega_zM[i_step][ii];
            }

            ytmp = find_max_value(dNdOmega_M);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dNdOmega_M);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &M_vec[0], &dNdOmega_M[0]);
            sprintf(tmp_leg, "n_plot_int_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + nm_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#Deltaz = [%.*g, %.*g]",  gSIM_SIGDIGITS, zmin, gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      }  else if (c == 3) {
         for (Int_t i = 0; i < nm_plot; ++i) {
            if (nm != 1) i_step = int(round(i * (nm - 1) / nm_plot));  // vec_step[i];// =

            for (int ii = 0; ii < (int)z_vec.size(); ii++) {
               dNdOmegadz_z[ii] = dNdOmegadz_zM[ii][i_step];
            }

            ytmp = find_max_value(dNdOmegadz_z);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dNdOmegadz_z);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nz, &z_vec[0], &dNdOmegadz_z[0]);
            sprintf(tmp_leg, "nd_plot_diff_M=%.*gMsol", gSIM_SIGDIGITS, M_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#DeltaM = [%.*g M_{#odot}, %.*g M_{#odot}]",  gSIM_SIGDIGITS, M_vec[i_step], gSIM_SIGDIGITS, Mmax);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }
         }
      } else if (c == 4) {

         for (Int_t i = 0; i < nz_plot; ++i) {
            if (nz != 1) i_step = int(round((i + 1) * (nz - 1) / nz_plot)); // vec_step[i];// =

            for (int ii = 0; ii < (int)M_vec.size(); ii++) {
               dNdOmegadlnMh_Mh[ii] = dNdOmegadlnMh_zM[i_step][ii];
            }

            ytmp = find_max_value(dNdOmegadlnMh_Mh);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(dNdOmegadlnMh_Mh);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr = new TGraph(nm, &Mh_var_vec[0], &dNdOmegadlnMh_Mh[0]);
            sprintf(tmp_leg, "n_plot_diff_z=%.*g", gSIM_SIGDIGITS, z_vec[i_step]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(nz_plot + nm_plot + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "#Deltaz = [%.*g, %.*g]",  gSIM_SIGDIGITS, zmin, gSIM_SIGDIGITS, z_vec[i_step]);
            leg[c]->AddEntry(gr, tmp_leg, "L");
            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }

         }
      } else if (c == 5) {

         int i_max = int(gCOSMO_Z_GRID.size());
         vector<double> z_plot = gCOSMO_Z_GRID;
         if (gSIM_EXTRAGAL_FLAG_GROWTHFACTOR != kPKZ_FROMFILE) {
            i_max = 1;
            z_plot[0] = 0.;
         }

         for (int i = 0; i < i_max; ++i) {

            vector<double> link_vec, linp_vec;
            vector<double> nonlink_vec, nonlinp_vec;
            int nk_lin = int(linear_lnk_vecvec[i].size());
            int nk_nonlin = int(nonlinear_lnk_vecvec[i].size());
            for (int j = 0; j < nk_lin; ++j) {
               link_vec.push_back(exp(linear_lnk_vecvec[i][j]));
               linp_vec.push_back(exp(linear_lnp_vecvec[i][j]) * pow(link_vec[j], 3) / 2. / PI / PI);
            }
            for (int j = 0; j < nk_nonlin; ++j) {
               nonlink_vec.push_back(exp(nonlinear_lnk_vecvec[i][j]));
               nonlinp_vec.push_back(exp(nonlinear_lnp_vecvec[i][j]) * pow(nonlink_vec[j], 3) / 2. / PI / PI);
            }

            double ktmp = find_max_value(link_vec);
            if (ktmp > kmax) kmax = ktmp;
            ktmp = find_min_value(link_vec);
            if (ktmp < kmin) kmin = ktmp;

            ytmp = find_max_value(nonlinp_vec);
            if (ytmp > ymax[c]) ymax[c] = ytmp;
            ytmp = find_min_value(linp_vec);
            if (ytmp < ymin[c]) ymin[c] = ytmp;

            TGraph *gr_nl = new TGraph(nk_nonlin, &nonlink_vec[0], &nonlinp_vec[0]);
            sprintf(tmp_leg, "nonlin_pk_plot_z=%.*g", gSIM_SIGDIGITS, z_plot[i]);
            gr_nl->SetName(tmp_leg);
#if IS_ALPHA_METHOD
            gr_nl->SetLineColorAlpha(rootcolor(z_plot.size() + 2 + i), 0.5);
#else
            gr_nl->SetLineColor(rootcolor(z_plot.size() + 2 + i));
#endif
            gr_nl->SetLineWidth(2);
            gr_nl->SetLineStyle(4);
            gr_nl->SetTitle(tmp_leg);
            gr_nl->GetXaxis()->SetTitle(x_name[c].c_str());
            gr_nl->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr_nl, "L");

            TGraph *gr = new TGraph(nk_lin, &link_vec[0], &linp_vec[0]);
            sprintf(tmp_leg, "lin_pk_plot_z=%.*g", gSIM_SIGDIGITS, z_plot[i]);
            gr->SetName(tmp_leg);
            gr->SetLineColor(rootcolor(z_plot.size() + 2 + i));
            gr->SetLineWidth(2);
            gr->SetLineStyle(1);
            gr->SetTitle(tmp_leg);
            gr->GetXaxis()->SetTitle(x_name[c].c_str());
            gr->GetYaxis()->SetTitle(y_name[c].c_str());
            multi[c]->Add(gr, "L");
            sprintf(tmp_leg, "z = %.*g",  gSIM_SIGDIGITS, z_plot[i]);
            leg[c]->AddEntry(gr, tmp_leg, "L");

            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gr->Write();
            }

         }
      }
   }

   TCanvas **c_mf = NULL;
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      c_mf = new TCanvas*[n_cvs];
      for (Int_t c = 0; c < n_cvs; ++c) {
         Int_t c_pos = c;
         if (c > 2) c_pos = c + 1;
         if (c == 5) c_pos = 3;

         c_mf[c] = new TCanvas(title[c].c_str(), title[c].c_str(), c_pos % 3 * 650, c_pos / 3 * 550, 650, 500);
         if (c == 0)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 1)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);
         else if (c == 2)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 4)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_MLOG);
         else if (c == 3)  c_mf[c]->SetLogx(gSIM_EXTRAGAL_IS_ZLOG);
         c_mf[c]->SetLogy(1);
         c_mf[c]->SetGridx(0);
         c_mf[c]->SetGridy(0);

         // Plot graphs and legends
         multi[c]->Draw("A");
         gSIM_CLUMPYAD->Draw();
         multi[c]->GetXaxis()->SetTitle(x_name[c].c_str());
         multi[c]->GetYaxis()->SetTitle(y_name[c].c_str());
         gPad->SetTickx(1);
         gPad->SetTicky(1);
         leg[c]->Draw("SAME");

         // Set y-range:
         if (c == 5) {
            ymax[c] = 2 * ymax[c];
            multi[c]->SetMaximum(ymax[c]);
            if (ymin[c] <  ymax[c] * 1e-7) {
               ymin[c] = ymax[c] * 1e-7;
               multi[c]->SetMinimum(ymin[c]);
            }
            if (kmax > 1e6) multi[c]->GetXaxis()->SetLimits(kmin, 1e6);
         } else if (ymin[c] <  ymax[c] / y_range) {
            ymin[c] = 0.8 * ymax[c] / y_range;
            if (c == 0 or c == 4)  multi[c]->SetMaximum(1.5 * ymax[c]);
            if (c == 0 or c == 4)  multi[c]->SetMinimum(ymin[c]);
            else if (c == 1 or c == 2) {
               if (ymax[c] > 1e7) multi[c]->SetMinimum(1);
               else multi[c]->SetMinimum(1e-7 * ymax[c]);
            } else if (c == 3) {
               multi[c]->SetMinimum(1e-7 * ymax[c]);
            }
         }

         // Text if required
         double x_txt_delta = 0.;
         if (c == 1 or c == 3) x_txt_delta = 0.4;
         double y_txt = 0.55;
         if (c == 5) y_txt = 0.59;
         TPaveText *txt = new TPaveText(0.14 + x_txt_delta, y_txt, 0.5 + x_txt_delta, 0.7, "NDC");
         txt->SetTextSize(0.03);
         txt->SetTextFont(132);
         txt->SetFillColor(0);
         txt->SetBorderSize(0);
         txt->SetTextColor(kGray + 2);
         txt->SetFillStyle(0);
         txt->SetTextAlign(12);
         for (int ii = 0; ii < int(cvs_text.size()); ++ii) {
            if (c == 0) txt->AddText(cvs_text[ii].c_str());
            else if (c == 5) {
               if (ii != int(cvs_text.size() - 1)) txt->AddText(cvs_text[ii].c_str());
            } else txt->AddText(cvs_text_alt[ii].c_str());
         }
         txt->Draw();

         if (c == 5) {
            TLine *line_extrapol = new TLine(1e-40, 1e-40, 1e-40, 1e-40);
            line_extrapol->SetLineColor(13);
            line_extrapol->SetLineStyle(2);

            TLine *line_nonlin = new TLine(1e-40, 1e-40, 1e-40, 1e-40);
            line_nonlin->SetLineColor(13);
            line_nonlin->SetLineWidth(2);
            line_nonlin->SetLineStyle(4);

            char tmp_cvs[1000];

            sprintf(tmp_cvs, "non-linear P(k)");
            leg[c]->AddEntry(line_nonlin, tmp_cvs, "L");

            sprintf(tmp_cvs, "Use P(k) #propto k^{n_{s}-4} #times ln^{2}(k) for  high k");
            bool is_extrapol = false;
            if (kmax >  1e4) {
               TLine *line_draw = new TLine(1e4, ymin[c], 1e4, ymax[c]);
               line_draw->SetLineColor(13);
               line_draw->SetLineStyle(2);
               line_draw->Draw();
               is_extrapol = true;
            }

            if (is_extrapol) leg[c]->AddEntry(line_extrapol, tmp_cvs, "L");
         }


         c_mf[c]->Update();
         c_mf[c]->Modified();

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gPad->Write();
         }
      }

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY) {
         cout << "  Plotting mass function at " << nz_plot << " redshift values (otherwise overcrowded figure)." << endl;
         cout << "  All your demanded redshift slices are saved in the output file, if gSIM_IS_PRINT=1." << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      if (leg) delete[] leg;
      leg = NULL;
      if (multi) delete[] multi;
      multi = NULL;
      if (c_mf) delete[] c_mf;
   }
#endif

   return;
}

//______________________________________________________________________________
void d_intensitymultiplier_dlnMh(double &lnMh, double par[26], double &res)
{
   //--- Returns the intensity multiplier d<delta^2>/dlnMh * rho_m0_mean^2 (1+z)^3
   //    as defined in Huetten et al. (2017), identical
   //    with the definition in, e.g., Ando and Komatsu (2013).
   //    Note that this function is only called for annihilating DM when
   //    integrating over lnMh.

   // INPUTS:
   //  lnMh          log mass [Omega_m0 h^-1 Msol]
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl               [only for ANNIHILATION and nlevel>1]
   // OUTPUT:
   //  res           d<delta^2>/dlnMh * rho_m0_mean^2 (1+z)^3 [Msol^2 kpc^-3 Mpc^-3 h^3],
   //               dn/dlnMh* M [Msol h^3 Mpc^-3] if decay

   double Mdelta = exp(lnMh) / gCOSMO_M_to_MH;

   // Set halo parameters:
   extragal_set_par(Mdelta, par[8] /*redshift*/, par);
   //print_parhost(par);

   // calculate luminosity:
   res = mean1cl_lumn_r_m(&par[2], 1); // [Msol^2/kpc^3] for annihil, [Msol] for decay.

   // multiply with dn/d (lnMh):
   res *= dNdVhdlnMh_interpol(par[8] /*redshift*/, exp(lnMh));
   //cout <<  "z = " << par[8] <<  "\t Lum = " << mean1cl_lumn_r_m(&par[2]) << "\t dNdVhdlnMh_interpol = " <<  dNdVhdlnMh_interpol(par[8] /*redshift*/, exp(lnMh)) * pow(gCOSMO_HUBBLE,3)  << endl;

   // [res] = [Msol^2 kpc^-3 Mpc^-3 h^3] if annihil
   // [res] = [Msol Mpc^-3 h^3] if decay

   // note that we do not longer need to translate the mass m_Delta between the
   // masses used here and the masses for which the mass functions are calculated,
   // because the mass function grid is already calculated in init_extragal() for
   // the proper m_Delta used in this run.

   return;
}

//______________________________________________________________________________
double dPhidOmega(double &e_gev_min, double &e_gev_max, double par[37])
{
   //--- Returns extragalactic integrated intensity [cm^-2 s^-1 sr^-1]
   //    integrated between
   //    e_gev_min and e_gev_max, zmin and zmax, and ln Mh_min and ln Mh_max

   // INPUTS:
   //  e_gev_min     Lower energy bound for intensity integration [GeV]
   //  e_gev_max     Upper energy bound for intensity integration [GeV]
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]

   //  ----
   //  par[26]       ln Mh_min [Omega_m0 h^-1 Msol]
   //  par[27]       ln Mh_max [Omega_m0 h^-1 Msol]
   //  ----
   //  par[28]       Mass of DM candidate [GeV]
   //  par[29]       Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[30]       Card for PP final state (gENUM_FINALSTATE)
   //  par[31]       Redshift of the emitted particle
   //  par[32]       Exponent to multiply differential intensity with energy at source (UNUSED, set to zero)
   //  par[33]       systematic uncertainty on EBL

   //  par[34]       Energy at which to compute differential intensity at observer (overwritten in integration)

   //  ----
   //  par[35]       zmin
   //  par[36]       zmax
   // OUTPUT:
   //  res           Integrated intensity, [cm^-2 s^-1 sr^-1]
   double par_diff[38];
   for (int i = 0; i < 37; ++i) {
      par_diff[i] = par[i];
   }

   par_diff[7] = gSIM_EPS;
   par_diff[32] = 0.; // do not multiply differential intensity with energy factor in integration.

   double res = 0;
   simpson_log_adapt(dPhidOmegadE, e_gev_min, e_gev_max, par_diff, res, par_diff[7]); // [cm^-2 s^-1 sr^-1]
   return res;
}

//______________________________________________________________________________
void dPhidOmegadE(double &e_gev, double par[38], double &res)
{
   //--- Returns extragalactic differential intensity
   //    E^(n) dPhi/dE in [GeV^(par[39]-1) cm^-2 s^-1 sr^-1]
   //    integrated between zmin and zmax and between ln Mh_min and ln Mh_max

   // INPUTS:
   //  e_gev         Energy at which differential intensity is evaluated [GeV]
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]

   //  ----
   //  par[26]       ln Mh_min [Omega_m0 h^-1 Msol]
   //  par[27]       ln Mh_max [Omega_m0 h^-1 Msol]
   //  ----
   //  par[28]       Mass of DM candidate [GeV]
   //  par[29]       Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[30]       Card for PP final state (gENUM_FINALSTATE)
   //  par[31]       Redshift of the emitted particle
   //  par[32]       Exponent to multiply intensity with energy at source (UNUSED, set to zero).
   //  par[33]       systematic uncertainty on EBL

   //  par[34]       Energy at which to compute differential intensity at observer (overwritten in integration)

   //  ----
   //  par[35]       zmin
   //  par[36]       zmax
   //  ----
   //  par[37]       factor to multiply dPhi/dE (to get E dPhi/dE, E^2 dPhi/dE) at observer
   // OUTPUT:
   //  res           Differential intensity, [GeV^(par[33]-1) cm^-2 s^-1 sr^-1]

   par[7] = gSIM_EPS;
   par[34] = e_gev;
   double epower_flux = par[37];
   par[32] = 0.; // set now to 0, because it would multiply the intensity at source in the following

   if (gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.999999999 * par[28]) {
      res = 1.e-40;
      return;
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.499999999 * par[28]) {
      res = 1.e-40;
      return;
   }

   //cout << "E = " << e_gev << " GeV, zmin = " << par[34] << ", zmax = " << par[35] << endl;
   res = 0.;
   if (gSIM_EXTRAGAL_IS_ZLOG)
      simpson_log_adapt(dPhidOmegadEdz, par[35], par[36], par, res, par[7]); // [GeV^-1 cm^-2 s^-1 sr^-1]
   else
      simpson_lin_adapt(dPhidOmegadEdz, par[35], par[36], par, res, par[7]); // [GeV^-1 cm^-2 s^-1 sr^-1]
   res *= pow(e_gev, epower_flux); // [GeV^(n-1) cm^-2 s^-1 sr^-1]
   return;
}

//______________________________________________________________________________
void dPhidOmegadEdlnMh(double &lnMh, double par[37], double &res)
{
   //--- Returns extragalactic differential intensity in mass bins bins
   //    dPhi/dE/dlnMh in [GeV^-1 cm^-2 s^-1 sr^-1] integrated
   //    over the redshift range between zmin = par[34] and zmax = par[35].

   //    DO NOT use for double integration first over z and then over lnMh.
   //    Instead, it is much smarter (faster) to integrate first over lnMh
   //    (with dPhidOmegadEdz() over dNdVhdlnMh_integrand_logMh), and then over z
   //    (with dPhidOmegadE() over dPhidOmegadEdz()).

   // INPUTS:
   //  lnMh          log mass in units [Omega_m0 h^-1 Msol] at which dPhi/dE/dlnMh is evaluated
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]

   //  ----
   //  par[26]       ln Mh [Omega_m0 h^-1 Msol]
   //  par[27]       -- unused (ln Mh_max [Omega_m0 h^-1 Msol])
   //  ----
   //  par[28]       Mass of DM candidate [GeV]
   //  par[29]       Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[30]       Card for PP final state (gENUM_FINALSTATE)
   //  par[31]       Redshift of the emitted particle
   //  par[32]       Exponent to multiply intensity with energy at source (UNUSED, should always be set to zero)
   //  par[33]       systematic uncertainty on EBL

   //  par[34]       Energy at which to compute differential intensity at observer (overwritten in integration)

   //  ----
   //  par[35]       zmin
   //  par[36]       zmax
   // OUTPUT:
   //  res           Differential intensity, dPhi/dE/dlnMh in [GeV^-1 cm^-2 s^-1 sr^-1]

   par[7] = gSIM_EPS;
   par[26] = lnMh;

   res = 0.;
   if (gSIM_EXTRAGAL_IS_ZLOG)
      simpson_log_adapt(dPhidOmegadEdlnMhdz, par[35], par[36], par, res, par[7]); // [GeV^-1 cm^-2 s^-1 sr^-1]
   else
      simpson_lin_adapt(dPhidOmegadEdlnMhdz, par[35], par[36], par, res, par[7]); // [GeV^-1 cm^-2 s^-1 sr^-1]
   return;

}

//______________________________________________________________________________
void dPhidOmegadEdz(double &z, double par[35], double &res)
{
   //--- Returns extragalactic differential intensity in redshift bins
   //    dPhi/dE/dz in [GeV^-1 cm^-2 s^-1 sr^-1] integrated
   //    over the mass interval between log Mhmin = par[26] and log Mhmax = par[27]

   // INPUTS:
   //  z             redshift at which dPhi/dE/dz is evaluated
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]

   //  ----
   //  par[26]       ln Mh_min [Omega_m0 h^-1 Msol]
   //  par[27]       ln Mh_max [Omega_m0 h^-1 Msol]
   //  ----
   //  par[28]       Mass of DM candidate [GeV]
   //  par[29]       Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[30]       Card for PP final state (gENUM_FINALSTATE)
   //  par[31]       Redshift of the emitted particle
   //  par[32]       Exponent to multiply intensity with energy at source (UNUSED, should always be set to zero)
   //  par[33]       systematic uncertainty on EBL

   //  par[34]       Energy at which to compute differential intensity at observer (overwritten in integration)

   // OUTPUT:
   //  res           Differential intensity, dPhi/dE/dz in [GeV^-1 cm^-2 s^-1 sr^-1]

   par[7] = gSIM_EPS;
   par[31] = z;

   double e_gev = par[34];
   if (e_gev * (1. + z) > (1. - SMALL_NUMBER) * par[28]) {
      res = 0.;
      return;
   }

   res = 0.;

   double par_spec[6];
   par_spec[0] = par[28];
   par_spec[1] = par[29];
   par_spec[2] = par[30];
   par_spec[3] = par[31];
   par_spec[4] = 0.; // no multiplication with power of energy at this point.
   par_spec[5] = par[33];

   double *params = NULL;

   // return simple integral for decay:
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY) {
      res =  H0_over_H(z, params) * dPPdE(e_gev, par_spec) / GEVperCM2_to_MSOLperKPC2 * RHO_CRITperh2_MSOLperKPC3 * gCOSMO_OMEGA0_CDM; // [sr^-1 s^-1 cm^-2 GeV^-1 kpc^-1 * h^2]
      res *= HUBBLE_LENGTHxh_Mpc / KPC_to_MPC;  // [sr^-1 s^-1 cm^-2 GeV^-1 * h]
      res *= pow(gCOSMO_HUBBLE, 1);             // [sr^-1 s^-1 cm^-2 GeV^-1]
      return;
   }

   // Or return <delta^2> * rho_m0_mean^2 (1+z)^3 in case of annihilation:

   // integrate between ln(Mmin) and ln(Mmax)
   // we check whether the  same intensity multiplier is repeatedly calculated
   // -> in this case, we reuse a single calculation only done once.
   // If input parameters differ, we perform always the full integral.
   static bool is_constant_intensitymultiplier = true;
   if (vec_paramcheck.size() == 0) {
      vec_paramcheck.push_back(par[26]); /* ln Mh_min */
      vec_paramcheck.push_back(par[27]); /* ln Mh_max */
   } else {
      //check
      for (int j = 0; j < (int)vec_paramcheck.size(); j++) {
         if (vec_paramcheck[j] != par[26 + j]) is_constant_intensitymultiplier = false;
      }
   }
   // calculate intensity multiplier only once:
   if (is_constant_intensitymultiplier && (vec_intensitymultiplier.size() == 0 || vec_z.size() == 0)) {
      double intensitymultiplier_z;
      int nz = int(20 / gSIM_EPS);
      vec_z =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, nz, false);

      cout << " ... computing intensity multiplier (may take a while) ..." << endl;

      for (int i = 0; i < (int)vec_z.size(); i++) {
         par[8] = vec_z[i];
         simpson_lin_adapt(d_intensitymultiplier_dlnMh, par[26] /* ln Mh_min */,
                           par[27] /* ln Mh_max */, par,
                           intensitymultiplier_z, par[7] /* eps*/);            // [Msol^2 kpc^-3 * Mpc^-3 h^3] if annihil, [Msol Mpc^-3] h^3 if decay
         // multiply with various constants:
         intensitymultiplier_z *= KPC3_to_MPC3;                      // [Msol^2 * kpc^-6 h^3] if annihil, [Msol kpc^-3 h^3] if decay
         intensitymultiplier_z *= HUBBLE_LENGTHxh_Mpc / KPC_to_MPC;  // [Msol^2 * kpc^-5 h^2] if annihil, [Msol kpc^-2 h^2] if decay
         intensitymultiplier_z *= pow(gCOSMO_HUBBLE, 2);             // [Msol^2 * kpc^-5] if annihil, [Msol kpc^-2] if decay
         vec_intensitymultiplier.push_back(intensitymultiplier_z);
      }
   }

   if (is_constant_intensitymultiplier) res = interp1D(z, vec_z, vec_intensitymultiplier, kLINLOG);
   else {
      par[8] = z;
      simpson_lin_adapt(d_intensitymultiplier_dlnMh, par[26] /* ln Mh_min */,
                        par[27] /* ln Mh_max */, par,
                        res, par[7] /* eps*/);            // [Msol^2 kpc^-3 * Mpc^-3 h^3] if annihil, [Msol Mpc^-3] h^3 if decay
      // multiply with various constants:
      res *= KPC3_to_MPC3;                      // [Msol^2 * kpc^-6 h^3] if annihil, [Msol kpc^-3 h^3] if decay
      res *= HUBBLE_LENGTHxh_Mpc / KPC_to_MPC;  // [Msol^2 * kpc^-5 h^2] if annihil, [Msol kpc^-2 h^2] if decay
      res *= pow(gCOSMO_HUBBLE, 2);             // [Msol^2 * kpc^-5] if annihil, [Msol kpc^-2] if decay
   }
   //cout << "E = " << e_gev << "\t z = " << z << "\t <delta^2> * rho_m0_mean^2 = " <<  res   << endl; abort();

   res *=  H0_over_H(z, params) * dPPdE(e_gev, par_spec) / GEV2perCM5_to_MSOL2perKPC5 * pow(gCOSMO_OMEGA0_CDM / gCOSMO_OMEGA0_M, 2); // [GeV^-1 cm^-2 s^-1 sr^-1]

   return;
}

//______________________________________________________________________________
void dPhidOmegadEdlnMhdz(double &z, double par[35], double &res)
{
   //--- Returns extragalactic differential intensity
   //    in per redshift and per mass decade dPhi/dE/dz/dlnMh
   //    in [GeV^-1 cm^-2 s^-1 sr^-1], written to be integrated over z first.

   //    DO NOT use for double integration first over z and then over lnMh.
   //    Instead, it is much faster to integrate first over lnMh
   //    (with dPhidOmegadEdz() over dNdVhdlnMh_integrand_logMh), and then over z
   //    (with dPhidOmegadE() over dPhidOmegadEdz()).

   // INPUTS:
   //  z             redshift at which dPhi/dE/dz/dlnMh is evaluated
   //  --- Extragalactic halo properties (set by extragal_set_partot(),
   //      matches order needed for mean1cl_lumn_r_m() function)
   //  par[0]        density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]        r_scale of halo with mass m (written during integration)
   //  par[2]        rho_halo(r) shape parameter #1 (inner profile for all haloes)
   //  par[3]        rho_halo(r) shape parameter #2 (inner profile for all haloes)
   //  par[4]        rho_halo(r) shape parameter #3 (inner profile for all haloes)
   //  par[5]        profile flag for all haloes
   //  par[6]        r_vir of halo with mass m (written during integration)
   //  ---
   //  par[7]        eps: relative precision sought L calculation
   //  par[8]        redshift of halo
   //  par[9]        mass mdelta of halo [Msol]
   //  par[10]       -- unused
   //  par[11]       -- unused
   //  par[12]       dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]       dPdc mean concentration <c>
   //  par[14]       dPdc standard deviation              [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]       dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  --- Halo substructure parameters
   //  par[16]       f: mass fraction of substructures    [only for ANNIHILATION and nlevel>1]
   //  par[17]       nlevel: sub-sub...halos (1=no sub)   [only for ANNIHILATION and nlevel>1]
   //  par[18]       dPdV normalisation  [kpc^{-3}]       [only for ANNIHILATION and nlevel>1]
   //  par[19]       dPdV scale radius [kpc]              [only for ANNIHILATION and nlevel>1]
   //  par[20]       dPdV shape parameter #1              [only for ANNIHILATION and nlevel>1]
   //  par[21]       dPdV shape parameter #2              [only for ANNIHILATION and nlevel>1]
   //  par[22]       dPdV shape parameter #3              [only for ANNIHILATION and nlevel>1]
   //  par[23]       dPdV card_profile [gENUM_PROFILE]    [only for ANNIHILATION and nlevel>1]
   //  par[24]       dPdV radius [kpc]                    [only for ANNIHILATION and nlevel>1]
   //  par[25]       ratio rs dPdV to rs_cl                [only for ANNIHILATION and nlevel>1]

   //  ----
   //  par[26]       ln Mh [Omega_m0 h^-1 Msol]
   //  par[27]       -- unused (ln Mh_max [Omega_m0 h^-1 Msol])
   //  ----
   //  par[28]       Mass of DM candidate [GeV]
   //  par[29]       Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[30]       Card for PP final state (gENUM_FINALSTATE)
   //  par[31]       Redshift of the emitted particle
   //  par[32]       Exponent to multiply intensity with energy at source (UNUSED, should always be set to zero)
   //  par[33]       systematic uncertainty on EBL

   //  par[34]       Energy at which to compute differential intensity at observer (overwritten in integration)

   // OUTPUT:
   //  res           Differential intensity, dPhi/dE/dz/dlnM in [GeV^-1 cm^-2 s^-1 sr^-1]

   par[7] = gSIM_EPS;
   par[8] = z; // Redshift of the emitted particle
   par[31] = z;
   double lnMh = par[26];

   double e_gev = par[34];
   if (e_gev * (1. + z) > 0.999999999 * par[28]) {
      res = 0.;
      //return;
   }

   res = 0.;

   double par_spec[6];
   par_spec[0] = par[28];
   par_spec[1] = par[29];
   par_spec[2] = par[30];
   par_spec[3] = par[31];
   par_spec[4] = 0.; // no multiplication with power of energy.
   par_spec[5] = par[33];

   double *params = NULL;

   // return error in case of decay:
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY) {
      printf("\n====> ERROR: dPhidOmegadEdlnMhdz() in extragal.cc");
      printf("\n             This function is only designed to analyze annihilating DM.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // return d<delta^2>/dlnMh * rho_m0_mean^2 * (1+z)^3 in case of annihilation:
   d_intensitymultiplier_dlnMh(lnMh, par, res);   // [Msol^2 kpc^-3 * Mpc^-3 h^3] if annihil, [Msol Mpc^-3] h^3 if decay
   // multiply with various constants:
   res *= KPC3_to_MPC3;                      // [Msol^2 * kpc^-6 h^3] if annihil, [Msol kpc^-3 h^3] if decay
   res *= HUBBLE_LENGTHxh_Mpc / KPC_to_MPC;  // [Msol^2 * kpc^-5 h^2] if annihil, [Msol kpc^-2 h^2] if decay
   res *= pow(gCOSMO_HUBBLE, 2);             // [Msol^2 * kpc^-5] if annihil, [Msol kpc^-2] if decay
//   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
   res *=  H0_over_H(z, params) * dPPdE(e_gev, par_spec) / GEV2perCM5_to_MSOL2perKPC5 * pow(gCOSMO_OMEGA0_CDM / gCOSMO_OMEGA0_M, 2); // [GeV^-1 cm^-2 s^-1 sr^-1]
//   else
//      res *=  H0_over_H(z, params) * dPPdE(e_gev, par_spec) / GEVperCM2_to_MSOLperKPC2 * gCOSMO_OMEGA0_CDM / gCOSMO_OMEGA0_M;   // [GeV^-1 cm^-2 s^-1 sr^-1]
   return;
}

//______________________________________________________________________________
void extragal_set_par(double const &Mdelta, double const &z, double par[26])
{
   //--- Fills parameters for mass distribution, number and profiles for Extragalactic subhalos.

   //   nlevel = 0 -> no substructures = no subhalos of galaxies
   //   nlevel = 1 -> one level (sub-substructures = substructures of galaxies)

   // OUTPUTS:
   //  par[0]   density normalization [Msol/kpc^3], rho(r_scale)
   //  par[1]   r_scale of halo with mass Mdelta (written during integration)
   //  par[2]   rho_haloes shape parameter #1
   //  par[3]   rho_haloes shape parameter #2
   //  par[4]   rho_haloes shape parameter #3
   //  par[5]   rho_haloes card_profile [gENUM_PROFILE]
   //  par[6]   r_delta of halo with mass Mdelta
   //  par[7]   eps: relative precision sought L calculation
   //  par[8]   redshift of halo
   //  par[9]   mass mdelta of halo [Msol]
   //  par[10]  dPdM normalisation [1/Msol]            [only for ANNIHILATION and nlevel>1]
   //  par[11]  dPdM slope alphaM                      [only for ANNIHILATION and nlevel>1]
   //  par[12]  dPdc gENUM_CDELTAMDELTA (cdelta-mdelta)
   //  par[13]  dPdc mean concentration <c>
   //  par[14]  dPdc standard deviation                [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[15]  dPdc card_profile [gENUM_CDELTA_DIST]  [UNUSED if gENUM_CDELTA_DIST=kDIRAC]
   //  par[16]  f: mass fraction of substructures (mass in galaxies) [UNUSED for DECAY]
   //  par[17]  nlevel: sub-sub...halos (1=no subhalos of galaxies)  [UNUSED for DECAY]
   //  par[18]  dPdV normalisation  [kpc^{-3}]         [UNUSED for DECAY]
   //  par[19]  dPdV scale radius [kpc]                [UNUSED for DECAY]
   //  par[20]  dPdV shape parameter #1                [UNUSED for DECAY]
   //  par[21]  dPdV shape parameter #2                [UNUSED for DECAY]
   //  par[22]  dPdV shape parameter #3                [UNUSED for DECAY]
   //  par[23]  dPdV card_profile [gENUM_PROFILE]      [UNUSED for DECAY]
   //  par[24]  dPdV radius [kpc]                      [UNUSED for DECAY]
   //  par[25]  ratio rs dPdV to rs_cl                 [only for ANNIHILATION and nlevel>1]

   if (gDM_SUBS_NUMBEROFLEVELS > 0 && (
            (gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] == kDPDV_GAO04
             && (gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][2] - gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1] > 1.))
            || (gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL] == kZHAO && gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1] < 2.))) {

      printf("\n====> ERROR: extragal_set_parsubs() in extragal.cc");
      printf("\n             Selected parameters for dpdv profile (%s) do not allow sub-substructures (outer slope < 2)", gNAMES_PROFILE[gMW_SUBS_DPDV_FLAG_PROFILE]);
      printf("\n             => abort()\n\n");
      abort();
   }

   par[0] = 1.; // rho(r_scale) will be updated later.
   par[1] = 1.; // r_scale will be updated later.
   par[2] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0]; // same structures parameters for all levels of extragalacitc halos.
   par[3] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1];
   par[4] = gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2];
   par[5] = gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL];
   par[6] = 1.; // Rdelta will be updated later.
   par[7] = gSIM_EPS;
   par[8] = z;
   par[9] = Mdelta;
   par[10] = 0.; // dPdM normalisation will be updated later
   par[11] = gHALO_SUBS_DPDM_SLOPE[kEXTRAGAL];

   if (gEXTRAGAL_FLAG_CDELTAMDELTA == kPIERI11_AQUARIUS || gEXTRAGAL_FLAG_CDELTAMDELTA == kPIERI11_VIALACTEA || gEXTRAGAL_FLAG_CDELTAMDELTA == kB01_VIR_RAD || gEXTRAGAL_FLAG_CDELTAMDELTA == kMOLINE17_200) {
      char tmp[1024];
      sprintf(tmp, " Distance-dependent cdelta-mdelta profile %s not applicable for extragalactic field halos.", gNAMES_CDELTAMDELTA[gEXTRAGAL_FLAG_CDELTAMDELTA]);
      print_error("clumps.cc", "mdelta_to_cdelta()", string(tmp));
   }

   par[12] = gEXTRAGAL_FLAG_CDELTAMDELTA;
   par[13] = 0.; // concentration will be calculated later.
   par[14] = gDM_LOGCDELTA_STDDEV;
   par[15] = gDM_FLAG_CDELTA_DIST;
   if (gDM_SUBS_NUMBEROFLEVELS == 0) {
      par[16] = 0.; //  f: mass fraction of substructures = 0
      par[17] = 1;  // put one level to avoid code to crash (no subs controlled by f=0)
   } else {
      par[16] = gHALO_SUBS_MASSFRACTION[kEXTRAGAL]; //  f: mass fraction of substructures
      par[17] = gDM_SUBS_NUMBEROFLEVELS + 1; // Galaxies are now the subhalos of the clusters.;
   }
   par[18] = 1.; // dPdV normalisation will be updated later
   par[19] = 1.; // dPdV scale radius will be updated later.
   par[20] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][0];
   par[21] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][1];
   par[22] = gHALO_SUBS_DPDV_SHAPE_PARAMS[kEXTRAGAL][2];
   par[23] = gHALO_SUBS_DPDV_FLAG_PROFILE[kEXTRAGAL];
   par[24] = 1.; // Rdelta will be updated later.
   par[25] = gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST[kEXTRAGAL];

   // Update inner profile (has only effects for mass-dependent profiles, like ISHIYAMA14)
   double r = -1, Rvir = 0.;
   double Delta_c = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, par[8] /*redshift*/);
   mdelta_to_innerslope(Mdelta, Delta_c, par[12], &par[2], z);

   return;
}

//______________________________________________________________________________
void init_extragal(const vector<double> &z_vec, const vector<double> &M_vec,
                   const int card_mf, const int card_window)
{
   //--- Fills in global variable arrays for cosmological quantities (declared in params.h)
   //    required for all extragalactic analysis:
   //    - gCOSMO_D_TRANS_GRID, gCOSMO_D_LUM_GRID, gCOSMO_D_ANG_GRID, gCOSMO_DNDVHDLNMH_Z in cosmo.h
   //    The idea is that the calculation of these quantities requires integrations/ complex calcuations, and
   //    these calculations are only performed once, filling some global variables with the tabulated values. The corresponding quantities
   //    throughout the code are then calculated by interpolating the tabulated values.

   // INPUTS:
   //  z_vec          range of z values for the computation: Will only be used to determine the z range
   //                 for which the grid will be computed, and whether the base grid will be computed on
   //                 lin or log scale. The fineness of the interpolation grid is given by gSIM_EXTRAGAL_DELTAZ_PRECOMP
   //  M_vec          range of M values for the computation: Will only be used to check whether the M range
   //                 does not exceed the range determined by gSIM_EXTRAGAL_MMIN_PRECOMP and gSIM_EXTRAGAL_MMAX_PRECOMP,
   //                 which are hard-coded corresponding to the P(k) calculation, and whether the M grid is calculated
   //                 on lin or log scale. The fineness of the interpolation grid is given by gSIM_EXTRAGAL_NM_PRECOMP.
   //  card_mf        selects the mass function to use (e.g. Tinker, Jenkins, etc.)
   // OUTPUTS:
   //  gCOSMO_Z_GRID        grid of redshift values
   //  gCOSMO_MH_GRID       grid of mass values [Omega_m0 h^-1 Msol]
   //  gCOSMO_D_TRANS_GRID  grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_D_LUM_GRID    grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_D_ANG_GRID    grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_DNDVHDLNMH_Z     2d grid of the mass function over tabulated masses and redshifts.

   gCOSMO_Z_GRID.clear();
   gCOSMO_MH_GRID.clear();
   gCOSMO_D_TRANS_GRID.clear();
   gCOSMO_D_LUM_GRID.clear();
   gCOSMO_D_ANG_GRID.clear();
   gCOSMO_DNDVHDLNMH_Z.clear();

   bool is_log = false;
   if (z_vec.size() > 2 and fabs(z_vec[2] + z_vec[0] - 2. * z_vec[1]) > SMALL_NUMBER)
      is_log = true;

   // fill pretabulated z-grid
   init_gCOSMO_Z_GRID(z_vec[0], z_vec[z_vec.size() - 1], is_log);

   // always load P(k,0) because it is needed for sigma8 normalisation:
   vector<double> linear_lnk_vec_0;
   vector<double> linear_lnp_vec_0;
   vector< vector<double> >  linear_lnk_vecvec(gCOSMO_Z_GRID.size());
   vector< vector<double> >  linear_lnp_vecvec(gCOSMO_Z_GRID.size());
   double sigma8_input;

   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
      get_pk(0, linear_lnk_vec_0, linear_lnp_vec_0);

      double kmax_precomp = 40;
      double kmin_precomp = -40;
      if (gSIM_EXTRAGAL_FLAG_GROWTHFACTOR == kPKZ_FROMFILE) {
         // load P(k,z) and check for its size:
         for (int i = 0; i < int(gCOSMO_Z_GRID.size()); i++) {
            get_pk(gCOSMO_Z_GRID[i], linear_lnk_vecvec[i], linear_lnp_vecvec[i]);
            double tmp = find_max_value(linear_lnk_vecvec[i]);
            if (tmp < kmax_precomp) kmax_precomp = tmp;
            tmp = find_min_value(linear_lnk_vecvec[i]);
            if (tmp > kmin_precomp) kmin_precomp = tmp;
         }
      } else {
         // fill only first element of k, P(k) matrix with P(k,0)
         // and determine kmin_precomp, kmax_precomp from P(k,0):
         linear_lnk_vecvec[0] = linear_lnk_vec_0;
         linear_lnp_vecvec[0] = linear_lnp_vec_0;
         vector<double> dummy(linear_lnk_vec_0.size(), 1e-40);
         for (int i = 1; i < int(gCOSMO_Z_GRID.size()); i++) {
            linear_lnk_vecvec[i] = dummy;
            linear_lnp_vecvec[i] = dummy;
         }
         kmax_precomp = find_max_value(linear_lnk_vec_0);
         kmin_precomp = find_min_value(linear_lnk_vec_0);
      }
      kmax_precomp = exp(kmax_precomp);
      kmin_precomp = exp(kmin_precomp);

      double Mmin_pk =  gamma_f(card_window) * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3 / gCOSMO_M_to_MH *
                        pow(PI / kmax_precomp, 3); // in units of Msol
      double Mmax_pk =  gamma_f(card_window) * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3 / gCOSMO_M_to_MH *
                        pow(PI / kmin_precomp, 3); // in units of Msol

      Mmin_pk *= (5e3 * 8);  // because we integrate over P(k) from k_min = 1e-3 k in lnsigma2() in cosmo.cc
      Mmax_pk /= (5e11 / 8); //
      double Mmin = M_vec[0];
      double Mmax = M_vec[M_vec.size() - 1];

      if (Mmin < Mmin_pk) {
         int kmax_required = int(2 * PI * pow(gamma_f(card_window) * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3 / gCOSMO_M_to_MH
                                              / Mmin * 5e3, 1. / 3.)) + 1;
         printf("\n====> ERROR: init_extragal() in extragal.cc");
         printf("\n             Demanded mass range exceeds available k_max = %g", kmax_precomp);
         printf("\n             <=> Mmin = %g Msol.", Mmin_pk);
         printf("\n             => abort()\n");
         printf("\n             You may want to recompute the power spectrum P(k) up to k_max = %ld", long(kmax_required));
         printf("\n             e.g. triggered by rerunning this sim, setting the hidden variable");
         printf("\n             gSIM_EXTRAGAL_KMAX_PRECOMP = %d (will need CLASS to do this)\n\n", kmax_required);
         abort();
      }

      if (Mmax >  Mmax_pk) {
         printf("\n====> ERROR: init_extragal() in extragal.cc");
         printf("\n             Demanded mass range exceeds available k_min = %g", kmin_precomp);
         printf("\n             <=> Mmax =  %g Msol", Mmax_pk);
         printf("\n             => abort()\n\n");
         abort();
      }


      //if (M_vec.size() > 2 and fabs(M_vec[2] + M_vec[0] - 2. * M_vec[1]) < SMALL_NUMBER)
      //   gSIM_EXTRAGAL_M_PRECOMP_ISLOG = false;
      double Mmin_precomp = max(Mmin_pk, 0.01 * Mmin);
      double Mmax_precomp = min(Mmax_pk, HALO_MMAX);
      init_gCOSMO_MH_GRID(Mmin_precomp, Mmax_precomp);
      //cout << gCOSMO_MH_GRID[gCOSMO_MH_GRID.size() -1] << endl; abort();

      cout << " => Computing P(k) normalisation for z = 0 and given cosmology ...";
      // calculate sigma_8 according to input Power spectrum:
      sigma8_input = sigma8(linear_lnk_vec_0, linear_lnp_vec_0, card_window);
      cout << "done.\n" << endl;

   }

   for (int i = 0; i < int(gCOSMO_Z_GRID.size()); i++) {
      gCOSMO_D_TRANS_GRID.push_back(dh_trans(gCOSMO_Z_GRID[i]));
      gCOSMO_D_LUM_GRID.push_back(dh_l(gCOSMO_Z_GRID[i]));
      gCOSMO_D_ANG_GRID.push_back(dh_a(gCOSMO_Z_GRID[i]));

      if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
         // get matter power spectrum linear_lnplnkz for each z:
         // reads either precalculated results for p(k,z) or calls CLASS to calculate them.
         // rescale P(k):
         if (i == 0 or gSIM_EXTRAGAL_FLAG_GROWTHFACTOR == kPKZ_FROMFILE) {
            for (int j = 0; j < int(linear_lnp_vecvec[i].size()); ++j) {
               linear_lnp_vecvec[i][j] = linear_lnp_vecvec[i][j] / sigma8_input * gCOSMO_SIGMA8;
            }
         }
         // Now calculate the mass function!
         vector<double> res;
         if (gSIM_EXTRAGAL_FLAG_GROWTHFACTOR == kPKZ_FROMFILE)
            res = compute_massfunction(gCOSMO_Z_GRID[i], gCOSMO_MH_GRID, card_mf, linear_lnk_vecvec[i], linear_lnp_vecvec[i], card_window, gSIM_EXTRAGAL_FLAG_GROWTHFACTOR);
         else
            res = compute_massfunction(gCOSMO_Z_GRID[i], gCOSMO_MH_GRID, card_mf, linear_lnk_vecvec[0], linear_lnp_vecvec[0], card_window, gSIM_EXTRAGAL_FLAG_GROWTHFACTOR);
         gCOSMO_DNDVHDLNMH_Z.push_back(res);
      }
   }
   return;
}

//______________________________________________________________________________
void init_extragal_manipulatedHMF(const vector<double> &z_vec, const vector<double> &M_vec,
                                  const int card_mf, const int card_window)
{
   //--- Fills in global variable arrays for cosmological quantities (declared in params.h)
   //    like in init_extragal(), however, manipulates the halo mass function:
   //      - cuts the mass function such that rho_halos = gDM_RHOHALOES_TO_RHOMEAN * rho_mean is preserved.
   //      - Constrains the mass function slope to be steeper than gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE.

   // INPUT:
   //  z_vec          range of z values for the computation: Will only be used to determine the z range
   //                 for which the grid will be computed, and whether the base grid will be computed on
   //                 lin or log scale. The fineness of the interpolation grid is given by gSIM_EXTRAGAL_DELTAZ_PRECOMP
   //  M_vec          range of M values for the computation: Will only be used to check whether the M range
   //                 does not exceed the range determined by gSIM_EXTRAGAL_MMIN_PRECOMP and gSIM_EXTRAGAL_MMAX_PRECOMP,
   //                 which are hard-coded corresponding to the P(k) calculation, and whether the M grid is calculated
   //                 on lin or log scale. The fineness of the interpolation grid is given by gSIM_EXTRAGAL_NM_PRECOMP.
   //  card_mf        selects the mass function to use (e.g. Tinker, Jenkins, etc.)
   // OUTPUTS:
   //  gCOSMO_Z_GRID        grid of redshift values
   //  gCOSMO_MH_GRID       grid of mass values [Omega_m0 h^-1 Msol]
   //  gCOSMO_D_TRANS_GRID  grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_D_LUM_GRID    grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_D_ANG_GRID    grid with tabulated comoving transverse distance values for cosmology defined in global variables
   //  gCOSMO_DNDVHDLNMH_Z     2d grid of the mass function over tabulated masses and redshifts, truncated at low masses.

   init_extragal(z_vec, M_vec, card_mf, card_window);

   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {

      if (fabs(gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE + 1.) > SMALL_NUMBER) {
         // constrain slope of halo mass function:

         if (gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE < 1) {
            printf("\n====> ERROR: init_extragal_manipulatedHMF() in extragal.cc");
            printf("\n             gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE > 1 required ");
            printf("\n             (Will calculate dn/dM ~ M^(-gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE)");
            printf("\n             and require slope steeper than dn/dM ~ M^(-1) .)");
            printf("\n             => abort()\n\n");
            abort();
         }

         for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {

            double mhlim = gEXTRAGAL_HMF_SMALLSCALE_DPDM_MLIM * gCOSMO_M_to_MH;

            vector<double> dNdVhdlnMh_slopelim = gCOSMO_DNDVHDLNMH_Z[i];

            int nm = dNdVhdlnMh_slopelim.size();
            vector<double> dNdVhdlnMh_slopelim_test(nm);

            // now do the trick by multiplying th dn/dlnM with Mh^(slope-1):
            for (int j = 0; j < nm; j++) {
               dNdVhdlnMh_slopelim_test[j] = dNdVhdlnMh_slopelim[j]  * pow(gCOSMO_MH_GRID[j], gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE - 1);
            }

            // find the grid point where the slope becomes shallower:
            int jfixmax = 0.;
            int jfixmax2ndDeriv = 0.;
            double diff_min = 1e40;
            for (int j = 1; j < nm - 3; j++) {
               double diff_tmp = log(dNdVhdlnMh_slopelim_test[nm - j - 1]) - log(dNdVhdlnMh_slopelim_test[nm - j]);
               //cout <<gCOSMO_MH_GRID[nm - j] <<" " << diff_tmp << endl;
               if (gCOSMO_MH_GRID[nm - j] < mhlim and gCOSMO_MH_GRID[nm - j] > 1e-24 * mhlim and diff_tmp < diff_min) {
                  jfixmax2ndDeriv = nm - j - 1;
                  diff_min = diff_tmp;
                  //cout << gCOSMO_Z_GRID[i]<< " "<<jfixmax2ndDeriv << " "<<gCOSMO_MH_GRID[jfixmax2ndDeriv] <<" "<< diff_tmp <<" "<< diff_min << endl;
               }

               if (gCOSMO_MH_GRID[nm - j] < mhlim and diff_tmp < 0) {
                  jfixmax = nm - j - 1;
                  break;
               }
            }
            // if no transition was found, search for position where slope comest at least closest:
            if (diff_min > 0) jfixmax = jfixmax2ndDeriv;

            // fix the value for all lower masses:
            for (int j = 0; j < jfixmax; j++) {
               dNdVhdlnMh_slopelim_test[j] = dNdVhdlnMh_slopelim_test[jfixmax + 1];
            }

            // finally, overwrite gCOSMO_DNDVHDLNMH_Z:
            for (int j = 0; j < (int)gCOSMO_MH_GRID.size(); j++) {
               gCOSMO_DNDVHDLNMH_Z[i][j] = dNdVhdlnMh_slopelim_test[j] / pow(gCOSMO_MH_GRID[j], gEXTRAGAL_HMF_SMALLSCALE_DPDM_SLOPE - 1);
            }
         }
      }

      cout << "\n Checking the total mass contained in halos:" << endl;
      // cut of mass function of demanded fraction of universal matter density is exceeded:
      vector< vector<double> > dNdVhdlnMh_cutoff(gCOSMO_Z_GRID.size());
      for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {

         dNdVhdlnMh_cutoff[i] = gCOSMO_DNDVHDLNMH_Z[i];

         double rhoh_halos = 0.;
         double par[3];
         par[0] = gCOSMO_Z_GRID[i];
         par[1] = 0.; // will be updated later.
         double mh_low = gCOSMO_MH_GRID[0];
         double mh_up = gCOSMO_MH_GRID[gCOSMO_MH_GRID.size() - 1];
         simpson_log_adapt(dNdVhdlnMh_integrand_Mh, mh_low, mh_up, par, rhoh_halos, gSIM_EPS);
         printf("  z = %g:\trho_halos = %.3f rho_mean\n", par[0], rhoh_halos  * KPC3_to_MPC3 /  RHO_CRITperh2_MSOLperKPC3);

         // find cut-off:
         if (rhoh_halos > gDM_RHOHALOES_TO_RHOMEAN * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3) {
            par[2] = gSIM_EXTRAGAL_MF_SIGMA_CUTOFF;
            mh_low = HALO_MMIN;
            mh_up = 2 * HALO_MMAX;
            gsl_function F;
            F.function = &solve_Mhmin;
            Mmin_params_for_rootfinding params = {gCOSMO_Z_GRID[i], par[2]};
            F.params = &params;
            int return_status = 0;
            double Mhmin = rootsolver_gsl(gsl_root_fsolver_brent, F, mh_low, mh_up, gSIM_EPS, return_status);
            printf("\t\t==> rho_halos != rho_mean: Mmin = %.2e Msol\n", Mhmin / gCOSMO_M_to_MH);
            par[1] = Mhmin;
            for (int j = 0; j < (int)gCOSMO_MH_GRID.size(); j++) {
               double res_tmp = 0.;
               dNdVhdlnMh_sigmoid(gCOSMO_MH_GRID[j], par, res_tmp);
               dNdVhdlnMh_cutoff[i][j] = res_tmp;
            }
         }
//      // check
//      double rhoh_halos_check = 0.;
//      par[1] = Mhmin;
//      simpson_log_adapt(dNdVhdlnMh_sigmoid, gCOSMO_MH_GRID[0], gCOSMO_MH_GRID[gCOSMO_MH_GRID.size() - 1], par, rhoh_halos_check, gSIM_EPS);
//      cout << "z = " << par[0] <<", rho_halos_check = " << rhoh_halos_check  * KPC3_to_MPC3 /  RHO_CRITperh2_MSOLperKPC3  << " rho_m" << endl;
      }
      // now overwrite gCOSMO_DNDVHDLNMH_Z:
      for (int i = 0; i < (int)gCOSMO_Z_GRID.size(); i++) {
         for (int j = 0; j < (int)gCOSMO_MH_GRID.size(); j++) {
            gCOSMO_DNDVHDLNMH_Z[i][j] = dNdVhdlnMh_cutoff[i][j];
         }
      }
   }
   return;
}

//______________________________________________________________________________
void init_gCOSMO_MH_GRID(double &Mmin, const double &Mmax)
{
   gCOSMO_MH_GRID =  make_1D_grid(Mmin, Mmax, gSIM_EXTRAGAL_NM_PRECOMP, gSIM_EXTRAGAL_M_PRECOMP_ISLOG);
   for (int i = 0; i < int(gCOSMO_MH_GRID.size()); i++) {
      gCOSMO_MH_GRID[i] *= gCOSMO_M_to_MH;
   }
}

//______________________________________________________________________________
void init_gCOSMO_Z_GRID(const double &zmin, const double &zmax, bool is_log)
{
   // fill pretabulated z-grid
   if (zmin < SMALL_NUMBER) is_log = false;

   if (gSIM_EXTRAGAL_DELTAZ_PRECOMP < SMALL_NUMBER) {
      printf("\n====> ERROR: init_gCOSMO_Z_GRID() in extragal.cc");
      printf("\n             gSIM_EXTRAGAL_DELTAZ_PRECOMP > 0 required.\n");
      printf("\n             => abort()\n\n");
      abort();
   }

   double z0;
   if (!is_log) {
      z0 = 0.;
      while (z0 <= zmin + SMALL_NUMBER) z0 += gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      z0 -= gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      while (z0 < zmax - SMALL_NUMBER) {
         gCOSMO_Z_GRID.push_back(z0);
         z0 += gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      }
   } else {
      z0 = 1.;
      if (z0 - gSIM_EXTRAGAL_DELTAZ_PRECOMP > 0) gSIM_EXTRAGAL_DELTAZ_PRECOMP = z0 / (z0 - gSIM_EXTRAGAL_DELTAZ_PRECOMP);
      else {
         printf("\n====> ERROR: init_gCOSMO_Z_GRID() in extragal.cc");
         printf("\n             gSIM_EXTRAGAL_DELTAZ_PRECOMP < 1 required for");
         printf("\n             log-spaced grid.\n");
         printf("\n             => abort()\n\n");
         abort();
      }
      if (zmin < z0) {
         while (z0 >= zmin - SMALL_NUMBER)
            z0 /= gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      } else {
         while (z0 <= zmin + SMALL_NUMBER) z0 *= gSIM_EXTRAGAL_DELTAZ_PRECOMP;
         z0 /= gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      }
      while (z0 < zmax) {
         gCOSMO_Z_GRID.push_back(z0);
         z0 *= gSIM_EXTRAGAL_DELTAZ_PRECOMP;
      }
   }
   gCOSMO_Z_GRID.push_back(z0 + SMALL_NUMBER);
}

