/*! \file spectra.cc \brief (see spectra.h) */

// CLUMPY includes
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/params.h"
#include "../include/spectra.h"
#include "../include/misc.h"

// ROOT includes
#if IS_ROOT
#include <TAxis.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGraph.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TLegendEntry.h>
#include <TLegend.h>
#endif

// C++ std libraries
using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>

// "spectra.cc"-specific vectors and matrices (to avoid repeated time-consuming loading from files)
vector<vector<vector<double> > > matrix_CIRELLI11_GAMMA, matrix_CIRELLI11_NU[gN_NUFLAVOUR];
vector<double> vec_mdm_CIRELLI11, vec_logx_CIRELLI11, vec_ebl_redshifts;
vector<vector<double> > matrix_ebl_coeff;

//______________________________________________________________________________
double dNdE(double const &e_gev, double par_spec[6])
{
   //--- Returns number of particles emitted by PP reaction summed over all
   //    final states and branching ratios [/GeV].
   //
   //  e_gev         Energy of the observed photon [GeV]
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)

   const int buflen = 500;
   char char_tmp[buflen];

   // N.B.: energy of produced particle cannot be larger than particle DM mass

   if (gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.999999999 * par_spec[0]) {
      // printf("\n====> WARNING: dNdE() in spectra.cc");
      // printf("\n               e_gev>=m_DM (%.2le vs %.2le), returns -1.e-40.\n\n", e_gev, par_spec[0]);
      return -1.e-40;
   } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.499999999 * par_spec[0]) {
      return -1.e-40;
   }

   int flag_model = (int)par_spec[1];
   int flag_finalstate = (int)par_spec[2];

   // If not kCIRELLI11, only handles ANNIHILATION into GAMMA!
   if (flag_model != kCIRELLI11_EW && flag_model != kCIRELLI11_NOEW && (!gPP_DM_IS_ANNIHIL_OR_DECAY || flag_finalstate != kGAMMA)) {
      printf("\n====> ERROR: dNdE() in spectra.cc");
      printf("\n             Model %s does not handle", gNAMES_PP_SPECTRUMMODEL[flag_model]);
      if (flag_finalstate != kGAMMA) {
         printf(" final state which is not gamma-rays.");
         printf("\n             Please set gSIM_FLUX_FLAG_FINALSTATE = kGAMMA");
      } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY)
         printf("  decaying DM");
      printf("\n             => abort()\n\n");
      abort();
   }

   // No attenuation and/or redshift at z=0:
   if (par_spec[3] < 1.e-5) {
      double dnde = 1.e-40;
      switch (flag_model) {
         case kCIRELLI11_EW:
         case kCIRELLI11_NOEW:
            dnde = dNdE_CIRELLI11(e_gev, par_spec);
            break;
         case kBERGSTROM98:
            dnde = dNdE_BERGSTROM98(e_gev, par_spec);
            break;
         case kTASITSIOMI02:
            dnde = dNdE_TASITSIOMI02(e_gev, par_spec);
            break;
         case kBRINGMANN08:
            dnde = dNdE_BRINGMANN08(e_gev, par_spec) + dNdE_BERGSTROM98(e_gev, par_spec);
            break;
         default :
            printf("\n====> ERROR: dNdE() in spectra.cc");
            printf("\n             flag_model=%d does not correspond to any dNdE model", flag_model);
            printf("\n             => abort()\n\n");
            abort();
            break;
      }

      bool is_verbose = false;
      if (std::isnan(dnde)) {
         if (is_verbose) {
            snprintf(char_tmp, buflen, "dN/dE(%.2le GeV) is nan, returns -1.e-40.", e_gev);
            print_warning("spectra.cc", "dNdE()", string(char_tmp));
         }
         return -1.e-40;
      } else if (dnde < 0) {
         if (is_verbose) {
            snprintf(char_tmp, buflen, "dN/dE(%.2le GeV) = %.2le is negative, returns -1.e-40.\n\n", e_gev, dnde);
            print_warning("spectra.cc", "dNdE()", string(char_tmp));
         }
         return -1.e-40;
      } else
         return pow(e_gev, par_spec[4]) * dnde;
   } else {

      // For gamma-rays and neutrinos, if redshift non-null, energy and spectrum are shifted
      double E_emit = e_gev * (1. + double(par_spec[3]));
      double z_ref = par_spec[3];

      // save systematic EBl uncertainty (for gammas only)
      double sys_error_ref;
      if (flag_finalstate == kGAMMA) sys_error_ref = par_spec[5];

      // Calculate spectrum at z=0 for the 'shifted' energy
      par_spec[3] = 0.;
      par_spec[5] = 0.;
      double res =  dNdE(E_emit, par_spec);

      par_spec[3] = z_ref;
      if (flag_finalstate == kGAMMA) par_spec[5] = sys_error_ref;
      //cout << "res dnendtruc:      " << res <<endl;

      // To take into account the optical depth of gamma-rays, we need to multiply by
      //   *exp_minus_opt_depth(e_gev, par_spec[3]);
      //cout << "res dnendtruc:      " << e_gev<<"        "<< par_spec[3] <<endl;
      if (flag_finalstate == kGAMMA) return res * exp_minus_opt_depth(e_gev, par_spec[3], gEXTRAGAL_FLAG_ABSORPTIONPROFILE, par_spec[5]) ;
      else return res;
   }
}

//______________________________________________________________________________
double dNdE_BERGSTROM98(double const &e_gev, double par[1])
{
   //--- Returns dN/dE for gamma-rays (continuum) for annihilating DM.
   //    From Bergstr�m, Ullio, Buckley, Astroparticle Physics 9 (1998) 137.
   //
   //  e_gev         Energy of the gamma ray [GeV]
   //  par[0]        Mass of the DM particle [GeV]

   double x = e_gev / par[0];
   return 1. / par[0] * 0.73 * exp(-7.8 * x) / pow(x, 1.5);
}

//______________________________________________________________________________
double dNdE_BRINGMANN08(double const &e_gev, double par[1])
{
   //--- Returns dN/dE for gamma-rays (continuum) for annihilating DM. This
   //    spectrum assumes a pure Wino (or Higgsino), as given in Eq.(3.7) from
   //    Bringmann, Bergstroem, Edsjoe, Journal of HEP 1 (2008) 49.
   //
   //  e_gev         Energy of the gamma ray [GeV]
   //  par[0]        Mass of the DM particle [GeV]

   double x = e_gev / par[0];
   double m_W = 80.398; // mass of W bosons, in GeV
   double epsilon = m_W / par[0];
   double alpha_em = 1. / 137.03599911; // Fine structure constant
   return  1. / par[0] * alpha_em / PI * 4.*pow(1. - x + x * x, 2)
           / ((1. - x + epsilon / 2.) * x)
           * (log(2 * (1. - x + epsilon / 2.) / epsilon) - 1. / 2. + x - x * x * x);
}

//______________________________________________________________________________
double dNdE_CIRELLI11(double const &e_gev, double par[3])
{
   //--- Returns dN/dE for gamma-rays or neutrinos (continuum), for annihilating
   //    or decaying DM. From Cirelli et al. (2011),  JCAP 03, 051 [PPPC 4 DM ID].
   //
   //  e_gev         Energy of the gamma ray [GeV]
   //  par[0]        Mass of DM candidate [GeV]
   //  par[1]        Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par[2]        Card for PP final state (gENUM_FINALSTATE)

   // Valid DM mass range is [5 GeV - 100 TeV]
   if (par[0] < 5. || par[0] > 1.e5) {
      printf("\n====> ERROR: dNdE_CIRELLI11() in spectra.cc");
      printf("\n             Valid m_DM range is [5 GeV - 100 TeV] and you asked for m_DM=%.2le GeV", par[0]);
      printf("\n             => abort()\n\n");
      abort();
   }

   if (par[0] - 5. < SMALL_NUMBER) par[0] += SMALL_NUMBER;

   // If decay, must pass to CIRELLI11 m_DM/2
   double mdm_gev = par[0];
   if (!gPP_DM_IS_ANNIHIL_OR_DECAY)
      mdm_gev /= 2.;

   // Valid energy range is log10(E/mdm_gev) in [-9, 0]
   double logx = log10(e_gev / mdm_gev);
   if (logx < -9 || logx > 0) {
      printf("\n====> ERROR: dNdE_CIRELLI11_interp() in spectra.cc");
      printf("\n             Valid log10(E / m_MD) range is in [-9,0] and you asked for logx=%.2le", logx);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Use EW or NoEW files
   string path = "";
   int spec_cirelli = par[1];
   if (spec_cirelli == kCIRELLI11_EW)
      path = gPATH_TO_CLUMPY_DATA +"/PPPC4DMID-spectra/AtProduction_";
   else if (spec_cirelli == kCIRELLI11_NOEW)
      path = gPATH_TO_CLUMPY_DATA +"/PPPC4DMID-spectra/AtProductionNoEW_";


   // Print channels and branching ratios...
   double checksum_br = 0;
   for (int channel = 0; channel < gN_PP_BR; ++channel) {
      if (gPP_BR[channel] < 0. or gPP_BR[channel] > 1.) {
         printf("\n====> ERROR: dNdE_CIRELLI11() in spectra.cc");
         printf("\n             branching ratios must be in [0,1].");
         printf("\n             => abort()\n\n");
         abort();
      } else {
         //cout << "channel[" << channel << "]  " << gNAMES_PP_BR[channel]
         //     << "   Branching ratio=" << gPP_BR[channel] << endl;
         checksum_br = checksum_br + gPP_BR[channel];
      }
   }
   // check sumBR>=1:
   if (checksum_br > 1) {
      static bool is_br_ratio_warning = true;
      if (is_br_ratio_warning == true) {
         const int buflen = 500;
         char char_tmp[buflen];
         snprintf(char_tmp, buflen, "Sum of branching ratios > 1 (sumBR = %.2le). Will be renormalized to 1.", checksum_br);
         print_warning("spectra.cc", "dNdE_CIRELLI11()", string(char_tmp));
         is_br_ratio_warning = false;
      }
      for (int channel = 0; channel < gN_PP_BR; ++channel) {
         gPP_BR[channel] /= checksum_br;
         //cout << "channel[" << channel << "]  " << gNAMES_PP_BR[channel]
         //     << "   Branching ratio=" << gPP_BR[channel] << endl;
      }
   }
   // check sumBR>0:
   if (checksum_br < SMALL_NUMBER) {
      printf("\n====> ERROR: dNdE_CIRELLI11() in spectra.cc");
      printf("\n             Set at least one branching ratio != zero.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // Calculate dN/dE depending on the final state (kGAMMA or kNEUTRINO)
   int flag_finalstate = (int)par[2];
   if (flag_finalstate == kGAMMA) {
      // dNdE for gamma-ray

      // If CIRELLI11 gama files were not previously loaded,
      // fill matrices and vectors!
      if (matrix_CIRELLI11_GAMMA.size() == 0 || vec_mdm_CIRELLI11.size() == 0
            || vec_logx_CIRELLI11.size() == 0) {
         string file = path + "gammas.dat";
         dNdE_CIRELLI11_load(file, matrix_CIRELLI11_GAMMA, vec_mdm_CIRELLI11, vec_logx_CIRELLI11);
      }

      // Sum dN/dE over non-null branching ratios
      double dnde = 0;
      for (int channel = 0; channel < gN_PP_BR; ++channel) {
         if (gPP_BR[channel] > 0.)
            dnde += gPP_BR[channel]
                    * dNdE_CIRELLI11_interp(matrix_CIRELLI11_GAMMA, vec_mdm_CIRELLI11, vec_logx_CIRELLI11, e_gev, mdm_gev, channel);
      }
      return dnde;

   } else if (flag_finalstate == kNEUTRINO) {
      // dNdE for neutrinos (gN_NUFLAVOUR)
      // If CIRELLI11 gama files were not previously loaded,
      // fill matrices and vectors!
      if (matrix_CIRELLI11_NU[0].size() == 0 || matrix_CIRELLI11_NU[1].size() == 0 || matrix_CIRELLI11_NU[2].size() == 0
            || vec_mdm_CIRELLI11.size() == 0 || vec_logx_CIRELLI11.size() == 0) {
         // Read file and load values (for each nu flavour)
         string files[gN_NUFLAVOUR] = {"neutrinos_e.dat",
                                       "neutrinos_mu.dat",
                                       "neutrinos_tau.dat"
                                      };
         for (int i = 0; i < gN_NUFLAVOUR; ++i) {
            string file = path + files[i];
            dNdE_CIRELLI11_load(file, matrix_CIRELLI11_NU[i], vec_mdm_CIRELLI11, vec_logx_CIRELLI11);
         }
      }

      // Sum dN/dE over all non-null branching ratios (for each nu flavour)
      vector<double> dnde_in(gN_NUFLAVOUR, 0.);
      for (int i = 0; i < gN_NUFLAVOUR; ++i) {
         for (int channel = 0; channel < gN_PP_BR; ++channel) {
            if (gPP_BR[channel] > 0.)
               dnde_in[i] += gPP_BR[channel]
                             * dNdE_CIRELLI11_interp(matrix_CIRELLI11_NU[i], vec_mdm_CIRELLI11, vec_logx_CIRELLI11, e_gev, mdm_gev, channel);
         }
      }

      // Oscillation matrix to get final dnde
      vector<double> dnde_out(gN_NUFLAVOUR, 0.);
      for (int i = 0; i < gN_NUFLAVOUR; ++i) {
         for (int j = 0; j < gN_NUFLAVOUR; ++j)
            dnde_out[i] += gPP_NUOSCILLATIONMATRIX[i][j] * dnde_in[j];
      }

      return dnde_out[gSIM_FLUX_FLAG_NUFLAVOUR];
   }
   return 0.;
}

//______________________________________________________________________________
double dNdE_CIRELLI11_interp(vector<vector<vector<double> > > &matrix_PPPC, vector<double> &vec_mdm,
                             vector<double> &vec_logx, double const &e_gev,
                             double const &mdm_gev, int channel)
{
   //--- Returns dNdE(E) for DM candidate of mass mdm_gev, interpolated between matrix
   //    values closest in mass (vec_mdm) and energy (vec_logx). If mass bewteen
   //    threshold, returns 0.
   //
   //  matrix        Matrix[Nmdm*Nlogx][gN_PP_BR] of dN/dlog10x values
   //  vec_mdm       Vector[Nmdm] of DM masses [GeV]
   //  vec_logx      Vector[Nlogx] of logx=log10(E/m_DM) values [-]
   //  e_gev         Energy for which to calculate dNdE [GeV]
   //  mdm_gev       Mass of DM candidate [GeV]
   //  channel       Index of channel among the gN_PP_BR ones

   double logx = log10(e_gev / mdm_gev);
   if (logx > 0) {
      printf("\n====> ERROR: dNdE_CIRELLI11_interp() in spectra.cc");
      printf("\n             Ratio e_gev / mdm_gev > 1 -> Interpolation will fail");
      printf("\n             => abort()\n\n");
      abort();
   }

   double dnde = interp2D(mdm_gev, logx, vec_mdm, vec_logx, matrix_PPPC[channel], kLINLIN);
   return dnde / (e_gev * log(10));

}

//______________________________________________________________________________
void dNdE_CIRELLI11_load(string const &f_name, vector<vector<vector<double> > > &matrix_PPPC,
                         vector<double> &vec_mdm, vector<double> &vec_logx)
{
   //--- Loads matrix for several DM masses (vec_mdm) and logx values (vec_logx)
   //    from a PPPC4DMID ascii file (from Cirelli et al. (2011),  JCAP 03, 051).
   //
   // INPUTS:
   //  is            PPPC4DMID file to read
   //  is_ew_or_noew Use electro-weak correction or not
   // OUTPUT:
   //  matrix        Matrix[Nmdm][Nlogx][gN_PP_BR] of dN/dlog10x values
   //  vec_mdm       Vector[Nmdm] of DM masses [GeV]
   //  vec_logx      Vector[Nlogx] of logx=log10(E/m_DM) values [-]

   // clear first
   matrix_PPPC.clear();
   vec_mdm.clear();
   vec_logx.clear();

   ifstream is(f_name.c_str());
   if (!is.good()) {
      printf("\n====> ERROR: dNdE_CIRELLI11_load() in spectra.cc");
      printf("\n             File %s not found", f_name.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }

   cout << "     .... load PPPC4DMID ascii file " << f_name << endl;

   // in the ascii files, first columns are DM masses [GeV], second column logx=log10(E/m_DM) values
   // all other columns branching channels -> read the latter into matrix:
   vector<vector<double> > matrix_PPPC_raw = read_ascii(f_name, -1, false);

   // extract DM mass values:
   vec_mdm.push_back(matrix_PPPC_raw[0][0]);
   for (int i = 1; i < int(matrix_PPPC_raw.size()); ++i) {
      if (matrix_PPPC_raw[i][0] > matrix_PPPC_raw[i - 1][0]) {
         vec_mdm.push_back(matrix_PPPC_raw[i][0]);
      }
   }

   // extract logx values:
   vec_logx.push_back(matrix_PPPC_raw[0][1]);
   for (int i = 1; i < int(matrix_PPPC_raw.size()); ++i) {
      if (matrix_PPPC_raw[i][1] < matrix_PPPC_raw[i - 1][1]) break;
      vec_logx.push_back(matrix_PPPC_raw[i][1]);
   }

   // Check: vec_mdm.size()*vec_logx.size() = matrix.size()
   // If not, means that there is for sure a problem in the reading
   if (vec_mdm.size()*vec_logx.size() != matrix_PPPC_raw.size()) {
      printf("\n====> ERROR: dNdE_CIRELLI11_load() in spectra.cc");
      printf("\n             Format read does not match %d (values in logx) times %d (values in mdm)",
             (int)vec_logx.size(), (int)vec_mdm.size());
      printf("\n             => abort()\n\n");
      abort();
   } else if (gN_PP_BR != int(matrix_PPPC_raw[0].size()) - 2) {
      printf("\n====> ERROR: dNdE_CIRELLI11_load() in spectra.cc");
      printf("\n             Number of colums in matrix is %d,  whereas there should be", int(matrix_PPPC_raw[0].size()) - 2);
      printf("\n             %d values (for all branching ratios).", gN_PP_BR);
      printf("\n             => abort()\n\n");
      abort();
   }

   // fill matrix_PPPC_raw into 3D object:
   vector<vector<double> > matrix_PPPC_tmp;
   vector<double> vec_tmp;
   for (int i = 2; i < gN_PP_BR + 2; ++i) {
      matrix_PPPC_tmp.clear();
      for (int j = 0; j < (int)vec_mdm.size(); ++j) {
         vec_tmp.clear();
         for (int k = 0; k < (int)vec_logx.size(); ++k) {
            vec_tmp.push_back(matrix_PPPC_raw[j * int(vec_logx.size()) + k][i]);
         }
         matrix_PPPC_tmp.push_back(vec_tmp);
      }
      matrix_PPPC.push_back(matrix_PPPC_tmp);
   }
//   // Check: print content of vectors
//   for (int br=0; br<1; ++br) {
//    for (int m=0; m<(int)vec_mdm.size(); ++m) {
//       for (int t=0; t<(int)vec_logx.size(); ++t) {
//            cout << vec_mdm[m] << "\t";
//             cout << vec_logx[t] << "\t";
//             cout << matrix_PPPC[br][m][t] << endl;
//          }
//       }
//    }
//    abort();
}

//______________________________________________________________________________
double dNdE_TASITSIOMI02(double const &e_gev, double par[1])
{
   //--- Returns dN/dE for gamma-rays (continuum) for annihilating DM.
   //    From Tasitsiomi, Olinto, Phys. Rev. D 66 (2002) 083006.
   //
   //  e_gev         Energy of the gamma ray [GeV]
   //  par[0]        Mass of the DM particle [GeV]

   double x = e_gev / par[0];
   return 1. / par[0] * 10. / 3. - 5. / 4.* pow(x, 1. / 2.) - 5. / 2.
          * pow(x, -1. / 2.) + 5. / 12.* pow(x, -3. / 2.);
}

//______________________________________________________________________________
double dPPdE(double &e_gev, double par_spec[6])
{
   //--- Returns particle physics term for model and channel.
   //       - Annihilation: <sigma v>/(4PI delta m_{DM}^2) dN/dE [cm^3 /s /GeV^3].
   //       - Decay: 1/(4PI Gamma m_{DM}) dN/dE  [1/s /GeV^2]
   //
   // INPUTS:
   //  e_gev         Energy of the observed photon [GeV]
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)


   // Get dN/dE for model and final state
   double dnde = dNdE(e_gev, par_spec);

   // Terms for annihilation and decay
   dnde /= (4. * PI *  par_spec[0]);

   // If annihilation or decay
   if (gPP_DM_IS_ANNIHIL_OR_DECAY)
      return dnde * gPP_DM_ANNIHIL_SIGMAV_CM3PERS / (gPP_DM_ANNIHIL_DELTA * par_spec[0]);
   else
      return dnde / gPP_DM_DECAY_LIFETIME_S;
}

//______________________________________________________________________________
void dPPdE(double &e_gev, double par_spec[6], double &res)
{
   //--- Sets res = particle physics term for model and channel [cm^3 /s /GeV^3].
   //       - Annihilation: <sigma v>/(4PI delta m_{DM}^2) dN/dE
   //       - Decay: Gamma/(4PI m_{DM}) dN/dE
   //
   // INPUTS:
   //  e_gev         Energy of the observed photon [GeV]
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   // OUTPUT:
   //  res           Particle physics factor

   res = dPPdE(e_gev, par_spec);
}

//______________________________________________________________________________
double dPPdE_integrated(double par_spec[6], double &e1_gev, double &e2_gev, double const &eps)
{
   //--- Returns integrated particle physics term for model and channel [cm^3 /s /GeV^2],
   //    between energies [E1,E2].
   //       - Annihilation: <sigma v>/(4PI delta m_{DM}^2) \int_E1^E2 dN/dE
   //       - Decay: Gamma/(4PI m_{DM}) \int_E1^E2 dN/dE
   //
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   //  e1_gev        Minimal energy of integration [GeV]
   //  e2_gev        Maximal energy of integration [GeV]
   //  eps           Relative precision sought for integration

   double res = 0.;
   if (e2_gev > e1_gev)
      simpson_log_adapt(dPPdE, e1_gev, e2_gev, par_spec, res, eps);

   return res;
}

//_____________________________________________________________________________
double opt_depth(double const &e_gev, double const &z, int flag)
{

//--- Returns tau(E_obs,z), the optical depth for a photon
//    emitted at redshift z, and observed at energy E.
//    e_gev         Energy of the observed gamma ray [GeV]
//    z             Redshift of the emitted photon
//    flag          Type of absorption model used: Finke 2010, Franceschini 2008 or Gilmore 2012

   // For convenience, tau(E,z) was fitted using a polynomial
   int n_pol = 0;
   int n_z   = 0;
   string file;

   double delta_upwards = 1;
   double delta_downwards = 1;

   if (flag == kNOEBL) {
      return 0.;
   } else if (flag == kFRANCESCHINI08) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Franceschini08.dat";
      delta_upwards = 2e-3;
      delta_downwards = 0.02;
   } else if (flag == kFRANCESCHINI17) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Franceschini17.dat";
      delta_upwards = 7e-3;
      delta_downwards = 0.02;
   } else if (flag == kGILMORE12_FIDUCIAL) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Gilmore_fid12.dat";
      delta_upwards = 2e-4;
      delta_downwards = 0.02;
   } else if (flag == kGILMORE12_FIXED) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Gilmore_fix12.dat";
      delta_upwards = 2e-4;
      delta_downwards = 0.02;
   } else if (flag == kFINKE10) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Finke10.dat";
      delta_upwards = 2e-7;
      delta_downwards = 0.02;
   } else if (flag == kDOMINGUEZ11_REF) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Dominguez11.dat";
      delta_upwards = 5e-4;
      delta_downwards = 0.02;
   } else if (flag == kDOMINGUEZ11_LO) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Dominguez11_low.dat";
      delta_upwards = 5e-4;
      delta_downwards = 0.02;
   } else if (flag == kDOMINGUEZ11_UP) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Dominguez11_up.dat";
      delta_upwards = 5e-4;
      delta_downwards = 0.02;
   } else if (flag == kINOUE13_REF) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Inoue13.dat";
      delta_upwards = 1e-6;
      delta_downwards = 5e-3;
   } else if (flag == kINOUE13_LO) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Inoue13_low.dat";
      delta_upwards = 1e-6;
      delta_downwards = 5e-3;
   } else if (flag == kINOUE13_UP) {
      file = gPATH_TO_CLUMPY_DATA +"/EBL/Inoue13_up.dat";
      delta_upwards = 1e-6;
      delta_downwards = 5e-3;
   }

   // avoid time-consuming loading of coefficients at each call of the function
   if (matrix_ebl_coeff.size() == 0 or vec_ebl_redshifts.size() == 0) {
      ifstream is(file.c_str());
      if (!is.good()) {
         printf("\n====> ERROR: exp_minus_opt_depth() in spectra.cc");
         printf("\n             File %s not found", file.c_str());
         printf("\n             => abort()\n\n");
         abort();
      }
      cout << "     .... load EBL polynomial coefficients from file " << file << endl;
      matrix_ebl_coeff  = read_ascii(file, -1, false, 1); // start reading with first column
      vector<vector <double> > tmp;
      tmp = read_ascii(file, 1); // only read first columnf
      for (int i = 0; i < int(tmp.size()); ++i)
         vec_ebl_redshifts.push_back(tmp[i][0]);
   }

   n_pol = matrix_ebl_coeff[0].size();
   n_z = vec_ebl_redshifts.size();

   // Locate in 2-d array obtained, the closest z (lower)
   // -> search in redshift[] the closest value to z
   double zmin, zmax;
   int iref_lo = -1;
   int iref_up = -1;
   // we will do a power-law extrapolation of tau if exceeding the tabulated redshift range
   if (z < vec_ebl_redshifts[0]) {
      iref_lo = 0;
      iref_up = 1;
   } else if (z >= vec_ebl_redshifts[n_z - 1]) {
      iref_lo = n_z - 2;
      iref_up = n_z - 1;
   } else {
      iref_lo = binary_search(vec_ebl_redshifts, z);
      if (iref_lo == int(vec_ebl_redshifts.size() - 1)) iref_lo--;
      iref_up = iref_lo + 1;
   }
   zmin = vec_ebl_redshifts[iref_lo];
   zmax = vec_ebl_redshifts[iref_up];

   // N.B.: tables are for E in TeV
   double log_E = log10(e_gev * 1.e-3);

   // Compute tau(E) for the bracketing redshifts
   double log_tau_min = 0;
   double log_tau_max = 0;

   // get log_tau_min:
   if (abs(log_E) < SMALL_NUMBER) {
      // computation at pivot point = 1 TeV:
      if (z < vec_ebl_redshifts[0]) {
         log_tau_min = matrix_ebl_coeff[iref_lo][n_pol - 1];
         log_tau_max = log_tau_min + 0.1;
      } else if (z > vec_ebl_redshifts[n_z - 1]) {
         log_tau_min = log_tau_max - 0.0075;
         log_tau_max = matrix_ebl_coeff[iref_up][n_pol - 1];
      } else {
         log_tau_min = matrix_ebl_coeff[iref_lo][n_pol - 1];
         log_tau_max = matrix_ebl_coeff[iref_up][n_pol - 1];
      }
   } else {
      // computation at other energies:
      double pow_loge = pow(log_E, n_pol - 1);
      for (int k = 0; k < n_pol; ++k) {
         if (z < vec_ebl_redshifts[0]) {
            log_tau_min += pow_loge * matrix_ebl_coeff[iref_lo][k];
            log_tau_max += pow_loge * matrix_ebl_coeff[iref_lo][k] + delta_downwards / (log(zmax) - log(zmin));
         } else if (z > vec_ebl_redshifts[n_z - 1]) {
            log_tau_min += pow_loge * matrix_ebl_coeff[iref_up][k] - delta_upwards / (log(zmax) - log(zmin));
            log_tau_max += pow_loge * matrix_ebl_coeff[iref_up][k];
         } else {
            log_tau_min += pow_loge * matrix_ebl_coeff[iref_lo][k];
            log_tau_max += pow_loge * matrix_ebl_coeff[iref_up][k];
         }
         pow_loge = pow_loge / log_E;
      }

   }
   //printf("z =%f, tau_min=%le, tau_max=%le, tau_interpol=%le\n", z, pow(10., log_tau_min), pow(10., log_tau_max), interp_linlin(z, zmin, zmax, pow(10., log_tau_min), pow(10., log_tau_max)));
   double res = pow(10, interp_loglin(z, zmin, zmax, log_tau_min, log_tau_max));
   if (res > 1e6) return 1e6;
   if (res < 1e-10) return 1e-10;
   else return res;
}

//_____________________________________________________________________________
double exp_minus_opt_depth(double const &e_gev, double const &z, int flag, double const sys_uncertainty)
{
   //--- Returns exp[-tau(E_obs,z)], where tau is the optical depth for a photon
   //    emitted at redshift z, and observed at energy E.
   //    e_gev           Energy of the observed gamma ray [GeV]
   //    z               Redshift of the emitted photon
   //    flag            Type of absorption model used: Finke 2010, Franceschini 2008 or Gilmore 2012
   //    sys_uncertainty Scale tau by a constant factor (1 + sys_uncertainty)

   double tau = opt_depth(e_gev, z, flag);
   if (tau > 92.10340371976183) return 1e-40;
   return exp(-tau * (1. + sys_uncertainty));
}


//______________________________________________________________________________
string legend_for_spectrum(double par_spec[6], bool is_finalstate, bool is_z)
{
   //--- Returns a name as "mass model final_state (br) z".
   //
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   //  is_finalstate Add final state or not
   //  is_z          Add z or not

   string model = gNAMES_PP_SPECTRUMMODEL[(int)par_spec[1]];
   string final_state = gNAMES_FINALSTATE[(int)par_spec[2]];
   if ((int)par_spec[2] == kNEUTRINO)
      final_state = gNAMES_NUFLAVOUR[gSIM_FLUX_FLAG_NUFLAVOUR];

   string br = "";
   for (int channel = 0; channel < gN_PP_BR; ++channel) {
      if (gPP_BR[channel] > 0.) {
         char tmp[2000];
         sprintf(tmp, "%s = %g", gNAMES_PP_BR[channel], gPP_BR[channel]);
         if (br == "")
            br = string(tmp);
         else
            br = br + ";" + (string)tmp;
      }
   }
   if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW or gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW) {
      br = "[" + br + "]";
   } else
      br = "";

   char legend[1000];
   if (gPP_DM_IS_ANNIHIL_OR_DECAY) {
      if (is_finalstate) {
         if (is_z)
            if (par_spec[2] == kGAMMA) sprintf(legend, "#splitline{m_{DM} = %g GeV, #sigma v = %g cm^{3}s^{-1}, %s: %s %s,}{z = %g, EBL model: %s}", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                                                  final_state.c_str(), br.c_str(), par_spec[3], gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE]);
            else sprintf(legend, "#splitline{m_{DM} = %g GeV, #sigma v = %g cm^{3}s^{-1}, %s: %s %s,}{z = %g}", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                            final_state.c_str(), br.c_str(), par_spec[3]);

         else
            sprintf(legend, "m_{DM} = %g GeV, #sigma v = %g cm^{3}s^{-1}, %s: %s %s", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                    final_state.c_str(), br.c_str());
      } else {
         if (is_z)
            if (par_spec[2] == kGAMMA)  sprintf(legend, "m_{DM}=%g GeV, #sigma v = %g cm^{3}s^{-1}, %s %s, z = %g, EBL model: %s", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                                                   br.c_str(), par_spec[3],  gNAMES_ABSORPTIONPROFILE[gEXTRAGAL_FLAG_ABSORPTIONPROFILE]);
            else sprintf(legend, "m_{DM}=%g GeV, #sigma v = %g cm^{3}s^{-1}, %s %s, z = %g", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                            br.c_str(), par_spec[3]);
         else
            sprintf(legend, "m_{DM}=%g GeV, #sigma v = %g cm^{3}s^{-1}, %s %s", par_spec[0], gPP_DM_ANNIHIL_SIGMAV_CM3PERS, model.c_str(),
                    br.c_str());
      }
   } else {
      if (is_finalstate) {
         if (is_z)
            sprintf(legend, "m_{DM} = %g GeV, #tau = %g s,  %s: %s %s, z = %g", par_spec[0], gPP_DM_DECAY_LIFETIME_S, model.c_str(),
                    final_state.c_str(), br.c_str(), par_spec[3]);
         else
            sprintf(legend, "m_{DM} = %g GeV, #tau = %g s,  %s: %s %s", par_spec[0], gPP_DM_DECAY_LIFETIME_S, model.c_str(),
                    final_state.c_str(), br.c_str());
      } else {
         if (is_z)
            sprintf(legend, "m_{DM}=%g GeV, #tau = %g s, %s %s, z = %g", par_spec[0], gPP_DM_DECAY_LIFETIME_S, model.c_str(),
                    br. c_str(), par_spec[3]);
         else
            sprintf(legend, "m_{DM}=%g GeV, #tau = %g s, %s %s", par_spec[0], gPP_DM_DECAY_LIFETIME_S, model.c_str(),
                    br.c_str());
      }
   }

   return (string)legend;
}

//_____________________________________________________________________________
void nu_oscillationmatrix(double nu_pij[gN_NUFLAVOUR][gN_NUFLAVOUR], bool is_verbose)
{
   //--- Returns oscillation matrix (from oscillation angles defined in param file).
   //
   // OUTPUTS:
   //  nu_pij        Oscillation matrix [3][3]
   //  is_verbose    Print or not mixing angles and oscillation matrix

   // Neutrino oscillation angles
   if (is_verbose) {
      cout << "Neutrino CP phase and oscillation angles (in deg)" << endl;
      cout << "deltaCP = " << gPP_NUMIXING_CPPHASE_DEG << " "
           << "theta12 = " << gPP_NUMIXING_THETA12_DEG << " "
           << "theta13 = " << gPP_NUMIXING_THETA13_DEG << " "
           << "theta23 = " << gPP_NUMIXING_THETA23_DEG << endl;
   }

   // Neutrino mixing angle
   double s12 = sin(gPP_NUMIXING_THETA12_DEG * DEG_to_RAD);
   double c12 = sqrt(1 - s12 * s12);
   double s13 = sin(gPP_NUMIXING_THETA13_DEG * DEG_to_RAD);
   double c13 = sqrt(1 - s13 * s13);
   double s23 = sin(gPP_NUMIXING_THETA23_DEG * DEG_to_RAD);
   double c23 = sqrt(1 - s23 * s23);
   double sCP = sin(gPP_NUMIXING_CPPHASE_DEG * DEG_to_RAD);
   double cCP = sqrt(1 - sCP * sCP);

   if (is_verbose) {
      cout << "Neutrino mixing angles " << endl;
      cout << "s12 = " << s12 << " c12 = " << c12 << " s13 = " << s13 << endl;
      cout << "c13 = " << c13 << " s23 = " << s23 << " c23 = " << c23 << endl;
   }

   //--- Calculation including non-null CP phase
   // From Bilenky, S. M. and Petcov, S. T., Rev. Mod. Phys. 59 (1987) 671.
   // and Workman, R. L. et al., Progress of Theoretical and Experimental Physics 2022 (2022) 083C01
   // |U_jk|^2
   double U11_square = pow(c12 * c13, 2);
   double U12_square = pow(s12 * c13, 2);
   double U13_square = pow(s13, 2);
   double U21_square = pow(-s12 * c23 - c12 * s23 * s13 * cCP, 2) + pow(c12 * s23 * s13 * sCP, 2);
   double U22_square = pow(c12 * c23 - s12 * s23 * s13 * cCP, 2) + pow (s12 * s23 * s13 * sCP, 2);
   double U23_square = pow(s23 * c13, 2);
   double U31_square = pow(s12 * s23 - c12 * c23 * s13 * cCP, 2) + pow(c12 * c23 * s13 * sCP, 2);
   double U32_square = pow(-c12 * s23 - s12 * c23 * s13 * cCP, 2) + pow(s12 * c23 * s13 * sCP, 2);
   double U33_square = pow(c23 * c13, 2);

   // Oscillation matrix P_il = sum_k |U_lk|^2 * |Uik|^2
   double P11 = pow(U11_square, 2) + pow(U12_square, 2) + pow(U13_square, 2);
   double P22 = pow(U21_square, 2) + pow(U22_square, 2) + pow(U23_square, 2);
   double P33 = pow(U31_square, 2) + pow(U32_square, 2) + pow(U33_square, 2);
   double P12 = U11_square*U21_square + U12_square*U22_square + U13_square*U23_square;
   double P13 = U11_square*U31_square + U12_square*U32_square + U13_square*U33_square;
   double P23 = U21_square*U31_square + U22_square*U32_square + U23_square*U33_square;
   // P21=pow(U21_noCP,2)*pow(U11_noCP,2)+pow(U22_noCP,2)*pow(U12_noCP,2)+pow(U23_noCP,2)*pow(U13_noCP,2)
   // P31=pow(U31_noCP,2)*pow(U11_noCP,2)+pow(U32_noCP,2)*pow(U12_noCP,2)+pow(U33_noCP,2)*pow(U13_noCP,2)
   // P32=pow(U31_noCP,2)*pow(U21_noCP,2)+pow(U32_noCP,2)*pow(U22_noCP,2)+pow(U33_noCP,2)*pow(U23_noCP,2)
   double P21 = P12;
   double P31 = P13;
   double P32 = P23;


   if (is_verbose) {
      cout << endl;
      cout << "Oscillation matrix (with CP phase)" << endl;
      cout << P11 << " " << P12 << " " << P13 << endl;
      cout << P21 << " " << P22 << " " << P23 << endl;
      cout << P31 << " " << P32 << " " << P33 << endl;
      cout << endl;
   }

   nu_pij[0][0] = P11;
   nu_pij[0][1] = P12;
   nu_pij[0][2] = P13;
   nu_pij[1][0] = P21;
   nu_pij[1][1] = P22;
   nu_pij[1][2] = P23;
   nu_pij[2][0] = P31;
   nu_pij[2][1] = P32;
   nu_pij[2][2] = P33;
}

//_____________________________________________________________________________
double flux(double par_spec[6], double &e_gev, double const &jfact_pp_unit, bool is_integrated)
{
   //--- Returns flux for annihilation or decay at a given energy.
   //       - if jfact_pp_unit<0 => dPP/dE [cm^3/s/GeV^3]
   //       - otherwise => flux [cm^2/s/GeV]
   //
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   //  e_gev         Energy of observed flux [GeV]
   //  jfact_pp_unit J |GeV^2/cm^5] or D [GeV/cm^2] in PP physics unit (if <0, not used)
   //  is_integrated If true, calculate integrated flux in [gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV]
   //                (calculate int. flux in [gSIM_FLUX_EMIN_GEV, par_spec[0]]
   //                 for gSIM_FLUX_EMAX_GEV > par_spec[0])

   double pp = 0.;

   if (is_integrated) {
      double emax;
      double emin = gSIM_FLUX_EMIN_GEV;
      if ((gPP_DM_IS_ANNIHIL_OR_DECAY == 0 && emin > 0.499999999 *  par_spec[0]) ||
            (gPP_DM_IS_ANNIHIL_OR_DECAY == 1 && emin > 0.999999999 *  par_spec[0])) {
         cout << endl;
         cout << "Invalid energy integration range ==> abort !" << endl;
         cout << "- if decay, Emin = " << emin << " GeV must be < gPP_DM_MASS_GEV/2 =" << par_spec[0] / 2. << " GeV" << endl;
         cout << "- if annihilation, Emin = " << emin << " GeV must be < gPP_DM_MASS_GEV =" << par_spec[0] << " GeV" << endl;
         cout << "--> Check your parameter file!" << endl;
         cout << endl;
         abort();
      }
      if ((gPP_DM_IS_ANNIHIL_OR_DECAY == 1 && gSIM_FLUX_EMAX_GEV > par_spec[0]) ||
            (gPP_DM_IS_ANNIHIL_OR_DECAY == 0 && gSIM_FLUX_EMAX_GEV > par_spec[0]) / 2.) {
         // print info only once:
         static bool is_intflux_warning = true;
         if (is_intflux_warning == true) {
            print_warning("spectra.cc", "flux()", "E_max > m_DM --> Setting E_max=m_DM (annihilation) or E_max=m_DM/2 (decay).");
            is_intflux_warning = false;
         }
         if (gPP_DM_IS_ANNIHIL_OR_DECAY == 1) emax = 0.999999999 * par_spec[0];
         else emax = 0.499999999 * par_spec[0];
      } else emax =  0.999999999 * gSIM_FLUX_EMAX_GEV;
      pp = dPPdE_integrated(par_spec, emin, emax, gSIM_EPS);
   } else {
      if (gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.999999999 * par_spec[0]) {
         return 1.e-40;
      } else if (!gPP_DM_IS_ANNIHIL_OR_DECAY and e_gev > 0.499999999 * par_spec[0]) {
         return 1.e-40;
      } else pp = dPPdE(e_gev, par_spec);
   }

   double res;
   if (jfact_pp_unit > 0.)
      res =  jfact_pp_unit * pp;
   else
      res = pp;
   if (res < 0.) {
      const int buflen = 500;
      char char_tmp[buflen];
      snprintf(char_tmp, buflen, "Obtained negative flux or particle physics factor (%g) Set to 1e-40.", res);
      print_warning("spectra.cc", "flux()", string(char_tmp));
      res = 1e-40;
   }
   return res;
}

//_____________________________________________________________________________
void fluxes_plot(double par_spec[6], double e1_gev, double e2_gev, int n_e, double const &jfact_pp_unit)
{
   //--- Returns differential flux spectrum for annihilation or decay:
   //       - if jfact_pp_unit<0 => dPP/dE [cm^3/s/GeV^3]
   //       - otherwise => differential flux [cm^2/s/GeV]
   //
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   //  e1_gev        Minimal energy to display [GeV]
   //  e2_gev        Maximal energy to display [GeV]
   //  n_e           Number of energy bins
   //  jfact_pp_unit J |GeV^2/cm^5] or D [GeV/cm^2] in PP physics unit (if <0, not used)
   //

   double par_spec_atsource[6];
   par_spec_atsource[0] = par_spec[0];
   par_spec_atsource[1] = par_spec[1];
   par_spec_atsource[2] = par_spec[2];
   par_spec_atsource[3] = 0.;
   par_spec_atsource[4] = par_spec[4];
   par_spec_atsource[5] = 0.;
   double e_pow = par_spec[4];

#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   string model = gNAMES_PP_SPECTRUMMODEL[(int)par_spec[1]];
   string final_state = gNAMES_FINALSTATE[(int)par_spec[2]];

   // Fill vector of energies
   vector<double> e_gev, e_gev_pow;
   if (e1_gev == 0.) e1_gev = 1.e-5;
   e_gev.push_back(e1_gev);
   double step = pow(10., log10(e2_gev / e1_gev) / (double)(n_e - 1));
   for (int k = 1; k < n_e - 1; ++k)
      e_gev.push_back(e_gev[k - 1]*step);
   e_gev.push_back(e2_gev);
   for (int k = 0; k < n_e; ++k)
      e_gev_pow.push_back(pow(e_gev[k], e_pow));


   double dppde_min = 1.e40, dppde_max = -1.e40, dnde_min = 1.e40;

   // Calculate dNdE for each branching ratio
   vector<int> i_br;
   vector<double> val_ibr;
   vector<double> val_br_ref;
   // set reference and non-null br
   for (int channel = 0; channel < gN_PP_BR; ++channel) {
      val_br_ref.push_back(gPP_BR[channel]);
      if (gPP_BR[channel] > 0.) {
         i_br.push_back(channel);
         val_ibr.push_back(gPP_BR[channel]);
      }
   }
   int n_non0_br = i_br.size();
   vector<vector<double> > dnde;
   vector<vector<double> > dnde_atsource;

   for (int i = 0; i < n_non0_br; ++i) {
      // Reset all to zero, but current br
      for (int channel = 0; channel < gN_PP_BR; ++channel) {
         if (i_br[i] == channel)
            gPP_BR[channel] = val_ibr[i];
         else
            gPP_BR[channel] = 0.;
      }
      //cout << "    dNdE for " << i_br[i] << "=" << gNAMES_PP_BR[i_br[i]] << " (" <<  val_ibr[i] << ")" << endl;
      vector<double> dnde_tmp;
      vector<double> dnde_atsource_tmp;
      for (int k = 0; k < n_e; ++k) {
         double tmp =  dNdE(e_gev[k], par_spec);
         if (tmp < dnde_min && tmp > 1.e-39)
            dnde_min  = tmp;

         dnde_tmp.push_back(tmp);

         tmp = dNdE(e_gev[k], par_spec_atsource);
         if (tmp < dnde_min && tmp > 1.e-39)
            dnde_min  = tmp;
         dnde_atsource_tmp.push_back(tmp);

         //cout << "      " << k << " e=" << e_gev[k] << " flux=" << dnde_tmp[k] << endl;
      }
      dnde.push_back(dnde_tmp);
      dnde_atsource.push_back(dnde_atsource_tmp);
   }
   // Br back to original:
   for (int channel = 0; channel < gN_PP_BR; ++channel)
      gPP_BR[channel] = val_br_ref[channel];


   // Calculate flux (sum over all branching ratios)
   vector<double> diff_flux;
   vector<double> diff_flux_atsource;
   for (int k = 0; k < n_e; ++k) {
      double tmp = dPPdE(e_gev[k], par_spec);
      diff_flux.push_back(tmp);
      if (tmp < dppde_min && tmp > 1.e-39)
         dppde_min  = tmp;
      if (tmp > dppde_max)
         dppde_max  = tmp;

      tmp = dPPdE(e_gev[k], par_spec_atsource);
      diff_flux_atsource.push_back(tmp);

      if (jfact_pp_unit > 0.) {
         diff_flux[k] *= jfact_pp_unit;
         diff_flux_atsource[k] *= jfact_pp_unit;
      }

      //cout << k << " e=" << e_gev[k] << " flux=" << diff_flux[k] << endl;
   }
   // Calculate integral flux
   gSIM_FLUX_EMIN_GEV = e1_gev;
   gSIM_FLUX_EMAX_GEV = e2_gev;
   double dummy_egev = -999;
   double int_flux = flux(par_spec, dummy_egev, jfact_pp_unit, true);

   // old calculation:
   //int_flux = dPPdE_integrated(par_spec, e1_gev, e2_gev, gSIM_EPS);
   //if (jfact_pp_unit > 0.)
   //   int_flux *= jfact_pp_unit;

   char tmp_erange[1000];
   char tmp_pow[20];
   char tmp_pow2[20];
   if (jfact_pp_unit > 0.) sprintf(tmp_erange, "Integrated flux #Phi in [%g, %g] GeV: #Phi = %.*le cm^{-2} s^{-1}", e1_gev, e2_gev, gSIM_SIGDIGITS, int_flux);
   else if (gPP_DM_IS_ANNIHIL_OR_DECAY) sprintf(tmp_erange, "Integrated PP-factor in [%g, %g] GeV: %.*le cm^{3} GeV^{-2} s^{-1}", e1_gev, e2_gev, gSIM_SIGDIGITS, int_flux);
   else sprintf(tmp_erange, "Integrated PP-factor in [%g, %g] GeV: %.*le GeV^{-1} s^{-1}", e1_gev, e2_gev, gSIM_SIGDIGITS, int_flux);
   string tmp_erange_str(tmp_erange);
   if (fabs(e_pow) > 0.01) {
      sprintf(tmp_pow, "%g", e_pow);
      tmp_erange_str += " #times GeV^{" + (string)tmp_pow + "}";
   }
   sprintf(tmp_erange, "%s", tmp_erange_str.c_str());

   string y_axis = "Flux [# cm^{-2} s^{-1}";

   if (jfact_pp_unit <= 0.) {
      if (gPP_DM_IS_ANNIHIL_OR_DECAY)
         y_axis = "PPterm [# cm^{3} s^{-1} GeV^{-2}";
      else
         y_axis = "PPterm [# s^{-1} GeV^{-1} ";
   }

   string y_axis_dppde = y_axis;
   string y_axis_dppde_mev = y_axis;
   string y_axis_dnde = "# per interaction";
   sprintf(tmp_pow, "%g", e_pow);
   sprintf(tmp_pow2, "%g", e_pow - 1);
   if (fabs(e_pow) < SMALL_NUMBER) {
      y_axis_dppde += "  GeV^{-1}]";
      y_axis_dppde_mev += "  MeV^{-1}]";
      y_axis_dnde += "  [GeV^{-1}]";
   } else if (fabs(e_pow - 1) < SMALL_NUMBER) {
      y_axis_dppde = "E #times " + y_axis_dppde + "]";
      y_axis_dppde_mev = "E #times " + y_axis_dppde_mev + "]";
      y_axis_dnde = "E #times " + y_axis_dnde + " [-]";
   } else if (fabs(e_pow - 2) < SMALL_NUMBER) {
      y_axis_dppde = "E^{2} #times " + y_axis_dppde + "  GeV]";
      y_axis_dppde_mev = "E^{2} #times " + y_axis_dppde_mev + "  MeV]";
      y_axis_dnde = "E^{2} #times " + y_axis_dnde + " [GeV]";
   } else {
      y_axis_dppde = "E^{" + (string)tmp_pow + "} #times " + y_axis_dppde + " #times GeV^{" + (string)tmp_pow2 + "}]";
      y_axis_dppde_mev = "E^{" + (string)tmp_pow + "} #times " + y_axis_dppde_mev + " #times MeV^{" + (string)tmp_pow2 + "}]";
      y_axis_dnde = "E^{" + (string)tmp_pow + "} #times " + y_axis_dnde + " [GeV^{" + (string)tmp_pow2 + "}]";
   }

   y_axis = y_axis_dppde;


   char tmp[1000];
   sprintf(tmp, "%s_%s_m%d", model.c_str(), final_state.c_str(), (int)par_spec[0]);

   // Print header
   if (gSIM_IS_PRINT) {
      // write ASCII file
      string f_ascii = gSIM_OUTPUT_DIR + "spectra_" + string(tmp) + ".txt";
      string f_dnde_ascii = gSIM_OUTPUT_DIR + "dnde_" + string(tmp) + ".txt";

      FILE *fp_ascii = fopen(f_ascii.c_str(), "w");
      FILE *fp_dnde_ascii = fopen(f_dnde_ascii.c_str(), "w");

      printf(" E [GeV]    %s\n", y_axis.c_str());
      fprintf(fp_ascii, "#  E [MeV]   %s\n", y_axis_dppde_mev.c_str());
      fprintf(fp_dnde_ascii, "#  E [GeV]   %s\n", y_axis_dnde.c_str());
      for (int k = 0; k < n_e; ++k) {
         printf("  %.*g   %.*g\n", gSIM_SIGDIGITS + 2, e_gev[k], gSIM_SIGDIGITS, diff_flux[k]);
         if (diff_flux[k] < 0) fprintf(fp_ascii, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, e_gev[k] * 1000, gSIM_SIGDIGITS,  1.e-40);
         else fprintf(fp_ascii, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, e_gev[k] * 1000, gSIM_SIGDIGITS, diff_flux[k]*pow(1000, gSIM_XPOWER - 1.));
         fprintf(fp_dnde_ascii, "  %.*le   %.*le\n", gSIM_SIGDIGITS + 2, e_gev[k], gSIM_SIGDIGITS, dnde[0][k]);

      }
      cout << endl;
      fclose(fp_ascii);
      fclose(fp_dnde_ascii);

      cout << " ... output written in: " << f_ascii << endl;
      cout << "                        " << f_dnde_ascii << endl;
   }

#if IS_ROOT
   // Displays

   string legend;
   if (par_spec[3] > SMALL_NUMBER)  legend = legend_for_spectrum(par_spec, true, true);
   else legend = legend_for_spectrum(par_spec, true, false);
   string legend_atsource = "Spectrum at source (z = 0)";
   string legend_dnde_atsource  = "dN/dE at source (z = 0)";

   TCanvas *c_dnde = NULL;
   TMultiGraph *mg_dnde = NULL;
   TMultiGraph *mg_dppde = NULL;
   TLegend *leg_dnde = NULL;
   TCanvas *c_dppde = NULL;
   TGraph *gr_dppde = NULL;
   TGraph *gr_dppde_atsource = NULL;
   TGraph *gr_dnde_atsource = NULL;
   TLegend *leg_dppde = NULL;

   if (gSIM_IS_PRINT && !gSIM_IS_WRITE_ROOTFILES)
      cout << "_______________________" << endl << endl;

   string f_root = gSIM_OUTPUT_DIR + "spectra_" + string(tmp) + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");

   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      // dNdE
      leg_dnde = new TLegend(0.15, 0.15, 0.6, 0.3);
      leg_dnde->SetFillColor(kWhite);
      leg_dnde->SetTextSize(0.03);
      leg_dnde->SetBorderSize(0);
      leg_dnde->SetFillStyle(0);
      mg_dnde = new TMultiGraph();
      mg_dnde->SetName(("mg_dnde_" + string(tmp)).c_str());
      for (int i = 0; i < n_non0_br; ++i) {
         int channel = i_br[i];
         char br_text[1000], gr_name[1000];
         //  par_spec[0]   Mass of DM candidate [GeV]
         //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
         //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
         //  par_spec[3]   Redshift of the emitted particle
         //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
         //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)

         if (gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_EW or gPP_FLAG_SPECTRUMMODEL == kCIRELLI11_NOEW)
            sprintf(br_text, "m_{DM} = %g GeV, %s = %g, z = %g", gPP_DM_MASS_GEV, gNAMES_PP_BR[channel], val_ibr[i], par_spec[3]);
         else
            sprintf(br_text, "m_{DM} = %g GeV, z = %g", gPP_DM_MASS_GEV, par_spec[3]);

         sprintf(gr_name, "dnde_%s_%s_m%d_br%d", model.c_str(), final_state.c_str(), (int)par_spec[0], channel);
         TGraph *gr_dnde = new TGraph(n_e, &e_gev[0], &dnde[i][0]);
         //cout << gr_name << endl;
         gr_dnde->SetName(gr_name);
         gr_dnde->SetTitle("");
         gr_dnde->SetLineColor(rootcolor(channel));
         gr_dnde->SetLineWidth(3);
         gr_dnde->SetLineStyle(channel % 9);
         gr_dnde->GetXaxis()->SetTitle("E [GeV]");
         gr_dnde->GetYaxis()->SetTitle(y_axis_dnde.c_str());
         mg_dnde->Add(gr_dnde, "L");

         leg_dnde->AddEntry(gr_dnde, br_text, "L");

         if (par_spec[3] > SMALL_NUMBER) {
            gr_dnde_atsource = new TGraph(n_e, &e_gev[0], &dnde_atsource[i][0]);
            sprintf(gr_name, "dnde_atsource_%s_%s_m%d_br%d", model.c_str(), final_state.c_str(), (int)par_spec[0], channel);
            gr_dnde_atsource->SetName(gr_name);
            gr_dnde_atsource->SetTitle("");
#if IS_ALPHA_METHOD
            gr_dnde_atsource->SetLineColorAlpha(rootcolor(channel), 0.3);
#else
            gr_dnde_atsource->SetLineColor(rootcolor(channel));
#endif
            gr_dnde_atsource->SetLineWidth(5);
            gr_dnde_atsource->SetLineStyle(channel % 9);
            mg_dnde->Add(gr_dnde_atsource, "L");
         }

         if (gSIM_IS_WRITE_ROOTFILES) {
            root_file->cd();
            gr_dnde->Write();
            if (par_spec[3] > SMALL_NUMBER) gr_dnde_atsource->Write();
         }

      }
      if (par_spec[3] > SMALL_NUMBER) leg_dnde->AddEntry(gr_dnde_atsource, legend_dnde_atsource.c_str(), "L");

      // Draw canvas
      c_dnde = new TCanvas("c_dnde", "c_dnde", 600, 0, 650, 500);
      c_dnde->SetLogx(1);
      c_dnde->SetLogy(1);
      // Plot graphs and legends
      mg_dnde->Draw("A");
      gSIM_CLUMPYAD->Draw();
      mg_dnde->GetXaxis()->SetTitle("E [GeV]");
      mg_dnde->GetYaxis()->SetTitle(y_axis_dnde.c_str());
      mg_dnde->SetMinimum(dnde_min);

      leg_dnde->Draw("SAME");
      c_dnde->Update();
      c_dnde->Modified();

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         mg_dnde->Write();
      }

      // dPPdE
      leg_dppde = new TLegend(0.15, 0.15, 0.5, 0.3);
      leg_dppde->SetFillColor(kWhite);
      leg_dppde->SetTextSize(0.03);
      leg_dppde->SetBorderSize(0);
      leg_dppde->SetFillStyle(0);
      mg_dppde = new TMultiGraph();
      mg_dppde->SetName(("mg_dppde_" + string(tmp)).c_str());

      //
      gr_dppde = new TGraph(n_e, &e_gev[0], &diff_flux[0]);
      char gr_name[1000];
      sprintf(gr_name, "dppde_%s_%s_m%d", model.c_str(), final_state.c_str(), (int)par_spec[0]);
      gr_dppde->SetName(gr_name);
      gr_dppde->SetTitle("");
      gr_dppde->SetLineColor(kBlack);
      gr_dppde->SetLineWidth(2);
      gr_dppde->SetLineStyle(1);
      gr_dppde->GetXaxis()->SetTitle("E [GeV]");
      gr_dppde->GetYaxis()->SetTitle(y_axis_dppde.c_str());
      mg_dppde->Add(gr_dppde, "L");

      leg_dppde->AddEntry(gr_dppde, legend.c_str(), "L");

      if (par_spec[3] > SMALL_NUMBER) {
         gr_dppde_atsource = new TGraph(n_e, &e_gev[0], &diff_flux_atsource[0]);
         sprintf(gr_name, "dppde_atsource_%s_%s_m%d", model.c_str(), final_state.c_str(), (int)par_spec[0]);
         gr_dppde_atsource->SetName(gr_name);
         gr_dppde_atsource->SetTitle("");
#if IS_ALPHA_METHOD
         gr_dppde_atsource->SetLineColorAlpha(kBlack, 0.3);
#else
         gr_dppde_atsource->SetLineColor(kBlack);
#endif
         gr_dppde_atsource->SetLineWidth(5);
         gr_dppde_atsource->SetLineStyle(2);
         gr_dppde_atsource->GetXaxis()->SetTitle("E [GeV]");
         gr_dppde_atsource->GetYaxis()->SetTitle(y_axis_dppde.c_str());
         mg_dppde->Add(gr_dppde_atsource, "L");
         leg_dppde->AddEntry(gr_dppde_atsource, legend_atsource.c_str(), "L");
      }

      // print integrated flux result:
      leg_dppde->AddEntry((TObject *)0, tmp_erange, "h");

      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->cd();
         gr_dppde->Write();
         if (par_spec[3] > SMALL_NUMBER) gr_dppde_atsource->Write();
      }

      // Draw canvas
      c_dppde = new TCanvas("c_dppde", "c_dppde", 0, 0, 650, 500);
      c_dppde->SetLogx(1);
      c_dppde->SetLogy(1);
      // Plot graphs and legends
      mg_dppde->Draw("A");
      gSIM_CLUMPYAD->Draw();
      mg_dppde->GetYaxis()->SetRange(dppde_min, dppde_max);
      mg_dppde->GetXaxis()->SetTitle("E [GeV]");
      mg_dppde->GetYaxis()->SetTitle(y_axis_dppde.c_str());
      leg_dppde->Draw("SAME");
      c_dppde->Update();
      c_dppde->Modified();


      if (gSIM_IS_WRITE_ROOTFILES) {
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }

      if (gSIM_IS_PRINT) {
         cout << "_______________________" << endl << endl;
      }

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);


      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
      }

      // Free memory
      if (mg_dnde) delete mg_dnde;
      mg_dnde = NULL;
      if (leg_dnde) delete leg_dnde;
      leg_dnde = NULL;
      if (c_dnde) delete c_dnde;
      c_dnde = NULL;

      if (gr_dppde) delete gr_dppde;
      gr_dppde = NULL;
      delete leg_dppde;
      if (leg_dppde) leg_dppde = NULL;
      if (c_dppde) delete c_dppde;
      c_dppde = NULL;
   }
#endif
}

