/*! \file cosmo.cc \brief (see cosmo.h) */

// CLUMPY includes
#include "../include/cosmo.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/params.h"
#include "../include/misc.h"
#include "../include/clumps.h"

// C++ std libraries
using namespace std;
#include <iomanip>
#include <cstring>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <vector>

// GSL includes
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_chebyshev.h>

//______________________________________________________________________________
void compute_class_pk(double z)
{
   //--- Uses CLASS to compute the linear and nonlinear matter power spectrum p(k,z) at redshift z.
   //    Writes on disk file the output where [k]=h Mpc^-1 and [p(k,z)]=h^-3 Mpc^3.
   //    NOTE: We use CLASS for the computation up to kmax *h = 10000 and use the analytic
   //    result P(k) ~ k^(n_s-4) * ln^2(k) for larger k.
   //
   //  z             Redshift


   bool is_extrapolate = false;
   double kmax = gSIM_EXTRAGAL_KMAX_PRECOMP;
   double kmax_class = 1e4;
   if (z > 3.5) kmax_class = 1e4;

   if (kmax > kmax_class) {
      const int buflen = 1000;
      char char_tmp[buflen];
      snprintf(char_tmp, buflen, "Will use CLASS only to compute P(k) up to k = %d, "
              "and extrapolate linear P(k) ~ k^(n_s-4) * ln^2(k) for larger k.", int(kmax_class));
      print_warning("cosmo.cc", "compute_class_pk()", string(char_tmp));
      is_extrapolate = true;
      kmax = kmax_class;
   }

   char const *gPATH_TO_CLASS_tmp = getenv("CLASS");

   string line = "";
   ostringstream convert;
   convert << round(gCOSMO_HUBBLE, 3);
   string h_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_M, 3);
   string OmegaM_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_B, 4);
   string OmegaB_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_LAMBDA, 3);
   string OmegaL_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B, 3);
   string OmegaCDM_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_K, 3);
   string OmegaK_str = convert.str();
   convert.str("");
   convert << gCOSMO_N_S;
   string ns_str = convert.str();
   convert.str("");
   convert << gCOSMO_TAU_REIO;
   string tau_str = convert.str();
   convert.str("");
   convert << kmax;
   string kmax_str = convert.str();
   convert.str("");

   const int buflen = 25;
   char char_tmp[buflen];
   snprintf(char_tmp, buflen, "%.3g", z);
   if (z > 10) snprintf(char_tmp, buflen, "%.5g", z);
   string zstr = string(char_tmp);

   string filename_base = "h" + h_str + "_OmegaB"
                          + OmegaB_str + "_OmegaM" + OmegaM_str + "_OmegaL" + OmegaL_str
                          + "_ns" + ns_str + "_tau" + tau_str + "_z" + zstr;

   string filename_template = "PREFIX_" + filename_base + "_XX.dat";

   if (gPATH_TO_CLASS_tmp == NULL) {
      printf("\n====> ERROR: compute_class_pk() in cosmo.cc");
      printf("\n             Environmental variable CLASS not set.");
      printf("\n             You have two options:");
      printf("\n             1.)  Set CLASS variable and point it to an installation of CLASS, http://class-code.net/.");
      printf("\n             2.)  Provide manually an ASCII file with the name %s", filename_template.c_str());
      printf("\n                  in the directory %s/pk_precomp/", gPATH_TO_CLUMPY_DATA.c_str());
      printf("\n                  and PREFIX of your choice, and XX either 'lin' or 'nl', if the linear");
      printf("\n                  or non-linear power spectrum is required, respectively.");
      printf("\n                  The file must have two columns containing ");
      printf("\n                  k in units of [h/Mpc] and the P(k) in units of [(Mpc/h)^3].");
      printf("\n                  Make also sure that kmax >= %d in that case,", int(ceil(gSIM_EXTRAGAL_KMAX_PRECOMP)));
      printf("\n                  or lower the hidden parameter gSIM_EXTRAGAL_KMAX_PRECOMP.\n");
      printf("\n             => abort()\n\n");
      abort();
   } else gPATH_TO_CLASS = string(gPATH_TO_CLASS_tmp);

   string infile = gPATH_TO_CLUMPY_DATA +"/pk_precomp/class_input_template.ini";
   string inifile = gPATH_TO_CLASS + "/class_input_for_clumpy.ini";

   string command = "cd " + gPATH_TO_CLASS +
                    "; mkdir -p output_tmp/; mv output/* output_tmp/";

   sys_safeexe(command);

   ifstream filein(infile.c_str());
   ofstream myfile(inifile.c_str());

   while (getline(filein, line)) {
      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      if (line.substr(0, 1) == string("h")) line = "h = " + h_str;
      else if (line.substr(0, 7) == string("Omega_b")) line = "Omega_b = " + OmegaB_str;
      else if (line.substr(0, 9) == string("Omega_cdm")) line = "Omega_cdm = " + OmegaCDM_str;
      else if (line.substr(0, 7) == string("Omega_k")) line = "Omega_k = " + OmegaK_str;
      else if (line.substr(0, 3) == string("n_s")) line = "n_s = " + ns_str;
      else if (line.substr(0, 8) == string("tau_reio")) line = "tau_reio = " + tau_str;
      else if (line.substr(0, 13) == string("P_k_max_h/Mpc")) line = "P_k_max_h/Mpc = " + kmax_str;
      else if (line[0] == 'z')line = "z_pk = " + zstr;
      myfile << line << endl;
   }
   filein.close();
   myfile.close();

   string filename_lin = "class_" + filename_base + "_lin.dat";

   string filename_nl = "class_" + filename_base + "_nl.dat";

   command = "cd " + gPATH_TO_CLASS +
             "; ./class class_input_for_clumpy.ini; cp output/class_input_for_clumpy00_pk.dat "
             + gPATH_TO_CLUMPY_DATA +"/pk_precomp/" + filename_lin + "; "
             + "cp output/class_input_for_clumpy00_pk_nl.dat "
             + gPATH_TO_CLUMPY_DATA +"/pk_precomp/" + filename_nl + "; "
             + "rm -f output/*; mv output_tmp/* output; rm -rf output_tmp; "
             ;//+ "rm class_input_for_clumpy.ini";

   sys_safeexe(command);

   if (is_extrapolate) {
      // we will append extrapolated P(k) ~ k^(n_s-4) * ln(k) to the file.
      vector<double> linear_lnk_vec;
      vector<double> linear_lnp_vec;
      string file = gPATH_TO_CLUMPY_DATA +"/pk_precomp/" + filename_lin;
      vector< vector<double> > table = read_ascii(file);
      for (int i = 0; i < int(table.size()); ++i) {
         for (int j = 0; j < int(table[i].size()); ++j) {
            if (j == 0) linear_lnk_vec.push_back(log(table[i][j]));
            else if (j == 1) linear_lnp_vec.push_back(log(table[i][j]));
         }
      }

      int n_k_old = int(linear_lnk_vec.size());
      double delta_lnk = linear_lnk_vec[linear_lnk_vec.size() - 1] - linear_lnk_vec[linear_lnk_vec.size() - 2];

      // calculate normalisation:
      double exponent = gCOSMO_N_S - 4.;
      double lnnorm = linear_lnp_vec[linear_lnp_vec.size() - 1] - exponent * linear_lnk_vec[linear_lnk_vec.size() - 1] - 2. * log(linear_lnk_vec[linear_lnk_vec.size() - 1] + log(gCOSMO_HUBBLE));


      while (linear_lnk_vec[linear_lnk_vec.size() - 1] < log(gSIM_EXTRAGAL_KMAX_PRECOMP)) {
         linear_lnk_vec.push_back(linear_lnk_vec[linear_lnk_vec.size() - 1] + delta_lnk);
         linear_lnp_vec.push_back(lnnorm + exponent * linear_lnk_vec[linear_lnk_vec.size() - 1] + 2. * log(linear_lnk_vec[linear_lnk_vec.size() - 1] + log(gCOSMO_HUBBLE)));
      };
      int n_k_new = int(linear_lnk_vec.size());

      FILE *fp;
      fp = fopen(file.c_str(), "a");
      for (int i = n_k_old; i < n_k_new; ++i) {
         fprintf(fp, "       %.12e       %.12e \n", exp(linear_lnk_vec[i]), exp(linear_lnp_vec[i]));
      }
      fclose(fp);
   }

   return;
}

//______________________________________________________________________________
vector<double>  compute_massfunction(const double z, const vector<double> &Mh_grid, const int card_mf,
                                     const vector<double>  &linear_lnk_vec, const vector<double>  &linear_lnp_vec,
                                     const int card_window, const int card_growth_mthd)
{
   //--- Computes dn/dln(Mh)  (the number density per mass bin) at redshift z.
   //    The mass function parametrisation is selected by card_mf = kTINKER08, kJENKINS01, kSHETHTORMEN99....
   //    Note 1: Mh is in units of Omega_m0 h^-1 Msol
   //    Note 2: dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).
   //
   // INPUTS:
   //  z                Redshift z
   //  Mh_grid          Mass values, length n_m, unit [Omega_m0 h^-1 Msol]
   //  card_mf          Selects the mass function to use (e.g. Tinker, Jenkins, etc.)
   //  linear_lnk_vec   k values for p_k
   //  linear_lnp_vec   lnp, must have same dimension as linear_lnk_vec
   //  card_window      Window function
   //  card_growth_mthd Selects the method to obtain P(k,z): either P(k,z) is directly given (kPKZ_FROMFILE),
   //                   or P(k,0) is given, and sigma^2(z) is calculated from a growth factor model.
   // OUTPUT:
   //  dNdVhdlnMh_z     Vector of mass function over tabulated masses at redshift z,
   //                   length n_m, units [halos/ (h^-1 Mpc)^3].

   double rho_critperh2_MsolperMpc3 = RHO_CRITperh2_MSOLperKPC3  / KPC3_to_MPC3 ; // *  in units of h^2 * Msol Mpc^-3
   vector<double> dNdVhdlnMh_z;

   const int buflen = 50;
   char char_tmp[buflen];
   snprintf(char_tmp, buflen, "%.3g", z);
   if (z > 10) snprintf(char_tmp, buflen, "%.5g", z);
   string zstr = string(char_tmp);

   cout << " Computing mass function for z = " << zstr << " and given cosmology ...";

   // check sizes:
   if (linear_lnk_vec.size() != linear_lnp_vec.size()) {
      printf("\n====> ERROR: compute_massfunction() in cosmo.cc");
      printf("\n             Dimension of input arrays do not match:");
      printf("\n             len(linear_lnk_grid) = %d", int(linear_lnk_vec.size()));
      printf("\n             len(linear_lnplnkz[) = %d", int(linear_lnp_vec.size()));
      printf("\n             => abort()\n\n");
      abort();
   }

   // Determine now the reference m_delta for mf descriptions.
   // We ca either (i) convert all masses via the mdelta1_to_mdelta2 trafo, or
   // (ii) sometimes, different fitting formulae are provided in the literature for different Delta.
   double Delta_c_mf;
   vector<double> Mh_Delta_grid;
   if (!gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE) {
      // use interpolations from literature, if available.
      Delta_c_mf = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, z);
      Mh_Delta_grid = Mh_grid;
   } else {
      printf("\n ... transform Delta from halo profile ...");
      // calculate mf for Delta_crit = 200 (178, ...), and connect to input delta
      // by mdelta1_to_mdelta2 conversion:
      if (gEXTRAGAL_FLAG_MASSFUNCTION == kJENKINS01 or
            gEXTRAGAL_FLAG_MASSFUNCTION == kPRESSSCHECHTER74 or
            gEXTRAGAL_FLAG_MASSFUNCTION == kSHETHTORMEN99) {
         Delta_c_mf = 178; // w.r.t. the critical density
      } else if (gEXTRAGAL_FLAG_MASSFUNCTION == kRODRIGUEZPUEBLA16_PLANCK) {
         Delta_c_mf = delta_x_to_delta_crit(-1, kBRYANNORMAN98, z); // w.r.t. the critical density
      } else {
         Delta_c_mf = 200; // w.r.t. the critical density
      }
      double Delta_in = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, z);  // w.r.t. the critical density
      double par_prof[4] = {gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][0],
                            gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][1],
                            gHALO_SUBS_SHAPE_PARAMS[kEXTRAGAL][2],
                            double(gHALO_SUBS_FLAG_PROFILE[kEXTRAGAL])
                           };
      if (gEXTRAGAL_FLAG_CDELTAMDELTA == kPRADA12_200) {
         print_warning("cosmo.cc", "compute_massfunction()",
                       "Mdelta conversion might fail for large masses and kPRADA12_200 cdelta-mdelta parametrization.");
      }
      for (int j = 0; j < (int)Mh_grid.size(); j++) {
         // get the M_200,c (M_178,c) corresponding to user-chosen input mass_Delta_in,c:
         double Mh_Delta_mf = gCOSMO_M_to_MH * mdelta1_to_mdelta2(Mh_grid[j] / gCOSMO_M_to_MH, Delta_in,
                              gEXTRAGAL_FLAG_CDELTAMDELTA, par_prof, z, Delta_c_mf);
         // cout  << Mh_Delta_mf << endl;
         Mh_Delta_grid.push_back(Mh_Delta_mf);
      }
      printf(" done.\n");
   }

   //Get nu (nu_deriv[0]) and d ln nu / dlnMh (nu_deriv[1]):
   vector< vector<double> > nu_deriv = nu_andderiv(z, Mh_Delta_grid, linear_lnk_vec,  linear_lnp_vec,  card_window, card_growth_mthd);

   //Now, compute mass function!
   for (int j = 0; j < (int)Mh_Delta_grid.size(); j++) {
      double  dNdVhdlnMh = nu_deriv[1][j] / Mh_Delta_grid[j] * rho_critperh2_MsolperMpc3 * mf(card_mf, nu_deriv[0][j], z, Delta_c_mf);
      if (gDM_IS_IDM) {
         double factor1 = pow(1 + gEXTRAGAL_IDM_MHALFMODE / Mh_Delta_grid[j] * gCOSMO_M_to_MH / gEXTRAGAL_IDM_BETA, gEXTRAGAL_IDM_ALPHA);
         double factor2 = pow(1 + gEXTRAGAL_IDM_MHALFMODE / Mh_Delta_grid[j] * gCOSMO_M_to_MH / gEXTRAGAL_IDM_GAMMA, gEXTRAGAL_IDM_DELTA);
         dNdVhdlnMh *= (factor1 * factor2);
      }
      if (dNdVhdlnMh < 1e-40) dNdVhdlnMh = 1e-40;
      // if (Mh_grid[j] / gCOSMO_M_to_MH >= 1e10)
      //    cout <<  "z = " << z << "\t M = " <<  Mh_grid[j] / gCOSMO_M_to_MH << "\t dNdVhdlnMh = " <<  dNdVhdlnMh * pow(gCOSMO_HUBBLE,3)  << endl; abort();
      dNdVhdlnMh_z.push_back(dNdVhdlnMh);
   }
   cout << " done." << endl;

   return dNdVhdlnMh_z;
}

//______________________________________________________________________________
double d_to_z(const double &d, const int switch_d)
{
   //--- Returns the redshift from a given distance.
   //
   //  d         Distance [Mpc]
   //  switch_d  Distance to calculate: 0 (d_c), 1 (d_trans), 2(d_l), or 3 (d_a)

   gsl_function F;
   F.function = &solve_d_to_z;
   d_to_z_params_for_rootfinding params = {d, switch_d};
   F.params = &params;
   double zmin = 0.;
   double zmax = 1e6;
   int return_status = 0;
   return rootsolver_gsl(gsl_root_fsolver_brent, F, zmin, zmax, gSIM_EPS, return_status);
}

//______________________________________________________________________________
double dh_c(const double &z)
{
   //--- Returns the comoving line-of-sight distance in [h^-1 Mpc] by
   //    for the cosmology defined by the global gCOSMO_OMEGA parameters.
   //
   //  z       Redshift

   gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
   double eta, error;
   gsl_function F;
   F.function = &H0_over_H;

   gsl_integration_qags(&F, 0, z, 0, gSIM_EPS, 1000, w, &eta, &error);
   gsl_integration_workspace_free(w);
   return HUBBLE_LENGTHxh_Mpc * eta;
}

//______________________________________________________________________________
double dh_trans(const double &z)
{
   //--- Returns the comoving transverse distance in [h^-1 Mpc] by integration
   //    for the cosmology defined by the global gCOSMO_OMEGA parameters.
   //
   //  z         Redshift

   // do again computation to save a division in the result
   gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
   double eta, error;
   gsl_function F;
   F.function = &H0_over_H;

   gsl_integration_qags(&F, 0, z, 0, gSIM_EPS, 1000, w, &eta, &error);
   gsl_integration_workspace_free(w);

   double result;
   if ((abs(gCOSMO_OMEGA0_K)) < 1.e-5) result = HUBBLE_LENGTHxh_Mpc * eta;  //ok0=gCOSMO_OMEGA0_K// //Hubble distance//
   else if (gCOSMO_OMEGA0_K > 0)       result = HUBBLE_LENGTHxh_Mpc * sinh(sqrt(gCOSMO_OMEGA0_K) * eta) / sqrt(gCOSMO_OMEGA0_K);  //ok0=gCOSMO_OMEGA0_K// //Hubble distance//
   else                                result = HUBBLE_LENGTHxh_Mpc * sin(sqrt(-gCOSMO_OMEGA0_K) * eta) / sqrt(-gCOSMO_OMEGA0_K); //ok0=gCOSMO_OMEGA0_K// //Hubble distance//
   return result;
}

//______________________________________________________________________________
double dh_a(const double &z)
{
   //--- Returns the angular diameter distance dh_a=dh_trans/(1+z) in [h^-1 Mpc] by
   //    integration for the cosmology defined by the global gCOSMO_OMEGA parameters.
   //
   //  z       Redshift

   return dh_trans(z) / (1 + z);
}

//______________________________________________________________________________
double dh_l(const double &z)
{
   //--- Returns the luminosity distance dh_l=dc*(1+z) in [h^-1 Mpc] by integration
   //    for the cosmology defined by the global gCOSMO_OMEGA parameters.
   //
   //  z       Redshift

   return dh_trans(z) * (1 + z);
}

//______________________________________________________________________________
double  dNdVhdlnMh_interpol(const double &z, const double &Mh)
{
   //--- Returns dn/dln(Mh)  (the number density per mass bin) for redshift z
   //    and mass M by interpolating the values stored in gCOSMO_DNDVHDLNMH_Z.
   //    Note 1: Mh is in units of Omega_m0 h^-1 Msol
   //    Note 2: dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).
   //
   //  z        Redshift z
   //  Mh       Mass in unit [Omega_m0 h^-1 Msol]

   // return error if base grids are empty (check only one of them; if this one is ok
   //  but the others not, an error will be drawn in the interpolation function):
   if (gCOSMO_Z_GRID.empty()) {
      printf("\n====> ERROR: dNdVhdlnMh_interpol() in cosmo.cc");
      printf("\n             gCOSMO_Z_GRID grid is empty. Please fill it before");
      printf("\n             via init_extragal().");
      printf("\n             => abort()\n\n");
      abort();
   }

   return interp2D(z, Mh, gCOSMO_Z_GRID, gCOSMO_MH_GRID, gCOSMO_DNDVHDLNMH_Z, kLINLOG);
}

//______________________________________________________________________________
void dNdVhdlnMh_integrand_Mh(double &Mh, double par_z[1], double &res)
{
   //--- Same as dNdVhdlnMh_interpol(), but written to be integrated with
   //    simpson_lin/log_adapt over Mh.
   //
   // INPUTS:
   //  Mh        Mass [Omega_m0 h^-1 Msol]
   //  par_z[0]  Redshift z of the halo hosting the sub-clumps
   // OUTPUTS:
   //  res       dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).

   res = dNdVhdlnMh_interpol(par_z[0] /*redshift*/, Mh); // h^3 Mpc^-3
   return;
}

//______________________________________________________________________________
void dNdVhdlnMh_integrand_logMh(double &logMh, double par_z[1], double &res)
{
   //--- Same as dNdVhdlnMh_interpol(), but written to be integrated with
   //    simpson_lin/log_adapt over logMh.
   //
   // INPUTS:
   //  logMh     log(mass) [Omega_m0 h^-1 Msol]
   //  par_z[0]  Redshift z of the halo hosting the sub-clumps
   // OUTPUTS:
   //  res       dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).

   res = dNdVhdlnMh_interpol(par_z[0] /*redshift*/, exp(logMh)); // h^3 Mpc^-3
   return;
}

//______________________________________________________________________________
void dNdVhdlnMh_sigmoid(double &Mh, double par[3], double &res)
{
   //--- Computes the mass function truncated at low masses with a sigmoid function.
   //    Can be integrated with simpson_lin/log_adapt over Mh.
   //
   // INPUTS:
   //  M        Mass [Omega_m0 h^-1 Msol]
   //  par[0]   Redshift z of the halo hosting the sub-clumps
   //  par[1]   Mhmin [Omega_m0 h^-1 Msol], where the mass function is cut off.
   //  par[2]   Zigma [Omega_m0 h^-1 Msol], the sharpness of the cut-off.
   // OUTPUTS:
   //  res      dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).

   res = sigmoid_window(Mh, par[1] /*Mhmin*/, par[2] /*sigma*/) * dNdVhdlnMh_interpol(par[0] /*redshift*/, Mh); // h^3 Mpc^-3
   return;
}



//______________________________________________________________________________
double  dNdVhdlnMh_literature(const double &z, const double &Mh_var, const int card_mf, vector<string> &label_literature)
{
   //--- Returns dn/dln(Mh)  (the number density per mass bin) for redshift z and mass M.
   //    from the curves in published papers.
   //    Note 1: Mh is in units of Omega_m0 h^-1 Msol
   //    Note 2: dn/dln(Mh) = Mh * dn/d(Mh) = M dn/dM, in units of halos/ (h^-1 Mpc)^3 (M in units of Msol).
   //
   //  z        Redshift z
   //  Mh_var   Mass [h^-1 Msol]
   //  card_mf  Mass function card profile (from gENUM_MASSFUNCTION)

   int n_points;
   vector<double> log10_Mh_base; // [Msol/h]
   vector<double> log10_dNdlnMh_base;
   const int buflen = 50;
   char Delta_c_char[buflen];

   switch (card_mf) {
      case kRODRIGUEZPUEBLA16_PLANCK: {
            string label_base = "Rodriguez-Puebla et al. (2016)";

            string Delta_str;
            if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
               double tmp = delta_x_to_delta_crit(-1, kBRYANNORMAN98, z);
               snprintf(Delta_c_char, buflen, "%d", int(round(delta_crit_to_delta_x(tmp, kRHO_MEAN, z))));
               Delta_str = "#Delta_{m} = " + string(Delta_c_char);
            } else {
               snprintf(Delta_c_char, buflen, "%.1f", delta_x_to_delta_crit(-1, kBRYANNORMAN98, z));
               Delta_str = "#Delta_{c} = " + string(Delta_c_char);
            }
            vector<vector<double> > rodriguez16_fig7;
            if (z <= 0.75) {
               n_points = 29;
               double rodriguez16_fig7_arr_z0[29][2] = {
                  {6.69e9, 0.737},
                  {9.85e9, 0.522},
                  {1.41e10, 0.369},
                  {2.20e10, 0.245},
                  {3.47e10, 0.164},
                  {4.61e10, 0.126},
                  {6.48e10, 0.0944},
                  {1.74e11, 0.0392},
                  {2.56e11, 0.0273},
                  {4.66e11, 0.0163},
                  {1.24e12, 0.00695},
                  {2.59e12, 0.00360},
                  {4.03e12, 0.00241},
                  {6.35e12, 0.00161},
                  {1.01e13, 0.00106},
                  {1.62e13, 6.83e-4},
                  {2.59e13, 4.33e-4},
                  {4.11e13, 2.72e-4},
                  {6.62e13, 1.63e-4},
                  {9.95e13, 9.91e-5},
                  {1.58e14, 5.46e-5},
                  {2.57e14, 2.66e-5},
                  {3.24e14, 1.79e-5},
                  {4.16e14, 1.18e-5},
                  {5.16e14, 7.49e-6},
                  {6.35e14, 4.78e-6},
                  {7.72e14, 3.01e-6},
                  {9.21e14, 1.93e-6},
                  {1.15e15, 1.03e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z0[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z0[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z0[i][1]));
               }
               static bool is_rodriguez0_added = false;
               if (!is_rodriguez0_added) {
                  label_literature.push_back("#splitline{" + label_base + ", z = 0, " + Delta_str + "}"
                                             "{(#Omega_{#Lambda},#Omega_{m},#Omega_{b},#sigma_{8},h,n_{s}) = (0.693,0.307,0.048,0.829,0.678,0.96)}");
                  is_rodriguez0_added = true;
               }
            } else if (z > 0.75 and z <= 1.5) {
               n_points = 22;
               double rodriguez16_fig7_arr_z1[22][2] = {
                  {1.72e10, 0.349},
                  {6.65e10, 0.104},
                  {1.03e11, 0.0672},
                  {1.47e11, 0.0489},
                  {2.28e11, 0.0325},
                  {4.18e11, 0.0186},
                  {7.49e11, 0.0107},
                  {1.47e12, 0.00564},
                  {2.65e12, 0.00309},
                  {4.22e12, 0.00189},
                  {6.58e12, 0.00117},
                  {1.08e13, 6.47e-4},
                  {1.61e13, 3.90e-4},
                  {2.55e13, 2.09e-4},
                  {4.16e13, 9.91e-5},
                  {6.55e13, 4.44e-5},
                  {9.35e13, 2.19e-5},
                  {1.27e14, 1.10e-5},
                  {1.66e14, 5.60e-6},
                  {2.09e14, 2.88e-6},
                  {2.43e14, 1.84e-6},
                  {2.88e14, 1.02e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z1[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z1[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z1[i][1]));
               }
               static bool is_rodriguez1_added = false;
               if (!is_rodriguez1_added) {
                  label_literature.push_back(label_base + ", z = 1, " + Delta_str);
                  is_rodriguez1_added = true;
               }
            } else if (z > 1.5 and z <= 2.5) {
               n_points = 20;
               double rodriguez16_fig7_arr_z2[20][2] = {
                  {6.51e9, 0.927},
                  {1.22e10, 0.500},
                  {2.57e10, 0.242},
                  {5.73e10, 0.113},
                  {1.24e11, 0.0521},
                  {2.43e11, 0.0262},
                  {5.14e11, 0.0116},
                  {1.01e12, 0.00545},
                  {2.01e12, 0.00234},
                  {4.05e12, 8.95e-4},
                  {6.55e12, 4.36e-4},
                  {1.01e13, 2.09e-4},
                  {1.40e13, 1.18e-4},
                  {2.08e13, 5.13e-5},
                  {3.36e13, 1.68e-5},
                  {4.16e13, 9.29e-6},
                  {5.06e13, 5.27e-6},
                  {6.09e13, 2.94e-6},
                  {7.26e13, 1.72e-6},
                  {8.35e13, 1.00e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z2[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z2[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z2[i][1]));
               }
               static bool is_rodriguez2_added = false;
               if (!is_rodriguez2_added) {
                  label_literature.push_back(label_base + ", z = 2, " + Delta_str);
                  is_rodriguez2_added = true;
               }
            } else if (z > 2.5 and z <= 3.5) {
               n_points = 21;
               double rodriguez16_fig7_arr_z3[21][2] = {
                  {6.55e9,  0.865},
                  {9.02e9,  0.616},
                  {1.51e10, 0.367},
                  {2.57e10, 0.209},
                  {4.29e10, 0.124},
                  {9.65e10, 0.0506},
                  {1.59e11, 0.0279},
                  {2.60e11, 0.0156},
                  {4.11e11, 0.00855},
                  {8.05e11, 0.00343},
                  {1.29e12, 0.00168},
                  {2.02e12, 8.12e-4},
                  {3.24e12, 3.57e-4},
                  {5.03e12, 1.45e-4},
                  {6.97e12, 7.21e-5},
                  {1.01e13, 2.91e-5},
                  {1.22e13, 1.70e-5},
                  {1.59e13, 8.14e-6},
                  {2.02e13, 3.91e-6},
                  {2.51e13, 1.81e-6},
                  {2.96e13, 9.93e-7},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z3[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z3[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z3[i][1]));
               }
               static bool is_rodriguez3_added = false;
               if (!is_rodriguez3_added) {
                  label_literature.push_back(label_base + ", z = 3, " + Delta_str);
                  is_rodriguez3_added = true;
               }
            } else if (z > 3.5 and z <= 4.5) {
               n_points = 17;
               double rodriguez16_fig7_arr_z4[17][2] = {
                  {6.41e9, 0.763},
                  {1.85e10, 0.240},
                  {3.36e10, 0.117},
                  {6.22e10, 0.0554},
                  {1.22e11, 0.0228},
                  {2.46e11, 0.00873},
                  {4.18e11, 0.00388},
                  {8.01e11, 0.00131},
                  {1.31e12, 5.22e-4},
                  {2.14e12, 1.86e-4},
                  {3.08e12, 7.94e-5},
                  {4.05e12, 3.97e-5},
                  {5.35e12, 1.86e-5},
                  {6.97e12, 8.43e-6},
                  {8.83e12, 3.83e-6},
                  {1.09e13, 1.89e-6},
                  {1.28e13, 1.01e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z4[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z4[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z4[i][1]));
               }
               static bool is_rodriguez4_added = false;
               if (!is_rodriguez4_added) {
                  label_literature.push_back(label_base + ", z = 4, " + Delta_str);
                  is_rodriguez4_added = true;
               }
            } else if (z > 4.5 and z <= 5.5) {
               n_points = 17;
               double rodriguez16_fig7_arr_z5[17][2] = {
                  {6.41e9,  0.544},
                  {1.08e10, 0.286},
                  {2.01e10, 0.130},
                  {4.03e10, 0.0506},
                  {6.28e10, 0.0262},
                  {9.85e10, 0.0131},
                  {1.56e11, 0.00622},
                  {2.53e11, 0.00267},
                  {3.93e11, 0.00116},
                  {6.19e11, 4.48e-4},
                  {9.50e11, 1.69e-4},
                  {1.50e12, 5.28e-5},
                  {1.95e12, 2.50e-5},
                  {2.44e12, 1.26e-5},
                  {3.16e12, 5.57e-6},
                  {4.07e12, 2.29e-6},
                  {5.01e12, 1.03e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z5[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z5[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z5[i][1]));
               }
               static bool is_rodriguez5_added = false;
               if (!is_rodriguez5_added) {
                  label_literature.push_back(label_base + ", z = 5, " + Delta_str);
                  is_rodriguez5_added = true;
               }
            } else if (z > 5.5 and z <= 6.5) {
               n_points = 13;
               double rodriguez16_fig7_arr_z6[13][2] = {
                  {6.55e9,  0.333},
                  {1.60e10, 0.0918},
                  {4.05e10, 0.0219},
                  {6.32e10, 0.0101},
                  {9.85e10, 0.00455},
                  {1.58e11, 0.00178},
                  {3.00e11, 4.39e-4},
                  {4.71e11, 1.47e-4},
                  {7.53e11, 4.06e-5},
                  {1.02e12, 1.72e-5},
                  {1.33e12, 6.85e-6},
                  {1.90e12, 1.97e-6},
                  {2.25e12, 1.01e-6},

               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z6[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z6[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z6[i][1]));
               }
               static bool is_rodriguez6_added = false;
               if (!is_rodriguez6_added) {
                  label_literature.push_back(label_base + ", z = 6, " + Delta_str);
                  is_rodriguez6_added = true;
               }
            } else if (z > 6.5 and z <= 7.5) {
               n_points = 12;
               double rodriguez16_fig7_arr_z7[12][2] = {
                  {6.48e9,  0.191},
                  {1.42e10, 0.0554},
                  {2.64e10, 0.0193},
                  {4.24e10, 0.00814},
                  {7.97e10, 0.00231},
                  {1.53e11, 5.36e-4},
                  {2.68e11, 1.29e-4},
                  {3.89e11, 4.50e-5},
                  {5.35e11, 1.67e-5},
                  {7.11e11, 6.57e-6},
                  {9.40e11, 2.51e-6},
                  {1.19e12, 1.02e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z7[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z7[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z7[i][1]));
               }
               static bool is_rodriguez7_added = false;
               if (!is_rodriguez7_added) {
                  label_literature.push_back(label_base + ", z = 7, " + Delta_str);
                  is_rodriguez7_added = true;
               }
            } else if (z > 7.5 and z <= 8.5) {
               n_points = 14;
               double rodriguez16_fig7_arr_z8[14][2] = {
                  {6.45e9,  0.0893},
                  {1.24e10, 0.0277},
                  {2.02e10, 0.0111},
                  {3.31e10, 0.00406},
                  {4.96e10, 0.00168},
                  {6.28e10, 9.59e-4},
                  {9.55e10, 3.49e-4},
                  {1.53e11, 9.57e-5},
                  {1.96e11, 4.69e-5},
                  {2.55e11, 2.16e-5},
                  {3.20e11, 9.95e-6},
                  {4.22e11, 3.80e-6},
                  {5.22e11, 1.73e-6},
                  {6.00e11, 1.03e-6},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z8[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z8[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z8[i][1]));
               }
               static bool is_rodriguez8_added = false;
               if (!is_rodriguez8_added) {
                  label_literature.push_back(label_base + ", z = 8, " + Delta_str);
                  is_rodriguez8_added = true;
               }
            } else if (z > 8.5 and z <= 9.5) {
               n_points = 12;
               double rodriguez16_fig7_arr_z9[12][2] = {
                  {6.45e9,  0.0301},
                  {1.03e10, 0.0118},
                  {1.72e10, 0.00402},
                  {2.59e10, 0.00154},
                  {3.97e10, 5.55e-4},
                  {6.09e10, 1.72e-4},
                  {9.95e10, 4.14e-5},
                  {1.30e11, 1.79e-5},
                  {1.60e11, 8.67e-6},
                  {1.99e11, 4.08e-6},
                  {2.48e11, 1.83e-6},
                  {2.90e11, 9.93e-7},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(rodriguez16_fig7, &rodriguez16_fig7_arr_z9[i][0], n_points);
                  log10_Mh_base.push_back(log10(rodriguez16_fig7_arr_z9[i][0]));
                  log10_dNdlnMh_base.push_back(log10(rodriguez16_fig7_arr_z9[i][1]));
               }
               static bool is_rodriguez9_added = false;
               if (!is_rodriguez9_added) {
                  label_literature.push_back(label_base + ", z = 9, " + Delta_str);
                  is_rodriguez9_added = true;
               }
            } else return 0;
            double log10_res = interp1D(log10(Mh_var), log10_Mh_base, log10_dNdlnMh_base, kSPLINE, true);
            return pow(10, log10_res) / log(10);
         }
      case kTINKER08_N:
      case kTINKER08: {
            if (z > 0) {
               return 0;
            }

            double Delta_c0 = delta_x_to_delta_crit(gCOSMO_DELTA0, gCOSMO_FLAG_DELTA_REF, 0);
            double Delta_m0 = delta_crit_to_delta_x(Delta_c0, kRHO_MEAN, 0);
            snprintf(Delta_c_char, buflen, "%d", int(round(delta_x_to_delta_crit(200, kRHO_MEAN, z))));
            string Delta_m0_200_str = string(Delta_c_char);
            snprintf(Delta_c_char, buflen, "%d", int(round(delta_x_to_delta_crit(800, kRHO_MEAN, z))));
            string Delta_m0_800_str = string(Delta_c_char);
            snprintf(Delta_c_char, buflen, "%d", int(round(delta_x_to_delta_crit(3200, kRHO_MEAN, z))));
            string Delta_m0_3200_str = string(Delta_c_char);

            double tinker08_fig5_arr[22][2];
            string Delta0_str;
            n_points = 22;
            if (Delta_m0 < 700) {
               double tmp[22][2] = {
                  {10.01, -1.55 },
                  {10.51, -1.50 },
                  {11.05, -1.43 },
                  {11.59, -1.37 },
                  {12.11, -1.30 },
                  {12.63, -1.23 },
                  {13.08, -1.19 },
                  {13.46, -1.16 },
                  {13.79, -1.17 },
                  {14.08, -1.21 },
                  {14.36, -1.29 },
                  {14.55, -1.39 },
                  {14.77, -1.55 },
                  {14.93, -1.75 },
                  {15.06, -1.93 },
                  {15.18, -2.16 },
                  {15.28, -2.40 },
                  {15.36, -2.61 },
                  {15.43, -2.83 },
                  {15.50, -3.09 },
                  {15.56, -3.35 },
                  {15.61, -3.60 }
               };
               memcpy(tinker08_fig5_arr, tmp, sizeof(tinker08_fig5_arr));
               if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) Delta0_str = "#Delta_{m} = 200";
               else Delta0_str = "#Delta_{c} = " + Delta_m0_200_str;
            } else if (Delta_m0 < 2000) {
               double tmp[22][2] = {
                  {10.02, -1.53 },
                  {10.29, -1.51 },
                  {10.63, -1.49 },
                  {10.96, -1.46 },
                  {11.44, -1.43 },
                  {11.78, -1.40 },
                  {12.22, -1.36 },
                  {12.72, -1.33 },
                  {13.07, -1.31 },
                  {13.49, -1.31 },
                  {13.93, -1.38 },
                  {14.22, -1.47 },
                  {14.44, -1.61 },
                  {14.66, -1.81 },
                  {14.80, -1.98 },
                  {14.94, -2.21 },
                  {15.07, -2.47 },
                  {15.16, -2.70 },
                  {15.23, -2.93 },
                  {15.30, -3.16 },
                  {15.36, -3.37 },
                  {15.41, -3.59 }
               };
               memcpy(tinker08_fig5_arr, tmp, sizeof(tinker08_fig5_arr));
               if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) Delta0_str = "#Delta_{m} = 800";
               else Delta0_str = "#Delta_{c} = " + Delta_m0_800_str;
            } else {
               double tmp[22][2] = {
                  {10.01, -1.57 },
                  {10.55, -1.55 },
                  {10.98, -1.53 },
                  {11.51, -1.51 },
                  {11.85, -1.50 },
                  {12.30, -1.49 },
                  {12.72, -1.49 },
                  {13.01, -1.50 },
                  {13.35, -1.53 },
                  {13.64, -1.59 },
                  {13.85, -1.66 },
                  {14.07, -1.76 },
                  {14.24, -1.89 },
                  {14.40, -2.03 },
                  {14.53, -2.20 },
                  {14.64, -2.37 },
                  {14.71, -2.50 },
                  {14.79, -2.68 },
                  {14.87, -2.86 },
                  {14.95, -3.09 },
                  {15.03, -3.33 },
                  {15.10, -3.60 }
               };
               memcpy(tinker08_fig5_arr, tmp, sizeof(tinker08_fig5_arr));
               if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) Delta0_str = "#Delta_{m} = 3200";
               else Delta0_str = "#Delta_{c} = " + Delta_m0_3200_str;
            }
            vector<vector<double> > tinker08_fig5;
            for (int i = 0; i < n_points; ++i) {
               pushback_array2vector(tinker08_fig5, &tinker08_fig5_arr[i][0], n_points);
               log10_Mh_base.push_back(tinker08_fig5[i][0]);
               log10_dNdlnMh_base.push_back(tinker08_fig5[i][1]);
            }
            double log10_res = interp1D(log10(Mh_var), log10_Mh_base, log10_dNdlnMh_base, kSPLINE, true);
            static bool is_tinker_added = false;
            if (!is_tinker_added) {
               label_literature.push_back("#splitline{Tinker et al. (2008), WMAP1, z = 0, " + Delta0_str + "}"
                                          "{(#Omega_{#Lambda},#Omega_{m},#Omega_{b},#sigma_{8},h,n_{s}) = (0.7,0.3,0.04,0.9,0.7,1)}");
               is_tinker_added = true;
            }
            // rescale and return values:
            return pow(10, log10_res) * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3 / Mh_var * gCOSMO_OMEGA0_M;
         }
      case kTINKER10:
         break;
      case kBOCQUET16_DMONLY:
      case kBOCQUET16_HYDRO: {
            string label_base = "Bocquet et al. (2016, hydro)";
            snprintf(Delta_c_char, buflen, "%d", int(round(delta_x_to_delta_crit(200, kRHO_MEAN, z))));
            string Delta0_str;
            if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) Delta0_str = "#Delta_{m} = 200";
            else Delta0_str = "#Delta_{c} = " + string(Delta_c_char);
            vector<vector<double> > bocquet16_fig2;
            if (z <= 0.3) {
               n_points = 15;
               double bocquet16_fig2_arr_z0[15][2] = {
                  {6.39e11, 0.00210},
                  {2.14e12, 7.37e-4},
                  {8.55e12, 2.22e-4},
                  {2.41e13, 8.11e-5},
                  {8.84e13, 2.19e-5},
                  {2.24e14, 7.03e-6},
                  {4.29e14, 2.57e-6},
                  {7.30e14, 9.22e-7},
                  {1.22e15, 2.77e-7},
                  {1.69e15, 1.04e-7},
                  {2.57e15, 2.35e-8},
                  {3.57e15, 5.56e-9},
                  {4.79e15, 1.18e-9},
                  {6.03e15, 2.92e-10},
                  {7.92e15, 3.74e-11},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(bocquet16_fig2, &bocquet16_fig2_arr_z0[i][0], n_points);
                  log10_Mh_base.push_back(log10(bocquet16_fig2_arr_z0[i][0] * gCOSMO_HUBBLE));
                  log10_dNdlnMh_base.push_back(log10(bocquet16_fig2_arr_z0[i][1]));
               }
               static bool is_bocqet1_added = false;
               if (!is_bocqet1_added) {
                  label_literature.push_back("#splitline{" + label_base + ", z = 0, " + Delta0_str + "}"
                                             "{(#Omega_{#Lambda},#Omega_{m},#Omega_{b},#sigma_{8},h,n_{s}) = (0.728,0.272,0.0456,0.809,0.704,0.963)}");
                  is_bocqet1_added = true;
               }
            } else if (z > 0.3 and z <= 0.8) {
               n_points = 17;
               double bocquet16_fig2_arr_z0_5[17][2] = {
                  {6.62e11, 0.00201},
                  {1.44e12, 9.78e-4},
                  {2.87e12, 5.31e-4},
                  {6.65e12, 2.37e-4},
                  {1.09e13, 1.46e-4},
                  {2.32e13, 6.60e-5},
                  {4.33e13, 3.10e-5},
                  {9.22e13, 1.16e-5},
                  {1.44e14, 5.65e-6},
                  {3.11e14, 1.37e-6},
                  {5.76e14, 2.90e-7},
                  {8.94e14, 7.48e-8},
                  {1.40e15, 1.22e-8},
                  {1.89e15, 2.83e-9},
                  {2.46e15, 6.69e-10},
                  {3.13e15, 1.42e-10},
                  {4.02e15, 2.03e-11},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(bocquet16_fig2, &bocquet16_fig2_arr_z0_5[i][0], n_points);
                  log10_Mh_base.push_back(log10(bocquet16_fig2_arr_z0_5[i][0] * gCOSMO_HUBBLE));
                  log10_dNdlnMh_base.push_back(log10(bocquet16_fig2_arr_z0_5[i][1]));
               }
               static bool is_bocqet2_added = false;
               if (!is_bocqet2_added) {
                  label_literature.push_back(label_base + ", z = 0.5, " + Delta0_str);
                  is_bocqet2_added = true;
               }
            } else if (z > 0.8 and z <= 1.6) {
               n_points = 16;
               double bocquet16_fig2_arr_z1_2[16][2] = {
                  {7.00e11, 0.00184},
                  {3.08e12, 4.00e-4},
                  {7.70e12, 1.40e-4},
                  {1.81e13, 4.40e-5},
                  {3.64e13, 1.54e-5},
                  {6.64e13, 5.18e-6},
                  {1.14e14, 1.59e-6},
                  {1.87e14, 4.39e-7},
                  {2.72e14, 1.44e-7},
                  {3.60e14, 5.39e-8},
                  {5.00e14, 1.36e-8},
                  {6.81e14, 3.15e-9},
                  {8.88e14, 7.30e-10},
                  {1.08e15, 2.20e-10},
                  {1.29e15, 6.32e-11},
                  {1.50e15, 1.94e-11},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(bocquet16_fig2, &bocquet16_fig2_arr_z1_2[i][0], n_points);
                  log10_Mh_base.push_back(log10(bocquet16_fig2_arr_z1_2[i][0] * gCOSMO_HUBBLE));
                  log10_dNdlnMh_base.push_back(log10(bocquet16_fig2_arr_z1_2[i][1]));
               }
               static bool is_bocqet3_added = false;
               if (!is_bocqet3_added) {
                  label_literature.push_back(label_base + ", z = 1.2, " + Delta0_str);
                  is_bocqet3_added = true;
               }
            } else if (z > 1.6 and z <= 2.5) {
               n_points = 14;
               double bocquet16_fig2_arr_z2[14][2] = {
                  {6.90e11, 0.00142},
                  {1.62e12, 5.19e-4},
                  {3.13e12, 2.22e-4},
                  {5.58e12, 9.66e-5},
                  {9.24e12, 4.40e-5},
                  {1.66e13, 1.58e-5},
                  {2.91e13, 5.07e-6},
                  {5.81e13, 8.82e-7},
                  {1.01e14, 1.54e-7},
                  {1.57e14, 2.86e-8},
                  {2.35e14, 5.10e-9},
                  {3.27e14, 8.50e-10},
                  {4.41e14, 1.42e-10},
                  {5.72e14, 2.03e-11},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(bocquet16_fig2, &bocquet16_fig2_arr_z2[i][0], n_points);
                  log10_Mh_base.push_back(log10(bocquet16_fig2_arr_z2[i][0] * gCOSMO_HUBBLE));
                  log10_dNdlnMh_base.push_back(log10(bocquet16_fig2_arr_z2[i][1]));
               }
               static bool is_bocqet4_added = false;
               if (!is_bocqet4_added) {
                  label_literature.push_back(label_base + ", z = 2, " + Delta0_str);
                  is_bocqet4_added = true;
               }
            } else return 0;

            double log10_res = interp1D(log10(Mh_var), log10_Mh_base, log10_dNdlnMh_base, kSPLINE, true);
            return pow(10, log10_res) / pow(gCOSMO_HUBBLE, 3);
         }
      case kSHETHTORMEN99:
         break;
      case kPRESSSCHECHTER74:
         break;
      case kJENKINS01: {
            vector<vector<double> > springel05_fig2;
            string label_base = "Springel et al. (2005)";
            string Delta0_str;
            snprintf(Delta_c_char, buflen, "%d", int(round(delta_x_to_delta_crit(180, kRHO_MEAN, z))));
            if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) Delta0_str = "#Delta_{m} = 180";
            else Delta0_str = "#Delta_{c} = " + string(Delta_c_char);
            if (z <= 1.) {
               n_points = 27;
               double springel05_fig2_arr_z0[27][2] = {
                  {10.00, -1.64},
                  {10.40, -1.55},
                  {10.72, -1.49},
                  {11.13, -1.43},
                  {11.52, -1.38},
                  {11.99, -1.34},
                  {12.44, -1.30},
                  {12.93, -1.27},
                  {13.36, -1.23},
                  {13.77, -1.23},
                  {14.05, -1.25},
                  {14.30, -1.31},
                  {14.46, -1.39},
                  {14.64, -1.49},
                  {14.79, -1.62},
                  {14.94, -1.79},
                  {15.06, -1.98},
                  {15.16, -2.16},
                  {15.26, -2.39},
                  {15.43, -2.85},
                  {15.55, -3.25},
                  {15.65, -3.65},
                  {15.80, -4.28},
                  {15.84, -4.57},
                  {15.92, -5.04},
                  {15.97, -5.37},
                  {16.00, -5.52},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(springel05_fig2, &springel05_fig2_arr_z0[i][0], n_points);
                  log10_Mh_base.push_back(springel05_fig2_arr_z0[i][0]);
                  log10_dNdlnMh_base.push_back(springel05_fig2_arr_z0[i][1]);
               }
               static bool is_springel1_added = false;
               if (!is_springel1_added) {
                  label_literature.push_back("#splitline{" + label_base + ", z = 0, " + Delta0_str + "}"
                                             "{(#Omega_{#Lambda},#Omega_{m},#Omega_{b},#sigma_{8},h,n_{s}) = (0.75,0.25,0.045,0.9,0.73,1)}");
                  is_springel1_added = true;
               }
            } else if (z > 1. and z <= 2.) {
               n_points = 24;
               double springel05_fig2_arr_z1_5[24][2] = {
                  {10.00, -1.48},
                  {10.33, -1.45},
                  {10.76, -1.43},
                  {11.11, -1.40},
                  {11.56, -1.37},
                  {11.97, -1.37},
                  {12.32, -1.37},
                  {12.68, -1.40},
                  {12.97, -1.46},
                  {13.20, -1.55},
                  {13.45, -1.70},
                  {13.62, -1.83},
                  {13.79, -2.00},
                  {13.93, -2.19},
                  {14.15, -2.55},
                  {14.29, -2.86},
                  {14.43, -3.17},
                  {14.57, -3.60},
                  {14.69, -4.04},
                  {14.77, -4.35},
                  {14.88, -4.84},
                  {14.96, -5.24},
                  {15.02, -5.55},
                  {15.05, -5.74},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(springel05_fig2, &springel05_fig2_arr_z1_5[i][0], n_points);
                  log10_Mh_base.push_back(springel05_fig2_arr_z1_5[i][0]);
                  log10_dNdlnMh_base.push_back(springel05_fig2_arr_z1_5[i][1]);
               }
               static bool is_springel2_added = false;
               if (!is_springel2_added) {
                  label_literature.push_back(label_base + ", z = 1.5, " + Delta0_str);
                  is_springel2_added = true;
               }
            } else if (z > 2. and z <= 4.) {
               n_points = 26;
               double springel05_fig2_arr_z3_06[26][2] = {
                  {10.00, -1.46},
                  {10.44, -1.47},
                  {10.79, -1.47},
                  {11.13, -1.49},
                  {11.49, -1.52},
                  {11.71, -1.58},
                  {11.93, -1.63},
                  {12.08, -1.70},
                  {12.28, -1.78},
                  {12.43, -1.87},
                  {12.59, -1.99},
                  {12.76, -2.15},
                  {12.90, -2.29},
                  {13.07, -2.51},
                  {13.15, -2.61},
                  {13.28, -2.86},
                  {13.37, -3.03},
                  {13.47, -3.22},
                  {13.58, -3.46},
                  {13.66, -3.69},
                  {13.77, -3.99},
                  {13.84, -4.21},
                  {13.93, -4.51},
                  {14.03, -4.87},
                  {14.11, -5.24},
                  {14.22, -5.70},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(springel05_fig2, &springel05_fig2_arr_z3_06[i][0], n_points);
                  log10_Mh_base.push_back(springel05_fig2_arr_z3_06[i][0]);
                  log10_dNdlnMh_base.push_back(springel05_fig2_arr_z3_06[i][1]);
               }
               static bool is_springel3_added = false;
               if (!is_springel3_added) {
                  label_literature.push_back(label_base + ", z = 3.06, " + Delta0_str);
                  is_springel3_added = true;
               }
            } else if (z > 4. and z <= 8.) {
               n_points = 15;
               double springel05_fig2_arr_z5_72[15][2] = {
                  {10.00, -1.72},
                  {10.20, -1.75},
                  {10.44, -1.81},
                  {10.72, -1.91},
                  {10.98, -2.04},
                  {11.25, -2.23},
                  {11.52, -2.44},
                  {11.76, -2.67},
                  {11.90, -2.86},
                  {12.08, -3.11},
                  {12.23, -3.36},
                  {12.45, -3.75},
                  {12.70, -4.35},
                  {12.93, -4.99},
                  {13.12, -5.69},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(springel05_fig2, &springel05_fig2_arr_z5_72[i][0], n_points);
                  log10_Mh_base.push_back(springel05_fig2_arr_z5_72[i][0]);
                  log10_dNdlnMh_base.push_back(springel05_fig2_arr_z5_72[i][1]);
               }
               static bool is_springel4_added = false;
               if (!is_springel4_added) {
                  label_literature.push_back(label_base + ", z = 5.72, " + Delta0_str);
                  is_springel4_added = true;
               }
            } else if (z > 8. and z <= 15.) {
               n_points = 10;
               double springel05_fig2_arr_z10_07[10][2] = {
                  {10.00, -2.82},
                  {10.19, -2.98},
                  {10.41, -3.23},
                  {10.63, -3.50},
                  {10.80, -3.71},
                  {10.95, -3.98},
                  {11.17, -4.35},
                  {11.34, -4.69},
                  {11.51, -5.11},
                  {11.79, -5.66},
               };
               for (int i = 0; i < n_points; ++i) {
                  pushback_array2vector(springel05_fig2, &springel05_fig2_arr_z10_07[i][0], n_points);
                  log10_Mh_base.push_back(springel05_fig2_arr_z10_07[i][0]);
                  log10_dNdlnMh_base.push_back(springel05_fig2_arr_z10_07[i][1]);
               }
               static bool is_springel4_added = false;
               if (!is_springel4_added) {
                  label_literature.push_back(label_base + ", z = 10.07, " + Delta0_str);
                  is_springel4_added = true;
               }
            } else return 0;

            double log10_res = interp1D(log10(Mh_var), log10_Mh_base, log10_dNdlnMh_base, kSPLINE, true);
            return pow(10, log10_res) * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3 / Mh_var * gCOSMO_OMEGA0_M;
         }
      default :
         printf("\n====> ERROR: dNdVhdlnMh_literature() in cosmo.cc");
         printf("\n             card_mf = %d not a valid mass function!", card_mf);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0;
}

//______________________________________________________________________________
double dNdOmega(double &zmin, double &zmax, double const &Mmin, double const &Mmax)
{
   //--- Returns the mean number of DM haloes per steradian, dN/dOmega [sr^-1],
   //    between [zmin-zmax] and [Mmin-Mmax]
   //
   //  zmin     Min redshift of interval
   //  zmax     Max redshift of interval
   //  Mmin     Min mass of interval [Msol]
   //  Mmax     Max mass of interval [Msol]

   double Mhmin = Mmin * gCOSMO_M_to_MH;
   double Mhmax = Mmax * gCOSMO_M_to_MH;
   double par[2] = {Mhmin, Mhmax};
   double res = 0.;
   simpson_lin_adapt(dNdOmegadz, zmin, zmax, par, res, gSIM_EPS);
   return res;
}

//______________________________________________________________________________
void dNdOmegadlnMh(double &lnMh, double par_z[2], double &res)
{
   //--- Computes the number of halos per lnMh and solid angle in the redshift
   //    interval [zmin, zmax]. Can be integrated with simpson_lin/log_adapt over log mass.
   //
   //    DO NOT use for double integration first over z and then over lnMh.
   //    Instead, it is much smarter (faster) to integrate first over lnMh
   //    (with dNdOmegadz() over dNdVhdlnMh_integrand_logMh), and then over z
   //    (dNdOmega() is already designed to be integrated over dNdOmegadz() after lnMh).
   //
   // INPUTS:
   //  lnMh       log mass in units [Omega_m0 h^-1 Msol]
   //  par_z[0]   zmin
   //  par_z[1]   zmax
   // OUTPUTS:
   //  res        dN/dz/dOmega [sr^-1].

   double par_Mh[1] = {exp(lnMh)};
   double zmin = par_z[0];
   double zmax = par_z[1];
   res = 0.;
   simpson_lin_adapt(dNdOmegadlnMhdz, zmin, zmax, par_Mh, res, gSIM_EPS);
   return;
}

//______________________________________________________________________________
void dNdOmegadz(double &z, double par_Mh[2], double &res)
{
   //--- Computes the number of halos per redshift and solid angle in the mass
   //    interval [Mhmin, Mhmax]. Can be integrated with simpson_lin/log_adapt over z.
   //
   // INPUTS:
   //  z           redshift
   //  par_Mh[0]   Mhmin [Omega_m0 h^-1 Msol]
   //  par_Mh[1]   Mhmax [Omega_m0 h^-1 Msol]
   // OUTPUTS:
   //  res         dN/dz/dOmega [sr^-1].

   double par_z[1] = {z};
   double lnMhmin = log(par_Mh[0]);
   double lnMhmax = log(par_Mh[1]);
   res = 0.;
   simpson_lin_adapt(dNdVhdlnMh_integrand_logMh, lnMhmin, lnMhmax, par_z, res, gSIM_EPS);
   res *= dVhdzdOmega(z); // # of halos per sr per between z and z+dz
   return;
}

//______________________________________________________________________________
void dNdOmegadlnMhdz(double &z, double par_Mh[1], double &res)
{
   //--- Computes the number of halos per steradian, log mass, and redshift.
   //    Can be integrated with simpson_lin/log_adapt over z.
   //
   //    DO NOT use for double integration first over z and then over lnMh.
   //    Instead, it is much faster to integrate first over lnMh
   //    (with dNdOmegadz() over dNdVhdlnMh_integrand_logMh), and then over z
   //    (dNdOmega() is already designed to be integrated over dNdOmegadz() after lnMh).
   //
   // INPUTS:
   //  z           Redshift
   //  par_Mh[0]   %ass [Omega_m0 h^-1 Msol]
   // OUTPUTS:
   //  res         dN/dln(Mh)/dz/dOmega = Mh * dN/d(Mh)/dz/dOmega = M dN/dM/dz/dOmega, in units of halos/ steradian.

   res = dNdVhdlnMh_interpol(z, par_Mh[0]); // h^3 Mpc^-3
   res *= dVhdzdOmega(z); // # of halos per sr per between z and z+dz
   return;
}

//______________________________________________________________________________
double ds2dlnk(double lnk, void *p)
{
   //--- Returns d(sigma^2)/d(lnk) \propto k^3 P(k) W(kr) where r is the scale over which.
   //
   //  lnk    Value
   //  p      Parameters for integration

   pk_params_for_integration *params = (struct pk_params_for_integration *)p;
   double rh = (params->rh);
   vector<double> vec_lnk = (params->vec_lnk);
   vector<double> linear_lnpkz = (params->linear_lnpkz);
   int card_window = params->card_window;

   double k = exp(lnk);
   double x = k * rh;

   // choose window function:
   double win = window_fnct(x, card_window);
   // Given tabulated linear P(k,z), interpolate the value of p(k) for in log(k):
   double linear_pk = exp(interp1D(log(k), vec_lnk, linear_lnpkz, kSPLINE));
   return pow(k, 3.) * (linear_pk * pow(win, 2));
}

//______________________________________________________________________________
double dVhdzdOmega(double z)
{
   //--- Returns the differential comoving volume dV/dz element per steradian at
   //    redshift z [h^-3 Mpc^3 sr^-1].
   //
   //  z      Redshift

   return pow(dh_trans(z), 2.) * (HUBBLE_LENGTHxh_Mpc * H0_over_H(z)); //Hubble distance//
}

//______________________________________________________________________________
string legend_for_delta()
{
   //--- Returns a meaningful delta subscript according to the delta choice.

   const int buflen = 50;
   char Delta_char[buflen];
   snprintf(Delta_char, buflen, "%g", gCOSMO_DELTA0);
   string Delta_str = string(Delta_char);
   if (gCOSMO_FLAG_DELTA_REF == kRHO_CRIT) {
      return Delta_str + ",c";
   } else if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
      return Delta_str + ",m";
   } else if (gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98) {
      if (Delta_str == "-1") return "Bryan&Norman";
      else return Delta_str + ",Bryan&Norman";
   } else {
      return "unknown";
   }
}

//______________________________________________________________________________
double  lnsigma2(double lnRh, void *p)
{
   //--- Returns ln(2pi^2/D(z)^2 * sigma^2) for the scale R (D(z): growth factor)
   //    integration is performed between kmin=1.e-4 and kmax=20./R...
   //    Upper bound ensures enough modes are considered, without integrating over unnecessary region?
   //
   //  lnRh      ln of scale radius [Mpc h^-1]
   //  p         Parameters for integration

   pk_params_for_integration *params = (struct pk_params_for_integration *)p;
   params->rh = exp(lnRh);

   const int buflen = 500;
   char char_tmp[buflen];

   double lnkmin_in = find_min_value(params->vec_lnk);
   double lnkmax_in = find_max_value(params->vec_lnk);
   double lnkmin = log(1.e-3 / params->rh);
   double lnkmax = log(1.e2 / params->rh);
   if (lnkmin_in > lnkmin) {
      snprintf(char_tmp, buflen, "Integration truncated by limits of input k grid: k_min = %g -> %g for scale %g",
              exp(lnkmin), exp(lnkmin_in), 2 * PI / params->rh);
      print_warning("cosmo.cc", "lnsigma2()", string(char_tmp));
      lnkmin = lnkmin_in;
   }
   if (lnkmax_in < lnkmax) {
      snprintf(char_tmp, buflen, "Integration truncated by limits of input k grid: k_max = %g -> %g for scale %g",
              exp(lnkmax), exp(lnkmax_in), 2 * PI / params->rh);
      print_warning("cosmo.cc", "lnsigma2()", string(char_tmp));
      lnkmax = lnkmax_in;
   }

   gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
   double res, error;
   gsl_function F;
   F.function = &ds2dlnk;
   F.params = params;

   double eps = gSIM_EPS;
   if (gSIM_EXTRAGAL_FLAG_WINDOWFUNC == kSHARP_K) eps *= 1e-2;
   // integration over ln(k):
   gsl_integration_qags(&F, lnkmin, lnkmax, 0, eps, 1000, w, &res, &error);
   gsl_integration_workspace_free(w);

   return log(res);
}

//______________________________________________________________________________
double gamma_f(const int card_window)
{
   //--- Returns gamma_f corresponding to the window function in k-space
   //    Adapted from van den Bosch presentation slides,
   //    http://www.astro.yale.edu/vdbosch/astro610_lecture9.pdf
   //
   //  card_window   Choice of window function

   double res;
   // Calculate W(x)
   switch (card_window) {
      case kTOP_HAT:
         res = 4.* PI / 3.;
         break;
      case kGAUSS:
         res = pow(2. * PI, 1.5);
         break;
      case kSHARP_K:
         res = 6 * pow(PI, 2.);
         break;
      default :
         printf("\n====> ERROR: gamma_f() in cosmo.cc");
         printf("\n             window card_window=%d does not correspond to any window function", card_window);
         printf("\n             => abort()\n\n");
         abort();
   }
   return res;
}

//______________________________________________________________________________
void get_pk(const double &z, vector<double>  &lnk_vec, vector<double>  &lnp_vec, const bool is_nonlinear, const bool is_force_recompute)
{
   //--- Computes the matter power spectrum at the redshifts z
   //
   // INPUT
   //  z                   Redshift
   //  is_nonlinear        Linear or non-linear P(k)
   //  is_force_recompute  Force re-fill
   //
   // OUTPUTS              [If the p_matter(k,z) file is not present, calls CLASS to compute it]
   //  lnk_vec             Array with mode number ln(k) [k in h Mpc^-1]
   //  lnp_vec             Array in linear_lnpkz with ln(p_matter(k,z)) [Mpc^3 h^-3]


   // Because we push back elements to a vector accessed by reference,
   // we want to make sure that it is empty at this point:
   lnk_vec.clear();
   lnp_vec.clear();

   ostringstream convert;
   convert << round(gCOSMO_HUBBLE, 3);
   string h_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_M, 3);
   string OmegaM_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_B, 4);
   string OmegaB_str = convert.str();
   convert.str("");
   convert << round(gCOSMO_OMEGA0_LAMBDA, 3);
   string OmegaL_str = convert.str();
   convert.str("");
   convert << gCOSMO_N_S;
   string ns_str = convert.str();
   convert.str("");
   convert << gCOSMO_TAU_REIO;
   string tau_str = convert.str();
   convert.str("");

   double zz = z;
   if (z <= 1e-5) zz = 0.;

   const int buflen = 25;
   char char_tmp[buflen];
   snprintf(char_tmp, buflen, "%.3g", zz);
   if (z > 10) snprintf(char_tmp, buflen, "%.5g", zz);
   string zstr = string(char_tmp);

   convert.str("");

   string file = "dummy";
   string path = gPATH_TO_CLUMPY_DATA +"/pk_precomp/";

   string filename_base = "h" + h_str + "_OmegaB"
                          + OmegaB_str + "_OmegaM" + OmegaM_str + "_OmegaL" + OmegaL_str
                          + "_ns" + ns_str + "_tau" + tau_str + "_z" + zstr;

   string file_pattern;

   if (is_nonlinear) {
      file_pattern = "_" + filename_base + "_nl.dat";
   } else {
      file_pattern = "_" + filename_base + "_lin.dat";
   }

   string command = "find " + path + "*" + file_pattern;

   string found_files = sys_command(command);
   string prefix;
   if (found_files != "") {
      prefix = found_files.substr(found_files.find_last_of("\\/") + 1, found_files.size());
      file = path + prefix;
      prefix = prefix.substr(0, prefix.find_first_of("_"));
   }
   file = removeblanks_from_startstop(file);

   ifstream ifile(file.c_str());

   if (!ifile or is_force_recompute) {
      ifile.close();
      if (is_force_recompute) cout << " => Force recomputation of P(k,z) file for" << endl;
      else cout << " => P(k,z) file does not exist for" << endl;
      cout << " (z,h,OmegaB,OmegaM,OmegaL,ns,tau) = ("
           + zstr + "," + h_str + "," + OmegaB_str + "," + OmegaM_str + "," + OmegaL_str
           + "," + ns_str + "," + tau_str + ")." << endl;
      cout << "   => Running CLASS to compute it ..." << endl;
      compute_class_pk(zz);
      file = path + "class" + file_pattern;
      ifstream ifile_check(file.c_str());
      if (!ifile_check) {
         printf("\n====> ERROR: get_pk() in cosmo.cc");
         printf("\n             Something went wrong in computing P(k,z) with CLASS (probably aborted.)");
         printf("\n             => abort()\n\n");
         abort();
      }
      ifile.open(file.c_str());
   } else {
      if (is_nonlinear) cout << " Loading non-linear P(k) file '" << prefix << "' for z = " << zstr << " and given cosmology ";
      else cout << " Loading linear P(k) file '" << prefix << "' for z = " << zstr << " and given cosmology ";
   }

   vector< vector<double> > table = read_ascii(file);


   for (int i = 0; i < int(table.size()); ++i) {
      for (int j = 0; j < int(table[i].size()); ++j) {
         if (j == 0) lnk_vec.push_back(log(table[i][j]));
         else if (j == 1) {
            double pk = table[i][j];
            if (gDM_KMAX > 0) {
               pk *= (1 - sigmoid_window(table[i][0], gDM_KMAX, gDM_KMAX_SIGMA_CUTOFF));
               if (pk < 1e-40) pk = 1e-40;
            }
            lnp_vec.push_back(log(pk));
         } else {
            printf("\n====> ERROR: get_pk() in cosmo.cc");
            printf("\n             File %s contains more than two columns", file.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
      }
   }

   // check if loaded linear P(k) exists up to gSIM_EXTRAGAL_KMAX_PRECOMP:
   double kmax = exp(find_max_value(lnk_vec));
   static int please_break = 0; // don"t get lost in an infinite loop...
   if (!is_nonlinear and kmax < gSIM_EXTRAGAL_KMAX_PRECOMP) {
      please_break += 1;
      if (please_break < 2) {
         // load recursively:
         cout << "\n   => P(k,z) file does not exist up to k_max = " << gSIM_EXTRAGAL_KMAX_PRECOMP << endl;
         cout << "   => Running CLASS to compute it ..." << endl;
         compute_class_pk(zz);
         get_pk(z, lnk_vec, lnp_vec);
      } else {
         printf("\n====> ERROR: get_pk() in cosmo.cc");
         printf("\n             Abort after one unsuccessful trial to recompute P(k) file.");
         printf("\n             => abort()\n\n");
         abort();
      }
   } else {
      please_break = 0;
      cout << "...done." << endl;
   }
   return;
}

//______________________________________________________________________________
double growth_factor(const double &z, const int card_growth_mthd)
{
   //--- Returns the linear growth rate factor of linear density perturbations.
   //
   //  z                Redshift
   //  card_growth_mthd Approximation/Model of the linear growth rate

   if (z == 0) return 1.;

   double res;
   // Calculate growth factor:
   switch (card_growth_mthd) {
      case kCARROLL92: {
            // approximation to integral solution from Heath (1977)
            double a = 1. / (1. + z);
            double omegaM0_var = 1. / (1 +  gCOSMO_OMEGA0_LAMBDA / gCOSMO_OMEGA0_M);
            double omegaL0_var = 1 - omegaM0_var;
            double omegaM_var = 1. / (1 + gCOSMO_OMEGA0_LAMBDA / gCOSMO_OMEGA0_M * pow(a, 3));
            double omegaL_var = 1 - omegaM_var;
            // discard factor 2.5 in numerators for ratio:
            double g0 = omegaM0_var / (pow(omegaM0_var, 4. / 7.)   - omegaL0_var + (1. + omegaM0_var / 2.) / (1. + omegaL0_var / 70.));
            double gz = omegaM_var * a / (pow(omegaM_var, 4. / 7.) - omegaL_var + (1. + omegaM_var / 2.) / (1. + omegaL_var  / 70.));
            res = gz / g0;
            break;
         }
      case kHEATH77: {
            // exact analytical solution from Heath (1977)
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
            double res_int0, res_intz, error;
            gsl_function F;
            F.function = &growth_integrand;
            gsl_integration_qagiu(&F, 0, 0, gSIM_EPS, 1000, w, &res_int0, &error);
            gsl_integration_qagiu(&F, z, 0, gSIM_EPS, 1000, w, &res_intz, &error);
            gsl_integration_workspace_free(w);
            // discard factor 2.5 Omega_M in numerators for ratio:
            res =   H_over_H0(z) * res_intz / res_int0;
            break;
         }
      default :
         printf("\n====> ERROR: growth_factor() in cosmo.cc");
         printf("\n             growth factor card = %d does not correspond to any model", card_growth_mthd);
         printf("\n             => abort()\n\n");
         abort();
   }
   return res;
}

//______________________________________________________________________________
double growth_integrand(const double z, void *params)
{
   //--- Returns (1+z) * H_0^3/H(z)^3 to be integrated with gsl
   //
   //  z         Redshift
   //  params    Dummy for function parameters

   return (1 + z) * pow(H2_over_H02(z), -1.5);
}

//______________________________________________________________________________
double mf(int card_mf, double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value.
   //
   //  card_mf   Mass function parametrization (from gENUM_MASSFUNCTION)
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density

   switch (card_mf) {
      case kRODRIGUEZPUEBLA16_PLANCK:
         return  mf_bolshoi_planck(nu, z, Delta_c);
      case kBOCQUET16_HYDRO:
         return  mf_magneticum(nu, z, Delta_c, true);
      case kBOCQUET16_DMONLY:
         return  mf_magneticum(nu, z, Delta_c, false);
      case kTINKER08:
         return  mf_tinker(nu, z, Delta_c);
      case kTINKER10:
         return  mf_tinker10(nu, z, Delta_c);
      case kTINKER08_N:
         return  mf_tinker_norm(nu, z, Delta_c);
      case kSHETHTORMEN99:
         return  mf_sheth_tormen(nu, z, Delta_c);
      case kPRESSSCHECHTER74:
         return  mf_press_schechter(nu, z, Delta_c);
      case kJENKINS01:
         return  mf_jenkins(nu, z, Delta_c);
      default :
         printf("\n====> ERROR: mf() in cosmo.cc");
         printf("\n             card_mf = %d not a valid mass function!", card_mf);
         printf("\n             => abort()\n\n");
         abort();
   }
   return 0;
}

//______________________________________________________________________________
double mf_bolshoi_planck(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value for kRODRIGUEZPUEBLA16_PLANCK (Bolshoi simulation).
   //    Halo multiplicity function per log(nu): 0.5*f(sigma), where sigma=deltac/sqrt(nu).
   //      => \int_{-infty}^infty mf(lnnu,z) dlnnu = 1 is NOT satisfied!
   //    Ref: Eq.(25) of Rodriguez-Puebla (2016), after Tinker et al. (2008)
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density

   double Delta_c_check = delta_x_to_delta_crit(-1, kBRYANNORMAN98, z);

   if (fabs(Delta_c - Delta_c_check) > 0.5) {
      printf("\n\n====> ERROR: mf_bolshoi_planck() in cosmo.cc");
      printf("\n             Rodriguez-Puebla (2016) HMF parametrization is only");
      printf("\n             given for values Delta_c(z=%.f) = %d", z, int(Delta_c_check));
      printf("\n             (scaling according to Bryan & Norman, 1998)");
      printf("\n             If you want to use a different Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double params[4][3] = {
      {0.144, -0.011,  0.003},
      {1.351,  0.068,  0.006},
      {3.113, -0.077, -0.013},
      {1.187,  0.009,  0.000},
   };

   double A, a, b, c;

   A = params[0][0] + params[0][1] * z + params[0][2] * pow(z, 2);
   a = params[1][0] + params[1][1] * z + params[1][2] * pow(z, 2);
   b = params[2][0] + params[2][1] * z + params[2][2] * pow(z, 2);

   if (b < 0) {
      static bool is_print_msg = false;
      if (!is_print_msg) {
         const int buflen = 500;
         char char_tmp[buflen];
         snprintf(char_tmp, buflen, "b<0 obtained at z>=%g (causes negative root). Set b=0 resulting in extrapolation f(sigma)=A*exp(-c/sigma^2).", z);
         print_warning("cosmo.cc", "mf_bolshoi_planck()", string(char_tmp));
         is_print_msg = true;
      }
      b = 1e-40;
   }
   c = params[3][0] + params[3][1] * z + params[3][2] * pow(z, 2);

   double sigma = DELTA_COLLAPSE / sqrt(nu);
   return 0.5 * (A * (pow(sigma / b, -a) + 1.) * exp(-c / pow(sigma, 2.)));
}

//______________________________________________________________________________
double mf_jenkins(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value for kJENKINS01.
   //    Halo multiplicity function per log(nu): 0.5*f(sigma), where sigma=deltac/sqrt(nu).
   //      =W \int_{-infty}^infty mf(lnnu) dlnnu = 1 is NOT satisfied!
   //    Ref: Eq.(B3) of Jenkins et al. (2001)
   //    Delta_c = 178
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density

   double Delta_m = delta_crit_to_delta_x(178, kRHO_MEAN, z);
   if (Delta_c < 177.5 or Delta_c > 178.5) {
      printf("\n\n====> ERROR: mf_jenkins() in cosmo.cc");
      printf("\n             Jenkins et al. (2001) HMF parametrization is ");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         printf("\n             given for Delta_m(z) = %d (Delta_c = 178)", int(round(Delta_m)));
      } else if (gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98) {
         double Delta_bn = delta_crit_to_delta_x(178, kBRYANNORMAN98, z);
         printf("\n             given for Delta_Bryan() = %d (Delta_c = 178)", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_c = 178 (Delta_m(z) = %d)", int(round(Delta_m)));
      printf("\n             If you want to use a different Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double sigma = DELTA_COLLAPSE / sqrt(nu);
   return 0.5 * 0.301 * exp(-pow(abs(log(1. / sigma) + 0.64), 3.82)); // Delta=180
}

//______________________________________________________________________________
double mf_magneticum(double const &nu, double const &z, double const &Delta_c, const bool is_hydro)
{
   //--- Returns halo mass function value.kBOCQUET16_DMONLY or kBOCQUET16_HYDRO/
   //    Halo multiplicity function per log(nu): 0.5*f(sigma), where sigma=deltac/sqrt(nu).
   //      => \int_{-infty}^infty mf(lnnu,z) dlnnu = 1 is NOT satisfied!
   //    Coefficients correspond to Delta_mean=200, Hydro or DM only (Table 2 of Bocquet et al. (2016))
   //    Ref: Eq.(1) of Bocquet et al. (2016)
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density
   //  is_hydro  DM only (false) or hydro (true) simulation

   double Delta_c_of_Deltam_200 = delta_x_to_delta_crit(200, kRHO_MEAN, z);

   if (Delta_c < Delta_c_of_Deltam_200) {
      printf("\n\n====> ERROR: mf_magneticum() in cosmo.cc");
      printf("\n             Bocquet et al. (2016) HMF parametrization is only");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         printf("\n             given for values Delta_m >= 200");
      } else if (gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98) {
         double Delta_bn = delta_crit_to_delta_x(Delta_c_of_Deltam_200, kBRYANNORMAN98, z);
         printf("\n             given for values Delta_Bryan >= %d", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_m >= 200.");
      printf("\n             (This corresponds to Delta_c >= %d at z = %g)", int(round(Delta_c_of_Deltam_200)), z);
      printf("\n             If you want to use a smaller Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   } else if (Delta_c > 500) {
      printf("\n\n====> ERROR: mf_magneticum() in cosmo.cc");
      printf("\n             Bocquet et al. (2016) HMF parametrization is only");
      printf("\n             given for values Delta_c <= 500");
      printf("\n             If you want to use a larger Delta_c,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   // avoid problems if redshift-dependent Delta_c(Delta_m200) becomes larger than 200,
   // and also stop at 195 to avoid non-steady transition:
   if (Delta_c_of_Deltam_200 > 195) {
      Delta_c_of_Deltam_200 = 195;
   }

   double deltas[3] = {
      Delta_c_of_Deltam_200, 200, 500
   };

   vector<double> deltas_vec;
   deltas_vec.assign(deltas, deltas + 3);
   vector < vector<double> > params_vec;

   if (is_hydro) {
      double params[8][3] = {
         {  0.228,  0.202,  0.18 },
         {  2.15,   2.21,   2.29 },
         {  1.69,   2.00,   2.44 },
         {  1.30,   1.57,   1.97 },
         {  0.285,  1.147,  1.088},
         { -0.058,  0.375,  0.15 },
         { -0.366, -1.074, -1.008},
         { -0.045, -0.196, -0.322},
      };
      pushback_array2vector(params_vec, &params[0][0], sizeof params[0]);
      pushback_array2vector(params_vec, &params[1][0], sizeof params[1]);
      pushback_array2vector(params_vec, &params[2][0], sizeof params[2]);
      pushback_array2vector(params_vec, &params[3][0], sizeof params[3]);
      pushback_array2vector(params_vec, &params[4][0], sizeof params[4]);
      pushback_array2vector(params_vec, &params[5][0], sizeof params[5]);
      pushback_array2vector(params_vec, &params[6][0], sizeof params[6]);
      pushback_array2vector(params_vec, &params[7][0], sizeof params[7]);
   } else {
      double params[8][3] = {
         {  0.175,  0.222,  0.241},
         {  1.53,  1.71,  2.18 },
         {  2.55,  2.24,  2.35 },
         {  1.19,  1.46,  2.02 },
         { -0.012,  0.26,  0.370},
         { -0.040,  0.321,  0.251},
         { -0.194, -0.62, -0.698},
         { -0.021, -0.153, -0.310},
      };
      pushback_array2vector(params_vec, &params[0][0], sizeof params[0]);
      pushback_array2vector(params_vec, &params[1][0], sizeof params[1]);
      pushback_array2vector(params_vec, &params[2][0], sizeof params[2]);
      pushback_array2vector(params_vec, &params[3][0], sizeof params[3]);
      pushback_array2vector(params_vec, &params[4][0], sizeof params[4]);
      pushback_array2vector(params_vec, &params[5][0], sizeof params[5]);
      pushback_array2vector(params_vec, &params[6][0], sizeof params[6]);
      pushback_array2vector(params_vec, &params[7][0], sizeof params[7]);
   }

   double Ap0 = interp1D(Delta_c, deltas_vec, params_vec[0], kLINLIN);
   double a0 =  interp1D(Delta_c, deltas_vec, params_vec[1], kLINLIN);
   double b0 =  interp1D(Delta_c, deltas_vec, params_vec[2], kLINLIN);
   double c0 =  interp1D(Delta_c, deltas_vec, params_vec[3], kLINLIN);

   double Apz = interp1D(Delta_c, deltas_vec, params_vec[4], kLINLIN);
   double az =  interp1D(Delta_c, deltas_vec, params_vec[5], kLINLIN);
   double bz =  interp1D(Delta_c, deltas_vec, params_vec[6], kLINLIN);
   double cz =  interp1D(Delta_c, deltas_vec, params_vec[7], kLINLIN);

   double Ap, a, b, c;

   Ap = Ap0 * pow(1. + z, Apz);
   a = a0 * pow(1. + z, az);
   b = b0 * pow(1. + z, bz);
   c = c0 * pow(1. + z, cz);

   double sigma = DELTA_COLLAPSE / sqrt(nu);
   double mf = 0.5 * (Ap * (pow(sigma / b, -a) + 1.) * exp(-c / pow(sigma, 2.)));
   if (std::isnan(mf)) mf = 0.;
   return mf; // Delta_mean = 200
}

//______________________________________________________________________________
double mf_press_schechter(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value forkPRESSSCHECHTER74.
   //    Halo multiplicity function per log(nu): nu*f(nu)
   //      => \int_{-infty}^infty mf(lnnu) dlnnu = 1
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density


   double Delta_m = delta_crit_to_delta_x(178, kRHO_MEAN, z);
   if (Delta_c < 177.5 or Delta_c > 178.5) {
      printf("\n\n====> ERROR: mf_press_schechter() in cosmo.cc");
      printf("\n             Press & Schechter HMF parametrization is ");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         printf("\n             given for Delta_m(z) = %d (Delta_c = 178)", int(round(Delta_m)));
      } else if (gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98) {
         double Delta_bn = delta_crit_to_delta_x(178, kBRYANNORMAN98, z);
         printf("\n             given for Delta_Bryan() = %d (Delta_c = 178)", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_c = 178 (Delta_m(z) = %d)", int(round(Delta_m)));
      printf("\n             If you want to use a different Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   return 1. / sqrt(2.*PI) * sqrt(nu) * exp(-nu / 2.);
}

//______________________________________________________________________________
double mf_tinker(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value for kTINKER08.
   //    Halo multiplicity function per log(nu): 0.5*f(sigma), where sigma=deltac/sqrt(nu).
   //      => \int_{-infty}^infty mf(lnnu,z) dlnnu = 1 is NOT satisfied!
   //    Ref: Eq.(3) of Tinker et al. (2008)
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density


   double Delta_c_min = delta_x_to_delta_crit(200, kRHO_MEAN, z);
   double Delta_c_max = delta_x_to_delta_crit(3200, kRHO_MEAN, z);

   if (Delta_c <  Delta_c_min) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2008) HMF parametrization is only");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_CRIT) {
         printf("\n             given for values Delta_c >= %d", int(round(Delta_c_min)));
      } else if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         double Delta_bn = delta_crit_to_delta_x(Delta_c_min, kBRYANNORMAN98, z);
         printf("\n             given for values Delta_Bryan >= %d", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_m >= 200");
      printf("\n             If you want to use a smaller Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   } else if (Delta_c >  Delta_c_max) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2008) HMF parametrization is only");
      printf("\n             given for values Delta_m <= 3200");
      printf("\n             If you want to use a larger Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double deltas[9] = {
      delta_x_to_delta_crit(200, kRHO_MEAN, z),
      delta_x_to_delta_crit(300, kRHO_MEAN, z),
      delta_x_to_delta_crit(400, kRHO_MEAN, z),
      delta_x_to_delta_crit(600, kRHO_MEAN, z),
      delta_x_to_delta_crit(800, kRHO_MEAN, z),
      delta_x_to_delta_crit(1200, kRHO_MEAN, z),
      delta_x_to_delta_crit(1600, kRHO_MEAN, z),
      delta_x_to_delta_crit(2400, kRHO_MEAN, z),
      delta_x_to_delta_crit(3200, kRHO_MEAN, z)
   };
   double params[4][9] = {
      {0.186, 0.200, 0.212, 0.218, 0.248, 0.255, 0.260, 0.260, 0.260},
      {1.47,  1.52,  1.56,  1.61,  1.87,  2.13,  2.30,  2.53,  2.66},
      {2.57,  2.25,  2.05,  1.87,  1.59,  1.51,  1.46,  1.44,  1.41},
      {1.19,  1.27,  1.34,  1.45,  1.58,  1.80,  1.97,  2.24,  2.44},
   };

   vector<double> deltas_vec;
   deltas_vec.assign(deltas, deltas + 9);
   vector < vector<double> > params_vec;
   pushback_array2vector(params_vec, &params[0][0], sizeof params[0]);
   pushback_array2vector(params_vec, &params[1][0], sizeof params[1]);
   pushback_array2vector(params_vec, &params[2][0], sizeof params[2]);
   pushback_array2vector(params_vec, &params[3][0], sizeof params[3]);

   double Ap0 = interp1D(Delta_c, deltas_vec, params_vec[0], kLINLIN);
   double a0 =  interp1D(Delta_c, deltas_vec, params_vec[1], kLINLIN);
   double b0 =  interp1D(Delta_c, deltas_vec, params_vec[2], kLINLIN);
   double c =   interp1D(Delta_c, deltas_vec, params_vec[3], kLINLIN);

   double Ap, a, b, alpha;
   double Delta_m = delta_crit_to_delta_x(Delta_c, kRHO_MEAN, z);

   Ap = Ap0 * pow(1. + z, -0.14);
   a = a0 * pow(1. + z, -0.06);
   alpha = exp(-pow(0.75 / log(Delta_m / 75.), 1.2));
   b = b0 * pow(1. + z, -alpha);

   double sigma = DELTA_COLLAPSE / sqrt(nu);
   return 0.5 * (Ap * (pow(sigma / b, -a) + 1.) * exp(-c / pow(sigma, 2.)));
}

//______________________________________________________________________________
double mf_tinker_norm(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value kTINKER08_N.
   //    Halo multiplicity function per log(nu): 0.5*f(sigma), where sigma=deltac/sqrt(nu).
   //      => \int_{-infty}^infty mf(lnnu,z) dlnnu = 1
   //    Coefficients correspond to Delta=200
   //    Ref: Tinker et al. (2008) appendix C parametrisation. No z dependence.
   //    This parametrisation IS normalised so that it does not diverge when going down to very low masses.
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density

   double Delta_c_min = delta_x_to_delta_crit(200, kRHO_MEAN, z);
   double Delta_c_max = delta_x_to_delta_crit(3200, kRHO_MEAN, z);

   if (Delta_c <  Delta_c_min) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2008) HMF parametrization is only");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_CRIT) {
         printf("\n             given for values Delta_c >= %d", int(round(Delta_c_min)));
      } else if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         double Delta_bn = delta_crit_to_delta_x(Delta_c_min, kBRYANNORMAN98, z);
         printf("\n             given for values Delta_Bryan >= %d", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_m >= 200");
      printf("\n             If you want to use a smaller Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   } else if (Delta_c >  Delta_c_max) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2008) HMF parametrization is only");
      printf("\n             given for values Delta_m <= 3200");
      printf("\n             If you want to use a larger Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double deltas[9] = {
      delta_x_to_delta_crit(200, kRHO_MEAN, z),
      delta_x_to_delta_crit(300, kRHO_MEAN, z),
      delta_x_to_delta_crit(400, kRHO_MEAN, z),
      delta_x_to_delta_crit(600, kRHO_MEAN, z),
      delta_x_to_delta_crit(800, kRHO_MEAN, z),
      delta_x_to_delta_crit(1200, kRHO_MEAN, z),
      delta_x_to_delta_crit(1600, kRHO_MEAN, z),
      delta_x_to_delta_crit(2400, kRHO_MEAN, z),
      delta_x_to_delta_crit(3200, kRHO_MEAN, z)
   };
   double params[4][9] = {
      {1.97, 2.06, 2.30, 2.56, 2.83, 2.92, 3.29, 3.37, 3.30 },
      {1.00, 0.99, 0.93, 0.93, 0.96, 1.04, 1.07, 1.12, 1.16 },
      {0.51, 0.48, 0.48, 0.45, 0.44, 0.40, 0.40, 0.36, 0.33 },
      {1.228, 1.310, 1.403, 1.553, 1.702, 1.907, 2.138, 2.394, 2.572},
   };

   vector<double> deltas_vec;
   deltas_vec.assign(deltas, deltas + 9);
   vector < vector<double> > params_vec;
   pushback_array2vector(params_vec, &params[0][0], sizeof params[0]);
   pushback_array2vector(params_vec, &params[1][0], sizeof params[1]);
   pushback_array2vector(params_vec, &params[2][0], sizeof params[2]);
   pushback_array2vector(params_vec, &params[3][0], sizeof params[3]);


   double dd = interp1D(Delta_c, deltas_vec, params_vec[0], kSPLINE);
   double ee = interp1D(Delta_c, deltas_vec, params_vec[1], kSPLINE);
   double ff = interp1D(Delta_c, deltas_vec, params_vec[2], kSPLINE);
   double gg = interp1D(Delta_c, deltas_vec, params_vec[3], kSPLINE);

   double Bp = 2. / (pow(ee, dd) * pow(gg, -dd / 2.) * tgamma(dd / 2.) + pow(gg, -ff / 2.) * tgamma(ff / 2.));

   double sigma = DELTA_COLLAPSE / sqrt(nu);
   return 0.5 * Bp * (pow(sigma / ee, -dd) + pow(sigma, -ff)) * exp(-gg / (sigma * sigma));
}

//______________________________________________________________________________
double mf_tinker10(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value for kTINKER10.
   //    Halo multiplicity function per log(nu) where sigma=deltac/sqrt(nu).
   //      => \int_{-infty}^infty mf(lnnu,z) dlnnu = 1 is satisfied!
   //    Ref: Eq.(8) of Tinker et al. (2010)
   //    Delta=500
   //    NB: They define nu=deltac/sigma while we have nu=(deltac/sigma)^2
   //    ==> what we call nu corresponds to their nu^2, hence the power of 2
   //    missing here and there when comparing what we return to Eq.(8) of Tinker+ (2010)
   //    Also, this implies f(nu_us)=0.5*f(nu_them)
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density


   double zz = 0.;
   if (z < 3.) zz = z;
   else zz = 3.; // following recommendation in Tinker+ (2010)

   double Delta_c_min = delta_x_to_delta_crit(200, kRHO_MEAN, z);
   double Delta_c_max = delta_x_to_delta_crit(3200, kRHO_MEAN, z);

   if (Delta_c <  Delta_c_min) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2010) HMF parametrization is only");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_CRIT) {
         printf("\n             given for values Delta_c >= %d", int(round(Delta_c_min)));
      } else if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         double Delta0_bn = delta_crit_to_delta_x(Delta_c_min, kBRYANNORMAN98, z);
         printf("\n             given for values Delta_Bryan >= %d", int(round(Delta0_bn)));
      } else
         printf("\n             given for values Delta_m >= 200");
      printf("\n             If you want to use a smaller Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   } else if (Delta_c >  Delta_c_max) {
      printf("\n\n====> ERROR: mf_tinker() in cosmo.cc");
      printf("\n             Tinker et al. (2010) HMF parametrization is only");
      printf("\n             given for values Delta_m <= 3200");
      printf("\n             If you want to use a larger Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double deltas[9] = {
      delta_x_to_delta_crit(200, kRHO_MEAN, z),
      delta_x_to_delta_crit(300, kRHO_MEAN, z),
      delta_x_to_delta_crit(400, kRHO_MEAN, z),
      delta_x_to_delta_crit(600, kRHO_MEAN, z),
      delta_x_to_delta_crit(800, kRHO_MEAN, z),
      delta_x_to_delta_crit(1200, kRHO_MEAN, z),
      delta_x_to_delta_crit(1600, kRHO_MEAN, z),
      delta_x_to_delta_crit(2400, kRHO_MEAN, z),
      delta_x_to_delta_crit(3200, kRHO_MEAN, z)
   };
   double params[5][9] = {
      {0.368, 0.363, 0.385, 0.389, 0.393, 0.365, 0.379, 0.355, 0.327 },
      {0.589, 0.585, 0.544, 0.543, 0.564, 0.623, 0.637, 0.673, 0.702 },
      {0.864, 0.922, 0.987, 1.090, 1.200, 1.340, 1.500, 1.680, 1.810 },
      { -0.729, -0.789, -0.910, -1.05, -1.20, -1.26, -1.45, -1.5, -1.49},
      { -0.243, -0.261, -0.261, -0.273, -0.278, -0.301, -0.301, -0.319, -0.336}
   };

   vector<double> deltas_vec;
   deltas_vec.assign(deltas, deltas + 9);
   vector < vector<double> > params_vec;
   pushback_array2vector(params_vec, &params[0][0], sizeof params[0]);
   pushback_array2vector(params_vec, &params[1][0], sizeof params[1]);
   pushback_array2vector(params_vec, &params[2][0], sizeof params[2]);
   pushback_array2vector(params_vec, &params[3][0], sizeof params[3]);
   pushback_array2vector(params_vec, &params[4][0], sizeof params[4]);

   double alpha =  interp1D(Delta_c, deltas_vec, params_vec[0], kLINLIN);
   double beta0 =  interp1D(Delta_c, deltas_vec, params_vec[1], kLINLIN);
   double gamma0 = interp1D(Delta_c, deltas_vec, params_vec[2], kLINLIN);
   double phi0 =   interp1D(Delta_c, deltas_vec, params_vec[3], kLINLIN);
   double eta0 =   interp1D(Delta_c, deltas_vec, params_vec[4], kLINLIN);

   double beta, gamma, phi, eta;

   beta = beta0 * pow(1. + zz, 0.2);
   gamma = gamma0 * pow(1. + zz, -0.01);
   phi = phi0 * pow(1. + zz, -0.08);
   eta = eta0 * pow(1. + zz, 0.27);

   return 0.5 * alpha * (1. + pow(beta, -2.*phi) * pow(nu, -phi)) * pow(nu, eta + 1. / 2.) * exp(-gamma * nu / 2.);
}

//______________________________________________________________________________
double mf_sheth_tormen(double const &nu, double const &z, double const &Delta_c)
{
   //--- Returns halo mass function value for kSHETHTORMEN99.
   //    Halo multiplicity function per log(nu): nu*f(nu)
   //      => \int_{-infty}^infty mf(lnnu) dlnnu = 1
   //    Ref: Sheth and Tormen (1999)
   //
   //  nu        nu value
   //  z         Redshift
   //  Delta_c   Delta w.r.t critical density


   double Delta_m = delta_crit_to_delta_x(178, kRHO_MEAN, z);
   if ((Delta_c < 177.5 or Delta_c > 178.5) and !gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE) {
      printf("\n\n====> ERROR: mf_sheth_tormen() in cosmo.cc");
      printf("\n             Sheth & Tormen HMF parametrization is ");
      if (gCOSMO_FLAG_DELTA_REF == kRHO_MEAN) {
         printf("\n             given for Delta_m(z) = %d (Delta_c = 178)", int(round(Delta_m)));
      } else if (gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98) {
         double Delta_bn = delta_crit_to_delta_x(178, kBRYANNORMAN98, z);
         printf("\n             given for Delta_Bryan() = %d (Delta_c = 178)", int(round(Delta_bn)));
      } else
         printf("\n             given for values Delta_c = 178 (Delta_m(z) = %d)", int(round(Delta_m)));
      printf("\n             If you want to use a different Delta,");
      printf("\n             you must choose gSIM_EXTRAGAL_IS_DELTA_HMF_FROM_PROFILE = 1.");
      printf("\n             => abort()\n\n");
      abort();
   }

   double q = 0.707;
   double Ap = 0.3222;
   double p = 0.3;

   return Ap / sqrt(2.*PI) * (1. + pow(q * nu, -p)) * sqrt(q * nu) * exp(-q * nu / 2.);
}

//______________________________________________________________________________
vector< vector<double> > nu_andderiv(const double z, const vector<double> &Mh_vec,
                                     const vector<double>  &linear_lnk_vec, const vector<double>  &linear_lnp_vec,
                                     const int card_window, const int card_growth_mthd)
{
   //--- Computes nu(Mh) and d ln(nu) / d ln(Mh) at redshift z for nu =  nu=(delta_collapse/sigma)^2
   //    Note 1: Mh is in units of Omega_m0 h^-1 Msol
   //    Note 2: The masses Mh reflect all the masses contained in a collapsed region
   //
   // INPUTS:
   //  z                Redshift z
   //  Mh_vec           Mass values, length n_m, unit [Omega_m0 h^-1 Msol]
   //                   or P(k,0) is given, and sigma^2(z) is calculated from a growth factor model.
   //  linear_lnk_grid  k values for p_k
   //  linear_lnplnk_z  lnp/lnk, must have same dimension as linear_lnk_grid
   //  card_window      Real-space window for collapse region (kTOP_HAT, ..)
   //  card_growth_mthd Selects the method to obtain P(k,z): either P(k,z) is directly given (kPKZ_FROMFILE),
   //
   // OUTPUT:
   //  res              Vector of two vectors with each of length len(Mh_vec), containing:
   //                   res[0]: mu(Mh, z)
   //                   res[1]:  d ln(nu) / d ln(Mh) (Mh, z)

   vector< vector<double> > res;
   vector< vector<double> > s2 = sigma2_andderiv(z, Mh_vec, linear_lnk_vec, linear_lnp_vec, card_window, card_growth_mthd);
   vector<double> nu, dlnnudlnMh;
   double delta2 = pow(DELTA_COLLAPSE, 2);
   for (int i = 0; i < (int)s2[0].size(); i++) {
      nu.push_back(delta2 / s2[0][i]); // nu
      dlnnudlnMh.push_back(-s2[1][i]); //  d ln(nu) / d ln(Mh)
   }
   res.push_back(nu);
   res.push_back(dlnnudlnMh);
   return res;
}

//______________________________________________________________________________
double H2_over_H02(const double &z)
{
   //--- Returns H(z)^2/H_0^2.
   //  z       Redshift

   // x = 1/a = (1+z)
   double x = (1. + z);
   double fac_lambda;
   if (abs(gCOSMO_WDE + 1.) > 1e-5) fac_lambda = pow(x, (3. + 3.*gCOSMO_WDE));
   else fac_lambda = 1.;

   return gCOSMO_OMEGA0_R * pow(x, 4.) + gCOSMO_OMEGA0_M * pow(x, 3.)
          + gCOSMO_OMEGA0_K * pow(x, 2.) + gCOSMO_OMEGA0_LAMBDA * fac_lambda;
}

//______________________________________________________________________________
double H_over_H0(const double &z)
{
   //--- Returns H(z)/H_0.
   //  z       Redshift

   return sqrt(H2_over_H02(z));
}

//______________________________________________________________________________
double H0_over_H(const double z, void *params)
{
   //--- Returns H0_over_H(z) = H_0/H(z).
   //  z       Redshift

   return 1. / H_over_H0(z);
}

//______________________________________________________________________________
double Omega_r(const double &z)
{
   //--- Returns Omega_r(z).
   //  z       Redshift

   return gCOSMO_OMEGA0_R / H2_over_H02(z) * pow((1 + z), 4.);
}

//______________________________________________________________________________
double Omega_m(const double &z)
{
   //--- Returns Omega_m(z).
   //  z       Redshift

   return gCOSMO_OMEGA0_M / H2_over_H02(z) * pow((1 + z), 3.);
}

//______________________________________________________________________________
double Omega_cdm(const double &z)
{
   //--- Returns Omega_CDM(z).
   //  z       Redshift

   return (gCOSMO_OMEGA0_M - gCOSMO_OMEGA0_B) / H2_over_H02(z) * pow((1 + z), 3.);
}

//______________________________________________________________________________
double Omega_b(const double &z)
{
   //--- Returns Omega_b(z).
   //  z       Redshift

   return gCOSMO_OMEGA0_B / H2_over_H02(z) * pow((1 + z), 3.);
}

//______________________________________________________________________________
double Omega_lambda(const double &z)
{
   //--- Returns Omega_lambda(z).
   //  z       Redshift

   double x = (1. + z);
   double fac_lambda;
   if (abs(gCOSMO_WDE + 1.) > 1e-5) fac_lambda = pow(x, (3. + 3.*gCOSMO_WDE));
   else fac_lambda = 1.;
   return gCOSMO_OMEGA0_LAMBDA / H2_over_H02(z) * fac_lambda;
}

//______________________________________________________________________________
double Omega_k(const double &z)
{
   //--- Returns Omega_k(z).
   //  z       Redshift

   return gCOSMO_OMEGA0_K / H2_over_H02(z) * pow((1 + z), 2.);
}

//______________________________________________________________________________
double rho_crit(const double &z)
{
   //--- Returns rho_crit [Msol kpc^-3]
   //  z       Redshift

   return  gCOSMO_RHO0_C * H2_over_H02(z);
}

//______________________________________________________________________________
vector< vector<double> > sigma2_andderiv(const double z, const vector<double> &Mh_vec,
      const vector<double>  &linear_lnk_vec, const vector<double>  &linear_lnp_vec,
      const int card_window, const int card_growth_mthd)
{
   //--- Computes sigma^2(Mh) and d ln(sigma^2) / d ln(Mh) at redshift z.
   //    Note 1: Mh is in units of Omega_m0 h^-1 Msol
   //    Note 2: The masses Mh reflect all the masses contained in a collapsed region
   //
   // IINPUTS:
   //  z                Redshift z
   //  Mh_vec           %ass values, length n_m, unit [Omega_m0 h^-1 Msol]
   //                   or P(k,0) is given, and sigma^2(z) is calculated from a growth factor model.
   //  linear_lnk_grid  k values for p_k
   //  linear_lnplnk_z  lnp/lnk, must have same dimension as linear_lnk_grid
   //  card_window      Real-space window for collapse region (kTOP_HAT, ..)
   //  card_growth_mthd Selects the method to obtain P(k,z): either P(k,z) is directly given (kPKZ_FROMFILE),
   //
   // OUTPUT:
   //  res              Vector of two vectors with each of length len(Mh_vec), containing:
   //                   res[0]: sigma^2(Mh, z)
   //                   res[1]:  d ln(sigma^2) / d ln(Mh) (Mh, z)

   double Rh, lnRh, lns2, dlns2dlnRh, dlns2dlnMh;
   double rho_critperh2_MsolperMpc3 = RHO_CRITperh2_MSOLperKPC3  / KPC3_to_MPC3 ; // *  in units of h^2 * Msol Mpc^-3

   vector<double> sigma2_vec;
   vector<double> dlns2dlnMh_vec;
   vector< vector<double> > res;

   bool is_rodriguez_sigmaapprox = false;
   bool is_ludlow_sigmaapprox  = false;
   if (gCOSMO_HUBBLE == 0.678 and
         gCOSMO_OMEGA0_M == 0.307 and
         gCOSMO_OMEGA0_B == 0.048 and
         gCOSMO_SIGMA8 == 0.829 and
         gCOSMO_N_S == 0.96 and
         gCOSMO_DELTA0 == -1 and
         gCOSMO_FLAG_DELTA_REF == kBRYANNORMAN98 and
         card_growth_mthd != kPKZ_FROMFILE) {
      static bool is_rodriguez_sigmaapprox_msg = true;  // have switched off analytic formula for now (chenge to false if you want to use it)
      is_rodriguez_sigmaapprox = false; // have switched off analytic formula for now (change to true if you want to use it)
      if (!is_rodriguez_sigmaapprox_msg) {
         printf("\n\n====> NOTE: compute_sigma2() in cosmo.cc");
         printf("\n            Analytic approximation for sigma(Mvir)");
         printf("\n            from Rodriguez-Puebla (2016) is used.\n");
         is_rodriguez_sigmaapprox_msg = true;
      }
   }

   // Following Komatsu, first compute the Chebyshev serie of lnsigma2 and its derivative.
   // This significantly speeds up the computation compared to performing the evaluation
   // and numerical derivative of lnsigma2 for each mass. We checked that the 2 approaches
   // give the same results.

   gsl_cheb_series *cs = gsl_cheb_alloc(30);
   gsl_cheb_series *deriv = gsl_cheb_alloc(30);

   if (!is_rodriguez_sigmaapprox) {
      pk_params_for_integration params = {0., linear_lnk_vec, linear_lnp_vec, card_window};
      gsl_function F;
      F.function = &lnsigma2;
      F.params = &params;

      double lnRhmin = log(pow(Mh_vec[0] / gamma_f(card_window) / rho_critperh2_MsolperMpc3, 1. / 3.));
      double lnRhmax = log(pow(Mh_vec[Mh_vec.size() - 1] / gamma_f(card_window) / rho_critperh2_MsolperMpc3, 1. / 3.));
      gsl_cheb_init(cs, &F, lnRhmin, lnRhmax);  // cs contains the Chebyshev coefficients
      gsl_cheb_calc_deriv(deriv, cs); // deriv contains the derivatives coefficients
   }

   // Compute growth factor (only used when card_growth_mthd != kPKZ_FROMFILE)
   double log_growthfac = 0.;
   if (card_growth_mthd != kPKZ_FROMFILE) {
      log_growthfac = log(growth_factor(z, card_growth_mthd));
   }

   //Now, loop on all masses
   for (int j = 0; j < (int)Mh_vec.size(); j++) {

      if (is_rodriguez_sigmaapprox) {
         double Mh = Mh_vec[j] * gCOSMO_OMEGA0_M;
         double lnsigma = log(17.111 * pow(1e12 / Mh, 0.405) / (1 + 1.306 * pow(1e12 / Mh, 0.22) + 6.218 * pow(1e12 / Mh, 0.317)));
         lns2 = 2 * lnsigma + 2. * log_growthfac;
         dlns2dlnMh = -2. * (0.088 + (0.317 + 55.2987 * pow(Mh, -0.22)) /
                             (1. + 570.09 * pow(Mh, -0.22) + 39595.9 * pow(Mh, -0.317)));
      } else if (is_ludlow_sigmaapprox) {
         double Mh = Mh_vec[j] * gCOSMO_OMEGA0_M;
         double y = 1.e10 / Mh;
         double lnsigma = log((22.26 * pow(y, 0.292) / (1. + 1.53 * pow(y, 0.275) + 3.36 * pow(y, 0.198))));
         lns2 = 2 * lnsigma + 2. * log_growthfac;
         dlns2dlnMh = -2 * (0.0455828 * pow(Mh, -0.275) + 0.292 * pow(Mh, -0.198) - 0.198 * pow(Mh, -0.198) + 0.000910005) / (2.68134 * pow(Mh, -0.275) + pow(Mh, -0.198) + 0.00311645);
         //dlns2dlnMh = -2 * (6.1121 * pow(y, 0.317) + 0.185 * pow(y, 0.22) + 0.000710415)/(69.4556 * pow(y, 0.317) + pow(y, 0.22) + 0.00175411);
      } else {
         Rh = pow(Mh_vec[j] / gamma_f(card_window) / (rho_critperh2_MsolperMpc3), 1. / 3.);  // in units of h^-1 Mpc
         lnRh = log(Rh);
         if (exp(linear_lnk_vec[linear_lnk_vec.size() - 1]) < 2.e1 / Rh) {
            cout << j << " Mass scale Mh = " << Mh_vec[j] << "<--> Rh = " << Rh <<
                 " is too small for available k range --> abort!" << endl;
            cout << "Ask for larger P_k_max_h/Mpc when running CLASS" << endl;
            abort();
         }

         // evaluate lnsigma2 at lnRh using Chebyshev coefficients.
         // We had calculated ln(2pi^2/D(z)^2  sigma^2), so multiply with D(z)^2/(2pi^2) here
         lns2 = gsl_cheb_eval(cs, lnRh) + 2. * log_growthfac - log(2) - 2. * log(PI);
         // evaluates derivative at lnRh using Chebyshev coefficients.
         // Derivation in log space -> D(z)^2/(2pi^2) dissappears.
         dlns2dlnRh = gsl_cheb_eval(deriv, lnRh);
         dlns2dlnMh = dlns2dlnRh / 3.;
      }
      sigma2_vec.push_back(exp(lns2));
      dlns2dlnMh_vec.push_back(dlns2dlnMh);
   }

   if (!is_rodriguez_sigmaapprox) {
      gsl_cheb_free(cs);
      gsl_cheb_free(deriv);
   }
   res.push_back(sigma2_vec);
   res.push_back(dlns2dlnMh_vec);
   return res;
}

//______________________________________________________________________________
double sigma8(vector<double>  &lnk_vec, vector<double>  &lnp_vec, const int card_window)
{
   //--- Returns sigma8 from given power spectrum
   //
   //  lnk_vec     Vector of lnk values
   //  lnp_vec     Vector of lnp values
   //  card_window Window function to use

   gsl_function lnsigma2_func;
   lnsigma2_func.function = &lnsigma2;
   pk_params_for_integration params = { -999, lnk_vec, lnp_vec, card_window};
   lnsigma2_func.params = &params;
   return 1 / (sqrt(2) * PI) * exp(0.5 * GSL_FN_EVAL(&lnsigma2_func, log(8)));
}

//______________________________________________________________________________
double solve_d_to_z(double z, void *p)
{
   //--- GSL Function to solve d(z0)=d0 for z0.

   d_to_z_params_for_rootfinding *params = (struct d_to_z_params_for_rootfinding *)p;
   int switch_distance = params->switch_distance;
   double distance = params->distance;
   double d = 0.;
   if (switch_distance == 0)
      d = dh_c(z) / gCOSMO_HUBBLE;
   else if (switch_distance == 1)
      d = dh_trans(z) / gCOSMO_HUBBLE;
   else if (switch_distance == 2)
      d = dh_l(z) / gCOSMO_HUBBLE;
   else if (switch_distance == 3)
      d = dh_a(z) / gCOSMO_HUBBLE;
   else {
      printf("\n====> ERROR: solve_d_to_z() in cosmo.cc");
      printf("\n             switch_distance < 4 required");
      printf("\n             => abort()\n\n");
      abort();
   }

   return d - distance;

}

//______________________________________________________________________________
double solve_Mhmin(double Mhmin, void *p)
{
   //--- GSL Function to solve the implicit equation
   //       \int_0^\infinty  (1+ exp[ -(M - Mmin)/Delta_M])^-1 * M * dN/dMdV dM = rho_mass
   //
   //  Mhmin    Value for which to solve
   //  p        Parameters for root findings

   Mmin_params_for_rootfinding *params = (struct Mmin_params_for_rootfinding *)p;

   double par[3] = {params->z, Mhmin, params->sigma_m};

   if (gCOSMO_MH_GRID.empty()) {
      printf("\n====> ERROR: solve_Mmin() in cosmo.cc");
      printf("\n             gCOSMO_MH_GRID grid is empty. Please fill it before");
      printf("\n             via init_extragal().");
      printf("\n             => abort()\n\n");
      abort();
   }

   double rhoh_halos = 0.; // result will be in units of  Omega_m0 h^-1 * h^3  Msol/Mpc^3
   double Mhmin_grid = gCOSMO_MH_GRID[0];
   double Mhmax_grid = gCOSMO_MH_GRID[gCOSMO_MH_GRID.size() - 1];
   simpson_log_adapt(dNdVhdlnMh_sigmoid, Mhmin_grid, Mhmax_grid, par, rhoh_halos, gSIM_EPS);
   return rhoh_halos - (gDM_RHOHALOES_TO_RHOMEAN * RHO_CRITperh2_MSOLperKPC3 / KPC3_to_MPC3);
}

//______________________________________________________________________________
double window_fnct(const double &x, const int card_window)
{
   //--- Returns window function W(x) in k-space.
   //    Adapted from van den Bosch presentation slides,
   //    http://www.astro.yale.edu/vdbosch/astro610_lecture9.pdf
   //    See also Percival (2001), astro-ph/0107437
   //
   //  x             k * r
   //  card_window   Choice of window function

   double res;
   // Calculate W(x)
   switch (card_window) {
      case kTOP_HAT:
         res = (3. / pow(x, 3)) * (sin(x)  - x * cos(x));
         break;
      case kGAUSS:
         res = exp(- pow(x, 2) / 2.);
         break;
      case kSHARP_K:
         if (x <= 1) res = 1;
         else res = 0;
         break;
      default :
         printf("\n====> ERROR: window_fnct() in cosmo.cc");
         printf("\n             window card_window=%d does not correspond to any window function", card_window);
         printf("\n             => abort()\n\n");
         abort();
   }
   return res;
}
