/*! \file jeans_analysis.cc \brief (see jeans_analysis.h) */

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"

// GSL includes
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_hyperg.h>

// C++ std libraries
using namespace std;
#include <iostream>
#include <fstream>

#if IS_ROOT
// ROOT includes
//#include <TArrow.h>
//#include <TH2D.h>
//#include <THStack.h>
//#include <TLegendEntry.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TPaveText.h>
//#include <TStyle.h>
//#include <TPaletteAxis.h>
//#include <TNtuple.h>
#include <TFile.h>
//#include <TF3.h>
//#include <TRandom.h>
#endif


//______________________________________________________________________________
double beta_anisotropy(double const &r, double const par_ani[5])
{
   //--- Returns the anisotropy profile (no unit) at a radius r [kpc] for any
   //    anisotropy profile available in gENUM_ANISOTROPYPROFILE (see params.h).
   //    Note that physical values for the anisotropy are smaller than 1.
   //
   //  r                    Radius [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)

   double beta = 0.;

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kBAES: {
            beta = beta_anisotropy_BAES(r, par_ani);
            break;
         }
      case kCONSTANT: {
            beta = beta_anisotropy_CONSTANT(r, par_ani);
            break;
         }
      case kOSIPKOV: {
            beta = beta_anisotropy_OSIPKOV(r, &par_ani[2]);
            break;
         }
      default: {
            printf("\n====> ERROR: beta_anisotropy() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
         }
   }

   // Beta_anisotropy must be <= 1 to be physical
   if (beta > 1.) {
      printf("\n====> ERROR: beta_anisotropy() in jeans_analysis.cc");
      printf("\n             beta_ani(r=%.2le kpc) > 1 is not allowed (here profile=%s)",
             r, gNAMES_ANISOTROPYPROFILE[card_profile]);
      printf("\n             => abort()\n\n");
      abort();
   }
   return beta;
}

//______________________________________________________________________________
double beta_anisotropy_fr(double const &r, double const par_ani[5], double const &eps)
{
   //--- Returns f(r)=f_r1 \int_{r1}^\infty (2/t) \beta^{ani}(t) dt. For most
   //    profiles, an analytical solution exists. Note that the integration on
   //    the lower boundary r1 leads to a normalisation factor that disappears
   //    in the solution \nu(r)\bar{v_r^2}(r), in which f(r) appears both in
   //    the numerator and denominator (hence f(r) has no unit).
   //
   //  r             Radius [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  eps           Relative precision sought for integrations [if required]

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kCONSTANT: {
            return pow(r, 2.*par_ani[0]);
         }
      case kBAES: {
            return pow(r, 2.*par_ani[0])
                   * pow(1. + pow(r / par_ani[2], par_ani[3]), 2.*(par_ani[1] - par_ani[0]) /
                         par_ani[3]);
         }
      case kOSIPKOV: {
            // N.B.: we do not divide by ra^2 which cancels out for Jeans calculations
            return (r * r + par_ani[2] * par_ani[2]);
         }
      default: {
            // N.B.: so far, eps unused, because an analytical form of f(r)
            // for all profiles is implemented!
            printf("\n====> ERROR: beta_anisotropy_fr() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
            //printf("DUMMY (no printed): %le\n", eps);
         }
   }
   return 0.;
}

//______________________________________________________________________________
double beta_anisotropy_kernel(double const &r, double const par_ani[5], double const &R)
{
   //--- Returns (if exist) the kernel (no unit) to calculate in one single
   //    integration the solution of the projected Jeans equation, as discussed
   //    in Appendix of Mamon & Lokas, MNRAS 363, 705 (2005).
   //
   //  r             Radius from halo centre [kpc]
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  R             Projected radius from halo centre [kpc]

   double u = r / R;

   if (u < 1.)
      return 0.;

   int card_profile = int(par_ani[4]);
   switch (card_profile) {
      case kCONSTANT: {

            double um2 = 1. / (u * u);
            double beta0 = par_ani[0];
            // N.B.: if beta_ani(r) too close to integer or half-integer (e.g.,
            // 0, 1, -0.5, etc.), the Gamma function (in the integrand) is numerically
            // unstable (see Mamon & Lokas 2005 addendum)
            //   => we slighly shift it
            double tol = 1.e-3;
            double residual = beta0 - 0.5;
            residual -= round(residual);
            if (abs(residual) < tol) {
               if (residual < 0)
                  beta0 -= tol;
               else
                  beta0 += tol;
            }
            double residual_2 = beta0 - round(beta0);
            if (abs(residual_2) < tol) {
               if (residual_2 < 0)
                  beta0 -= tol;
               else
                  beta0 += tol;
            }

            double a = beta0 + 0.5;
            double b = 0.5;

            // The regularized incomplete beta function I can be used directly, or it
            // can be calculated by means of the hypergeometric 2F1 and beta functions.
            // N.B.: validity range of gsl_sf_beta_inc (or gsl_sf_hyperg_2F1) is 0<=um2<=1
            // with gsl_sf_beta_inc(a,b,x=1) = gsl_sf_hyperg_2F1(a,b,x=1) = 1
            if (um2 < 0.) um2 = 0.;
            if (um2 > 1.) um2 = 1.;

            //double I = gsl_sf_beta_inc(a, b, um2);
            double I = (pow(um2, a) / a * gsl_sf_hyperg_2F1(a, 1. - b, a + 1., um2))
                       / gsl_sf_beta(a, b);
            // Kernel from Eq.(A16) of Mamon & Lokas MNRAS 363, 705 (2005) + MNRAS 370, 1582 (2006)
            return sqrt(1. - um2) / (1. - 2.*beta0) + 0.5 * sqrt(PI)
                   * gsl_sf_gamma(beta0 - 0.5) / gsl_sf_gamma(beta0)
                   * (1.5 - beta0) * pow(u, 2.*beta0 - 1.) * (1. - I);

         }
      case kOSIPKOV: {
            double ua = par_ani[2] / R;
            double ua2 = ua * ua;
            double u2 = u * u;

            // Kernel from Eq.(A16) of Mamon & Lokas MNRAS 363, 705 (2005)
            return (u2 + ua2) * (ua2 + 0.5) / (u * pow(ua2 + 1., 1.5))
                   * atan(sqrt((u2 - 1.) / (ua2 + 1.)))
                   - 0.5 * sqrt(1. - 1. / u2) / (ua2 + 1.) ;
         }
      default: {
            printf("\n====> ERROR: beta_anisotropy_kernel() in jeans_analysis.cc");
            printf("\n             Profile %d does not correspond to any anisotropy profile", card_profile);
            printf("\n             => abort()\n\n");
            abort();
         }
   }
   return 0.;
}

//______________________________________________________________________________
string beta_anisotropy_legend(double const par_ani[5], bool is_with_formula)
{
   //--- Returns a name as "profile name (#1,#2,#3,#4)" for beta_anisotropy.
   //
   //  par_ani[0]    Anisotropy at r=0
   //  par_ani[1]    Anisotropy at r=+infinity
   //  par_ani[2]    Anisotropy scale radius ra [kpc]
   //  par_ani[3]    Anisotropy shape parameter eta
   //  par_ani[4]    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  is_with_formula Whether to write full formula in legend or not

   int card_profile = (int)par_ani[4];
   char tmp[1000];
   string name = gNAMES_ANISOTROPYPROFILE[card_profile];
   string formula = "";

   switch (card_profile) {
      case kCONSTANT:
         sprintf(tmp, "%s (#beta_{0}=%.2lf)", name.c_str(), par_ani[0]);
         return tmp;
      case kOSIPKOV:
         if (is_with_formula)
            formula = "#beta(r) = #frac{r^{2}}{r^{2}+r_{a}^{2}}";
         sprintf(tmp, "%s %s (r_{a}=%.2lf)", name.c_str(), formula.c_str(), par_ani[2]);
         return tmp;
      case kBAES:
         if (is_with_formula)
            formula = "#beta(r) = #frac{#beta_{0} + #beta_{#infty}(r/r_{a})^{#eta}}{1+(r/r_{a})^{#eta}}";
         sprintf(tmp, "%s %s (#beta_{0}^{#infty}=_{%+.1lf}^{%+.1lf}, r_{a}=%.2lf, #eta=%.1lf)",
                 name.c_str(), formula.c_str(), par_ani[0], par_ani[1],
                 par_ani[2], par_ani[3]);
         return tmp;
      default :
         printf("\n====> ERROR: beta_anisotropy_legend() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any anisotropy profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return "";
}

//______________________________________________________________________________
double beta_anisotropy_CONSTANT(double const &r, double const par[1])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for constant profile.
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy constant value

   return par[0];
}

//______________________________________________________________________________
double beta_anisotropy_BAES(double const &r, double const par[4])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for Baes & van Hese
   //    profile, as described in Baes & van Hese, A&A 471, 419 (2007):
   //           beta(r) = [ beta_0 +beta_infty (r/ra)^eta] / [ 1 + (r/ra)^eta].
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy at r=0
   //  par[1]        Anisotropy at r=+infinity
   //  par[2]        Anisotropy scale radius [kpc]
   //  par[3]        Anisotropy shape parameter eta (transition smoothness)

   return (par[0] + par[1] * pow(r / par[2], par[3])) / (1. + pow(r / par[2], par[3]));
}

//______________________________________________________________________________
double beta_anisotropy_OSIPKOV(double const &r, double const par[1])
{
   //--- Returns anisotropy profile (no unit) at radius r [kpc] for Osipkov-Merrit,
   //    as described in Osipkov, PAZh 5, 77O (1979) and Merritt, AJ 90, 1027 (1985):
   //           beta(r) = r^2 / [r^2 + ra^2].
   //
   //  r             Radius [kpc]
   //  par[0]        Anisotropy scale radius [kpc]

   return pow(r, 2.) / (pow(r, 2.) + pow(par[0], 2.));
}

//______________________________________________________________________________
double light_i(double &R, double par_light[8])
{
   //--- Returns at projected radius R [kpc] the projected (2D) surface density I(R) [Lsol].
   //
   //  R             Projected radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   double rmax_integr = par_light[6];

   // In light_profile():
   //   - switch_qty set to 1 to calculate I(R)
   //   - par_light[6] set to R
   par_light[6] = R;
   double res = light_profile(R, par_light, 1, rmax_integr);

   // Reset par_light[6] to its former value
   par_light[6] = rmax_integr;

   if (res < 1.e-60) // If i_light is zero (e.g. for King profile above rh)
      return 0.;

   return res;
}

//______________________________________________________________________________
void light_i_integrand(double &y, double par_light[8], double &res)
{
   //--- Evaluates the light profile integrand [kpc^{-3}] which enters the calculation
   //    of the surface brightness (at a projected radius R [kpc]). We have
   //       I(R) = 2 \int_0^\infty nu(y) dy  with y = sqrt(r^2-R^2)
   //    so that the integrand is
   //       i_integrand = 2 * nu(y)
   //    N.B.: i_integrand = light_profile(y, par_light, 4).
   //
   // INPUTS:
   //  y             Integration variable y = sqrt(r^2-R^2)
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius R [kpc] considered for projection
   //  par_light[7]  UNUSED
   // OUTPUT:
   //  res                  2*nu(y) [integrand required to calculate I(R)]

   // In light_profile():
   //   - switch_qty set to 3 to calculate 2 * nu(y)
   res = light_profile(y, par_light, 3);
}

//______________________________________________________________________________
double light_i_norm(double par_light[8])
{
   //--- Returns normalisation [Lsol kpc^2] for surface brightness \int_0^rmax 2*pi*R*I(R)dR.
   //
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Maximum radius for integration [kpc]
   //  par_light[7]  Relative precision sought for integration

   double rmin = 1.e-8;
   double res = 0.;
   simpson_log_adapt(light_i_norm_integrand, rmin, par_light[1], par_light, res, par_light[7], false);
   double res_tmp = 0.;
   simpson_log_adapt(light_i_norm_integrand, par_light[1], par_light[6], par_light, res_tmp, par_light[7], false);
   res += res_tmp;

   return res;
}

//______________________________________________________________________________
void light_i_norm_integrand(double &R, double par_light[8], double &res)
{
   //--- Integrand 2*pi*R*I(R) [Lsol kpc^2] to calculate normalisation for surface brightness.
   //
   // INPUTS:
   //  R             Projected radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Maximum radius for integration [kpc]
   //  par_light[7]  Relative precision for the integration
   // OUTPUT:
   //  res                  2*PI*R*I(R) [integrand to calculate I(R) normalisation]

   res = 2.* PI * R * light_i(R, par_light);
}

//______________________________________________________________________________
double light_nu(double &r, double par_light[8])
{
   //--- Returns at radius r [kpc] the deprojected (3D) density profile nu(r) [kpc^[-3]).
   //
   //  r             Radius [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   double rmax_integr = par_light[6];

   // In light_profile():
   //   - switch_qty set to 2 to calculate nu(r)
   //   - par_light[6] set to r
   par_light[6] = r;
   double res = light_profile(r, par_light, 2, rmax_integr);

   // Reset par_light[6] to its former value
   par_light[6] = rmax_integr;

   return res;
}

//______________________________________________________________________________
void light_nu_integrand(double &Y, double par_light[8], double &res)
{
   //--- Evaluates the density profile integrand [Lsol/kpc2] which enters the
   //    calculation of density profile (at (de)projected radius r [kpc]). We have
   //       nu(r) =  -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
   //    so that the integrand is
   //       nu_integrand = -(1/pi) * dI(R)/dR * dY/R
   //    N.B.: nu_integrand = light_profile(y, par_light, 4).
   //
   // INPUTS:
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius r [kpc] considered for de-projection
   //  par_light[7]  UNUSED
   // OUTPUT:
   //  res           -(1/pi) * dI(R)/dR * dY/R [integrand required to calculate nu(r)]

   // In light_profile():
   //   - switch_qty set to 3 to calculate 2 * nu(y)
   res = light_profile(Y, par_light, 4);
}

//______________________________________________________________________________
double light_profile(double &rR_or_yY, double par_light[8], int switch_qty, double rmax_integr)
{
   //--- Calculates quantity (surface brightness, density profile, surface brightness
   //    integrand, etc.) for any light profile available in gENUM_LIGHTPROFILE
   //    (see params.h), depending on switch_qty (see below).
   //
   //  rR_or_yY      Distance (depends on switch_qty) if 3D: r or y=sqrt(r^2-R^2); if 2D: R or Y=sqrt(R^2-r^2)  [kpc]
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  Radius R (resp. r) [kpc] considered for projection (resp. deprojection)
   //  par_light[7]  Relative precision sought for integration (if switch_qty==3 or 4)
   //  switch_qty    Switch quantity to calculate:
   //                   1 -> I(R)=Sigma(R): surface brightness [Lsol]
   //                   2 -> nu(r)=rho(r): density profile [kpc^{-3}]
   //                   3 -> Integrand to calculate I(R) from nu(r): integrand = 2 * nu(y) with y = sqrt(r^2-R^2)]
   //                   4 -> Integrand to calculate nu(r) from I(R): integrand = -(1/pi) * dI(R)/dR * 1/R
   //  rmax_integr   Maximum radius for integration [kpc]


   // If switch_qty integer not in [0,4], abort!
   if (switch_qty < 1 || switch_qty > 4) {
      printf("\n====> ERROR: light_profile() in jeans_analysis.cc");
      printf("\n             switch_qty=%d is not allowed (should be 1, 2, 3, or 4)", switch_qty);
      printf("\n             => abort()\n\n");
      abort();
   }

   // Switch on profile selected to return desired quantity
   int card_profile = (int)par_light[5];
   switch (card_profile) {

      case kEXP2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_EXP2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_EXP2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kEXP3D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_EXP3D(rR_or_yY, par_light, true);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_EXP3D(rR_or_yY, par_light, false);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kKING2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_KING2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_KING2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kPLUMMER2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_PLUMMER2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_PLUMMER2D(rR_or_yY, par_light, true);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      case kSERSIC2D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_SERSIC2D(rR_or_yY, par_light, false);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_SERSIC2D(rR_or_yY, par_light, true, rmax_integr, par_light[7]);
         else if (switch_qty == 3) {
            // 2*nu(y)
            print_warning("jeans_analysis.cc", "light_profile()",
                          "I(R) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            // Calculate nu(r) from I(R) with integration performed on dY
            //        [the change of variable is Y = sqrt(R^2-r^2)]
            //   nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}
            //         = -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
            //   => integrand = -(1/pi) * dI(R)/dR * 1/R

            // For Sersic (using bn = 2n - 1/3 + 0.009876/n)
            //  I(R)= I0 * exp{-bn [(R/rc)^{1/n} - 1] }
            // so that
            //  dI(R)/dR = - [ bn / (R*n) * (R/rc)^[1/n] ] * I(R)
            double n = par_light[2];
            double bn = 2.*n - 0.3333333333 + 0.009876 / n;
            double Y = rR_or_yY;
            double ref_par6 = par_light[6];
            double r = par_light[6];
            double R = sqrt(Y * Y + r * r);
            double dIdR = -bn / n * pow(R / par_light[1], 1. / n) / R;
            double res = - dIdR * light_profile_SERSIC2D(R, par_light, false) / (PI * R);
            par_light[6] = ref_par6;
            return res;
         }

      case kZHAO3D:
         if (switch_qty == 1)
            // I(R)
            return light_profile_ZHAO3D(rR_or_yY, par_light, true, rmax_integr, par_light[7]);
         else if (switch_qty == 2)
            // nu(r)
            return light_profile_ZHAO3D(rR_or_yY, par_light, false);
         else if (switch_qty == 3) {
            // 2*nu(y)
            // Calculate I(R) from nu(r) with integration performed on dy:
            //        [the change of variable is y = sqrt(r^2-R^2)]
            //   I(R) = 2 \int_R^\infty r*nu(r) * dr/\sqrt{r^2-R^2}
            //        = 2 \int_0^\infty nu(y) dy
            //   => integrand = 2 * nu(y)

            double R = par_light[6];
            double y = rR_or_yY;
            double r = sqrt(y * y + R * R);
            return 2. * light_profile_ZHAO3D(r, par_light, false);
         } else if (switch_qty == 4) {
            // -(1/pi)*dI(R)/dR*1/R
            print_warning("jeans_analysis.cc", "light_profile()",
                          "nu(r) is analytical for " + string(gNAMES_LIGHTPROFILE[card_profile])
                          + " => No need to call light_nu_integrand().");
            return -1.;
         }

      default:
         printf("\n====> ERROR: light_profile() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any light profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return -1.;
}

//______________________________________________________________________________
string light_profile_legend(double par_light[6], bool is_with_formula)
{
   //--- Returns a name as "lightprofile name (#1,#2,#3,#4, #5)" for light profile.
   //
   //  par_light[0]  Light profile 1st free par (usually normalisation)
   //  par_light[1]  Light profile 2nd free par (usually scale radius [kpc])
   //  par_light[2]  Light profile 3rd free par (if used)
   //  par_light[3]  Light profile 4th free par (if used)
   //  par_light[4]  Light profile 5th free par (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  is_with_formula Whether to write full formula in legend or not

   int card_profile = (int)par_light[5];
   char tmp[1000];
   string name = gNAMES_LIGHTPROFILE[card_profile];
   string formula = "";

   // Switch on profile selected to return desired quantity
   switch (card_profile) {

      case kEXP2D:
         if (is_with_formula)
            formula = "I(R)#propto exp(-R/r_{c}) with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1]);
         return tmp;
      case kEXP3D:
         if (is_with_formula)
            formula = "#nu(r)#propto exp(-r/r_{c}) with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1]);
         return tmp;
      case kKING2D:
         if (is_with_formula)
            formula = "I(R)#propto [(1+#frac{R^{2}}{r_{c}^{2}})^{-1/2} - (1+#frac{r_{lim}^{2}}{r_{c}^{2}})^{-1/2}]^{2} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf, r_{lim}=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1], par_light[2]);
         return tmp;
      case kPLUMMER2D:
         if (is_with_formula)
            formula = "I(R)#propto #pi r_{c}^{2} (1+#frac{R^{2}}{r_{c}^{2}})^{-2} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf)", name.c_str(), formula.c_str(), par_light[1]);
         return tmp;
      case kSERSIC2D:
         if (is_with_formula)
            formula = "I(R)#propto exp(-b_{n} [(R/r_{c})^{1/n} - 1])";
         sprintf(tmp, "%s %s (r_{c}=%.2lf, n=%.2lf)", name.c_str(),
                 formula.c_str(), par_light[1], par_light[2]);
         return tmp;
      case kZHAO3D:
         if (is_with_formula)
            formula = "#nu(r)#propto #frac{(r/r_{c})^{-#gamma}}{[1+(r/r_{c})^{#alpha}]^{(#beta-#gamma)/#alpha}} with";
         sprintf(tmp, "%s %s (r_{c}=%.2lf,#alpha=%.1lf,#beta=%.1lf,#gamma=%.1lf)",
                 name.c_str(), formula.c_str(), par_light[1], par_light[2], par_light[3], par_light[4]);
         return tmp;
      default :
         printf("\n====> ERROR: light_profile_legend() in jeans_analysis.cc");
         printf("\n             card_profile=%d does not correspond to any light profile", card_profile);
         printf("\n             => abort()\n\n");
         abort();
   }
   return "";
}

//______________________________________________________________________________
double light_profile_EXP2D(double &R_or_r, double par[2], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for an exponential profile [Lsol].
   //    See Sect. 4.2 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          I(R)= I0 * exp{-R/rc}              [2D]
   //       => nu(r) = I0 / (PI*rc) * K0(r/rc)    [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   double arg = R_or_r / par[1];

   // De-projected (analytical) [3D]
   if (is_deproject) {
      if (arg > 500.)
         arg = 500.;
      gsl_sf_result x;
      int test = gsl_sf_bessel_K0_e(arg, &x);
      if (test == 15) // underflow is reached
         return 0.;
      else
         return par[0] / (PI * par[1]) * gsl_sf_bessel_K0(arg);
   }

   // Surface brightness [2D]
   return par[0] * exp(-arg);
}

//______________________________________________________________________________
double light_profile_EXP3D(double &r_or_R, double par[2], bool is_project)
{
   //--- Returns density profile nu(r) or projected (2D) for an exponential profile [kpc^{-3}].
   //    See Sect. 4.2 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          rho(r)= rho0 * exp{-r/rc}          [3D]
   //       => I(R) = 2 * rho0 * R * K1(R/rc)     [2D]
   //
   //  r_or_R        Distance (r) or projected distance (R) from profile centre [kpc]
   //  par[0]        Light density profile normalisation rho0 [kpc^{-3}]
   //  par[1]        Light critical radius rc [kpc]
   //  is_project    Whether to return nu(r) or I(R)

   // Projected (analytical) [2D]
   if (is_project) {
      if (r_or_R / par[1] > 500.) // underflow is reached
         return 0.;
      else
         return 2. * par[0] * r_or_R * gsl_sf_bessel_K1(r_or_R / par[1]);
   }

   // Density profile [3D]
   if (r_or_R / par[1] > 500.) // underflow is reached
      return 0.;
   else
      return par[0] * exp(-r_or_R / par[1]);
}

//______________________________________________________________________________
double light_profile_KING2D(double &R_or_r, double par[3], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a King profile [Lsol].
   //    See Supplementary information of Strigari et al., Nature 454, 1096 (2008):
   //          I(R)= I0 * [ (1+R^2/rc^2)^{-1/2} - (1+rlim^2/rc^2)^{-1/2} ]^2                      [2D]
   //       => nu(r) = I0 * [acos(z)/z)-(1-z^2)^{1/2}] / [ pi*rc*z^2 * (1+rlim^2/rc^2)^{3/2} ]    [3D]
   //          where z^2 = (1+r^2/rc^2)/(1+rlim^2/rc^2)
   //
   //  R_or_r        Projected distance R (or distance r) from profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light second radius rlim [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   // Profile is zero above rlim
   if (R_or_r > par[2])
      return 0;

   // Useful quantities
   double rc2 = par[1] * par[1];
   double rlim2 = par[2] * par[2];

   // De-projected (analytical)
   if (is_deproject) {
      double tmp = 1. + rlim2 / rc2;
      double z2 = (1. + R_or_r * R_or_r / rc2) / tmp;
      double z = sqrt(z2);
      return par[0] * (acos(z) / z - sqrt(1. - z2)) / (PI * par[1] * pow(tmp, 1.5) * z2);
   }

   // Surface brightness (2D)
   return par[0] * pow(pow(1. + R_or_r * R_or_r / rc2, -0.5) - pow(1. + rlim2 / rc2, -0.5), 2);
}

//______________________________________________________________________________
double light_profile_PLUMMER2D(double &R_or_r, double par[2], bool is_deproject)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a Plummer profile [Lsol].
   //    See Sect. 4.1 of Evan, An, and Walker, MNRAS 393, 50 (2009):
   //          I(R)= I0 / [ pi rc^2 * (1+R^2/rc^2)^2 ]               [2D]
   //       => nu(r) = 3*I0 / [ 4*pi*rc^3 * (1+r^2/rc^2)^{5/2} ]     [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from the profile centre [kpc]
   //  par[0]        Light (surface brightness) normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  is_deproject  Whether to return I(R) or nu(r).

   double rc2 = par[1] * par[1];

   // De-projected (analytical)  [3D]
   if (is_deproject)
      return 3.*par[0] / ((4.*PI * rc2 * par[1]) * pow(1. + (R_or_r * R_or_r) / rc2, 2.5));

   // Surface brightness [2D]
   return par[0] / (PI * rc2 * pow(1. + (R_or_r * R_or_r) / rc2, 2.));
}

//______________________________________________________________________________
double light_profile_SERSIC2D(double &R_or_r, double par[3], bool is_deproject,
                              double const &rmax_integr, double const &eps)
{
   //--- Returns surface brightness I(R) or de-projected (3D) for a Sersic profile [Lsol].
   //    See Graham and Driver, PASA 22, 118 (2005) or Merrit et al., AJ 132, 2685 (2006):
   //          I(R)= I0 * exp{-bn [(r/rc)^{1/n} - 1] }                       [2D]
   //               with bn = 2n - 1/3 + 0.009876/n
   //       => nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}   [3D]
   //
   //  R_or_r        Projected distance R (or distance r) from the profile centre [kpc]
   //  par[0]        Light surface brightness normalisation I0 [Lsol]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light shape parameter n (double) [-]
   //  is_deproject  Whether to return I(R) or nu(r).
   //  rmax_integr   Maximum radius for integration (if is_deproject==true) [kpc]
   //  eps           Relative precision sought for integration (if is_deproject==true)

   // Not valid below n<0.5
   if (par[2] < 0.5) {
      printf("\n====> ERROR: light_profile_SERSIC2D in jeans_analysis.cc");
      printf("\n             n<0.5 is not authorised in CLUMPY (here n=%.2le)", par[2]);
      printf("\n             => abort()\n\n");
      abort();
   }

   // De-projected (numerical integration)
   if (is_deproject) {
      // No analytical formula for SERSIC2D de-projection
      //       -> use of inverse Abel transform!
      //   nu(r) = -(1/pi) \int_r^\infty  dI(R)/dR * dR/\sqrt{R^2-r^2}
      //         = -(1/pi) \int_0^\infty  dI(R)/dR * dY/R
      // where we used the change of variable Y = sqrt(R^2-r^2).

      // Fill par_light parameters to enable calling of function light_nu_integrand()
      const int npar = 8;
      double par_light[npar];
      //  par_light[0...4] Light profile free pars (usually normalisation)
      //  par_light[5]     Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
      //  par_light[6]     Radius r [kpc] considered for de-projection
      //  par_light[7]     UNUSED
      for (int i = 0; i < 3; i++)
         par_light[i] = par[i];
      par_light[3] = 0.;
      par_light[4] = 0.;
      par_light[5] = kSERSIC2D;
      par_light[6] = R_or_r;
      par_light[7] = 0.;

      // Cast integration in three pieces:
      double zero = 0.;
      double y_min = 1.e-4 * par[1];
      double inter = 0;
      double y_max = rmax_integr;
      double y_inter = par[1];
      double res = 0.;
      //  1. Integrate on 'flat region'
      //    a) from 0 to y_min (linear step)
      simpson_lin_adapt(light_nu_integrand, zero, y_min, par_light, inter, eps);
      res += inter;
      //    b) from y_min to y_max (log-step)
      simpson_log_adapt(light_nu_integrand, y_min, y_inter, par_light, inter, eps);
      res += inter;
      //  2. Integrate after critical radius (decreasing part) -> log step
      simpson_log_adapt(light_nu_integrand, y_inter, y_max, par_light, inter, eps);
      res += inter;

      return res;
   }

   // Surface brightness (2D)
   double bn = 2.*par[2] - 0.3333333333 + 0.009876 / par[2];
   return  par[0] * exp(-bn * (pow(R_or_r / par[1], 1. / par[2]) - 1.));
}

//______________________________________________________________________________
double light_profile_ZHAO3D(double &r_or_R, double par[5], bool is_project,
                            double const &rmax_integr, double const &eps)
{
   //--- Returns density profile nu(r) or projected (2D) for an exponential profile [kpc^{-3}].
   //    See Hernquist, ApJ 356, 359 (1990) or Zhao, MNRAS 278, 488 (1996):
   //          nu(r)= rho0 / [ (r/rc)^\gamma (1+(r/rc)^\alpha)^{(\beta-\gamma)/\alpha}]   [3D]
   //       => I(R) = 2 \int_R^\infty r*rho(r) * dr/\sqrt{r^2-R^2}                        [2D]
   //
   //  r_or_R        Distance (r) or projected distance (R) from the profile centre [kpc]
   //  par[0]        Light density profile normalisation rho0 [kpc^{-3}]
   //  par[1]        Light critical radius rc [kpc]
   //  par[2]        Light shape parameter #1
   //  par[3]        Light shape parameter #2
   //  par[4]        Light shape parameter #3
   //  is_project    Whether to return nu(r) or I(R)
   //  rmax_integr   Maximum radius for integration (if is_deproject==true) [kpc]
   //  eps           Relative precision sought for integration (if is_deproject==true)

   // Projected (numerical integration) [2D]
   if (is_project) {
      // No analytical formula for ZHAO3D projection.
      //       -> use of Abel transform!
      //   I(R) = 2 * \int_R^\infty  nu(r) * r dr/\sqrt{r^2-R^2}
      //        = 2 * \int_0^\infty  nu[r(y)] * dy
      // where we used the change of variable y = sqrt(r^2-R^2).

      // Fill par_light parameters to enable calling of function light_i_integrand(y)
      const int npar = 8;
      double par_light[npar];
      //  par_light[0...4] Light profile free pars (usually normalisation)
      //  par_light[5]     Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
      //  par_light[6]     Radius R [kpc] considered for projection
      //  par_light[7]     UNUSED
      for (int i = 0; i < 5; i++)
         par_light[i] = par[i];
      par_light[5] = kZHAO3D;
      par_light[6] = r_or_R;
      par_light[7] = 0.;

      // Cast integration in four pieces: [0,y_min] [y_min,y_inter1] [y_inter1,y_inter2] [y_inter2,y_max]
      // Find max value of function (for y_min), and then find y_inter1 and y_inter2 such that:
      //    light_i_integrand(y_inter1) = 0.99*light_i_integrand(y_min)
      //    light_i_integrand(y_inter2) = 0.8*light_i_integrand(y_min)
      double y_min = 1.e-6 * par[1], y_max = rmax_integr;
      double res_ymin = 0.;
      light_i_integrand(y_min, par_light, res_ymin);
      double res_ref1 = 0.99 * res_ymin;
      double res_ref2 = 0.8 * res_ymin;
      double y_inter1 = 0.;
      double eps1 = 0.005;
      find_x_logstep(res_ref1, y_inter1, y_min, y_max, light_i_integrand, par_light, eps1);
      double y_inter2 = 0.;
      double eps2 = 0.01;
      find_x_logstep(res_ref2, y_inter2, y_inter1, y_max, light_i_integrand, par_light, eps2);

      // Fill intermediate points (for integration)
      vector<double> yy;
      yy.push_back(0.);
      if (y_min > yy[yy.size() - 1] && y_min < y_max)
         yy.push_back(y_min);
      if (y_inter1 > yy[yy.size() - 1] && y_inter1 < y_max)
         yy.push_back(y_inter1);
      if (y_inter2 > yy[yy.size() - 1] && y_inter2 < y_max)
         yy.push_back(y_inter2);
      if (y_max > yy[yy.size() - 1] * 10.)
         yy.push_back(sqrt(y_inter2 * y_max));
      yy.push_back(y_max);
      double res = 0.;
      for (int i = 0; i < (int)yy.size() - 1; ++i) {
         double tmp_res = 0.;
         if (i == 0)
            // N.B.: very inner parts have very small contribution => not necessary to have good contrib.
            simpson_lin_adapt(light_i_integrand, yy[i], yy[i + 1], par_light, tmp_res, 0.1);
         else
            simpson_log_adapt(light_i_integrand, yy[i], yy[i + 1], par_light, tmp_res, eps);
         res += tmp_res;
      }
      return res;
   }

   // Density profile [3D]
   return rho_ZHAO(r_or_R, par);
}

//______________________________________________________________________________
double jeans_nuvr2(double &r, double par_jeans[26])
{
   //--- Returns solution of Jeans equation (3D 'un'-projected) in [Msol/kpc^4]
   //       [nu(r)*\bar{v_r^2}(r)]/G = 1/f(r) \int_r^\infty f(s) nu(s) M(s)/s^2 ds
   //    with f(r) calculated in beta_anisotropy_fr.
   //    N.B.: We do not return directly nu*vr2, but nu*vr2/G instead
   //    (where G=6.67384e-11 = NEWTON_CONSTANT_SI in CLUMPY, see inlines.h), because
   //    numerically it is better to handle numbers as close as possible to 1.
   //
   //  r             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)

   int card_profile_anisotropy = int(par_jeans[19]);
   double rmax = par_jeans[6];
   double r1 = par_jeans[17];
   if (card_profile_anisotropy == kCONSTANT)
      r1 = par_jeans[8];

   double res = 0.;
   if (r > r1)
      simpson_log_adapt(jeans_nuvr2_integrand, r, rmax, par_jeans, res, par_jeans[14]);
   else {
      simpson_log_adapt(jeans_nuvr2_integrand, r, r1, par_jeans, res, par_jeans[14]);
      double tmp_res = 0.;
      simpson_log_adapt(jeans_nuvr2_integrand, r1, rmax, par_jeans, tmp_res, par_jeans[14]);
      res += tmp_res;
   }

   // Returns [nu(r)*\bar{v_r^2}(r)]/G in [Msol/kpc^4]
   return res / beta_anisotropy_fr(r, &par_jeans[15], par_jeans[14]);
}

//______________________________________________________________________________
void jeans_nuvr2_integrand(double &s, double par_jeans[26], double &res)
{
   //--- Calculates f(s)*nu(s)*M(s)/s^2 in [Msol/kpc^5], the integrand to get
   //    the solution nu(r)*\bar{v_r^2}(r) of the Jeans equation: f(s) is
   //    calculated with beta_anisotropy_fr(), whereas M(s) is the mass of
   //    the dark matter halo (and gas if any).
   //
   // INPUTS:
   //  s             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   // OUTPUT:
   //  res           f(s)*nu(s)*M(s)/s^2

   double R_ref = par_jeans[13];

   // Calculate total mass (DM + gas)
   double m_tot = mass(s, par_jeans);

   // Calculates f(s)*nu(s)*M(s)/s^2 in [Msol/kpc^5]
   // set maximum integration radius for computation of nu (needed for somes cases)
   par_jeans[13] = par_jeans[6];
   res = beta_anisotropy_fr(s, &par_jeans[15], par_jeans[14])
         * light_nu(s, &par_jeans[7]) * m_tot / (s * s);
   // set back par_jeans[13] to its initial value
   par_jeans[13] = R_ref;
}

//______________________________________________________________________________
void jeans_nuvr2_integrand_log(double &s, double par_jeans[26], double &res)
{
   //--- Calculates s*jeans_nuvr2_integrand in [Msol/kpc^4], with jeans_nuvr2_integrand()
   //    the integrand to get the solution nu(r)*\bar{v_r^2}(r) of the Jeans equation.
   //    This function is used to find radii where to cut the integrations in different
   //    sub-parts (because integrations are made in log-scale).
   //
   // INPUTS:
   //  s             Radius from halo center [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   // OUTPUT:
   //  res           s*jeans_nuvr2_integrand

   // Calculates s*[f(s)*nu(s)*M(s)/s^2] in [Msol/kpc^4]
   jeans_nuvr2_integrand(s, par_jeans, res);
   res *= s;
}

//______________________________________________________________________________
void jeans_isigmap2_integrand(double &y_or_r, double par_jeans[27], double &res)
{
   //--- Handler to deal with I*Sigma_p^2 integrand whether we use the Kernel
   //    solution of the general full integration solution: the choice is set
   //    in the 26-th (extra) parameter.
   //
   // INPUTS:
   //  y_or_r        y for par_jeans[21]=false, r otherwise [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[26] is_use_kernel: whether to use kernel solution (true) or full integration (false)
   // OUTPUT:
   //  res           jeans_isigmap2_integrand_numerical or jeans_isigmap2_integrand_withkernel in [Msol/kpc^4]

   bool is_use_kernel = (bool)par_jeans[26];
   if (is_use_kernel)
      jeans_isigmap2_integrand_withkernel(y_or_r, par_jeans, res);
   else
      jeans_isigmap2_integrand_numerical(y_or_r, par_jeans, res);
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_log(double &y_or_r, double par_jeans[27], double &res)
{
   //--- Handler to deal with I*Sigma_p^2 integrand (log) whether we use the Kernel
   //    solution of the general full integration solution: the choice is set in
   //    the 26-th (extra) parameter.
   //
   // INPUTS:
   //  y_or_r        Equals y if par_jeans[21]=false, r otherwise [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[26] is_use_kernel: whether kernel solution (true) or full integration (false)
   // OUTPUT:
   //  res           r*jeans_isigmap2_integrand in [Msol/kpc^3]

   jeans_isigmap2_integrand(y_or_r, par_jeans, res);
   res *= y_or_r;
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_numerical(double &y, double par_jeans[26], double &res)
{
   //--- Calculates the integrand 2 * (1-beta_ani(r)*R^2/r^2) * nuvr2(r)/G in [Msol/kpc^4]
   //    used to obtain the Jeans solution for projected quantities.
   //    N.B.1: we remind that the quantity to calculate will lead to I(R)*sigmap2(R)/G,
   //    because the function nuvr2() returns \nu(r)*\bar{v_r^2}(r)/G.
   //    N.B.2: this is the brute-force triple integration way to obtain [I(R)*sigmap2(R)/G]
   //    when no analytical shortcut can be taken (see jeans_isigmap2_integrand_withkernel()
   //    and beta_anisotropy_kernel() for an alternative for some anisotropy profiles).
   //    This full (and more time consumming) integration must be used, e.g., for kBAES
   //    anisotropy profile.
   //
   // INPUTS:
   //  y             Integration variable y=sqrt(r^2-R^2) for a fixed R projected radius [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   // OUTPUT:
   //  res           2 * (1-beta_ani(r)*R^2/r^2) * nuvr2(r)

   double R = par_jeans[13];
   double r = sqrt(y * y + R * R);

   // Integrand =  (1-beta(r)*R^2/r^2) * [nuvr2(r)/G] in [Msol/kpc^4]
   res = 2. * (1. - beta_anisotropy(r, &par_jeans[15]) * R * R / (r * r)) * jeans_nuvr2(r, par_jeans);
}

//______________________________________________________________________________
void jeans_isigmap2_integrand_withkernel(double &r, double par_jeans[26], double &res)
{
   //--- Calculates the integrand 2*K(r/R,r_a/R)*vu(r)*M(r)/r in [Msol/kpc^4]
   //    that is used to obtain the Jeans solution for projected quantities.
   //    N.B.1: the quantity to calculate will lead to I(R)*sigmap2(R)/G,
   //    N.B.2: this is the Kernel single integration way (fast) to obtain
   //    I(R)*sigmap2(R)/G when an analytical form exist (see jeans_isigmap2_integrand_numerical()
   //    for the triple integration when no alternative exist). The kernel
   //    solution available, coded in beta_anisotropy_kernel(), are taken
   //    from Appendix of Mamon & Lokas, MNRAS 363, 705 (2005).
   //
   // INPUTS:
   //  r             Radius from the halo centre [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   // OUTPUT:
   //  res           K(r/R,r_a/R)*vu(r)*M(r)/r


   // Calculate total mass (DM + gas)
   double m_tot = mass(r, par_jeans);

   // nu(r)
   // N.B.: in light_nu(), the parameter corresponding to par_jeans[13] must be the maximum radius!
   double par_ref13 = par_jeans[13];
   par_jeans[13] = par_jeans[6];
   double nu = light_nu(r, &par_jeans[7]);
   par_jeans[13] = par_ref13;

   // Kernel
   // N.B.: for kCONSTANT, the kernel relies on 2F1 hypergeometric function
   // that fails for some argument. As the integrand is used only to calculate
   // the integration domain, and as 2F1 is as the argument for the kCONSTANT
   // kernel is close to 1, we set it to 1, for r<R. This approximation is not
   // used in the jeans_sigmap2 calculation.
   double kernel = beta_anisotropy_kernel(r, &par_jeans[15], par_jeans[13]);

   // Kernel = 2*K(r/R,r_a/R)*nu(r)*M(r)/r in [Msol/kpc^4]
   res = 2. * kernel * m_tot * nu / r;
}

//______________________________________________________________________________
double jeans_sigmap2(double &R, double par_jeans[26], bool is_use_kernel)
{
   //--- Returns the projected velocity dispersion sigmap2(R) in [km^2/s^2]
   //    calculated from the Jeans equation. If a kernel solution exists,
   //    the integration to get I(R)*sigma_p^2(R)/G is obtained calling
   //    jeans_isigmap2_integrand_withkernel(), otherwise by calling
   //    jeans_isigmap2_integrand_numerical(). The integrand for the former
   //    solution is analytical, whereas it involves a double integration
   //    for the latter. The parameter "is_use_kernel" forces the use of
   //    the kernel if it exists (otherwise use tripe integration).
   //    N.B.: R and par_jeans[13] must be equal.
   //
   //  R             Projected radius from halo centre [kpc]
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   //  is_use_kernel [default=true] if true, use kernel integration if exists

   bool is_verbose = false;

   // We calculate sigma_p^2(R) = [ I(R)*sigma_p^2(R)/G ] * G / I(R) with
   //    [I*sigmap2/G] = [Msol/kpc^3]
   //    [I(R)] = [Lsol] = [kpc^{-2}]  (any norm. in I(R) is absorbed in the Jeans solution)
   //    [G] = [m^3/kg/s^2]
   // The above calculation corresponds to [Msol/kg m^3/kpc /s^2], which must
   // be converted into [km^2/s^2].
   // N.B.: in light_i(), the parameter corresponding to par_jeans[13] must
   // be the maximum radius!
   double par_ref = par_jeans[13];
   par_jeans[13] = par_jeans[6];
   double i_R = light_i(R, &par_jeans[7]);

   if (i_R < 1.e-60) { // If i_light is zero (e.g. for King profile above rlim)
      cout << "In jeans_sigmap2, the surface brightness profile I(" << R
           << ") is close to zero. sigmap2 is set to 0. " << endl;
      return 0.;
   }

   double prefactor = (NEWTON_CONSTANT_SI * M3perKG_to_KM2KPCperMSOL) / i_R;
   par_jeans[13] = par_ref;

   // N.B.: if no kernel exists, force numerical integration
   //   -> kBAES does not have a kernel
   if (int(par_jeans[19]) == kBAES)
      is_use_kernel = false;

   // Ensure par_jeans[13]=R
   par_jeans[13] = R;

   // To have the most generic calculation, we fill a par_jeans_opt[27] parameter,
   // where the last parameter decides whether to use the kernel solution
   // or full solution for the integrand
   const int n_new = 27;
   double par_jeans_opt[n_new];
   for (int i = 0; i < n_new - 1; ++i)
      par_jeans_opt[i] = par_jeans[i];
   par_jeans_opt[n_new - 1] = (int)is_use_kernel;

   // To optimise integration time, proceed in several steps:
   //    1. Find rmax which gives the maximum valmax of integrand
   //    2. Find rlow and rup for which integrand(rlow)=integrand(rup)=valmax*fraction
   //    3. Integrate on ranges [zero,rmin] + [rmin,rlow] + [rlow,rup] + [rup,rmax]
   // N.B.: there are slight differences whether the integration is performed on the kernel
   // solution (integrated on dr from 0 to Rmax), or on the general projected jeans solution
   // (integrated on dy from 0 to sqrt(Rmax^2-R^2), taken here to be Rmax)
   double xlow = 0.;
   double xup = 0.;

   if (is_verbose)
      cout << "   0. SQUEEZE AXES" << endl;

   // 0. Squeeze optimal range for integration
   // Start with large range, and adapt until range brackets reasonable values of integrand
   double ax = 1.e-3 * par_jeans[8];
   double cx = 1.e3 * par_jeans[8];
   double bx = 0.;
   if (is_use_kernel) {
      ax = R;
      //cx = min(1.e4*R,cx);
   }
   // N.B.: in case value is zero for ax or bx, adapt until it is non-zero
   //       (otherwise, goldenmin_logstep fails!).
   double par_ref_opt = par_jeans_opt[13];
   par_jeans_opt[13] = R ;
   par_jeans_opt[14] = 0.1; // set "bad" precision for this step

   int nstep = 10;
   double pas_log_r = pow(cx / ax, 1. / double(nstep - 1));
   double test = 0.;
   double y_R = 0.;
   double Rp = R * 1.5;
   jeans_isigmap2_integrand_log(Rp, par_jeans_opt, y_R);
   if (is_verbose)
      cout << "y_R before = " << y_R << endl;
   for (int j = 0; j < nstep; ++j) {
      double ytest = ax * pow(pas_log_r, j);
      jeans_isigmap2_integrand_log(ytest, par_jeans_opt, test);
      y_R = max(y_R, test);
   }

   if (y_R < 1.e-60) // the integrand is equal to 0 everywhere
      return 0.;
   if (is_verbose)
      cout << "y_R = " << y_R << endl;

   int N_it = 0;
   double tol;
   double size_step;
   if (is_use_kernel) {
      tol = 1.e-4;
      size_step = 1.2;
   } else {
      tol = 1.e-6;
      size_step = 1.5;
   }

   do {
      N_it = N_it + 1;
      double y_ax = 0., y_cx = 0.;
      jeans_isigmap2_integrand_log(ax, par_jeans_opt, y_ax);
      jeans_isigmap2_integrand_log(cx, par_jeans_opt, y_cx);
      if (is_verbose)
         cout << "  sigma_p loop: " << ax << " " << cx << "   "
              << y_ax << " " << y_cx << "  y(R)=" << y_R << endl;
      if (y_ax / y_R < tol || y_cx / y_R < tol) {
         if (y_ax / y_R < tol) ax *= size_step;
         if (y_cx / y_R < tol) cx /= size_step;
         if (ax > cx) {
            if (is_use_kernel) {
               ax = R;
               cx *= size_step;
               break;
            }
            printf("\n====> ERROR: jeans_sigmap2() in jeans_analysis.cc");
            printf("\n             Fail to find integration ranges");
            printf("\n             => abort()\n\n");
            abort();
         }
      } else {
         ax /= size_step;
         cx *= size_step;
         break;
      }
   } while (1);

   if (is_verbose)
      cout << "N iterations for finding integration range = " << N_it << endl;

   double ax_opt = ax;
   double cx_opt = cx;

   // 1. Search for maximum
   // Use integrand log to search for max (we integrate afterwards in log step).
   // We use goldenmin_logstep method in which y(bx) must be larger than y(ax) and y(cx).
   // N.B.: specific case if Kernel is used, as the integration starts at r=R
   if (is_verbose)
      cout << "   1. SEARCH MAX" << endl;
   bx = sqrt(ax * cx);
   double x_valmax = 0.;
   double valmax = 0.;
   par_jeans_opt[13] = R ;
   // Set minimum precision for computing of the integrand for
   // finding maximum and caracteristic radii
   par_jeans_opt[14] = 0.05;

   if (par_jeans[14] > 0. && par_jeans[14] < par_jeans_opt[14])
      par_jeans_opt[14] = par_jeans[14];

   // minimum precision for finding the optimal range of integration
   double eps = 0.05;
   if (par_jeans[14] < eps)
      eps = par_jeans[14];

   valmax = goldenmin_logstep(ax, bx, cx, jeans_isigmap2_integrand_log, par_jeans_opt, eps, false, x_valmax);
   par_jeans_opt[13] = par_ref_opt;

   if (is_verbose)
      cout << "           R=" << R << " x_valmax=" << x_valmax << " valmax=" << valmax << endl;

   // 2. Find xlow and xup int_log(xlow)=int_log(xup)=int_log(x_valmax)*fraction
   // N.B.: specific case if Kernel is used, as the integration starts at r=R
   //      - if x_valmax>R => xlow=R
   //double fraction = 0.9;
   double fraction = 0.9;
   double y_ref = valmax * fraction;
   if (is_verbose)
      cout << "   2.a. XLOW" << endl;
   // xlow
   double x1 = ax_opt;
   double x2 = x_valmax;

   find_x_logstep(y_ref, xlow, x1, x2, jeans_isigmap2_integrand_log, par_jeans_opt, eps);
   if (is_verbose)
      cout << "   xlow = " << xlow << endl;
   if (is_verbose)
      cout << "   2.b. XUP" << endl;
   // xup
   x1 = x_valmax;
   x2 = cx_opt;
   find_x_logstep(y_ref, xup, x1, x2, jeans_isigmap2_integrand_log, par_jeans_opt, eps);

   if (is_verbose)
      cout << "   xup = " << xup << endl;


   //  3. Integrate on ranges:
   //     - If standard integration: [zero,xlow] + [xlow,x_valmax], [x_val_max,xup] + [xup,cx_opt]
   //     - If Kernel: [ax_opt,xlow] + [xlow,x_valmax] + [x_valmax,xup] + [xup,cx_opt]
   if (is_verbose)
      cout << "   3. INTEGRATE" << endl;
   if (par_jeans[6] < cx_opt) {
      printf("\n====> ERROR: jeans_sigmap2() in jeans_analysis.cc");
      printf("\n             For calculation at R=%.2le, the (upper) integration boundary", R);
      printf("\n             Rintegr (here %.2le)  must be larger than %.2le",  par_jeans[6], cx_opt);
      printf("\n             => abort()\n\n");
      abort();
   }

   par_jeans_opt[14] = par_jeans[14]; // reset precision to its initial value

   // Fill intermediate points (for integration)
   vector<double> rr;
   if (is_use_kernel) {
      rr.push_back(R);
      if (sqrt(R * sqrt(R * xlow)) > rr[rr.size() - 1] && sqrt(R * sqrt(R * xlow)) < cx_opt)
         rr.push_back(sqrt(R * sqrt(R * xlow)));
      if (sqrt(R * xlow) > rr[rr.size() - 1] && sqrt(R * xlow) < cx_opt)
         rr.push_back(sqrt(R * xlow));
      if (sqrt(sqrt(R * xlow) * xlow) > rr[rr.size() - 1] && sqrt(sqrt(R * xlow) * xlow) < cx_opt)
         rr.push_back(sqrt(sqrt(R * xlow) * xlow));
   } else {
      rr.push_back(0.);
      rr.push_back(ax_opt);
   }
   if (xlow > rr[rr.size() - 1] && xlow < cx_opt)
      rr.push_back(xlow);
   if (x_valmax > rr[rr.size() - 1] && x_valmax < cx_opt)
      rr.push_back(x_valmax);
   if (xup > rr[rr.size() - 1] && xup < cx_opt)
      rr.push_back(xup);
   if (10.* rr[rr.size() - 1] < cx_opt) {
      rr.push_back(sqrt(rr[rr.size() - 1]*cx_opt));
   }
   rr.push_back(cx_opt);


   double res = 0.;
   for (int i = 0; i < (int)rr.size() - 1; ++i) {
      double tmp_res = 0.;
      if (i == 0 && !is_use_kernel)
         simpson_lin_adapt(jeans_isigmap2_integrand, rr[i], rr[i + 1], par_jeans_opt, tmp_res, par_jeans[14]);
      else
         simpson_log_adapt(jeans_isigmap2_integrand, rr[i], rr[i + 1], par_jeans_opt, tmp_res, par_jeans[14]);
      res += tmp_res;
   }
   if (is_verbose)
      cout << "   R = " << R << " DONE!" << endl;

   return res * prefactor;
}

//______________________________________________________________________________
void halo_jeans(vector<double> const &x, int switch_y)
{
   //--- Prints/Plots y(x) and y(x)/y(x[0]) for a halo with Jeans analysis parameters.
   //
   //  x              Grid of x-axis values
   //  switch_y       Quantity to display
   //                    0 => sigma_p(R_kpc)
   //                    1 => I(R_kpc)
   //                    2 => beta_ani(r_kpc)

   // Load CLUMPY parameters
   vector<struct gStructHalo> list_halos;
   vector<struct gStructJeansData> list_jeansdata;

   // load Jeans parameters
   halo_loadlist4jeans(gLIST_HALOES_JEANS, list_halos, list_jeansdata, switch_y);

   // Set ROOT style
#if IS_ROOT
   rootstyle_set2CLUMPY();
#endif

   int n_x = x.size();
   int n_obj = (int)list_halos.size();

   // What is going to be calculated (for prints and plots)
   //  => 3 values or y (switch_y=0, 1, or 2)
   string cvs_name[3] = {"sigmapR", "IR", "Betar"};
   string x_name[3] = {"R [kpc]", "R [kpc]", "r [kpc]"};
   string y_name[3] = {"#sigma_{p} [km s^{-1}]", "I [L_{#odot} kpc^{-2}]", "#beta_{ani}"};
   string y_norm_name[3] = {"Normalised #sigma_{p}(R)", "Normalised I(R)", "Normalised #beta_{ani}(r)"};

   string x_name_ecsv[3] = {"R", "R", "r"};
   string x_unit_ecsv[3] = {"kpc", "kpc", "kpc"};
   string y_name_ecsv[3] = {"sigma_p", "I", "beta_ani"};
   string y_unit_ecsv[3] = {"km s^-1", "Lsun kpc^-2", ""};
   string y_norm_name_ecsv[3] = {"sigma_p_norm", "I_norm", "beta_ani_norm"};

   cout << ">>>>> " << y_name[switch_y] << " for all halos in " << gLIST_HALOES_JEANS << endl;

   if (switch_y == 0 || switch_y == 1)
      cout << "  => Relative precision: " << gSIM_EPS << endl;

   if (n_obj <= 0) {
      cout << "...... no haloes found in the list: nothing to do" << endl;
      return;
   }

   // for plotting the associated data [optional]
   vector<vector <double> > x_data;
   vector<vector <double> > dx_data;
   vector<vector <double> > y_data;
   vector<vector <double> > dy_data;
   vector<string> name_files;
   bool is_data = false; // Are there data to plot?

   //--- Storage variables
   vector<string> names, namesdata, legend, legenddata;
   vector<double> y(n_obj * n_x, 0.);
   vector<double> y_norm(n_obj * n_x, 0.);

   string f_halo_name = gLIST_HALOES_JEANS;

   FILE *fp[2] = {NULL, NULL};
   FILE *fp_current;
   unsigned found = f_halo_name.find_last_of("/"); // if the file is in a directory, keep only the file name without the path
   if ((int)found != -1) // a "/" was found
      f_halo_name = f_halo_name.substr(found + 1);

   string f_name = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".ecsv";
   fp[0] = fopen(f_name.c_str(), "w");
   fp[1] = stdout;

   //vector<string> list_type;
   vector<int> type_data;

   // Calculate and fill params for all haloes
   for (int i = 0; i < n_obj ; ++i) {
      cout << i << endl;
      // Update par and store graph names
      const int nparjeans = 26;
      double par_jeans[nparjeans];
      stat_set_parjeans(i, par_jeans, list_halos); // set the Jeans parameters
      //print_parjeans(par_jeans);

      double Rmax = par_jeans[6];
      par_jeans[6] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
      par_jeans[14] = gSIM_EPS; // Set precision

      string tmp_name = halo_get_name(i, list_halos) + "_" + halo_get_type(i, list_halos);
      names.push_back(tmp_name);
      legend.push_back(list_halos[i].Name + " (" + list_halos[i].Type + ")");

      if (list_jeansdata.size() > 0) {
         is_data = true;
         cout << list_jeansdata[i].FileName << endl;

         string tmp_name_data = halo_get_name(i, list_halos) + "_" + halo_get_type(i, list_halos) + "_data";
         namesdata.push_back(tmp_name_data);
         legenddata.push_back(list_halos[i].Name + " (" + list_halos[i].Type + ")" + " - Data ");

         name_files.push_back(list_jeansdata[i].FileName);
      }

      vector<double> xdata;
      vector<double> dxdata;
      vector<double> ydata;
      vector<double> dydata;
      int xdatasize = 0;
      if (is_data) {
         xdatasize = list_jeansdata[i].R.size();
         type_data.push_back(list_jeansdata[i].Type);
         if (type_data.size() > 1) {
            if (type_data[type_data.size() - 1] != type_data[type_data.size() - 2]) { // Two data files have different TypeData keyword, abort
               printf("\n====> ERROR: halo_jeans() in jeans_analysis.cc");
               printf("\n             At least two data files have different data types (e.g., Sigmap and Sigmap2).");
               printf("\n             => abort()\n\n");
               abort();
            }
         }
         if (list_jeansdata[i].Type  == 2) { // One data file has Vel TypeData keyword (unbinned data), abort
            printf("\n====> ERROR: halo_jeans() in jeans_analysis.cc");
            printf("\n             At least one file contains binned velocities (keyword Vel => expect unbinned).");
            printf("\n             => abort()\n\n");
            abort();
         }
         if (list_jeansdata[i].Type  == 0) { // draw sigmap2 instead of sigmap
            print_warning("jeans_analysis.cc", "halo_jeans()", "At least one file contains velocity dispersions square => Draw sigmap^2.");
            cvs_name[0] = "sigmap2R";
            y_name[0] = "#sigma_{p}^{2} [km^{2} s^{-2}]";
            y_norm_name[0] = "Normalised #sigma_{p}^{2}(R)";
            f_name = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".ecsv";
         }

         // Fill the data
         for (int m = 0; m < xdatasize; m++) {
            xdata.push_back(list_jeansdata[i].R[m]);
            dxdata.push_back(list_jeansdata[i].RErr[m]);
            ydata.push_back(list_jeansdata[i].Val[m]);
            dydata.push_back(list_jeansdata[i].ValErr[m]);
         }
         // Fill the vector of data
         x_data.push_back(xdata);
         dx_data.push_back(dxdata);
         y_data.push_back(ydata);
         dy_data.push_back(dydata);
      }

      // Print header
      if (gSIM_IS_PRINT and i==0) {
         // Print header
         for (int ff = 0; ff < 2; ++ff) {
            fp_current = fp[ff];
            if (ff == 0){
                fprintf(fp_current, "# %%ECSV 1.0\n");
                fprintf(fp_current, "# ---\n");
                fprintf(fp_current, "# datatype:\n");
            }
            fprintf(fp_current, "# - {name: %s, unit: %s, datatype: float64}\n", x_name_ecsv[switch_y].c_str(), x_unit_ecsv[switch_y].c_str());
            fprintf(fp_current, "# - {name: %s, unit: %s, datatype: float64}\n", y_name_ecsv[switch_y].c_str(), y_unit_ecsv[switch_y].c_str());
            if (switch_y != 1){
               fprintf(fp_current, "# - {name: obj_name, unit: , datatype: string}\n");
               fprintf(fp_current, "%s %s obj_name\n", x_name_ecsv[switch_y].c_str(), y_name_ecsv[switch_y].c_str());
            }
            else{
               fprintf(fp_current, "# - {name: %s, unit:, datatype: float64}\n", y_norm_name_ecsv[switch_y].c_str());
               fprintf(fp_current, "# - {name: obj_name, unit: , datatype: string}\n");
               fprintf(fp_current, "%s %s %s obj_name\n", x_name_ecsv[switch_y].c_str(), y_name_ecsv[switch_y].c_str(), y_norm_name_ecsv[switch_y].c_str());
            }
         }
      }

      // Calculate y
      for (int k = 0; k < n_x; ++k) {
         double tmp_x = x[k];
         if (switch_y == 0) { // 0. sigma_p(R)
            par_jeans[13] = tmp_x; // Radius considered
            if (list_jeansdata[i].Type  == 0) // compute sigmap2
               y[i * n_x + k] = jeans_sigmap2(tmp_x, par_jeans);
            else
               y[i * n_x + k] = sqrt(jeans_sigmap2(tmp_x, par_jeans));
         } else if (switch_y == 1) { // 1. I(R)
            double temp = par_jeans[13];
            par_jeans[13] = gSIM_JEANS_RMAX; // Set the maximum radius of integration
            y[i * n_x + k] = light_i(tmp_x, &par_jeans[7]);
            par_jeans[13] = temp; // set back par_jeans[13] to its initial value
         } else if (switch_y == 2) { // 2. beta_ani(r)
            y[i * n_x + k] = beta_anisotropy(tmp_x, &par_jeans[15]);
         }
         if (abs(y[i * n_x + 0]) > 1.e-30)
            y_norm[i * n_x + k] = y[i * n_x + k] / y[i * n_x + 0];
         else {
            cout << y_name[switch_y].c_str() << " is close to 0 for first radius considered: y/y_norm is set to 1 " << endl;
            y_norm[i * n_x + k] = 1.;
         }
         // Print on screen and in file)
         if (gSIM_IS_PRINT) {
            for (int ff = 0; ff < 2; ++ff) {
               fp_current = fp[ff];
               if (switch_y != 1)
                  fprintf(fp_current, "  %.*le   %.*le %s\n", gSIM_SIGDIGITS + 2,  x[k],  gSIM_SIGDIGITS, y[i * n_x + k], names[i].c_str());
               else
                  fprintf(fp_current, "  %.*le   %.*le   %.*le %s\n",
                          gSIM_SIGDIGITS + 2, x[k],  gSIM_SIGDIGITS, y[i * n_x + k],  gSIM_SIGDIGITS, y_norm[i * n_x + k], names[i].c_str());
            }
         }
      }
      par_jeans[6] = Rmax; // set back par_jeans[6] to its initial value
   }

   vector<int> indexes;
   indexes.push_back(0);

   for (int i = 0; i < (int)name_files.size(); i++) {
      bool isequal = 0;
      for (int k = 0; k < (int)indexes.size(); k++) {
         if (name_files[i] == name_files[indexes[k]])
            isequal = 1;
      }
      if (isequal == 0)
         indexes.push_back(i);
   }

   if (gSIM_IS_PRINT) {
      fclose(fp[0]);
      cout << endl;
      cout << "______________________________________________" << endl << endl;
      cout << " ... output [ASCII] written in: " << f_name << endl;
      if (!gSIM_IS_WRITE_ROOTFILES)
         cout << "______________________________________________" << endl << endl;
   }

   // Displays  and/or save .root format
#if IS_ROOT
   string f_root = gSIM_OUTPUT_DIR + f_halo_name + "." + cvs_name[switch_y] + ".root";
   TFile *root_file = NULL;
   if (gSIM_IS_WRITE_ROOTFILES)
      root_file = new TFile(f_root.c_str(), "recreate");
   if (gSIM_IS_DISPLAY || gSIM_IS_WRITE_ROOTFILES) {
      // TGraph related
      bool is_logx = gSIM_IS_XLOG;
      bool is_logy = true;
      if (switch_y == 2)
         is_logy = false;

      // Tlegend related
      double leg_xy[4] = {0., 0., 0., 0.};
      string leg_header = "Displayed halos:";
      // TText related
      vector<string> text;
      bool is_text = false;

      // Set graph properties depending on switch_y
      //  1. rho(r)
      if (switch_y == 0) {
         leg_xy[0] = 0.65;
         leg_xy[1] = 0.58;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.92;

         //  2. J(alpha_int)
      } else if (switch_y == 1) {
         leg_xy[0] = 0.42;
         leg_xy[1] = 0.38;
         leg_xy[2] = 0.95;
         leg_xy[3] = 0.72;
         if (x[0] <= 1.e-40) is_logy = false;

         //  3. J(theta)
      } else if (switch_y == 2) {
         leg_xy[0] = 0.18;
         leg_xy[1] = 0.18;
         leg_xy[2] = 0.41;
         leg_xy[3] = 0.52;
      }

      int n_cvs = 2;
      TMultiGraph **multi = new TMultiGraph*[n_cvs];
      TLegend **leg = new TLegend*[n_cvs];
      TCanvas **c_list = new TCanvas*[n_cvs];
      TGraph **gr =  new TGraph*[2 * n_obj];
      TGraphErrors **grdata =  NULL;
      if (is_data)
         grdata = new TGraphErrors*[2 * n_obj];

      vector<double> xd;
      for (int k = 0; k < n_x; ++k)
         xd.push_back(x[k]);

      for (int c = 0; c < n_cvs; ++c) {
         // No normalisation for beta(r)
         if (switch_y != 1 && c == 1)
            continue;

         // Fill graphs
         multi[c] = new TMultiGraph();
         if (c == 2) {
            leg_xy[0] = 0.42;
            leg_xy[1] = 0.1;
            leg_xy[2] = 0.95;
            leg_xy[3] = 0.6;
         };
         leg[c] = new TLegend(leg_xy[0], leg_xy[1], leg_xy[2], leg_xy[3]);
         leg[c]->SetFillColor(kWhite);
         leg[c]->SetTextSize(0.03);
         leg[c]->SetBorderSize(0);
         leg[c]->SetHeader(leg_header.c_str());
         leg[c]->SetFillStyle(0);


         for (int i = 0; i < n_obj; ++i) {
            // Fill and set graph properties!
            if (c == 0 || c == 1) {
               string name = names[i];
               if (c == 0)
                  gr[c * n_obj + i] = new TGraph(n_x, &xd[0], &y[i * n_x + 0]);
               else if (c == 1) {
                  name = name + "_norm";
                  gr[c * n_obj + i] = new TGraph(n_x, &xd[0], &y_norm[i * n_x + 0]);
               }
               if (c == 1)
                  gr[c * n_obj + i]->SetName(names[i].c_str());
               gr[c * n_obj + i]->SetTitle(names[i].c_str());
               gr[c * n_obj + i]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
               gr[c * n_obj + i]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
               gr[c * n_obj + i]->SetLineColor(rootcolor(i));
               gr[c * n_obj + i]->SetLineStyle(i % 9 + 1);
               gr[c * n_obj + i]->SetLineWidth(2);
               if (gSIM_IS_WRITE_ROOTFILES)
                  gr[c * n_obj + i]->Write();
            }
            if (c == 0 && is_data) {
               grdata[c * n_obj + i] = new TGraphErrors(x_data[i].size(), &x_data[i][0], &y_data[i][0], &dx_data[i][0], &dy_data[i][0]);
               grdata[c * n_obj + i]->SetName(namesdata[i].c_str());
               grdata[c * n_obj + i]->SetTitle(namesdata[i].c_str());
               grdata[c * n_obj + i]->SetMarkerColor(rootcolor(i));
               grdata[c * n_obj + i]->SetMarkerStyle(rootmarker(i));
               grdata[c * n_obj + i]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
               grdata[c * n_obj + i]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
               if (gSIM_IS_WRITE_ROOTFILES)
                  grdata[c * n_obj + i]->Write();
            }
            // Add in multi and set legends
            multi[c]->Add(gr[c * n_obj + i], "L");
            leg[c]->AddEntry(gr[c * n_obj + i], legend[i].c_str(), "L");
         }

         // Add the data to the canvas; if two data files have the same name, the data set is drawn only once
         if (is_data) {
            for (int k = 0; k < (int)indexes.size(); k++) {
               if (c == 0 && x_data[indexes[k]].size() != 0) {
                  multi[c]->Add(grdata[c * n_obj + indexes[k]], "p");
                  leg[c]->AddEntry(grdata[c * n_obj + indexes[k]], legenddata[indexes[k]].c_str(), "p");
               }
            }
         }

         // Draw canvas
         string tmp_cvs = cvs_name[switch_y];
         if (c == 1) tmp_cvs = cvs_name[switch_y] + "_norm";
         if (gSIM_IS_DISPLAY) {
            c_list[c] = new TCanvas(tmp_cvs.c_str(), tmp_cvs.c_str(), c * 650, 0, 650, 500);
            c_list[c]->SetLogx(is_logx);
            c_list[c]->SetLogy(is_logy);


            // Plot graphs and legends
            gSIM_CLUMPYAD->Draw();
            multi[c]->Draw("A");
            multi[c]->GetXaxis()->SetTitle(x_name[switch_y].c_str());
            multi[c]->GetYaxis()->SetTitle(y_name[switch_y].c_str());
            if (c == 1) multi[c]->GetYaxis()->SetTitle(y_norm_name[switch_y].c_str());
            leg[c]->Draw("SAME");

            // Text if required
            TPaveText *txt = new TPaveText(0.3, 0.8, 0.5, 0.9, "NDC");
            txt->SetTextSize(0.05);
            txt->SetTextFont(132);
            txt->SetFillColor(0);
            txt->SetBorderSize(0);
            txt->SetTextColor(kGray + 2);
            txt->SetFillStyle(0);
            txt->SetTextAlign(12);
            for (int ii = 0; ii < (int)text.size(); ++ii)
               txt->AddText(text[ii].c_str());
            if (is_text) txt->Draw();

            c_list[c]->Update();
            c_list[c]->Modified();

            if (gSIM_IS_WRITE_ROOTFILES) {
               root_file->cd();
               gPad->Write();
            }
         }
      }
      if (gSIM_IS_WRITE_ROOTFILES) {
         root_file->Close();
         delete root_file;
         cout << " ... output [use ROOT TBrowser] written in: " << f_root << endl;
      }
      if (gSIM_IS_PRINT)
         cout << "_______________________" << endl << endl;

      if (gSIM_IS_DISPLAY)
         gSIM_ROOTAPP->Run(kTRUE);

      // Free memory
      delete[] leg;
      delete[] multi;
      if (c_list)
         delete[] c_list;
      if (is_data)
         if (grdata) delete[] grdata;
      if (gr) delete[] gr;
   }
#endif
}

//______________________________________________________________________________
void load_jeansdata(string const  &filename, struct gStructJeansData &jeans_data, bool is_verbose)
{
   //--- Reads a Jeans analysis data file (kinematics or surface brightness)
   //    The format must be as in data/dataSigmap.txt or data/dataLight.txt
   //    Data are stored in the gStructJeansData jeans_data.
   //    N.B.: a Jeans analysis kinematic data file must be formatted as follows
   //   (see data/dataVel.txt and data/dataSigmap.txt):
   //       TypeData
   //       R[kpc]  dR[kpc]  V[km/s_or_km^2/s^2] dV[km/s_or_km^2/s^2] (if use of Vel TypeData, you can add MembershipProba, RA[deg], Dec[deg])
   //    N.B.: a Jeans analysis surface brightness data file must be formatted
   //    as follows (see data/dataLight.txt):
   //      TypeData
   //      R[kpc]  dR[kpc]  I[L/kpc^2] dI[L/kpc^2]
   //
   // INPUTS:
   //  filename      File to be read
   //  jeans_data    gStructJeansData structure to store the data
   //  is_verbose    Chatter or not...
   // OUTPUT:
   //  jeans_data    gStructJeansData structure to store the data


   if (is_verbose) {
      cout << ">>>>> Read and load " << filename << endl;
      cout << "   N.B.: a Jeans analysis kinematic data file must be formatted as follows "
           "(see data/dataVel.txt and data/dataSigmap.txt):" << endl;
      cout << "     TypeData" << endl;
      cout << "     R[kpc]  dR[kpc]  V[km/s_or_km^2/s^2] dV[km/s_or_km^2/s^2]"
           "  (if use of Vel TypeData, you can add MembershipProba, RA[deg], Dec[deg])" << endl;
      cout << "   N.B.: a Jeans analysis surface brightness data file must be "
           "formatted as follows (see data/dataLight.txt):" << endl;
      cout << "     TypeData" << endl;
      cout << "     R[kpc]  dR[kpc]  I[L/kpc^2] dI[L/kpc^2]" << endl;

   }

   // Open file and check if exists
   ifstream f_read(filename.c_str());
   if (!f_read) {
      printf("\n====> ERROR: load_jeansdata() in jeans_analysis.cc");
      printf("\n             Cannot open (and read) file %s", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   // Get file name
   jeans_data.FileName = filename;

   const int n_keyword_JeansData = 9;
   string keyword_JeansData [n_keyword_JeansData] = {
      "R[kpc]", "dR[kpc]", "V[km/s_or_km^2/s^2]",
      "dV[km/s_or_km^2/s^2]", "MembershipProba", "I[L/kpc^2]",
      "dI[L/kpc^2]", "RA[deg]", "Dec[deg]"
   };

   string names_line;
   int iuse = 0;
   int i = 0;
   bool is_angles = 0;
   bool is_membership = 0;

   // Loop over the header - find the keywords
   while (iuse < 3) {
      getline(f_read, names_line);
      names_line.erase(remove_if(names_line.begin(), names_line.end(), isNotASCII), names_line.end());

      // if it is a comment (start with #) or a blanck line, skip it
      string line = removeblanks_from_startstop(names_line);
      if (line[0] == '#' || line.empty()) {
         i = i + 1;
         continue;
      }
      if (iuse == 1) { // 2nd non-comment or non-empty line (in file): contains "Sigmap2", "Sigmap", "Vel", or "SB"
         vector<string> type_params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", type_params);
         jeans_data.Type = index_in_list(jeans_data.TypeNames, type_params[0], false);

         if (jeans_data.Type < 0) {
            cout << type_params[0] << endl;
            printf("\n====> ERROR: load_jeansdata() in jeans_analysis.cc");
            printf("\n             TypeData keyword must be \"Vel\", \"Sigmap2\", \"Sigmap \", or \"SB\"");
            printf("\n             => abort()\n\n");
            abort();
         }
      }
      if (iuse == 2) { // 3nd non-comment or non-empty line (in file): contains "R[kpc]  dR[kpc]   V[km/s_or_km^2/s^2]"
         // or "I[#/kpc^2])  dV[km/s_or_km^2/s^2]"" (or dI[#/kpc^2])  MembershipProba
         // (if use of Vel TypeData) RA[deg] [optionnal] Dec[deg] [optionnal]
         vector<string> read_params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", read_params);
         if (jeans_data.Type == 2) { // Velocities
            if (read_params.size() < 4) {
               printf("\n====> ERROR: load_jeansdata() in jeans_analysis.cc");
               printf("\n             Wrong number (=%d) of kinematic data keywords, you must use at least (in that order)",
                      (int)read_params.size());
               printf("\n             \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"");
               printf("\n             => abort()\n\n");
               abort();
            }
            if (read_params.size() == 4) { //no Membership, Ra and Dec
               for (int m = 0; m < 4; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     printf("\n====> ERROR: load_jeansdata() in jeans_analysis.cc");
                     printf("\n             Wrong names for kinematic data keywords, there should be (in that order)");
                     printf("\n             \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"");
                     printf("\n             => abort()\n\n");
                     abort();
                  }
               }
            } else if (read_params.size() == 5) { //Membership probability but no Ra and Dec
               is_membership = 1;
               for (int m = 0; m < 5; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     abort();
                  }
               }
            } else if (read_params.size() == 6) { // Ra and Dec but no membership probabilities
               is_angles = 1;
               for (int m = 0; m < 4; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     abort();
                  }
               }
               if (read_params[4] != keyword_JeansData[7] || read_params[5] != keyword_JeansData[8]) {
                  cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"  [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                  abort();
               }
            } else if (read_params.size() == 7) { // Membership probability, Ra, and Dec
               is_angles = 1;
               is_membership = 1;
               for (int m = 0; m < 5; m++) {
                  if (read_params[m] != keyword_JeansData[m]) {
                     cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                     abort();
                  }
               }
               if (read_params[5] != keyword_JeansData[7] || read_params[6] != keyword_JeansData[8]) {
                  cout << "\n====> ERROR: in the kinematic data keywords,  you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\"  [+ optional MembershipProba RA[deg] Dec[deg]] in that order" << endl;
                  abort();
               }
            }
         } else if (jeans_data.Type == 0 || jeans_data.Type == 1) { // Velocity dispersions (sigmap or sigmap2)

            if (read_params.size() != 4) {
               cout << "\n====> ERROR: wrong number of kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" in that order" << endl;
               abort();
            }
            for (int m = 0; m < 4; m++) {
               if (read_params[m] != keyword_JeansData[m]) {
                  cout << "\n====> ERROR: in the kinematic data keywords, you must use \"R[kpc]\"  \"dR[kpc]\"   \"V[km/s_or_km^2/s^2]\"  \"dV[km/s_or_km^2/s^2]\" in that order" << endl;
                  abort();
               }
            }
         } else if (jeans_data.Type == 3) { // Surface brightness
            if (read_params.size() != 4) {
               cout << "\n====> ERROR: wrong number of Light Data Keyword, you must use \"R[kpc]\"  \"dR[kpc]\"   \"I[L/kpc^2]\"  \"dI[L/kpc^2]\" in that order" << endl;
               abort();
            }

            if (read_params[0] != keyword_JeansData[0] || read_params[1] != keyword_JeansData[1] || read_params[2] != keyword_JeansData[5] || read_params[3] !=
                  keyword_JeansData[6]) {
               cout << "\n====> ERROR: in the Light Data Keyword, you must use \"R[kpc]\"  \"dR[kpc]\"   \"I[L/kpc^2]\"  \"dI[L/kpc^2]\" in that order" << endl;
               abort();
            }
         }
      }
      i = i + 1;
      iuse = iuse + 1;
   }
   f_read.close();

   // Fill jeans_data with data
   ifstream f_data(filename.c_str());
   string line_input;
   int k = 0;
   while (getline(f_data, line_input).good()) {
      line_input.erase(remove_if(line_input.begin(), line_input.end(), isNotASCII), line_input.end());
      if (k < i) {
         k = k + 1;
      } else {
         // if it is a comment (start with #) or a blanck line, skip it
         string line = removeblanks_from_startstop(line_input);
         if (line[0] == '#' || line.empty()) {
            continue;
         }
         vector<double> params;
         replace_substring(line, "\t", " ");
         string2list(line, " ", params);
         jeans_data.R.push_back(params[0]);
         jeans_data.RErr.push_back(params[1]);
         jeans_data.Val.push_back(params[2]);
         jeans_data.ValErr.push_back(params[3]);
         if (jeans_data.Type == 2) { // Unbinned kinematic data file (velocities)
            if (is_membership && is_angles) {   // membership probabilities and angles
               jeans_data.MembershipProb.push_back(params[4]);
               jeans_data.RA.push_back(params[5]);
               jeans_data.Dec.push_back(params[6]);
            } else if (is_membership) // just membership
               jeans_data.MembershipProb.push_back(params[4]);
            else if (is_angles) { // just angles
               jeans_data.RA.push_back(params[4]);
               jeans_data.Dec.push_back(params[5]);
               jeans_data.MembershipProb.push_back(1.); // in case of no membership probabilities, set them to 1
            } else // no membership and no angles
               jeans_data.MembershipProb.push_back(1.); // in case of no membership probabilities, set them to 1
         }
      }
   }

   if (jeans_data.Type == 2) {
      printf("======== Unbinned data \"%s\" ========\n", jeans_data.TypeNames[jeans_data.Type].c_str());
      // Compute mean velocity for unbinned analysis
      update_meanvelocity(jeans_data, is_verbose);
   } else
      printf("======== Binned data \"%s\" ========\n", jeans_data.TypeNames[jeans_data.Type].c_str());

   f_data.close();
   return ;
}

//______________________________________________________________________________
void halo_loadlist4jeans(string const &file_halos, vector<struct gStructHalo> &list_halos,
                         vector<struct gStructJeansData> &list_jeansdata,  int switch_y, bool is_clear)
{
   //--- Reads list of haloes for Jeans analysis. The format must be as in
   //    data/list_generic_jeans.txt.
   //
   //  file_halos     File to be read
   //  list_halos     Vector of gStructHalo containing halo params
   //  switch_y       0->load JeansData velocity (sigmap); 1->load JeansData light (I)
   //  is_clear       Mode to load new objects from list
   //                    - true: clears any pre-loaded file (default call)
   //                    - queues the objects with some previously loaded ones (is_clear=false)
   //                      [checks that the object to be added is not already loaded]

   string file_halos_plain = file_halos;
   resolve_envvar(file_halos_plain);

   // Open file and check if exists
   ifstream f_list(file_halos_plain.c_str());
   if (!f_list) {
      printf("\n====> ERROR: halo_loadlist4jeans() in jeans_analysis.cc");
      printf("\n             Cannot find and open file %s", file_halos_plain.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   cout << ">>>>> Load " << file_halos_plain << endl;

   // get directory of file:
   string file_halos_dir = "";
   if (file_halos_plain.find_last_of("\\/") != string::npos) {
      file_halos_dir = file_halos_plain.substr(0, file_halos_plain.find_last_of("\\/"));
      file_halos_dir += "/";
   }

   vector<string> VelData; // name of data file
   vector<string> LightData;

   if (is_clear) {
      list_halos.clear();
      list_jeansdata.clear();
   }

   // Loop over lines: skip empty ones or those starting with #
   string line;
   int n_params = 0;
   bool is_at_least_onegas = false;
   while (getline(f_list, line)) {

      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());
      replace_substring(line, "\t", " ");

      struct gStructHalo tmp; // temporary structure to store the parameters and data
      size_t pos = line.find_first_not_of(" ");
      if (line.empty() || pos == string::npos || line[pos] == '#') continue;

      vector<string> params;
      string2list(line, " ", params);

      // Check file number of columns: 23 if no gas, 29 if gas distribution (e.g., for galaxy clusters)
      bool is_gas = false;
      if (params.size() != 23 && params.size() != 29) {
         printf("\n====> ERROR: halo_loadlist4jeans() in jeans_analysis.cc");
         printf("\n             Wrong number of columns (=%d) instead of 23 or 29 (if optionnal gas distrib. used).", (int)params.size());
         printf("\n             => abort()\n\n");
         abort();
      } else if (params.size() == 29) {
         is_gas = true;
         is_at_least_onegas = true;
      }

      n_params = max(n_params, (int)params.size());

      // Fill parameters
      for (int i = 0; i < n_params; ++i)
         params[i].erase(remove(params[i].begin(), params[i].end(), '\t'), params[i].end());
      if (params[4][0] != '/') params[4] = file_halos_dir + params[4];
      if (params[5][0] != '/') params[5] = file_halos_dir + params[5];


      tmp.Name = params[0];
      tmp.Type = params[1];
      tmp.Rvir = atof(params[2].c_str());
      gSIM_JEANS_RMAX = atof(params[3].c_str());
      VelData.push_back(params[4]);
      LightData.push_back(params[5]);

      struct gStructJeansData tmp_data;
      if (switch_y == 0 && params[4] != "-") { // Velocity data
         load_jeansdata(params[4], tmp_data, 0);
         list_jeansdata.push_back(tmp_data);
      } else if (switch_y == 1 && params[5] != "-") { // sigma or sigma2 data
         load_jeansdata(params[5], tmp_data, 0);
         list_jeansdata.push_back(tmp_data);
      }

      if (tmp.HaloProfile == kNODES and atof(params[6].c_str()) == -1) {
         tmp.Rhos = gHALO_NODES_INPUT_RHOSCALE[atoi(params[9].c_str())];
         tmp.Rscale = gHALO_NODES_INPUT_RSCALE[atoi(params[9].c_str())];
      } else {
         tmp.Rhos = atof(params[6].c_str());
         tmp.Rscale = atof(params[7].c_str());
      }
      tmp.HaloProfile = string_to_enum("FLAG_PROFILE", params[8]);
      tmp.ShapeParam1 = atof(params[9].c_str());
      tmp.ShapeParam2 = atof(params[10].c_str());
      tmp.ShapeParam3 = atof(params[11].c_str());

      // convert rho_s to rho_0 as used in profiles.cc for ZHAO profile:
      if (tmp.HaloProfile == kZHAO) tmp.Rhos *= pow(2, (tmp.ShapeParam2 - tmp.ShapeParam3) / tmp.ShapeParam1);
      else if (tmp.HaloProfile == kNODES) {
         // check if node data is loaded:
         if (gHALO_NODES_X_GRID.size() == 1) {
            int standardparam_length = string(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES]).length() + 1;

            cout << "\n====> Your choice of kNODES halo profile in " << file_halos_plain << " requires the additional input parameter:" << endl << endl;
            cout << string_fixlength("# Variable name", 40) << string_fixlength("Unit", gSIM_PARAMLENGTH_MAX) << string_fixlength("Standard Value", gSIM_PARAMLENGTH_MAX + 3)
                 << string_fixlength("(Format)", gSIM_PARAMLENGTH_MAX + 3) << "(Comment)" << endl << endl;
            cout << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_VARNAME], 40)
                 << string_fixlength(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_UNIT], gSIM_PARAMLENGTH_MAX)
                 << string_fixlength(gSIM_STANDARD_INPUTPARAMS[kLIST_HALOES_NODES], standardparam_length) << "   "
                 << string_fixlength("<" + string(gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DATATYPE]) + ">", gSIM_PARAMLENGTH_MAX + 3)
                 << gSIM_INPUTPARAMS[kLIST_HALOES_NODES][kSIM_INPUTPARAM_DESCRIPTION] << endl;
            cout << endl;
            cout         << "      Please add it to the command line or your input parameter file " <<  endl;
            cout       << "      and restart the program."  << endl <<  endl;
            abort();
         }
      }
      tmp.L =  atof(params[12].c_str());
      tmp.RLight =  atof(params[13].c_str());
      tmp.LightProfile = string_to_enum("FLAG_LIGHTPROFILE", params[14]);
      tmp.LightShapeParam1 = atof(params[15].c_str());
      tmp.LightShapeParam2 = atof(params[16].c_str());
      tmp.LightShapeParam3 = atof(params[17].c_str());
      tmp.Beta_Aniso_0 = atof(params[18].c_str());
      tmp.Beta_Aniso_Inf = atof(params[19].c_str());
      tmp.AnisoProfile = string_to_enum("FLAG_ANISOTROPYPROFILE", params[20]);
      tmp.RAniso = atof(params[21].c_str());
      tmp.AnisoShapeParam = atof(params[22].c_str());
      if (is_gas) {
         tmp.GasRhos = atoi(params[23].c_str());
         tmp.GasRscale = atof(params[24].c_str());
         tmp.GasProfile = string_to_enum("FLAG_PROFILE", params[25].c_str());
         tmp.GasShapeParam1 = atof(params[26].c_str());
         tmp.GasShapeParam2 = atof(params[27].c_str());
         tmp.GasShapeParam3 = atof(params[28].c_str());
      } else {
         tmp.GasProfile = 0;
         tmp.GasRhos = 0.;
         tmp.GasRscale = 0.;
         tmp.GasShapeParam1 = 0.;
         tmp.GasShapeParam2 = 0.;
         tmp.GasShapeParam3 = 0.;
      }

      // Find matching type for halo
      int i_type = -1;
      for (int ii = 0; ii < gN_TYPEHALOES; ++ii) {
         if (tmp.Type == gNAMES_TYPEHALOES[ii]) {
            i_type = ii;
            break;
         }
      }
      if (i_type == -1) {
         printf("\n====> ERROR: halo_loadlist4jeans() in jeans_analysis.cc");
         printf("\n             Type=%s (for Name=%s) found in %s does not match any existing type",
                tmp.Type.c_str(), tmp.Name.c_str(), gLIST_HALOES.c_str());
         printf("\n             Available types are: ");
         for (int ii = 0; ii < gN_TYPEHALOES; ++ii)
            printf("%s ", gNAMES_TYPEHALOES[ii]);
         printf("\n             => abort()\n\n");
         abort();
      }

      // If old list was not cleared, check if new list is really new!
      if (is_clear == false) {
         for (int i = 0; i < (int)list_halos.size(); ++i) {
            if (upper_case(tmp.Name) == upper_case(list_halos[i].Name) &&  upper_case(tmp.Type) == upper_case(list_halos[i].Type)) {
               const int buflen = 500;
               char char_tmp[buflen];
               snprintf(char_tmp, buflen, "Name = %s and Type = %s already loaded => Overrides old values with those read in %s.", (tmp.Name).c_str(), (tmp.Type).c_str(), file_halos_plain.c_str());
               print_warning("jeans_analysis.cc", "halo_loadlist4jeans()", string(char_tmp));
               list_halos[i] = tmp;
            }
         }
      } else list_halos.push_back(tmp);
   }

   printf("  - List of DM/light/anis/gas halos (for Jeans analysis) loaded from %s\n", file_halos_plain.c_str());
   if (is_at_least_onegas) {
      printf("#*********************************************************************************************************************************************************************************************************************************#\n");
      printf("#     [OBJECT SIZE]          |           [DATA]           |         DM DISTRIBUTION (RHO_TOT)        |             Light Profile                |        Anisotropy  Profile         |          Gas Profile [optional]           #\n");
      printf("# Name    Type    Rvir  Rmax |    Vel            Light    |  rhos       rs     prof.   #1   #2   #3  |     L        rs*    prof.   #1   #2   #3 | beta_0 beta_inf  prof.   ra   eta  |    rhos      rs*    prof.   #1   #2   #3  #\n");
      printf("#  -        -    [kpc] [kpc] |     -               -      |[Msol/kpc3]  [kpc]  [enum]    -    -    - |[Lsol/kpc3]  [kpc]  [enum]    -    -    - |   -       -     [enum]   kpc   -   |[Msol/kpc3]  [kpc]  [enum]    -    -    -  #\n");
      printf("#*********************************************************************************************************************************************************************************************************************************#\n");
   } else {
      printf("#*************************************************************************************************************************************************************************************#\n");
      printf("#     [OBJECT SIZE]          |           [DATA]           |         DM DISTRIBUTION (RHO_TOT)        |             Light Profile                |        Anisotropy  Profile         #\n");
      printf("# Name    Type    Rvir  Rmax |    Vel            Light    |  rhos       rs     prof.   #1   #2   #3  |     L        rs*    prof.   #1   #2   #3 | beta_0 beta_inf  prof.   ra   eta  #\n");
      printf("#  -        -    [kpc] [kpc] |     -               -      |[Msol/kpc3]  [kpc]  [enum]    -    -    - |[Lsol/kpc3]  [kpc]  [enum]    -    -    - |   -       -     [enum]   kpc   -   #\n");
      printf("#*************************************************************************************************************************************************************************************#\n");
   }
   for (int i = 0; i < (int)list_halos.size(); ++i) {
      if (list_halos[i].GasRhos > 1.e-40)
         printf("%-5s %-8s %.1le %le %-8s %-8s   %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf    %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf   %.2lf %.2lf %-5s  %5.1le %.2lf    %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf  \n",
                (list_halos[i].Name).c_str(), (list_halos[i].Type).c_str(), list_halos[i].Rvir, gSIM_JEANS_RMAX,
                VelData[i].c_str(), LightData[i].c_str(), list_halos[i].Rhos, list_halos[i].Rscale, gNAMES_PROFILE[(int)list_halos[i].HaloProfile],
                list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3,
                list_halos[i].L, list_halos[i].RLight, gNAMES_LIGHTPROFILE[(int)list_halos[i].LightProfile],
                list_halos[i].LightShapeParam1, list_halos[i].LightShapeParam2, list_halos[i].LightShapeParam3,
                list_halos[i].Beta_Aniso_0, list_halos[i].Beta_Aniso_Inf, gNAMES_ANISOTROPYPROFILE[(int)list_halos[i].AnisoProfile],
                list_halos[i].RAniso, list_halos[i].AnisoShapeParam,
                list_halos[i].GasRhos, list_halos[i].GasRscale, gNAMES_PROFILE[(int)list_halos[i].GasProfile],
                list_halos[i].GasShapeParam1, list_halos[i].GasShapeParam2, list_halos[i].GasShapeParam3);
      else
         printf("%-5s %-8s %.1le %le %-8s %-8s   %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf    %5.1le   %5.1le  %-5s %.2lf %.2lf %.2lf   %.2lf %.2lf %-5s  %5.1le %.2lf \n",
                (list_halos[i].Name).c_str(), (list_halos[i].Type).c_str(), list_halos[i].Rvir, gSIM_JEANS_RMAX,
                VelData[i].c_str(), LightData[i].c_str(), list_halos[i].Rhos, list_halos[i].Rscale, gNAMES_PROFILE[(int)list_halos[i].HaloProfile],
                list_halos[i].ShapeParam1, list_halos[i].ShapeParam2, list_halos[i].ShapeParam3,
                list_halos[i].L, list_halos[i].RLight, gNAMES_LIGHTPROFILE[(int)list_halos[i].LightProfile],
                list_halos[i].LightShapeParam1, list_halos[i].LightShapeParam2, list_halos[i].LightShapeParam3,
                list_halos[i].Beta_Aniso_0, list_halos[i].Beta_Aniso_Inf, gNAMES_ANISOTROPYPROFILE[(int)list_halos[i].AnisoProfile],
                list_halos[i].RAniso, list_halos[i].AnisoShapeParam);
   }
   if (is_at_least_onegas)
      printf("#*********************************************************************************************************************************************************************************************************************************#\n");
   else
      printf("#*************************************************************************************************************************************************************************************#\n");
}

//______________________________________________________________________________
void load_jeansconfig(string const &filename, struct gStructJeansAnalysis &jeans_struct, double const &eps)
{
   //--- Fills from a file the parameters to use in a Jeans analysis (free and
   //    fixed parameters, priors, etc.). An example file is ./data/params_jeans.
   //
   //  filename      Name of the file frow which to load parameters
   //  analysis      Structure in which to store Jeans analysis parameters
   //  eps           Relative precision used for Jeans analysis


   // Set precision
   jeans_struct.AnalysisEps = eps;
   jeans_struct.ParJeans[14] = eps;

   // Set default gas parameters (so that no contribution if not provided)
   // jeans_struct.ParJeans[20]: Gas profile normalisation [Msol/kpc^3] => Gas mass unused if <=0.
   // jeans_struct.ParJeans[21]: Gas profile scale radius [kpc]
   // jeans_struct.ParJeans[22]: Gas shape parameter #1
   // jeans_struct.ParJeans[23]: Gas shape parameter #2
   // jeans_struct.ParJeans[24]: Gas shape parameter #3
   // jeans_struct.ParJeans[25]: Gas card_profile [gENUM_PROFILE] (see params.h)
   jeans_struct.ParJeans[20] = 0.;
   jeans_struct.ParJeans[21] = 0.;
   jeans_struct.ParJeans[22] = 0.;
   jeans_struct.ParJeans[23] = 0.;
   jeans_struct.ParJeans[24] = 0.;
   jeans_struct.ParJeans[25] = 1;

   vector<string> light_param;

   // Open parameter file
   ifstream file(filename.c_str());
   if (!file) {
      printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
      printf("\n             Could not open file %s", filename.c_str());
      printf("\n             => abort()\n\n");
      abort();
   }
   cout << ">>>>> Load " << filename << endl;

   string line;
   int i_line = 0; // Counter for lines read
   int i_linewithpar = 0; // Counter for lines with a parameter
   int i_expected_light = 0; // Counter of lines found for light parametres
   int i_expected_info = 0; // Counter of lines found for info on object
   int i_expected_gas = 0; // Counter of lines found for optionnal gas parameters

   // Names of enabled free parameters (efreepars) and associated index in list of jeans_struct.ParJeans[]
   //    ParJeans[0]:  Dark matter profile normalisation [Msol/kpc^3]
   //    ParJeans[1]:  Dark matter profile scale radius [kpc]
   //    ParJeans[2]:  Dark matter shape parameter #1
   //    ParJeans[3]:  Dark matter shape parameter #2
   //    ParJeans[4]:  Dark matter shape parameter #3
   //    ParJeans[5]:  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //    ParJeans[6]:  Dark matter maximum radius for integration [kpc]
   //    ParJeans[7]:  Light profile normalisation [Lsol]
   //    ParJeans[8]:  Light profile scale radius [kpc]
   //    ParJeans[9]:  Light profile shape parameter #1
   //    ParJeans[10]: Light profile shape parameter #2
   //    ParJeans[11]: Light profile shape parameter #3
   //    ParJeans[12]: Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //    ParJeans[13]: R: projected radius considered for calculations of projected quantities [kpc]
   //    ParJeans[14]: eps: precision sought for integration
   //    ParJeans[15]: Anisotropy parameter #1
   //    ParJeans[16]: Anisotropy parameter #2
   //    ParJeans[17]: Anisotropy parameter #3
   //    ParJeans[18]: Anisotropy parameter #4
   //    ParJeans[19]: Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //    ParJeans[20]: Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //    ParJeans[21]: Gas profile scale radius [kpc]
   //    ParJeans[22]: Gas shape parameter #1
   //    ParJeans[23]: Gas shape parameter #2
   //    ParJeans[24]: Gas shape parameter #3
   //    ParJeans[25]: Gas card_profile [gENUM_PROFILE] (see params.h)

   // Set parameters enabled as free parameters
   int n_efreepars = 9;
   vector<string> names_efreepars{"log10(rho_s)", "log10(r_s)", "alpha", "beta", "gamma", "beta_0", "beta_infinity", "log10(r_a)", "eta"};
   vector<bool> is_log_efreepars{1, 1, 0, 0, 0, 0, 0, 1, 0};
   vector<int> i_in_parjeans{0, 1, 2, 3, 4, 15, 16, 17, 18};

   // Set to 0 if we put back Normalization parameter for light profile
   int decrement_nolightnorm = -1;
   // If not in file, must be initialise to non-zero value:
   if (decrement_nolightnorm == -1)
      jeans_struct.ParJeans[7] = 1.; //

   while (getline(file, line)) { // read parameters file
      i_line += 1;
      line.erase(remove_if(line.begin(), line.end(), isNotASCII), line.end());

      if (line[0] == '#' || line.empty()) continue; // skip comments or empty lines
      i_linewithpar += 1;
      vector<string> params;
      replace_substring(line, "\t", " ");
      string2list(line, " ", params); // split line into vector of params

      // Read free parameters of the analysis (1<=i_linewithpar<=9)
      if (i_linewithpar <= 9) {
         if (params.size() < 6) { // If ill-formatted
            printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
            printf("\n             Wrong format (less than 6 parameters) in line #%d = %s", i_line, line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }

         int i_efreepar = index_in_list(names_efreepars, params[0], true); // search index of free parameter
         if (i_efreepar < 0) { // If wrong free par name
            printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
            printf("\n             Wrong parameter name (please check format in default file) in line #%d = %s", i_line, line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }
         int i_par = i_in_parjeans[i_efreepar];
         jeans_struct.IsFreePar[i_par] = atoi(params[2].c_str()); // read is parameter is free or not
         jeans_struct.ParJeans[i_par] = atof(params[5].c_str());
         jeans_struct.IsLogVal[i_par] = is_log_efreepars[i_efreepar];

         if (jeans_struct.IsFreePar[i_par]) { // parameter is free
            if (params[3] == "-" || params[4] == "-") { // No ranges given for the free parameter
               printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
               printf("\n             No range given for the prior on %s, line #%d", params[0].c_str(), i_line);
               printf("\n             => abort()\n\n");
               abort();
            }
            jeans_struct.RangeLo[i_par] = atof(params[3].c_str());
            jeans_struct.RangeUp[i_par] = atof(params[4].c_str());
         } else {
            // Set default range to initial values
            jeans_struct.RangeLo[i_par] = jeans_struct.ParJeans[i_par];
            jeans_struct.RangeUp[i_par] = jeans_struct.ParJeans[i_par];
         }

      } else { // Profiles keywords, light parameters, info on object, or gas parameters all have three entries
         if (i_linewithpar == 20+decrement_nolightnorm && params.size() != 5) {
            printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
            printf("\n             5 strings (3 for name) expected, but %d provided in line #%d = %s", (int)params.size(), i_line, line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         } else if (i_linewithpar == 21+decrement_nolightnorm && params.size() != 6) {
            printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
            printf("\n             6 strings (4 for name) expected, but %d provided in line #%d = %s", (int)params.size(), i_line, line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         } else if (i_linewithpar != 20+decrement_nolightnorm && i_linewithpar != 21+decrement_nolightnorm && params.size() != 3) { // Check format
            printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
            printf("\n             3 parameters expected, but %d provided in line #%d = %s", (int)params.size(), i_line, line.c_str());
            printf("\n             => abort()\n\n");
            abort();
         }

         for (int n = 0; n < params.size(); n++)
            params[n].erase(remove(params[n].begin(), params[n].end(), '\t'), params[n].end());

         if (i_linewithpar == 10) { // Line for DM, light, and anisotropy keywords
            //    ParJeans[5]:  Dark matter card_profile [gENUM_PROFILE] (see params.h)
            //    ParJeans[12]: Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
            //    ParJeans[19]: Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
            jeans_struct.ParJeans[5]  = string_to_enum("FLAG_PROFILE", params[0]);
            jeans_struct.ParJeans[12] = string_to_enum("FLAG_LIGHTPROFILE", params[1]);
            jeans_struct.ParJeans[19] = string_to_enum("FLAG_ANISOTROPYPROFILE", params[2]);

         } else if (i_linewithpar <= 15+decrement_nolightnorm) { // Light profile parameters
            //    ParJeans[7]:  Light profile normalisation [Lsol]
            //    ParJeans[8]:  Light profile scale radius [kpc]
            //    ParJeans[9]:  Light profile shape parameter #1
            //    ParJeans[10]: Light profile shape parameter #2
            //    ParJeans[11]: Light profile shape parameter #3
            jeans_struct.ParJeans[7-decrement_nolightnorm + i_expected_light] = atof(params[2].c_str());
            i_expected_light += 1;

         } else if (i_linewithpar <= 21+decrement_nolightnorm) { // Information on object
            i_expected_info += 1;
            if (i_expected_info == 1)
               jeans_struct.Name = params[2];
            if (i_expected_info == 2)
               jeans_struct.Distance = atof(params[2].c_str());
            else if (i_expected_info == 3)
               jeans_struct.Longitude = atof(params[2].c_str());
            else if (i_expected_info == 4)
               jeans_struct.Latitude = atof(params[2].c_str());
            else if (i_expected_info == 5) {// Rvir
               jeans_struct.HaloSize = atof(params[4].c_str());
               jeans_struct.ParJeans[6] = jeans_struct.HaloSize;
            } else if (i_expected_info == 6)
               gSIM_JEANS_RMAX = atof(params[5].c_str());
         } else { // Optional gas profile
            //    ParJeans[20]: Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
            //    ParJeans[21]: Gas profile scale radius [kpc]
            //    ParJeans[22]: Gas shape parameter #1
            //    ParJeans[23]: Gas shape parameter #2
            //    ParJeans[24]: Gas shape parameter #3
            //    ParJeans[25]: Gas card_profile [gENUM_PROFILE] (see params.h)
            i_expected_gas += 1;
            if (i_expected_gas < 6)
               jeans_struct.ParJeans[19 + i_expected_gas] = atof(params[2].c_str());
            else
               jeans_struct.ParJeans[25] = string_to_enum("FLAG_PROFILE", params[2].c_str());
         }
      }
   }

   // Check that light profile, object info, and gas profile properly filed
   if (i_expected_light != 5+ decrement_nolightnorm) {
      printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
      printf("\n             Expected 5 light parameters, found %d",  i_expected_light);
      printf("\n             => abort()\n\n");
      abort();

   }
   if (i_expected_info != 6) {
      printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
      printf("\n             Expected 6 object info parameters, found %d",  i_expected_info);
      printf("\n             => abort()\n\n");
      abort();
   }
   if (i_expected_gas != 0 && i_expected_gas != 6) {
      printf("\n====> ERROR: load_jeansconfig() in jeans_analysis.cc");
      printf("\n             Expected 0 or 6 gas parameters, found %d",  i_expected_gas);
      printf("\n             => abort()\n\n");
      abort();
   }
}

//______________________________________________________________________________
double log_likelihood_jeans(double par_jeans[26], gStructJeansData &jeans_data)
{
   //--- Returns log-likelihood function for a Jeans analysis of analysis_type for 'par_jeans'
   //    free parameters (see, e.g., Bonnivard et al., 2015).
   //
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)
   //  jeans_data    Jeans data (against which the log-likelihood is calculated)

   double logL = 0.; // LogLikelihood
   int imax = jeans_data.R.size(); // Number of kinematic data points

   // If binned analysis (i.e. data are "Sigmap" or "Sigmap2") and few data points
   if (jeans_data.Type != 2 && imax < 15) {
      for (int i = 0; i < imax; i++) {
         par_jeans[13] = jeans_data.R[i];
         double res = jeans_sigmap2(jeans_data.R[i], par_jeans);
         if (jeans_data.Type == 1) // data = Sigmap
            res = sqrt(res);
         logL += pow((jeans_data.Val[i] - res), 2)
                 / pow(jeans_data.ValErr[i], 2);
      }
   } else { // Unbinned or more than 15 data points => interpolation
      const int ni = 15; //number of points for interpolation
      double sigmap_inter[ni]; // sigmap2 values for interpolation
      double xmin = find_min_value(jeans_data.R); // Minimum radius in kinematic data
      double xmax = find_max_value(jeans_data.R) + 0.001; // Maximum radius in kinematic data (+ safety factor)

      double step_log_r = pow(xmax / xmin, 1. / double(ni - 1)); // set the log scale of interpolation radii
      double xi[ni]; // radius values for interpolation

      for (int j = 0; j < ni; j++) { // loop over the interpolation points
         xi[j] = xmin * pow(step_log_r, j); // radius where to compute sigmap2
         par_jeans[13] = xi[j]; // set the values of radius
         sigmap_inter[j] = jeans_sigmap2(xi[j], par_jeans);
         if (jeans_data.Type == 1) // data = Sigmap
            sigmap_inter[j] = sqrt(sigmap_inter[j]); // set the sigmap values for interpolation
      }

      // loop over the entire kinematic data set
      for (int i = 0; i < imax; i++) {
         int index = -1;
         // find the radius values that enclose the ith radius
         for (int j = 0; j < ni - 1; j++) {
            if (xi[j] <= jeans_data.R[i] && xi[j + 1] > jeans_data.R[i]) {
               index = j;
               break;
            }
         }
         if (index < 0) {
            printf("\n====> ERROR: log_likelihood_jeans() in jeans_analysis.cc");
            printf("\n             Could not find interpolating radius");
            printf("\n             => abort()\n\n");
            abort();
         }
         // Compute log likelihood for unbinned velocity data (jeans_data.Type=2) or binned anaysis (jeans_data.Type=0 or 1)
         if (jeans_data.Type == 2)
            logL += jeans_data.MembershipProb[i] * log(pow(jeans_data.ValErr[i], 2) + interp_loglin(jeans_data.R[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1]))
                    + jeans_data.MembershipProb[i] * pow((jeans_data.Val[i] - jeans_data.Vmean), 2) / (pow(jeans_data.ValErr[i], 2) + interp_loglin(jeans_data.R[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1]));
         else
            logL += pow((jeans_data.Val[i]
                         - interp_loglin(jeans_data.R[i], xi[index], xi[index + 1], sigmap_inter[index], sigmap_inter[index + 1])), 2)
                    / pow(jeans_data.ValErr[i], 2);
      }
   }
   return -0.5 * logL;
}

//______________________________________________________________________________
double mass(double const &r, double const par_jeans[26])
{
   //--- Calculates total mass (dark matter + gas) inside radius r for par_jeans.
   // INPUTS:
   //  r             Radius up to which mass calculated
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)

   double par_tmp[7];
   par_tmp[6] = r;

   // DM mass calculated within r
   for (int i = 0; i < 6; ++i)
      par_tmp[i] = par_jeans[i];
   double m_tot = mass_singlehalo(par_tmp, par_jeans[14]);

   // Gas mass calculated within radius r (if there is some gas!)
   if (par_jeans[20] > 1.e-40) {
      // We cannot directly pass &par_jeans[20] of 6, because we
      // need a 7th entry for the integration radius!
      for (int i = 0; i < 6; ++i)
         par_tmp[i] = par_jeans[20 + i];
      m_tot += mass_singlehalo(par_tmp, par_jeans[14]);
   }
   return m_tot;
}

//______________________________________________________________________________
void print_jeansanalysis_object(struct gStructJeansAnalysis const &jeans_struct)
{
   //--- Print Jean analysis setup
   //  jeans_struct     Structure containing parameters of the analysed object

   printf("       ================= gStructJeansAnalysis (object) =================\n");
   printf("             Name                %s\n", jeans_struct.Name.c_str());
   printf("             Distance  [kpc]     %le\n", jeans_struct.Distance);
   printf("             Longitude [deg]     %f\n", jeans_struct.Longitude);
   printf("             Latitude  [deg]     %f\n", jeans_struct.Latitude);
   printf("             DM halo size [kpc]  %le\n", jeans_struct.HaloSize);
}

//______________________________________________________________________________
void print_jeansanalysis_setup(struct gStructJeansAnalysis const &jeans_struct)
{
   //--- Print Jean analysis setup
   //  jeans_struct     Structure containing parameters of the analysed object

   print_jeansanalysis_object(jeans_struct);


   printf("       ================= gStructJeansAnalysis (params) =================\n");
   printf("             Precision           %le\n", jeans_struct.AnalysisEps);
   printf("       ====  Param             Status    Init         [Lower,Upper]             IsLogVals\n");
   for (int i = 0; i < jeans_struct.ParNames.size(); ++i) {
      if (jeans_struct.IsFreePar[i])
         printf("             %-15s   free      %+.2le    [%+.2le,%+.2le]     %d\n",
                jeans_struct.ParNames[i].c_str(), jeans_struct.ParJeans[i],
                jeans_struct.RangeLo[i], jeans_struct.RangeUp[i],
                jeans_struct.IsLogVal[i]);
      else {
         if (jeans_struct.ParNames[i] == "profile")
            printf("             %-15s   fixed     %d (%s)\n", jeans_struct.ParNames[i].c_str(),
                   (int)jeans_struct.ParJeans[i], gNAMES_PROFILE[(int)jeans_struct.ParJeans[i]]);
         else if (jeans_struct.ParNames[i] == "lightprofile")
            printf("             %-15s   fixed     %d (%s)\n", jeans_struct.ParNames[i].c_str(),
                   (int)jeans_struct.ParJeans[i], gNAMES_LIGHTPROFILE[(int)jeans_struct.ParJeans[i]]);
         else if (jeans_struct.ParNames[i] == "anisoprofile")
            printf("             %-15s   fixed     %d (%s)\n", jeans_struct.ParNames[i].c_str(),
                   (int)jeans_struct.ParJeans[i], gNAMES_ANISOTROPYPROFILE[(int)jeans_struct.ParJeans[i]]);
         else if (jeans_struct.ParNames[i] == "gasprofile")
            printf("             %-15s   fixed     %d (%s)\n", jeans_struct.ParNames[i].c_str(),
                   (int)jeans_struct.ParJeans[i], gNAMES_PROFILE[(int)jeans_struct.ParJeans[i]]);
         else
            printf("             %-15s   fixed     %.2le\n", jeans_struct.ParNames[i].c_str(),
                   jeans_struct.ParJeans[i]);

      }
   }
   printf("       =================================================================\n\n");
}

//______________________________________________________________________________
void print_pargas(double const par_gas[6])
{
   //--- Prints gas parameters.
   //
   //  par_gas[0] Gas profile normalisation [Msol/kpc^3] => No gas if par_gas[0]<=0.
   //  par_gas[1] Gas profile scale radius [kpc]
   //  par_gas[2] Gas shape parameter #1
   //  par_gas[3] Gas shape parameter #2
   //  par_gas[4] Gas shape parameter #3
   //  par_gas[5] Gas card_profile [gENUM_PROFILE] (see params.h)

   printf("       ================= par_gas[6] =================\n");
   printf("         [0] Gas norm     = %le [Msol kpc^{-3}]\n", par_gas[0]);
   printf("         [1] Gas rs       = %le [kpc]\n", par_gas[1]);
   printf("         [2] Gas shape#1  = %le [-]\n", par_gas[2]);
   printf("         [3] Gas shape#2  = %le [-]\n", par_gas[3]);
   printf("         [4] Gas shape#3  = %le [-]\n", par_gas[4]);
   printf("         [5] Gas profile  = %d (%s)\n", (int)par_gas[5], gNAMES_PROFILE[(int)par_gas[5]]);
   printf("\n");
}

//______________________________________________________________________________
void print_parjeans(double const par_jeans[26])
{
   //--- Prints Jeans parameters.
   //
   //  par_jeans[0]  Dark matter profile normalisation [Msol/kpc^3]
   //  par_jeans[1]  Dark matter profile scale radius [kpc]
   //  par_jeans[2]  Dark matter shape parameter #1
   //  par_jeans[3]  Dark matter shape parameter #2
   //  par_jeans[4]  Dark matter shape parameter #3
   //  par_jeans[5]  Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  par_jeans[6]  Dark matter maximum radius for integration [kpc]
   //  par_jeans[7]  Light profile normalisation [Lsol]
   //  par_jeans[8]  Light profile scale radius [kpc]
   //  par_jeans[9]  Light profile shape parameter #1
   //  par_jeans[10] Light profile shape parameter #2
   //  par_jeans[11] Light profile shape parameter #3
   //  par_jeans[12] Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_jeans[13] R: projected radius considered for calculations of projected quantities [kpc]
   //  par_jeans[14] eps: precision sought for integration
   //  par_jeans[15] Anisotropy parameter #1
   //  par_jeans[16] Anisotropy parameter #2
   //  par_jeans[17] Anisotropy parameter #3
   //  par_jeans[18] Anisotropy parameter #4
   //  par_jeans[19] Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  par_jeans[20] Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
   //  par_jeans[21] Gas profile scale radius [kpc]
   //  par_jeans[22] Gas shape parameter #1
   //  par_jeans[23] Gas shape parameter #2
   //  par_jeans[24] Gas shape parameter #3
   //  par_jeans[25] Gas card_profile [gENUM_PROFILE] (see params.h)

   printf("       ================= par_jeans[26] =================\n");
   printf("         [0]  DM halo norm     = %le [Msol kpc^{-3}]\n", par_jeans[0]);
   printf("         [1]  DM halo rs       = %le [kpc]\n", par_jeans[1]);
   printf("         [2]  DM halo shape#1  = %le [-]\n", par_jeans[2]);
   printf("         [3]  DM halo shape#2  = %le [-]\n", par_jeans[3]);
   printf("         [4]  DM halo shape#3  = %le [-]\n", par_jeans[4]);
   printf("         [5]  DM halo profile  = %d (%s)\n", (int)par_jeans[5], gNAMES_PROFILE[(int)par_jeans[5]]);
   printf("         [6]  DM halo Rmax     = %le [kpc]\n", par_jeans[6]);
   printf("         [7]  Light norm       = %le [Lsol kpc^{-2}] or [Lsol kpc^{-3}] (for I(R) or nu(r))\n", par_jeans[7]);
   printf("         [8]  Light rs         = %le [kpc]\n", par_jeans[8]);
   printf("         [9]  Light shape#1    = %le\n", par_jeans[9]);
   printf("         [10] Light shape#2    = %le\n", par_jeans[10]);
   printf("         [11] Light shape#3    = %le\n", par_jeans[11]);
   printf("         [12] Light profile    = %d (%s)\n", (int)par_jeans[12], gNAMES_LIGHTPROFILE[(int)par_jeans[12]]);
   printf("         [13] R (projected)    = %le [kpc]\n", par_jeans[13]);
   printf("         [14] eps              = %le\n", par_jeans[14]);
   printf("         [15] Anisotropy par#1 = %le\n", par_jeans[15]);
   printf("         [16] Anisotropy par#2 = %le\n", par_jeans[16]);
   printf("         [17] Anisotropy par#3 = %le\n", par_jeans[17]);
   printf("         [18] Anisotropy par#4 = %le\n", par_jeans[18]);
   printf("         [19] Anisotropy prof  = %d (%s)\n", (int)par_jeans[19], gNAMES_ANISOTROPYPROFILE[(int)par_jeans[19]]);
   if (par_jeans[20] > 1.e-40) {
      printf("         [20] Gas norm     = %le [Msol kpc^{-3}]\n", par_jeans[20]);
      printf("         [21] Gas rs       = %le [kpc]\n", par_jeans[21]);
      printf("         [22] Gas shape#1  = %le [-]\n", par_jeans[22]);
      printf("         [23] Gas shape#2  = %le [-]\n", par_jeans[23]);
      printf("         [24] Gas shape#3  = %le [-]\n", par_jeans[24]);
      printf("         [25] Gas profile  = %d (%s)\n", (int)par_jeans[25], gNAMES_PROFILE[(int)par_jeans[25]]);
   } else
      printf("         N.B.: no gas distribution");

   printf("\n");
}

//______________________________________________________________________________
void print_parlight(double const par_light[8])
{
   //--- Prints light parameters.
   //
   //  par_light[0]  Light profile #1 (usually normalisation)
   //  par_light[1]  Light profile #2 (usually scale radius [kpc])
   //  par_light[2]  Light profile #3 (if used)
   //  par_light[3]  Light profile #4 (if used)
   //  par_light[4]  Light profile #5 (if used)
   //  par_light[5]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  par_light[6]  If not analytical, maximum radius for integration [kpc]
   //  par_light[7]  If not analytical, relative precision for the integration

   printf("       ================= par_light[8] =================\n");
   printf("         [0] norm             = %le [Lsol kpc^{-2}] or [Lsol kpc^{-3}] (for I(R) or nu(r))\n", par_light[0]);
   printf("         [1] scale radius     = %le [kpc]\n", par_light[1]);
   printf("         [2] shape#1          = %le\n", par_light[2]);
   printf("         [3] shape#2          = %le\n", par_light[3]);
   printf("         [4] shape#3          = %le\n", par_light[4]);
   printf("         [5] Light profile    = %d (%s)\n", (int)par_light[5], gNAMES_LIGHTPROFILE[(int)par_light[5]]);
   printf("         [6] Rmax             = %le [kpc]\n", par_light[6]);
   printf("         [7] eps              = %le\n", par_light[7]);
   printf("\n");
}

//______________________________________________________________________________
void printf_parjeans_header(FILE *fp, struct gStructJeansAnalysis const &jeans_struct, struct gStructJeansData const &jeans_data)
{
   //--- Print header of Jeans analysis result file (formatted to be re-read by CLUMPY).
   //  fp             FILE in which to print
   //  jeans_struct.Structure containing parameters of the analysed object

   int n_freepars = 0; // number of free parameters (used only for statistical file header)
   for (int i = 0; i < jeans_struct.ParNames.size(); ++i) {
      if (jeans_struct.IsFreePar[i])
         n_freepars += 1;
   }

   // Create header
   fprintf(fp, "Name     long.(deg)  lat.(deg)  l(kpc)  Ndata  Npar Rmax\n");
   fprintf(fp, "%s         %le        %le         %le     %d    %d   %le \n\n", jeans_struct.Name.c_str(), jeans_struct.Longitude, jeans_struct.Latitude, jeans_struct.Distance, int(jeans_data.R.size()), n_freepars, gSIM_JEANS_RMAX);
   // Print parameter names
   // N.B.: jeans_struct.ParJeans[20] = gas profile normalisation (unused if very small number).
   for (int j = 0; j < jeans_struct.ParPrintOrder.size(); ++j) {
      int i_par = jeans_struct.ParPrintOrder[j];
      // Skip gas component if not present
      if (i_par >= 20 && jeans_struct.ParJeans[20] < 1.e-40)
         break;

      fprintf(fp, "%s ", jeans_struct.ParNames[i_par].c_str());
   }
   fprintf(fp, "chi2\n");
}

//______________________________________________________________________________
void printf_parjeans_result(FILE *fp, struct gStructJeansAnalysis const &jeans_struct, double const &chi2min)
{
   //--- Print par_jeans into output file (formatted to be re-read by CLUMPY).
   //  fp             FILE in which to print
   //  jeans_struct.Structure containing parameters of the analysed object
   //  chi2val        Value of the chi2 obtaines for this parameters


   // Order of parameters to print, where we recall that
   //  jeans_struct.ParJeans[0]   Dark matter profile normalisation [Msol/kpc^3]
   //  jeans_struct.ParJeans[1]   Dark matter profile scale radius [kpc]
   //  jeans_struct.ParJeans[2]   Dark matter shape parameter #1
   //  jeans_struct.ParJeans[3]   Dark matter shape parameter #2
   //  jeans_struct.ParJeans[4]   Dark matter shape parameter #3
   //  jeans_struct.ParJeans[5]   Dark matter card_profile [gENUM_PROFILE] (see params.h)
   //  jeans_struct.ParJeans[6]   Dark matter maximum radius for integration [kpc]
   //  jeans_struct.ParJeans[7]   Light profile normalisation [Lsol]
   //  jeans_struct.ParJeans[8]   Light profile scale radius [kpc]
   //  jeans_struct.ParJeans[9]   Light profile shape parameter #1
   //  jeans_struct.ParJeans[10]  Light profile shape parameter #2
   //  jeans_struct.ParJeans[11]  Light profile shape parameter #3
   //  jeans_struct.ParJeans[12]  Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
   //  jeans_struct.ParJeans[13]  R: projected radius considered for calculations of projected quantities [kpc]
   //  jeans_struct.ParJeans[14]  eps: precision sought for integration
   //  jeans_struct.ParJeans[15]  Anisotropy parameter #1
   //  jeans_struct.ParJeans[16]  Anisotropy parameter #2
   //  jeans_struct.ParJeans[17]  Anisotropy parameter #3
   //  jeans_struct.ParJeans[18]  Anisotropy parameter #4
   //  jeans_struct.ParJeans[19]  Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
   //  jeans_struct.ParJeans[20]  Gas profile normalisation [Msol/kpc^3] => Gas mass unused if jeans_struct.ParJeans[20]<=0.
   //  jeans_struct.ParJeans[21]  Gas profile scale radius [kpc]
   //  jeans_struct.ParJeans[22]  Gas shape parameter #1
   //  jeans_struct.ParJeans[23]  Gas shape parameter #2
   //  jeans_struct.ParJeans[24]  Gas shape parameter #3
   //  jeans_struct.ParJeans[25]  Gas card_profile [gENUM_PROFILE] (see params.h)

   // Write parameters values in output file
   for (int j = 0; j < jeans_struct.ParPrintOrder.size(); ++j) {
      int i_par = jeans_struct.ParPrintOrder[j];
      // Skip gas component if not present
      if (i_par >= 20 && jeans_struct.ParJeans[20] < 1.e-40)
         break;

      // Special print for profile names
      if (i_par == 5 || i_par == 25)
         fprintf(fp, "%s ", ("k" + (string)gNAMES_PROFILE[(int)jeans_struct.ParJeans[i_par]]).c_str());
      else if (i_par == 12)
         fprintf(fp, "%s ", ("k" + (string)gNAMES_LIGHTPROFILE[(int)jeans_struct.ParJeans[i_par]]).c_str());
      else if (i_par == 19)
         fprintf(fp, "%s ", ("k" + (string)gNAMES_ANISOTROPYPROFILE[(int)jeans_struct.ParJeans[i_par]]).c_str());
      else { // Different format for fit and fixed parameters
         if (jeans_struct.IsFreePar[i_par]) {
            if (jeans_struct.IsLogVal[i_par]) // Special case if log parameter
               fprintf(fp, "%.*le ", gSIM_SIGDIGITS, pow(10., jeans_struct.ParJeans[i_par]));
            else
               fprintf(fp, "%.*le ", gSIM_SIGDIGITS, jeans_struct.ParJeans[i_par]);
         } else {
            if (jeans_struct.IsLogVal[i_par]) // Special case if log parameter
               fprintf(fp, "%g ", pow(10., jeans_struct.ParJeans[i_par]));
            else
               fprintf(fp, "%g ", jeans_struct.ParJeans[i_par]);
         }
      }
   }
   // And print chi2 value!
   fprintf(fp, "%.*le\n", gSIM_SIGDIGITS, chi2min);
}

//______________________________________________________________________________
void update_meanvelocity(struct gStructJeansData &jeans_data, bool is_verbose)
{
   //--- Updates structure member Vmean in jeans_data, the mean velocity for these data
   //  jeans_data    Jeans data (against which the log-likelihood is calculated)

   double termup = 0;
   double termdown = 0.;
   for (int kk = 0; kk < int(jeans_data.R.size()); kk++) {
      termup += jeans_data.MembershipProb[kk] * jeans_data.Val[kk] / pow(jeans_data.ValErr[kk], 2);
      termdown += jeans_data.MembershipProb[kk] / pow(jeans_data.ValErr[kk], 2);
      if (is_verbose)
         cout << "pi = " << jeans_data.MembershipProb[kk] << endl;
   }
   jeans_data.Vmean = termup / termdown;
   if (is_verbose)
      cout << "Vmean (for unbinned) = " << jeans_data.Vmean << endl;
}
