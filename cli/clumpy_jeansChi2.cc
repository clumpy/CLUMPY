// C++ std libraries
#include <stdlib.h>

// GreAT includes
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/jeans_analysis.h"

// ROOT includes
#if IS_ROOT
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#endif

struct gStructJeansData jeans_data; // structure containing the Jeans parameters (filled from a Jeans analysis parameters file)
struct gStructJeansAnalysis jeans_struct; // structure containing the kinematic data (filled from a Jeans kinematic data file)
bool is_verbose = false;

double chi2(const double par_jeans[27]);
void Minimise_jeans(int choice, int n_bootstrap, string const &output);

//______________________________________________________________________________
double chi2(const double *par_jeans)
{
   //---Calculates Chi2 for binned (either sigmap or sigmap2) or unbinned analysis
   // from par (relying on global jeans_struct configuration and jeans_data data).

   // Copy parameters proposed by the minimizer
   double par_tmp[jeans_struct.ParNames.size()];
   for (int i = 0; i < jeans_struct.ParNames.size(); ++i) {
      par_tmp[i] = par_jeans[i];

      // if proposed parameters are nan: skip
      if (std::isnan(par_tmp[i])) {
         cout << "--- skip nan values" << endl;
         return 1.e30;
      }
      // if proposed parameters are out of range: skip
      if (jeans_struct.IsFreePar[i] && (par_tmp[i] < jeans_struct.RangeLo[i] || par_tmp[i] > jeans_struct.RangeUp[i]))
         return 1.e30;
      // Unset log values if needed
      if (jeans_struct.IsLogVal[i])
         par_tmp[i] = pow(10., par_tmp[i]);
   }

   return -2.*log_likelihood_jeans(par_tmp, jeans_data);
}

#if IS_ROOT
//______________________________________________________________________________
void Minimise_jeans(int choice, int n_bootstrap, string const &output)
{
   // Choice: 1: chi2 minimization; 2: bootstrap

   // Create minimiser
   //    N.B.: the minimiser is defined by its name, its algorithm (optional),
   //          and several other parameters loaded from the par file. Possible
   //          combinations for {minimiser, algorithm} are:
   //          Name               Name algorithm
   //      Minuit /Minuit2        Migrad, Simplex,Combined,Scan  (default is Migrad)
   //      Minuit2                Fumili2
   //      Fumili                    -
   //      GSLMultiMin            ConjugateFR, ConjugatePR, BFGS,
   //                             BFGS2, SteepestDescent
   //      GSLMultiFit               -
   //      GSLSimAn                  -
   //      Genetic                   -
   ROOT::Math::Minimizer *minimiser = ROOT::Math::Factory::CreateMinimizer("Minuit", "Migrad");

   // Set minimiser parameters (tolerance , etc)
   minimiser->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2
   minimiser->SetMaxIterations(10000);  // for GSL
   minimiser->SetTolerance(0.001);
   minimiser->SetPrecision(1.e-8);
   minimiser->SetPrintLevel(0);

   // Create function wrapper for minimiser (IMultiGenFunction type):
   ROOT::Math::Functor f_jeansanalysis(&chi2, jeans_struct.ParNames.size());

   // Set the analysis type (binned, unbinned)
   if (choice == 2 && jeans_data.Type == 2) { // bootstrap binned analysis not possible!
      cout << "Error: no bootstrap analysis is allowed for a binned Jeans analysis: abort" << endl;
      abort();
   }

   // Set the likelihood function
   minimiser->SetFunction(f_jeansanalysis);

   double step = 0.01; // Step size

   // Create output file
   FILE *fp = fopen(output.c_str(), "w");
   printf_parjeans_header(fp, jeans_struct, jeans_data);

   if (choice == 2) { // Bootstrap analysis
      /* initialize random seed: */

      // Reload data (used as reference to generate bootstrap samples)
      gStructJeansData jeans_data_ref = jeans_data;
      int n_data = jeans_data.R.size();

      srand(time(NULL));
      for (int k = 0; k < n_bootstrap; ++k) { // Bootstrap the data
         cout << "===== BOOTSTRAP SAMPLE #" << k << "  (" << n_data << " DATA POINTS)" << endl;

         // Draw n_data indices among n_data
         if (is_verbose)
            cout << "Indices of bootstrap data: ";
         //vector<int> i_bootstrap;
         int l = 0;
         do {
            int index = rand() % n_data;
            if (is_verbose)
               cout << index << "  ";
            // Update jeans_data values from ref and this bootstrap index
            jeans_data.R[l]      = jeans_data_ref.R[index];
            jeans_data.Val[l]    = jeans_data_ref.Val[index];
            jeans_data.ValErr[l] = jeans_data_ref.ValErr[index];
            jeans_data.MembershipProb[l] = jeans_data_ref.MembershipProb[index];
            ++l;
         } while (l < n_data);
         if (is_verbose)
            cout << endl;

         // Update Vmean for bootstrap sample
         update_meanvelocity(jeans_data, is_verbose);

         // Set the free and fixed parameters
         for (int j = 0; j < jeans_struct.ParNames.size(); j++) {
            if (jeans_struct.IsFreePar[j])
               minimiser->SetLimitedVariable(j, jeans_struct.ParNames[j], jeans_struct.ParJeans[j], step, jeans_struct.RangeLo[j], jeans_struct.RangeUp[j]);
            else
               // DAVID : TO CLARIFY
               minimiser->SetFixedVariable(j, jeans_struct.ParNames[j], jeans_struct.ParJeans[j]);
            //minimiser->SetFixedVariable(j, jeans_struct.ParNames[j], par_jeans[j]);
         }

         // (minimise for each sample)
         minimiser->Minimize();
         double_t chi2_min = minimiser->MinValue();
         if (is_verbose)
            cout << "Minimum: " << minimiser->MinValue()  << std::endl;
         minimiser->PrintResults();

         const double_t *xi = minimiser->X();
         //const Double_t *err = minimiser->Errors();
         for (int i = 0; i < jeans_struct.ParNames.size(); ++i)
            jeans_struct.ParJeans[i] = xi[i];
         printf_parjeans_result(fp, jeans_struct, chi2_min);

      }
   } else { // Simple minimization

      // Set the free parameters and fixed parameters
      for (int j = 0; j < jeans_struct.ParNames.size(); j++) {
         string par_name = jeans_struct.ParNames[j];
         if (jeans_struct.IsLogVal[j])
            par_name = "log10_" + par_name;
         if (jeans_struct.IsFreePar[j])
            minimiser->SetLimitedVariable(j, par_name, jeans_struct.ParJeans[j], step, jeans_struct.RangeLo[j], jeans_struct.RangeUp[j]);
         else
            minimiser->SetFixedVariable(j, par_name, jeans_struct.ParJeans[j]);
      }

      // Minimize
      minimiser->Minimize();
      double_t chi2_min = minimiser->MinValue();
      if (is_verbose)
         cout << "Minimum: " << minimiser->MinValue()  << std::endl;
      minimiser->PrintResults();

      // Copy best-fit parameters and errors
      const double_t *xi = minimiser->X();
      //const Double_t *err = minimiser->Errors();
      for (int i = 0; i < jeans_struct.ParNames.size(); ++i)
         jeans_struct.ParJeans[i] = xi[i];
      printf_parjeans_result(fp, jeans_struct, chi2_min);
   }

   fclose(fp);
   fp = NULL;
}
#endif

//______________________________________________________________________________
int main(int argc, char *argv [])
{


   bool is_arg = true;
   int choice = 0;

   if (argc <= 1 || (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-r"))
      is_arg = false;
   else {
      // Search for selection
      string opt = argv[1];
      string ::size_type pos = opt.find_first_of("0123456789");
      string ::size_type pos_last = opt.find_last_of("0123456789");
      if (pos != string::npos)
         choice = atoi(opt.substr(pos, pos_last).c_str());

      // Print example values
      if (opt.find_first_of("D") != string::npos) {
         if (choice == 1)
            printf("   Jeans analysis with simple chi2 minimization:  %s -r1 %s/data_sigmap.txt output/chi2_binned.dat %s/params_jeans.txt 0.05\n", argv[0], gPATH_TO_CLUMPY_DATA.c_str(), gPATH_TO_CLUMPY_DATA.c_str());
         else if (choice == 2)
            printf("   Jeans analysis with bootstrap:  %s -r2 %s/data_vel.txt output/stat_bootstrap.dat %s/params_jeans.txt 0.05 100\n", argv[0], gPATH_TO_CLUMPY_DATA.c_str(), gPATH_TO_CLUMPY_DATA.c_str());
         printf("\n");

         return 1;
      }
   }

   // Usage of -1, -2, ..
   if (!is_arg || (choice <= 0 || choice > 2 || (choice == 1 && argc != 6))
         || (choice == 2 && argc != 7)) {
      printf("  usage:\n");
      printf("   [Chi2 minimization:        ]  %s -r1 data_file output_file param_file eps \n", argv[0]);
      printf("   [Bootstrap analysis:       ]  %s -r2 data_file output_file param_file eps Nbootstrap \n", argv[0]);
      printf("\n");
      printf(" N.B.: to see default parameter values (examples) and possible choices of dSphs, use \"-1D\", \"-2D\", etc.\n");
      printf("        -> data_file: file name (for sigma_p or velocity data)\n");
      printf("        -> output_file: output parameters\n");
      printf("        -> param_file: Jeans parameter file name\n");
      printf("        -> eps: precision\n");
      printf("        -> Nbootstrap: # of bootstrap samples\n");
      printf("\n\n");
      return 1;
   }


   string datafile = argv[2];
   string output = argv[3];
   string outdir = output.substr(0, output.find_last_of("\\/"));
   if (outdir != output) makedir(outdir.c_str());

   string param_file = argv[4];
   double eps = atof(argv[5]);
   gSIM_SIGDIGITS = int(ceil(-log10(eps)));

   int n_bootstrap = 0;
   if (choice == 2)
      n_bootstrap = atoi(argv[6]);

   // Initialise parameters and data (fill jeans_data)
   load_jeansdata(datafile, jeans_data, is_verbose);
   load_jeansconfig(param_file, jeans_struct, eps);
   print_jeansanalysis_setup(jeans_struct);

   // Run the analysis
#if IS_ROOT
   Minimise_jeans(choice, n_bootstrap, output);
#else
   print_error("clumpy_jeansChi2.cc", "main()",
               "Jeans analysis minimization requires ROOT to be installed.");
#endif

   cout << "Statistical file saved in " << output << endl;

}

/*! \file clumpy_jeansChi2.cc
  \brief <b>Jeans analysis executable</b> (minimisation or bootstrap analysis)
*/
