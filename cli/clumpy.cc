// CLUMPY includes
#include "../include/extragal.h"
#include "../include/healpix_fits.h"
#include "../include/inlines.h"
#include "../include/janalysis.h"
#include "../include/jeans_analysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/spectra.h"

// C++ std libraries
//using namespace std;
//#include <iostream>
//#include <math.h>
//#include <stdlib.h>

void           commandline_check_syntax(int const argc, char *argv[]);
void           commandline_parse_and_update_glopalparams(int const argc, char *argv[]);
void           generate_default_parfile(char const *exe_name, char const opt, int const opt_index, int const argc, bool is_write_all_params);
vector<string> get_defs_of_calculated_qties(char const opt);
void           print_clumpy_logo_and_version();
void           print_simu_mode(string const& simu_mode);
bool           print_usage(char const *exe_name, char const opt, int const opt_index, int const argc);
void           run_opt_e(int const opt_index);
void           run_opt_g(int const opt_index);
void           run_opt_h(int const opt_index);
void           run_opt_o(int const opt_index);
void           run_opt_s(int const opt_index);
void           run_opt_z(int const opt_index);

//______________________________________________________________________________
void commandline_check_syntax(int const argc, char *argv[])
{
   //--- Read the command line parameters and perform a coarse pre-check
   //    whether the user input makes sense: all argv values, except filenames,
   //    should start with a dash, a number, or a k (in case of keywords).
   //
   //  argc    Number of parameters in argv
   //  argv    Array of argc char*

   for (int i = 2; i < argc; ++i) {
      if (!isdigit(argv[i][0]) and argv[i][0] != '-') {
         // check if we have a filename:
         if (string(argv[i - 1]) != "-i" and string(argv[i - 1]) != "-r"
               and string(argv[i - 1]) != "--infile" and string(argv[i - 1]) != "--readfile"
               and argv[i][0] != 'k' /*CLUMPY keywords*/) {
            print_error("clumpy.cc", "commandline_check_syntax()", "Error while reading the command line parameter '"
                        + string(argv[i]) + "'.", false);
            printf("\n      Note that: \n");
            printf("       - variable names must be prepended by a dash\n");
            printf("       - command line variable values must be numbers (or CLUMPY keywords)\n");
            printf("       - a parameter file must be announced by a leading '-i' or '--infile' \n");
            printf(COLOR_BRED "      => abort()\n\n" COLOR_RESET);
            abort();
         }
      }
   }
}

//______________________________________________________________________________
void commandline_parse_and_update_glopalparams(int const argc, char *argv[])
{
   //--- Parses command-line parameters and update corresponding global parameters.
   //
   // INPUTS
   //  argc                     Number of parameters in argv
   //  argv                     Array of argc char*

   // Manage command-line parameters
   string file = "-1";
   string second_file = "-1"; // second file for modes which require two input files to be loaded
   vector<string> inputparams_from_commandline(gN_INPUTPARAMS, "-999");
   vector<bool> is_filled_inputparams_from_commandline(gN_INPUTPARAMS, false);

   // N.B.: optind is an external global variable used by getopt() library .
   optind = 1;

   // only when non printing/writing default parameter file:
   gSIM_IS_PRINT = true;
   gSIM_IS_DISPLAY = true;
   while (1) {
      int option_index = 0;
      string shortopts = "ghesoz1234567890::";
      int getopt_ret = getopt_long_only(argc, argv, shortopts.c_str(),
                                        gSIM_COMMANDLINE_OPTIONS,  &option_index);

      if (getopt_ret ==  -1) break;
      else if (getopt_ret == '?') {
         print_error("clumpy.cc", "main()",
                     "Something unexpected went wrong during input parameter loading with getopt().");
      } else if (getopt_ret == gN_INPUTPARAMS + 1) { // Check if any of the parameters shall be read from a file
         file = optarg;
         if (optarg[0] == '-') getopt_ret = '?';
      } else if (getopt_ret == gN_INPUTPARAMS + 2) { // Check if there is a second (FITS) file to read
         second_file = optarg;
         if (optarg[0] == '-') getopt_ret = '?';
      } else if (getopt_ret == gN_INPUTPARAMS + 3) // Check for -g => graph only, no result printed on screen
         gSIM_IS_DISPLAY = false;
      else  if (getopt_ret == gN_INPUTPARAMS + 4) // Check for -p => no print no graph (only saved file in this option)
         gSIM_IS_PRINT = false;
      else  if (getopt_ret == gN_INPUTPARAMS + 5) { // Check for -n => print only, no interactive plot
         gSIM_IS_PRINT = false;
         gSIM_IS_DISPLAY = false;
      }
      // else if (getopt_ret == gN_INPUTPARAMS + 6) // Check for -D (in case given separately from main option)
      //   is_print_default_params = true;
      //else if (getopt_ret == gN_INPUTPARAMS + 7)  // Check for write_all_params flag (in case given separately)
      //   is_write_all_params = true;

      // Now read all parameters from command line
cout << getopt_ret << " => " << gSIM_INPUTPARAMS[getopt_ret][kSIM_INPUTPARAM_VARNAME] << endl;
      for (int i = 0; i <= gN_INPUTPARAMS; ++i) {
         //inputparams_from_commandline[i] = "-999";
         if (getopt_ret == i && optarg != NULL) {
            cout << gSIM_INPUTPARAMS[i][kSIM_INPUTPARAM_VARNAME] << endl;
            int i_tweak;
            if (i != gN_INPUTPARAMS) i_tweak = i;
            else i_tweak = 63; // something is strange with index 63 - check for different systems!
            if (is_filled_inputparams_from_commandline[i_tweak]) {
               print_error("clumpy.cc", "main()",
                           "Error while reading the command line parameter '"
                           + string(gSIM_INPUTPARAMS[i_tweak][kSIM_INPUTPARAM_VARNAME])
                           + "'. Parameter is specified twice in the command line.");
            }
            is_filled_inputparams_from_commandline[i_tweak] = true;
            inputparams_from_commandline[i_tweak] = optarg;
            cout << optarg << endl;
         }
      }
   }

   // Load extra parameters from command line
   load_parameters(file, inputparams_from_commandline, gSIM_FLAG_MODE, true);
}

//______________________________________________________________________________
void generate_default_parfile(char const *exe_name, char const opt, int const opt_index, int const argc, bool is_write_all_params)
{
   //--- Generate default parameter file for option.
   //
   //  exe_name                Name of executable
   //  opt                     Current option (e.g. g, h, e...)
   //  opt_index                  Selection within option (e.g. 0, 1, 2, etc.)
   //  argc                    Number of arguments passed to initial command
   //  parfile      Standard parameter file options


   // Retrieve option definitions
   vector<string> opt_defs = get_defs_of_calculated_qties(opt);
   string parfile = "clumpy_params_" + string(gNAMES_SIMUMODES[gSIM_FLAG_MODE]) + ".txt";

   if (is_write_all_params) {
      gSIM_FLAG_MODE = -1;
      parfile = "clumpy_params_all.txt";
   }

   // Retrieve option definitions
   printf("   %s:  %s -%c%d -i %s\n", opt_defs[opt_index].c_str(), exe_name, opt, opt_index, parfile.c_str());

   inputparameters_write_paramfile(parfile, gSIM_FLAG_MODE, false);

   printf("\n");
}

//______________________________________________________________________________
vector<string> get_defs_of_calculated_qties(char const opt)
{
   //--- Returns list of option definitions for each option (g, h, o, s, and z).

   vector<string> opt_e_definitions = {
      "Distances & Omega's",       // e0
      "Mass function (M,z)",       // e1
      "cdelta-mdelta, Lum, Boost", // e2
      "Intensity multiplier",      // e3
      "EBL absorption tau",        // e4
      "Mean intens. dI/dE(M,z;E)", // e5
      "Mean intensity dI/dE(E)"    // e6
   };

   vector<string> opt_g_definitions = {
      "M(r)",                      // -g0
      "rho_sm+<sub>(r)",           // -g1
      "Jsm+<sub>(alpha_int)",      // -g2
      "Jsm+<sub>(theta)",          // -g3
      "Jsm+<sub>+list(theta)",     // -g4
      "2D-skymap Jsm+<sub)",       // -g5
      "2D-skymap Jsm+<sub>+list",  // -g6
      "2D-skymap Jsm+sub",         // -g7
      "2D-skymap Jsm+sub+list"     // -g8
   };

   vector<string> opt_h_definitions = {
      "M(r)",                      // -h0
      "rho_sm+<sub>(r)",           // -h1
      "Jsm+<sub>(alpha_int)",      // -h2
      "Jsm+<sub>(theta)",          // -h3
      "2D-skymap Jsm+<sub>(halo)", // -h4
      "2D-skymap Jsm+sub(halo)",   // -h5
      "alpha_int(f*Jtot)",         // -h6
      "dist(f*Jpointlike)",        // -h7
      "sigma_p(R)",                // -h8
      "I(R)",                      // -h9
      "beta_ani(r)"                // -h10
   };

   vector<string> opt_o_definitions = {
      "Content of skymap FITS file",           // -o0
      "Export skymap FITS extension to ASCII", // -o1
      "Retrieve params that generated FITS",   // -o2
      "Cutsky FITS map to whole-sky FITS map" // -o3
   };

   vector<string> opt_s_definitions = {
      "CLs M(r)",                  // -s0
      "PDF logL_or_chi2",          // -s1
      "PDF correlations",          // -s2
      "Best fit params",           // -s3
      "CLs params",                // -s4
      "CLs rho(r)",                // -s5
      "CLs J(alphaint)",           // -s6
      "CLs J(theta)",              // -s7
      "CLs sigma_p(R)",            // -s8
      "CLs vr2(r)",                // -s9
      "CLs beta_ani(r)",           // -s10
      "CLs nu(r)",                 // -s11
      "CLs I(R)"                   // -s12
   };

   vector<string> opt_z_definitions = {
      "Flux or dPP/dE (jfactor<0)" // z
   };

   if (opt == 'e') return opt_e_definitions;
   else if (opt == 'g') return opt_g_definitions;
   else if (opt == 'h') return opt_h_definitions;
   else if (opt == 'o') return opt_o_definitions;
   else if (opt == 's') return opt_s_definitions;
   else if (opt == 'z') return opt_z_definitions;
   else {
      vector<string> dummy;
      return dummy;
   }
}

//______________________________________________________________________________
void print_clumpy_logo_and_version()
{
   //--- Print CLUMPY logo text and version.

   int version_len = gCLUMPY_VERSION.size();
   int space1 = int(floor((78 - version_len) / 2.));
   string fill1(space1, ' ');
   string fill2(78  - version_len - space1, ' ');
   cout << " _____________________________________________________________  ___  __________ " << endl;
   cout << "|   _       o     _____  _       _    _  __  __  _____ __ .    /   \\       .   |" << endl;
   cout << "|  (_)   .       / ____|| |  o  | |  | ||  \\/  ||  __ \\\\ \\   /(     )          |" << endl;
   cout << "|            .  | |   . | |     | |  | || \\  / || |__) |\\ \\_/ /\\___/     o     |" << endl;
   cout << "| .             | |     | |   . | |  | || |\\/| ||  ___/  \\   /       .         |" << endl;
   cout << "|    __   o     | |____ | |____ | |__| || |  | || |     . | |     o         .  |" << endl;
   cout << "|   /  \\       . \\_____||______| \\____/ |_|  |_||_|  o    |_|    .         _   |" << endl;
   cout << "|   \\__/                                                               .  (_)  |" << endl;
   cout << "|"  << fill1 <<  gCLUMPY_VERSION  << fill2 << "|" << endl;
   cout << "|______________________________________________________________________________|\n" << endl;
}

//______________________________________________________________________________
void print_simu_mode(string const& simu_mode)
{
   //--- Print simulation mode.

   string mode_str = "Simulation mode: " + simu_mode;
   int mode_len = mode_str.size();
   int space1 = int(floor((78 - mode_len) / 2.));
   string fill3(space1, ' ');
   string fill4(78  - mode_len - space1, ' ');
   cout << " ______________________________________________________________________________ " << endl;
   cout << "|                                                                              |" << endl;
   cout << "|"  << fill3 <<  mode_str  << fill4 << "|" << endl;
   cout << "|______________________________________________________________________________|\n" << endl;
}

//______________________________________________________________________________
bool print_usage(char const *exe_name, char const opt, int const opt_index, int const argc)
{
   //--- Check correct command usage: returns true if format is OK, otherwise false (and print correct syntax)/
   //
   //  exe_name                Name of executable
   //  opt                     Current option (e.g. g, h, e...)
   //  opt_index                  Selection within option (e.g. 0, 1, 2, etc.)
   //  argc                    Number of arguments passed to initial command

   // Retrieve option definitions
   vector<string> opt_defs = get_defs_of_calculated_qties(opt);

   if ( (opt_index < 0 || opt_index > opt_defs.size()) || (argc < 4 && opt != 'o' && opt != 0) || (opt == 'o' && argc < 3) ){
      printf("  Incomplete or wrong command line input - mind the correct usage:\n");

      // Command definitions and arguments
      for (uint i = 0; i<opt_defs.size(); ++i) {
         if (opt == 'o')
            printf("   [%-38s]  %s -%c%d -i param_file -any_parameter (or --any_parameter)\n", opt_defs[i].c_str(), exe_name, opt, i);
         else if (opt == 's')
            printf("   [%-18s]  %s -%c%d -i param_file -any_parameter (or --any_parameter)\n", opt_defs[i].c_str(), exe_name, opt, i);
         else
            printf("   [%-25s]  %s -%c%d -i param_file -any_parameter (or --any_parameter)\n", opt_defs[i].c_str(), exe_name, opt, i);
      }

      // Extra information on command
      printf("\n\n");
      printf(" N.B.: To print default parameters and write them to a file, use \"-g1D\", \"-h3D\", \"-z0D\", etc.\n");
      printf("       Append flag --all or -a to print/write ALL possible parameters readable by CLUMPY\n");

      if (opt == 'o') { // Specific extra for o options
         // Extra information on command
         printf(" N.B.: for the -o2 option, the size of the whole-sky FITS files\n");
         printf("       will be about ~ GB (with lots of BLIND values) for NSIDE ~ 8000!\n");
      } else { // For all other options
         printf(" N.B.: Default output is both displays on screen and printing to files.\n");
#if IS_ROOT
         printf("       => use option -d for displays only (e.g., -g1 -d) => results not printed on screen\n");
         printf("       => use option -p for print only (e.g., -e1 -p) => no interactive plot\n");
         printf("       => use option -n for no displays/prints (e.g., -h3 -n)\n");
#else
         printf("       => use option -n for no prints on screen (e.g., -h3 -n) => results only stored\n");
#endif
         printf(" N.B.: The order of options (after the simulation module) does not matter.\n");
      }
      printf("\n\n");
      return false;
   }
   return true;
}

//______________________________________________________________________________
void run_opt_e(int const opt_index)
{
   //--- Runs option -e of CLUMPY (extragalactic related).
   //  opt_index      Selected opt_index for option

   // Vector of r (or alpha, or theta) denoted x (lin or log scale)
   vector <double> x;

   switch (opt_index) {
      case 0: {
            // Calculate cosmic distances and Omega parameters.
            x =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
            analyse_distances(x);
            break;
         }
      case 1: {
            // Calculate halo mass function at various redshifts
            vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
            vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
            analyse_massfunction(z_vec, M_vec, gEXTRAGAL_FLAG_MASSFUNCTION);
            break;
         }
      case 2: {
            // cdelta-mdelta and Lum
            plot_c_lum_boost_vs_m(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_NX, gEXTRAGAL_FLAG_CDELTAMDELTA_LIST,
                                  gDM_SUBS_MMIN, gDM_SUBS_MMAXFRAC, gHALO_SUBS_MASSFRACTION[kEXTRAGAL], gEXTRAGAL_SUBS_DPDM_SLOPE_LIST,
                                  gDM_SUBS_NUMBEROFLEVELS, gDM_LOGCDELTA_STDDEV);
            break;
         }
      case 3: {
            // Plot intensity multiplier
            vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
            vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
            analyse_intensitymultiplier(z_vec, M_vec);
            break;
         }
      case 4: {
            // Plot EBL absorption
            vector<double> e_vec =  make_1D_grid(gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX, 1);
            vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
            analyse_ebl(e_vec, z_vec, gEXTRAGAL_FLAG_ABSORPTIONPROFILE);
            break;
         }
      case 5: {
            // Calculate differential flux at fixed energy for various redshift/ mass decades
            vector<double> M_vec =  make_1D_grid(gSIM_EXTRAGAL_MMIN, gSIM_EXTRAGAL_MMAX, gSIM_EXTRAGAL_NM, gSIM_EXTRAGAL_IS_MLOG);
            vector<double> z_vec =  make_1D_grid(gSIM_EXTRAGAL_ZMIN, gSIM_EXTRAGAL_ZMAX, gSIM_EXTRAGAL_NZ, gSIM_EXTRAGAL_IS_ZLOG);
            analyse_intensity_mass_z_decades(z_vec, M_vec, gSIM_FLUX_AT_E_GEV);
            break;
         }
      case 6: {
            // Calculate differential flux for fixed redshift/ mass decades
            analyse_intensity_mean(gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX);
            break;
         }
      default:
         break;
   }
}

//______________________________________________________________________________
void run_opt_g(int const opt_index)
{
   //--- Runs option -g of CLUMPY (Galactic-halo related calls).
   //  opt_index      Selected opt_index for option

   // Vector of r (or alpha, or theta) denoted x (lin or log scale)
   vector <double> x;

   double theta_obs;
   // Switch selection
   switch (opt_index) {
      case 0:
         // M(r) for Gal. halo
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         gal_j1D(x, 3);
         break;
      case 1:
         // rho(r) for Gal. halo
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         gal_j1D(x, 0);
         break;
      case 2:
         // Jsm(alpha_int) + <Jsub(alpha_int)> for Gal. halo
         x =  make_1D_grid(gSIM_ALPHAINT_MIN, gSIM_ALPHAINT_MAX, gSIM_NX, gSIM_IS_XLOG);
         theta_obs = atof(gSIM_THETA_OBS_DEG.c_str()) * DEG_to_RAD;
         gal_j1D(x, 1, false, gSIM_PSI_OBS, theta_obs);
         break;
      case 3:
         // Jsm(theta_obs) + <Jsub(theta_obs)> for Gal. halo
         x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
         gal_j1D(x, 2);
         break;
      case 4:
         // Jsm(theta_obs) + <Jsub(theta_obs)> + Jlist
         x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
         gal_j1D(x, 2, true, 0., 0., gSIM_SORT_CONTRAST_THRESH, gSIM_PHI_CUT);
         break;
      case 5:
         // 2D-map Jsm+<sub> (Gal. halo)
         gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 0);
         break;
      case 6:
         // 2D-map Jsm+sub (Gal. halo) + Jlist
         gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 1, 0.);
         break;
      case 7:
         // 2D-map Jsm+sub (Gal. halo)
         gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 2, gSIM_USER_RSE);
         break;
      case 8:
         // 2D-map Jsm+sub (Gal. halo) + Jlist
         gal_j2D(gSIM_PSI_OBS, gSIM_THETA_OBS_DEG, gSIM_THETA_ORTH_SIZE_DEG, gSIM_THETA_SIZE, 3, gSIM_USER_RSE);
         break;
      default:
         break;
   }
}

//______________________________________________________________________________
void run_opt_h(int const opt_index)
{
   //--- Runs option -h of CLUMPY (single halo or list oh haloes).
   //  opt_index      Selected opt_index for option

   // Vector of r (or alpha, or theta) denoted x (lin or log scale)
   vector <double> x;

   switch (opt_index) {
      case 0:
         // M(r) for a list of halos
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_j1D(x, 3);
         break;
      case 1:
         // rho_sm(r) + dPdV_Jsub(r)> for a list of halos
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_j1D(x, 0);
         break;
      case 2:
         // Jsm(alpha_int) + <Jsub(alpha_int)> (and boost) for a list of halos
         x =  make_1D_grid(gSIM_ALPHAINT_MIN, gSIM_ALPHAINT_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_j1D(x, 1);
         break;
      case 3:
         // Jsm(theta) + <Jsub(theta)> for a list of halos
         x =  make_1D_grid(gSIM_THETA_MIN, gSIM_THETA_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_j1D(x, 2);
         break;
      case 4:
         // 2D-skymap Jsm+<sub> (for a single halo)
         halo_j2D(gLIST_HALONAME, gSIM_THETA_SIZE, false);
         break;
      case 5:
         // 2D-skymap Jsm+sub (for a single halo)
         halo_j2D(gLIST_HALONAME, gSIM_THETA_SIZE, true, gSIM_USER_RSE);
         break;
      case 6:
         // Containment angle [such as J(angle)= f*Jtot]
         halo_fracjtot_alphaint(gLIST_HALOES, gSIM_JFRACTION, gPP_DM_IS_ANNIHIL_OR_DECAY, gDM_RHOSAT);
         break;
      case 7:
         // Distance for which point-hike approx holds [such as J(d)= f*Jpointlike = f*L/d^2]
         halo_fracjpointlike_dist(gLIST_HALOES, gSIM_ALPHAINT, gSIM_JFRACTION, gPP_DM_IS_ANNIHIL_OR_DECAY, gDM_RHOSAT);
         break;
      case 8:
         // sigma_p for a list of halos
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_jeans(x, 0);
         break;
      case 9:
         // sigma_p for a list of halos
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_jeans(x, 1);
         break;
      case 10:
         // sigma_p for a list of halos
         x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
         halo_jeans(x, 2);
         break;
      default:
         break;
   }
}

//______________________________________________________________________________
void run_opt_o(int const opt_index)
{
   //--- Runs option -o of CLUMPY (skymap fits output manipulation).
   //
   //  opt_index     Selected opt_index for option

   switch (opt_index) {
      case 0: // FITS file content
         fits_print_info(gUTIL_SKYMAP_FITSFILE);
         break;
      case 1: // ASCII output
         fits_to_ascii(gUTIL_SKYMAP_FITSFILE, gUTIL_SKYMAP_EXTENSION, gSIM_OUTPUT_DIR);
         break;
      case 2: // Extract input parameters from FITS file:
         fits_params2reproduction(gUTIL_SKYMAP_FITSFILE, gUTIL_SKYMAP_EXTENSION, gSIM_OUTPUT_DIR);
         break;
      case 3: // Healpix whole sky maps:
         fits_to_simplefits(gUTIL_SKYMAP_FITSFILE, gUTIL_SKYMAP_EXTENSION, gUTIL_SKYMAP_FIELD, gSIM_OUTPUT_DIR);
         break;
      default:
         break;
   }
}

//______________________________________________________________________________
void run_opt_s(int const opt_index)
{
   //--- Runs option -s of CLUMPY (related to statistical analysis of single halo).
   //  opt_index      Selected opt_index for option


   // Convert comma-separated list of CLs into vector<double> of CLs
   vector<double> cls;
   if ((opt_index >= 5 && opt_index <= 12) || opt_index == 0) {
      vector<string> tmp_x_cl;
      string2list(gSTAT_CL_LIST, ",", tmp_x_cl);
      for (int ii = 0; ii < (int)tmp_x_cl.size(); ++ii)
         cls.push_back(atof(tmp_x_cl[ii].c_str()));
   }

   // Vector of r (or alpha, or theta) denoted x (lin or log scale)
   vector <double> x;

   switch (opt_index) {
      case 0: {
            // M(r) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 8, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
            break;
         }
      case 1: {
            // Chi2
            stat_draw_chi2(gSTAT_FILES, gSTAT_IS_LOGL_OR_CHI2);
            break;
         }
      case 2: {
            // Correlations
            vector<int> ids;
            vector<string> tmp_x_id;
            string2list(gSTAT_ID_LIST, ",", tmp_x_id);
            for (int ii = 0; ii < (int)tmp_x_id.size(); ++ii)
               ids.push_back(atof(tmp_x_id[ii].c_str()));
            stat_draw_correlations(ids, gSTAT_FILES, gSTAT_RKPC_FOR_MR, gDM_RHOSAT);
            break;
         }
      case 3: {
            // Retrieve best-fit model parameters
            if (gSTAT_MODE < 0 || gSTAT_MODE > 3) {
               print_error("clumpy.cc", "run_opt_s()", "Wrong stat method chosen");
            }
            stat_find_bestmodel(gSTAT_FILES, gSTAT_MODE);
            break;
         }
      case 4: {
            // Retrieve model with min and max val for all
            // models passing the x% cl constraint
            stat_find_CLs(gSTAT_FILES, gSTAT_CL, gSTAT_MODE);
            break;
         }
      case 5: {
            // rho CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 0, gDM_RHOSAT, 0., 0., gSTAT_MODE);
            break;
         }
      case 6: {
            // J(alpha_int) CLs
            double alphaint_min_deg = gSIM_ALPHAINT_MIN * RAD_to_DEG;
            double alphaint_max_deg = gSIM_ALPHAINT_MAX * RAD_to_DEG;
            x =  make_1D_grid(alphaint_min_deg, alphaint_max_deg, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 1, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
            break;
         }
      case 7: {
            // J(theta) CLs
            double theta_min_deg = gSIM_THETA_MIN * RAD_to_DEG;
            double theta_max_deg = gSIM_THETA_MAX * RAD_to_DEG;
            x =  make_1D_grid(theta_min_deg, theta_max_deg, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 2, gDM_RHOSAT, gSIM_EPS, gSIM_ALPHAINT, gSTAT_MODE);
            break;
         }
      case 8: {
            // sig_p2(R) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            if (gSTAT_DATAFILES == "-1")
               stat_CLs(x, gSTAT_FILES, cls, 3, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE);
            else
               stat_CLs(x, gSTAT_FILES, cls, 3, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE, gSTAT_DATAFILES);
            break;
         }
      case 9: {
            // vr2(r) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 4, gDM_RHOSAT, gSIM_EPS, 0, gSTAT_MODE);
            break;
         }
      case 10: {
            // beta_ani(r) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 5, gDM_RHOSAT, 0., 0., gSTAT_MODE);
            break;
         }
      case 11: {
            // nu(r) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            stat_CLs(x, gSTAT_FILES, cls, 6, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
            break;
         }
      case 12: {
            // I(R) CLs
            x =  make_1D_grid(gSIM_R_MIN, gSIM_R_MAX, gSIM_NX, gSIM_IS_XLOG);
            if (gSTAT_DATAFILES == "-1")
               stat_CLs(x, gSTAT_FILES, cls, 7, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE);
            else
               stat_CLs(x, gSTAT_FILES, cls, 7, gDM_RHOSAT, gSIM_EPS, 0., gSTAT_MODE, gSTAT_DATAFILES);
            break;
         }
      default:
         break;

   }
}

//______________________________________________________________________________
void run_opt_z()
{
   //--- Runs option -z of CLUMPY (gamma-ray and neutrino spectra).

   double par_spec[6];
   //  par_spec[0]   Mass of DM candidate [GeV]
   //  par_spec[1]   Card for PP spectrum parametrisation (gENUM_PP_SPECTRUMMODEL)
   //  par_spec[2]   Card for PP final state (gENUM_FINALSTATE)
   //  par_spec[3]   Redshift of the emitted particle
   //  par_spec[4]   e_pow (=0 -> particle flux, =1 -> energy flux, =2 ...)
   //  par_spec[5]   systematic uncertainty on EBL (only used for z>0)
   par_spec[0] = gPP_DM_MASS_GEV;
   par_spec[1] = gPP_FLAG_SPECTRUMMODEL;
   par_spec[2] = gSIM_FLUX_FLAG_FINALSTATE;
   par_spec[3] = gSIM_REDSHIFT;
   par_spec[4] = gSIM_XPOWER;
   par_spec[5] = 0.;

   if (gSIM_IS_ASTRO_OR_PP_UNITS) convert_to_PP_units(1, gSIM_JFACTOR);

   fluxes_plot(par_spec, gSIM_FLUX_EMIN_GEV, gSIM_FLUX_EMAX_GEV, gSIM_NX, gSIM_JFACTOR);
}

//______________________________________________________________________________
int main(int argc, char *argv [])
{
   //--- Run clumpy for various options.

   // CLUMPY logo in xterm
   print_clumpy_logo_and_version();

   //-----------------------------------//
   // Determine global mode (and check) //
   //-----------------------------------//
   // Self-explanatory executable: first argument must always be the simulation mode
   // Print explanation and abort if not found.
   if (argc <= 1 ||
         (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-g"
          && ((string)argv[1]).substr(0, 2) != "-h"
          && ((string)argv[1]).substr(0, 2) != "-e"
          && ((string)argv[1]).substr(0, 2) != "-s"
          && ((string)argv[1]).substr(0, 2) != "-o"
          && ((string)argv[1]).substr(0, 2) != "-z")) {
      printf("  Incomplete or wrong command line input - mind the correct usage:\n");
      printf("     %s -g  => run on the Galactic halo only + list of halos [optional]\n", argv[0]);
      printf("     %s -h  => run on (a list of) halos (not the Gal. halo)\n", argv[0]);
      printf("     %s -e  => run on extragalactic DM\n", argv[0]); // + list of halos [optional] + Galactic halo [optional]\n", argv[0]);
      printf("     %s -s  => run on statistical-like file (or on a list of files)\n", argv[0]);
      printf("     %s -o  => export/manipulate output FITS files\n", argv[0]);
      printf("     %s -z  => gamma and neutrino spectra \n", argv[0]);
      printf("\n");
      return 1;
   }

   // Retrieve simu mode and index
   string opt = string(argv[1]);
   string simu_mode = opt.substr(1, 1);
   int opt_index = -1;
   string::size_type pos = opt.find_first_of("0123456789");
   string::size_type pos_last = opt.find_last_of("0123456789");
   if (pos != string::npos) {
      opt_index = atoi(opt.substr(pos, pos_last-1).c_str());
      simu_mode += opt.substr(pos, pos_last-1).c_str();
      gSIM_FLAG_MODE = string_to_enum("FLAG_MODE", "k"  + simu_mode);
   }

   // Initialise required and hidden global parameters (for any option)
   initialise_required_and_hidden_globparams();


   //----------------------------------------//
   // Generate default param file (and stop) //
   //----------------------------------------//
   // If command has an extra D (e.g. g1D, h5D, etc.)
   if (opt.find_first_of("D") != string::npos ) {
      if ( (opt.find_first_of("a") != string::npos) || (argc == 3 && (string(argv[2]) == "-a" || string(argv[2]) == "--all") ) )
         // If extra a (e.g. g1Da), or -a or --all, also print all parameters (standard and hidden)
         generate_default_parfile(argv[0], opt[1], opt_index, argc, true);
      else
         generate_default_parfile(argv[0], opt[1], opt_index, argc, false);
      return 0;
   }

   //---------------------------------------------------//
   // Split simu mode and choice (stop if wrong format) //
   //---------------------------------------------------//
   // If wrong run options, print usage and quit
   bool is_format_ok = print_usage(argv[0], opt[1], opt_index, argc);
   if (!is_format_ok)
      return 0;


   //------------------------------//
   // Fill standard flags for mode //
   //------------------------------//
   print_simu_mode(simu_mode);
   // Find corresponding standard parameters
   gSIM_STANDARD_INPUTPARAMS = inputparameters_globalparams2string(gSIM_FLAG_MODE);
   // hack to allow hidden alpha_int in 2D skymaps:
   if (gSIM_FLAG_MODE == kg5 || gSIM_FLAG_MODE == kg6 || gSIM_FLAG_MODE == kg7 || gSIM_FLAG_MODE == kg8
         || gSIM_FLAG_MODE == kh4  || gSIM_FLAG_MODE == kh5) {
      gSIM_ALPHAINT = -1; // (alpha_int later automatically set corresponding to map resolution)
   }


   //--------------------------------//
   // Handle command-line parameters //
   //--------------------------------//
   // Check syntax of command-line parameters not wrong (does not ensure it is OK though)
   commandline_check_syntax(argc, argv);
   // Read command-line parameters and update global parameters
   commandline_parse_and_update_glopalparams(argc, argv);


   //------------//
   // Run option //
   //------------//
   // Open Root app. to enable displays (quite susceptible to segmentation faults)
#if IS_ROOT
      gSIM_ROOTAPP = new TApplication("App", NULL, NULL);
      gSIM_CLUMPYAD = orphanCLUMPYadonplots();
#endif

   if (opt[1] == 'g') run_opt_g(opt_index);
   else if (opt[1] == 'h') run_opt_h(opt_index);
   else if (opt[1] == 'e') run_opt_e(opt_index);
   else if (opt[1] == 'o') run_opt_o(opt_index);
   else if (opt[1] == 's') run_opt_s(opt_index);
   else if (opt[1] == 'z') run_opt_z();

#if IS_ROOT
      if (gSIM_IS_DISPLAY) delete gSIM_ROOTAPP;
      gSIM_ROOTAPP = NULL;
      delete gSIM_CLUMPYAD;
#endif

   return 0;
}

/*! \file clumpy.cc
  \brief <b>CLUMPY executable</b> (profiles, 1D or 2D-\f$J\f$ skymaps, stat. analysis, ...)

  \mainpage

<b> This is the Doxygen developer documentation. <a href=../index.html>Get back to the user documentation</a>.</b>

If you want to contribute to the code or have found a bug, don't hesitate to contact the CLUMPY crew, <a mailto=clumpy@lpsc.in2p3.fr>clumpy@lpsc.in2p3.fr</a>!

*/