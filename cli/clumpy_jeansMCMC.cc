// C++ std libraries
#include <stdlib.h>
using namespace std;

// CLUMPY includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"
#include "../include/jeans_analysis.h"

// GreAT includes
#include "TGreatModel.h"
#include "TGreatManager.h"
#include "TGreatEstimator.h"
#include "TGreatPoint.h"
#include "TGreatMCMCStep.h"
#include "TGreatMCMCAlgorithmCovariance.h"


// ROOT includes
#if IS_ROOT
#include <TApplication.h>
#include <TRandom.h>
#include <TFile.h>
#endif

struct gStructJeansData jeans_data; // structure containing the Jeans parameters (filled from a Jeans analysis parameters file)
struct gStructJeansAnalysis jeans_struct; // structure containing the kinematic data (filled from a Jeans kinematic data file)

int N = 0; // number of trials points
TGreatManager<TGreatMCMCAlgorithmCovariance> *MyManager;

int ndata; // number of points in data file - used only for the header of the output file

//______________________________________________________________________________
[[deprecated("Replaced by python-based workflow, which uses pymc4 over GReAT")]]
void Create_file(double longit, double lat, double dist, int numberdata, string name, string inputname)
{

   // This function creates a statistical file (readable by CLUMPY, options -s) from the MCMC output .root file.

   // Name of output .dat file
   string dat = ".dat";
   string outputfile;;
   outputfile = inputname;
   outputfile.erase(outputfile.end() - 5, outputfile.end());
   outputfile = outputfile + dat ;

   // Get the tree from the MCMC .root file
   TFile *file = TFile::Open(inputname.c_str());
   TTree *mcmc = (TTree *) file->Get("mcmc"); // The tree must have the name "mcmc"

   // Construct an estimator
   TGreatEstimator<TGreatMCMCAlgorithmCovariance> *estimator = new TGreatEstimator<TGreatMCMCAlgorithmCovariance>(mcmc);

   int Nfreepar =  estimator->GetNParameters(); // Number of free parameters
   vector<double> Likelihood;
   vector< vector <double> > Parameter;

   // Create the header of the output .txt file
   FILE *fp;
   fp = fopen(outputfile.c_str(), "w");
   printf_parjeans_header(fp, jeans_struct, jeans_data);

   // Get the parameters values from the MCMC. Only the "independant" samples are considered (burn-in and correlations removed)
   for (TGreatMCMCSample *sample = estimator->GetFirstIndSample(); sample != 0; sample = estimator->GetNextIndSample()) {
      // Copy parameters in ParJeans
      for (int i = 0; i < jeans_struct.ParNames.size(); i++)
         jeans_struct.ParJeans[i] = sample->GetValue(jeans_struct.ParNames[i].c_str());
      // Print in file
      printf_parjeans_result(fp, jeans_struct, sample->GetLogProb());
   }

   fclose(fp);
   cout << "file saved in : " << outputfile.c_str() << endl;
}

//______________________________________________________________________________
[[deprecated("Replaced by python-based workflow, which uses pymc4 over GReAT")]]
double MyLogLikelihoodFunction(TGreatPoint &point)
{
   // Number of trials points proposed
   N = N + 1;
   if (N % 1000 == 0)
      cout << N << " trial points proposed" << endl;

   // Get proposed parameters
   double par_tmp[jeans_struct.ParNames.size()];
   for (int i = 0; i < jeans_struct.ParNames.size(); i++) {
      if (jeans_struct.IsFreePar[i]) // free parameters
         par_tmp[i] = point.GetValue(jeans_struct.ParNames[i]);
      else
         par_tmp[i] = jeans_struct.ParJeans[i];
      if (std::isnan(par_tmp[i])) {
         cout << "--- skip nan values" << endl;
         return 1.e30;
      }
      // if proposed parameters are out of range: skip
      if (jeans_struct.IsFreePar[i] && (par_tmp[i] < jeans_struct.RangeLo[i] || par_tmp[i] > jeans_struct.RangeUp[i]))
         return 1.e30;
      // Unset log values if needed
      if (jeans_struct.IsLogVal[i])
         par_tmp[i] = pow(10., par_tmp[i]);
   }

   return log_likelihood_jeans(par_tmp, jeans_data);
}

//______________________________________________________________________________
[[deprecated("Replaced by python-based workflow, which uses pymc4 over GReAT")]]
int Run(string outputfilemcmc, string outputfileana, int n_chains, int n_ptsperchain)
{
   //--- Creates MCMC model (set fixes and free parameters, likelihood function to use, etc.).
   //  outputfilemcmc    Output file namme in which to store results
   //  outputfileana     Outpu file name in which to store covariance matrix
   //  n_chains          Number of chains to run
   //  n_ptsperchain     Number of points per chain

   // Create a model
   TGreatModel *MyModel = new TGreatModel();

   // Set model parameters
   // GreAT requires to have free parameters first, then fixed ones
   for (int k = 0; k < jeans_struct.ParNames.size(); k++) {
      if (jeans_struct.IsFreePar[k]) // free parameters
         MyModel->AddParameter(jeans_struct.ParNames[k], "", jeans_struct.ParJeans[k], jeans_struct.RangeLo[k], jeans_struct.RangeUp[k], TGreatParameter::kFreeP);
   }
   for (int k = 0; k < jeans_struct.ParNames.size(); k++) {
      if (!jeans_struct.IsFreePar[k]) // fixed parameters
         MyModel->AddParameter(jeans_struct.ParNames[k], "", jeans_struct.ParJeans[k], jeans_struct.RangeLo[k]*0.99-1., jeans_struct.RangeUp[k]*1.01+1., TGreatParameter::kFixedP);

//      cout << " => " << MyModel->GetParameterSet()->GetNParameters() << " params so far:" << endl;
//      for (int k=0; k<MyModel->GetParameterSet()->GetNParameters(); ++k)
//         cout << "   - " << MyModel->GetParameterSet()->GetParameterName(k) << endl;
   }

   // Set the likelihood function
   MyModel->SetLogLikelihoodFunction(MyLogLikelihoodFunction);

   //Define the factory to run the algorithm
   MyManager = new TGreatManager<TGreatMCMCAlgorithmCovariance> (MyModel);
   MyManager->GetAlgorithm()->SetUpdateStatus(true); // update the covariance matrix after each chain

   // Get the covariance matrix from a previous run if it exits, otherwise the
   // default covariance matrix will be used.
   TFile *File = new TFile(outputfileana.c_str());

   if (!File->IsZombie()) {
      TMatrixD *Cov = (TMatrixD *) File->Get("Cov");
      if (Cov) MyManager->GetAlgorithm()->SetCovarianceMatrix(Cov->GetMatrixArray());
   }

   // Set output file name
   MyManager->SetOutputFileName(outputfilemcmc.c_str());
   MyManager->SetTreeName("mcmc");

   // Set number of chains and trial points (per chain)
   MyManager->SetNTrialLists(n_chains);
   MyManager->SetNTrials(n_ptsperchain);

   // Run the MCMC analysis
   MyManager->Run();
   delete MyModel;
   return 0;
}

//______________________________________________________________________________
[[deprecated("Replaced by python-based workflow, which uses pymc4 over GReAT")]]
void Ana(string const &outputfileana, string const &mcmcfile = "0")
{
   string name;
   if (mcmcfile == "0") {
      name = MyManager->GetOutputFileName();
   } else {
      name = mcmcfile;
   }

   // Get the tree from the file produced in the function Run()
   TFile *file = TFile::Open(name.c_str());
   TTree *mcmc = (TTree *) file->Get("mcmc");

   // Construct an estimator
   TGreatEstimator<TGreatMCMCAlgorithmCovariance> *estimator = new TGreatEstimator<TGreatMCMCAlgorithmCovariance>(mcmc);

   // Compute and plot the PDF for the free parameters of the model
   estimator->ComputePDF();
   estimator->PlotPDF();

   // Compute and print the covariance matrix of the free parameters of the model
   TVectorD *Mu;
   TMatrixD *Cov;
   estimator->ComputeCovarianceMatrix(Mu, Cov);

   TFile *fileout = TFile::Open(outputfileana.c_str(), "recreate");
   Mu->Write("Mu");
   Cov->Write("Cov");
   fileout->Close();

   // Display covariance matrix
   Mu->Print();
   Cov->Print();

}

//______________________________________________________________________________
[[deprecated("Replaced by python-based workflow, which uses pymc4 over GReAT")]]
int main(int argc, char *argv [])
{
   //--- Root application (enables displays)
#if IS_ROOT
   gSIM_ROOTAPP = new TApplication("App", NULL, NULL);
   gSIM_CLUMPYAD = orphanCLUMPYadonplots();
#endif


   bool is_arg = true;
   if (argc <= 1 || (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-r"))
      is_arg = false;
   else {
      string opt = argv[1];

      //#################################//
      //# MCMC analysis related calls  #//
      //#################################//

      // Print example values
      if (opt.find_first_of("D") != string::npos) {
         printf("   MCMC Jeans analysis:  %s -r %s/data_sigmap.txt output/stat_example.root output/stat_exampleAna.root 10000 8 %s/params_jeans.txt 0.05\n", argv[0], gPATH_TO_CLUMPY_DATA.c_str(), gPATH_TO_CLUMPY_DATA.c_str());
         printf("\n");
         return 1;
      }
   }

   // Usage of -1, -2, ..
   if (!is_arg || argc != 9) {
      printf("  usage:\n");
      printf("   [MCMC analysis:         ]  %s -r data_file OutputfileMCMC OutputfileAna Npoints Nchains params_file eps\n", argv[0]);
      printf("        -> data_file: file name (for sigma_p or velocity data)\n");
      printf("        -> OutputfileMCMC: name of output MCMC root file\n");
      printf("        -> OutputfileAna: name of output analysis root file\n");
      printf("        -> Npoints: number of trials points per chain\n");
      printf("        -> Nchains: number of chains\n");
      printf("        -> param_file: Jeans parameter file name\n");
      printf("        -> eps: precision\n");
      printf("\n");
      printf(" N.B.: to see default parameter values (examples), use \"-rD\". \n");
      printf(" If a previous OutputfileAna exists, its covariance matrix will be used for the proposition function of this run.\n");
      printf(" !!! If the list of free parameters is changed, using the previous OutputfileAna will cause a crash. Please change the name of the output. !!! \n\n");
      printf(" The choice of analysis (binned or unbinned) depends on the Jeans keyword from data_file.\n");

      printf("\n\n");
      return 1;
   }

   string datafile = argv[2];
   string name_mcmc = argv[3];
   string outdir_mcmc = name_mcmc.substr(0, name_mcmc.find_last_of("\\/"));
   if (name_mcmc.size() < 6) {
      cout << "Error: OutputfileMCMC must end by \".root\" " << endl;
      return 0;
   }
   if (outdir_mcmc != name_mcmc) makedir(outdir_mcmc.c_str());

   string root = name_mcmc.substr(name_mcmc.size() - 5);
   if (root != ".root") {
      cout << "Error: OutputfileMCMC must end by \".root\" " << endl;
      return 0;
   }

   string name_ana = argv[4];
   string outdir_ana = name_ana.substr(0, name_ana.find_last_of("\\/"));
   if (name_ana.size() < 6) {
      cout << "Error: OutputfileAna must end by \".root\" " << endl;
      return 0;
   }
   if (outdir_ana != name_ana) makedir(outdir_ana.c_str());

   string rootana = name_ana.substr(name_ana.size() - 5);
   if (rootana != ".root") {
      cout << "Error: OutputfileAna must end by \".root\" " << endl;
      return 0;
   }

   // Initialise parameters and data (fill jeans_data)
   int Npoints = atoi(argv[5]);
   int Nchains = atoi(argv[6]);
   string param_file = argv[7];
   double eps = atof(argv[8]);

   load_jeansdata(datafile, jeans_data, false);
   load_jeansconfig(param_file, jeans_struct, eps);
   print_jeansanalysis_setup(jeans_struct);

   // Run the MCMC analysis
   Run(name_mcmc, name_ana, Nchains, Npoints);
   Create_file(jeans_struct.Longitude, jeans_struct.Latitude, jeans_struct.Distance, ndata, jeans_struct.Name, MyManager->GetOutputFileName());
   Ana(name_ana);



#if IS_ROOT
   gSIM_ROOTAPP->Run(0);
   delete gSIM_CLUMPYAD;
#endif
}

/*! \file clumpy_jeansMCMC.cc
  \brief <b>Jeans analysis executable</b> (MCMC analysis, only if GreAT is installed)
*/
