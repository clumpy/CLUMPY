// C++ std libraries
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <vector>

// Clumpy includes
#include "../include/clumps.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/inlines.h"

using namespace std;

bool execute_test(const string &test_flag, const vector<string> &filenames_test,
                  const string &test_dir, const string &compare_dir,
                  const string &exe_name, const string &special_command = "none",
                  bool is_verbose=false);
void stref17_tests(string test_dir, string compare_dir, const string &file_test, bool is_verbose);

//______________________________________________________________________________
bool execute_test(const string &test_flag, const vector<string> &filenames_test,
                  const string &test_dir, const string &compare_dir,
                  const string &exe_name, const string &special_command,
                  bool is_verbose)
{
   //--- Executes test of clumpy function.
   //
   // INPUTS:
   //  test_flag        Clumpy simulation test_flag to test (e.g., g0, g1, ...)
   //  filenames_test   Output file(s) to test.
   //  test_dir         Directory into which test results are written.
   //  compare_dir      Directory where to find output files against which to test
   //                   results (filenames_test) are compared.
   //  exe_name         Name of executable
   //  special_command  Add a special command of your choice when executing the test.
   //  is_verbose       Execute in verbose test_flag if true
   //
   // OUTPUT:
   //    true: test passed, false: test not passed.

   cout << " Testing -" << test_flag << " ..." << endl;
   string cd2testdir = "cd " + test_dir + "; ";

   // Run executable to create relevant outputs
   if (test_flag == "SL17")
      stref17_tests(test_dir, compare_dir, filenames_test[0], is_verbose);
   else {
      // Form base exe command
      string exe_cmd = cd2testdir + exe_name + " -" + test_flag + "D > dummy; " +  exe_name + " -" + test_flag + " -p -i clumpy_params_" + test_flag + ".txt --gSIM_OUTPUT_DIR=. --gSIM_IS_WRITE_ROOTFILES=0";
      if (test_flag == "r1")
         exe_cmd = cd2testdir + exe_name + " -" + test_flag + " " + gPATH_TO_CLUMPY_DATA +"/data_sigmap.txt chi2_binned.dat " + gPATH_TO_CLUMPY_DATA +"/params_jeans.txt 0.05";
      // Add option
      if (special_command != "none") {
         exe_cmd += " ";
         exe_cmd += special_command;
      }
      exe_cmd += " > dummy";
      sys_safeexe(exe_cmd, is_verbose);
   }

   // Check difference between generated and ref file (for all files to test)
   bool is_passed = true;
   for (std::size_t i = 0; i < filenames_test.size(); ++i) {
      string diff_files = test_dir + "/" + filenames_test[i] + " " + compare_dir + "/" + filenames_test[i];
      int sys = sys_safeexe(("diff -q " + diff_files).c_str(), is_verbose);
      if (sys == 0) {
         // Test passed
         // Remove all generated files (in test_dir)
         string rm_cmd = cd2testdir + "rm -f " + filenames_test[i];
         sys_safeexe(rm_cmd, is_verbose);
      } else {
         // Test not passed (for this file)
         printf("  - no match between: %s\n", diff_files.c_str());
         is_passed = false;
      }
   }
   // Remove other files
   string rm_cmd = cd2testdir + "rm -f clumpy_params_* *root *fits *.dat dummy";
   sys_safeexe(rm_cmd, is_verbose);

   if (is_passed) {
      cout <<  COLOR_BGREEN "  => OK" COLOR_RESET << endl;
   } else
      cout <<  COLOR_BRED "    => not passed" COLOR_RESET << endl;

   return is_passed;
}

//______________________________________________________________________________
void stref17_tests(string test_dir, string compare_dir, const string &file_test, bool is_verbose)
{
   //--- Tests of all functions related to Stref17 (all functions/variables start with 'stref17_').
   //  test_dir         Directory into which test results are written.
   //  compare_dir      Directory where to find output files against which to test
   //  file_test        File in which test is stored/referenced
   //  is_verbose       Execute in verbose test_flag if true


   FILE *fp[1] = {NULL};
   string testfile = test_dir + "/" + file_test;
   fp[0] = fopen(testfile.c_str(), "w");

   // Check reading table of cmin
   string filename = gPATH_TO_CLUMPY_DATA + "/subhalos_Stref17/cmin_nfw_eps1.dat";
   stref17_load_cmin200_vs_rgal(filename);
   // Print read values
   //for (std::size_t i=0; i< stref17_c200_min.size(); ++i)
   //   printf("%lf    %lf\n", i, stref17_rgal_kpc[i], stref17_c200_min[i]);
   for (std::size_t i = 0; i < gMW_SUBS_TABULATED_CMIN_OF_R_GRID.size(); i += gMW_SUBS_TABULATED_CMIN_OF_R_GRID.size() - 1)
      fprintf(fp[0], "i=%d   rgal=%lf [kpc]   stref17_c200_min=%lf [-]\n", (int)i, gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i], gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i]);

   // Check interpolation
   double r = 3.;
   int i_r = binary_search(gMW_SUBS_TABULATED_R_OF_CMIN_GRID, r);
   fprintf(fp[0], " Interp1D: stref17_c200_min(%f kpc)=%f/%f/%f/%f [kLINLIN,kLINLOG/kLOGLIN/kLOGLOG] -> between %f(@r=%f) and %f(@r=%f)\n\n", r,
           interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLINLIN), interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLINLOG),
           interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLOGLIN), interp1D(r, gMW_SUBS_TABULATED_R_OF_CMIN_GRID, gMW_SUBS_TABULATED_CMIN_OF_R_GRID, kLOGLOG),
           gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i_r], gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i_r], gMW_SUBS_TABULATED_CMIN_OF_R_GRID[i_r + 1], gMW_SUBS_TABULATED_R_OF_CMIN_GRID[i_r + 1]);


   // Check reading table of rt/rs
   filename = gPATH_TO_CLUMPY_DATA +"/subhalos_Stref17/rt_over_rs_nfw.dat";
   stref17_load_rt2rs_vs_rgal_c200(filename);
   // Print read values
   // -> stref17_c200 values
   fprintf(fp[0], "stref17_c200 [-]: \n");
   for (std::size_t i = 0; i < gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID.size(); ++i)
      fprintf(fp[0], "%lf\n", gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i]);
   fprintf(fp[0], "\n");
   // -> rt/rs values
   for (std::size_t j = 0; j < gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID.size(); j += (int)gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID.size() - 1) {
      fprintf(fp[0], "%lf\n", gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[j]);
      for (std::size_t i = 0; i < gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID.size(); ++i)
         fprintf(fp[0], "%le\n", gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[j][i]);
      fprintf(fp[0], "\n");
   }

   // Check 2D interpolation
   r = 3.;
   i_r = binary_search(gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID, r);
   double c = 10.;
   int i_c = binary_search(gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID, c);
   fprintf(fp[0], " Interp2D: rt/rs(r=%f kpc,c=%f)=%f [kLOGLOG] -> between %f(@r=%f,c=%f) and %f(@r=%f,c=%f)\n", r, c,
           interp2D(r, c, gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID, gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID, gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID, kLOGLOG), gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[i_r][i_c], gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[i_r], gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i_c],
           gMW_SUBS_TABULATED_RTIDAL_TO_RS_GRID[i_r + 1][i_c + 1], gMW_SUBS_TABULATED_R_OF_RTIDAL_TO_RS_GRID[i_r + 1], gMW_SUBS_TABULATED_C200_OF_RTIDAL_TO_RS_GRID[i_c + 1]);


   // Check reading table of lcrit:
   filename = gPATH_TO_CLUMPY_DATA +"/subhalos_Stref17/lcrit_tables/l_crit_core_eps1_rse5e-2.dat";
   vector <double> stref17_log10m1, stref17_log10m2, stref17_lcrit;
   stref17_load_lcrit(filename, stref17_log10m1, stref17_log10m2, stref17_lcrit);
   // Print read values
   fprintf(fp[0], "i    log10(m_i/1Msol)    l_crit [kpc]\n");
   for (std::size_t i = 0; i < stref17_log10m1.size(); i += 1)
      fprintf(fp[0], "%d   %.1f : %.1f    %le\n", (int)i, stref17_log10m1[i], stref17_log10m2[i], stref17_lcrit[i]);
   fclose(fp[0]);
}

//______________________________________________________________________________
int main(int argc, char *argv [])
{

   // Check CLUMPY_DATA environment variable is set
   if (gPATH_TO_CLUMPY_DATA == "") {
      string message = "Environment variable \"CLUMPY_DATA\" is not set, please look at the documentation to set it (mandatory for CLUMPY runs)";
      printf("\n====> ERROR: %s\n      => abort()\n\n", message.c_str());
      abort();
   }

   // Print command argument if no or wrong argument
   string flag = "";
   bool is_verbose = false;
   if (argc >1)
      flag = argv[1];
   if (argc >2)
      is_verbose = true;

   if (argc<=1 || flag[0] != '-' || (flag[1] != 'g' &&  flag[1] != 'h' && flag[1] != 'e' && flag[1] != 's' && flag[1] != 'r' && flag[1] != 'z' && flag != "-SL17" && flag != "-1D" && flag != "-2D" && flag != "-ALL")) {
      printf("USAGE: %s flag [verbose]\n"
             "   with flag:\n"
             "      -g or -gX    [Galactic halo tests: g0, g1, g2, g4, g6, g8 enabled]\n"
             "      -h or -hX    [Halo list tests: h0, h1, h2, h3, h5, h8, h10, h10 enabled]\n"
             "      -e or -eX    [Extra-galactic tests: e0, e1, e2, e3, e4, e5, e6 enabled]\n"
             "      -s or -sX    [Statistical tests: s0, s5, s6, s8, s9, s10 enabled]\n"
             "      -r           [Jeans analysis tests: r1 enabled]\n"
             "      -z           [Spectrum tests]\n"
             "      -SL17        [Galactic-subhalo disruption test]\n"
             "   or with flag combinations\n"
             "      -1D   [All 1D-related tests (fast)]\n"
             "      -2D   [All skymap-related tests (slow)]\n"
             "      -ALL  [All tests]\n"
             " EXEMPLE:\n"
             "   %s -g0\n"
             "   %s -g0 verbose\n",
             argv[0], argv[0], argv[0]);
      return -1;
   }

   string command;
   char* templ = strdup("/tmp/clumpytests_XXXXXX");
   char *dir_name = mkdtemp(templ);
   if(dir_name == NULL) {
      perror("mkdtemp failed: ");
      return 0;
   }
   string test_dir = string(dir_name);
   string compare_dir = gPATH_TO_CLUMPY_DATA +"/tests_ref/";

   // create clumpy_test_tmp directory
   command = "mkdir -p " + test_dir;
   sys_safeexe(command, is_verbose);

   vector< vector<string> > filenames_test;
   vector<string> exe_names;
   vector<string> filenames_tmp;
   vector<string> special_commands;
   vector<string> functions_tested;

   //-----------------------------
   // Dedicated test for Stref & Lavalle
   // clump disruption implementation
   //-----------------------------
   if (flag == "-SL17" || flag == "-1D" || flag == "-ALL") {
      functions_tested.push_back("SL17");
      filenames_tmp.push_back("stref17_tests.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("");
   }

   //-----------------------------
   // Related to Galactic options
   //-----------------------------
   if (flag == "-g0" || flag == "-g" || flag == "-1D" || flag == "-ALL") {
      // M(r)
      functions_tested.push_back("g0");
      filenames_tmp.push_back("gal.Mr.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-g1" || flag == "-g" || flag == "-1D" || flag == "-ALL") {
      // rho(r)
      functions_tested.push_back("g1");
      filenames_tmp.push_back("gal.rhor.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-g2" || flag == "-g" || flag == "-1D" || flag == "-ALL") {
      // Annihilation J(alpha)
      functions_tested.push_back("g2");
      filenames_tmp.push_back("gal.Jalphaint.ecsv");
      filenames_tmp.push_back("gal.fluxes_annihil.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999 > dummy; mv " + test_dir + "/gal.fluxes.output " + test_dir + "/gal.fluxes_annihil.output");
      // Decay D(alpha)
      functions_tested.push_back("g2");
      filenames_tmp.push_back("gal.Dalphaint.ecsv");
      filenames_tmp.push_back("gal.fluxes_decay.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gPP_DM_IS_ANNIHIL_OR_DECAY=0 --gPP_DM_DECAY_LIFETIME_S=1e+27 --gSIM_EPS=999 > dummy; mv " + test_dir + "/gal.fluxes.output " + test_dir + "/gal.fluxes_decay.output");
   }
   if (flag == "-g4" || flag == "-g" || flag == "-1D" || flag == "-ALL") {
      // J(theta)
      functions_tested.push_back("g4");
      filenames_tmp.push_back("gal.Jtheta.ecsv");
      filenames_tmp.push_back("gal.Jthetadiff.ecsv");
      filenames_tmp.push_back("gal.intensities.output");
      filenames_tmp.push_back("list_generic_test.txt.gal.list.Jtheta.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   }
   if (flag == "-g6" || flag == "-g" || flag == "-2D" || flag == "-ALL") {
      // Skymap Jsm+<sub>+list
      functions_tested.push_back("g6");
      string testfile1_tmp, testfile2_tmp, testfile3_tmp, testfile4_tmp;
      testfile1_tmp = "test_g6a.txt";
      testfile2_tmp = "test_g6b.txt";
      testfile3_tmp = "test_g6c.txt";
      testfile4_tmp = "annihil_gal2D_LOS0_0_FOV90x45_nside1024";
      filenames_tmp.push_back(testfile1_tmp);
      filenames_tmp.push_back(testfile2_tmp);
      filenames_tmp.push_back(testfile3_tmp);
      filenames_tmp.push_back(testfile4_tmp + ".list");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      string file_base = test_dir + "/" + testfile4_tmp;
      string clumpy_o1_base = "clumpy -o1 --gSIM_OUTPUT_DIR=. --gUTIL_SKYMAP_FITSFILE=" + file_base + ".fits --gUTIL_SKYMAP_EXTENSION=";
      special_commands.push_back("--gSIM_EPS=999 --gLIST_HALOES=" + compare_dir + "list_generic_test.txt > dummy; "
                                 + clumpy_o1_base + "1 > dummy; head -n +1500 " + file_base + "-JFACTOR.dat > " + test_dir + "/" + testfile1_tmp + ";"
                                 + clumpy_o1_base + "2 > dummy; head -n +1500 " + file_base + "-JFACTOR_PER_SR.dat > " + test_dir + "/" + testfile2_tmp + ";"
                                 + clumpy_o1_base + "3 > dummy; head -n +1500 " + file_base + "-INTEGRATED_FLUXES.dat > " + test_dir + "/" + testfile3_tmp + ";"
                                );
   }
   #if IS_ROOT
   if (flag == "-g8" || flag == "-g" || flag == "-2D" || flag == "-ALL") {
      // Skymap Jsm+<sub>+list
      functions_tested.push_back("g8");
      string testfile1_tmp, testfile2_tmp, testfile3_tmp;
      testfile1_tmp = "test_g8a.txt";
      testfile2_tmp = "test_g8b.txt";
      testfile3_tmp = "annihil_gal2D_LOS180_0_FOV4x4_nside1024";
      filenames_tmp.push_back(testfile1_tmp);
      filenames_tmp.push_back(testfile2_tmp);
      filenames_tmp.push_back(testfile3_tmp + ".list");
      filenames_tmp.push_back(testfile3_tmp + ".drawn");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      string file_base = test_dir + "/" + testfile3_tmp;
      string clumpy_o1_base = "clumpy -o1 --gSIM_OUTPUT_DIR=. --gUTIL_SKYMAP_FITSFILE=" + file_base + ".fits --gUTIL_SKYMAP_EXTENSION=";
      special_commands.push_back("--gSIM_EPS=999 --gLIST_HALOES=" + compare_dir + "list_generic_test.txt > dummy;"
                                 + clumpy_o1_base + "1 > dummy; head -n +3000 " + file_base + "-JFACTOR.dat > " + test_dir + "/" + testfile1_tmp + ";"
                                 + clumpy_o1_base + "2 > dummy; head -n +3000 " + file_base + "-JFACTOR_PER_SR.dat > " + test_dir + "/" + testfile2_tmp + ";");
   }
   #endif


   //-----------------------------
   // Related to halo list options
   //-----------------------------
   if (flag == "-h0" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      // M(r)
      functions_tested.push_back("h0");
      filenames_tmp.push_back("list_generic_test.txt.Mr.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   }
   if (flag == "-h1" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      // rho(r)
      functions_tested.push_back("h1");
      filenames_tmp.push_back("list_generic_test.txt.rhor.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   }
   if (flag == "-h2" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      // Jsm+<sub>(alpha_int)
      functions_tested.push_back("h2");
      filenames_tmp.push_back("list_generic_test.txt.Jalphaint.ecsv");
      filenames_tmp.push_back("list_generic_test.txt.fluxes.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   }
   if (flag == "-h3" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      // Jsm+<sub>(alpha_int)
      functions_tested.push_back("h3");
      filenames_tmp.push_back("list_generic_test.txt.Jtheta.ecsv");
      filenames_tmp.push_back("list_generic_test.txt.intensities.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES=" + compare_dir + "list_generic_test.txt --gSIM_EPS=999");
   }
   #if IS_ROOT
   if (flag == "-h5" || flag == "-h" || flag == "-2D" || flag == "-ALL") {
      functions_tested.push_back("h5");
      string testfile1_tmp, testfile2_tmp, testfile3_tmp, testfile4_tmp;
      testfile1_tmp = "test_h5a.txt";
      testfile2_tmp = "test_h5b.txt";
      testfile3_tmp = "test_h5c.txt";
      testfile4_tmp = "annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024";
      filenames_tmp.push_back(testfile1_tmp);
      filenames_tmp.push_back(testfile2_tmp);
      filenames_tmp.push_back(testfile3_tmp);
      filenames_tmp.push_back(testfile4_tmp + ".drawn");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      string file_base = test_dir + "/" + testfile4_tmp;
      string clumpy_o1_base = "clumpy -o1 --gSIM_OUTPUT_DIR=. --gUTIL_SKYMAP_FITSFILE=" + file_base + ".fits --gUTIL_SKYMAP_EXTENSION=";
      special_commands.push_back("--gSIM_EPS=998 --gSIM_USER_RSE=20 --gSIM_IS_CALC_JVARIANCE=1 --gLIST_HALOES=" + compare_dir + "list_generic_test.txt > dummy;"
                                 + clumpy_o1_base + "1 > dummy; tail -n +500 " + file_base + "-JFACTOR.dat > " + test_dir + "/" + testfile1_tmp + ";"
                                 + clumpy_o1_base + "2 > dummy; tail -n +500 " + file_base + "-JFACTOR_PER_SR.dat > " + test_dir + "/" + testfile2_tmp + ";"
                                 + clumpy_o1_base + "3 > dummy; tail -n +500 " + file_base + "-INTEGRATED_FLUXES.dat > " + test_dir + "/" + testfile3_tmp + ";"
                                );
   }
   #endif
   if (flag == "-h8" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      functions_tested.push_back("h8");
      filenames_tmp.push_back("list_generic_jeans.txt.sigmapR.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   }
   if (flag == "-h9" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      functions_tested.push_back("h9");
      filenames_tmp.push_back("list_generic_jeans.txt.IR.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   }
   if (flag == "-h10" || flag == "-h" || flag == "-1D" || flag == "-ALL") {
      functions_tested.push_back("h10");
      filenames_tmp.push_back("list_generic_jeans.txt.Betar.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gLIST_HALOES_JEANS=" + compare_dir + "list_generic_jeans.txt --gSIM_EPS=999");
   }


   //-----------------------------
   // Related to extragalactic options
   //-----------------------------
   if (flag == "-e0" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Pure cosmo functions (distances, omega, H0)
      functions_tested.push_back("e0");
      filenames_tmp.push_back("cosmo.distances.ecsv");
      filenames_tmp.push_back("cosmo.omegas.ecsv");
      filenames_tmp.push_back("cosmo.distances_norm.ecsv");
      filenames_tmp.push_back("cosmo.hubble.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e1" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Halo mass functions
      functions_tested.push_back("e1");
      filenames_tmp.push_back("cosmo.hmf_TINKER08_2D.output");
      filenames_tmp.push_back("cosmo.dndOmega_TINKER08_2D.output");
      filenames_tmp.push_back("cosmo.sigma2_2D.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e2" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Boost factor and Luminosity
      functions_tested.push_back("e2");
      filenames_tmp.push_back("boost_m_alpha.output");
      filenames_tmp.push_back("c_lum_m.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e3" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Intensity multiplier
      functions_tested.push_back("e3");
      filenames_tmp.push_back("cosmo.d_intensitymultiplier_dlnM_2D.output");
      filenames_tmp.push_back("cosmo.intensitymultiplier.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e4" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // EBL absorption
      functions_tested.push_back("e4");
      filenames_tmp.push_back("cosmo.ebl_tau.output");
      filenames_tmp.push_back("cosmo.ebl_exp-tau.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e5" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Extragal skymap at 1 energy
      functions_tested.push_back("e5");
      filenames_tmp.push_back("cosmo.dPhidOmegadE_at10GeV_2D.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-e6" || flag == "-e" || flag == "-1D" || flag == "-ALL") {
      // Extragal flux
      functions_tested.push_back("e6");
      filenames_tmp.push_back("cosmo.dPhidOmegadE_CIRELLI11_EW_GAMMA_m100.ecsv");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }

   //-----------------------------
   // Related to statistical options
   //-----------------------------
   #if IS_ROOT
   if (flag == "-s0" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CL M(r)
      functions_tested.push_back("s0");
      filenames_tmp.push_back("stat_example.dat.Mr_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-s5" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CL rho(r)
      functions_tested.push_back("s5");
      filenames_tmp.push_back("stat_example.dat.rhor_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-s6" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CL J(alpha)
      functions_tested.push_back("s6");
      filenames_tmp.push_back("stat_example.dat.Jalphaint_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-s8" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CL Sigma_p(R)
      functions_tested.push_back("s8");
      filenames_tmp.push_back("stat_example.dat.sigma_p_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-s9" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CS nur^2(r)
      functions_tested.push_back("s9");
      filenames_tmp.push_back("stat_example.dat.vr2_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   if (flag == "-s10" || flag == "-s" || flag == "-1D" || flag == "-ALL") {
      // CL beta_ani(r)
      functions_tested.push_back("s10");
      filenames_tmp.push_back("stat_example.dat.beta_cls.output");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }
   #endif

   //-----------------------------
   // Related to spectra options
   //-----------------------------
   if (flag == "-z0" || flag == "-z" || flag == "-1D" || flag == "-ALL") {
      functions_tested.push_back("z0");
      filenames_tmp.push_back("spectra_CIRELLI11_EW_GAMMA_m100.txt");
      filenames_tmp.push_back("dnde_CIRELLI11_EW_GAMMA_m100.txt");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy");
      special_commands.push_back("--gSIM_EPS=999");
   }

   //---------------------------
   // Related to Jeans analysis
   //---------------------------
   #if IS_ROOT
   if (flag == "-r1" || flag == "-r" || flag == "-1D" || flag == "-ALL") {
      // Minuit
      functions_tested.push_back("r1");
      filenames_tmp.push_back("chi2_binned.dat");
      filenames_test.push_back(filenames_tmp);
      filenames_tmp.clear();
      exe_names.push_back("clumpy_jeansChi2");
      special_commands.push_back("none");
   }
   #endif


   cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
   cout <<         "+++         CLUMPY Tests         +++" << endl;
#if IS_ROOT
   cout <<         "+++ (with ROOT functionalities)  +++" << endl;
#else
   cout <<         "+++  (w/o ROOT functionalities)  +++" << endl;
#endif
   cout << "++++++++++++++++++++++++++++++++++++" << endl;


   int n_tests = functions_tested.size();

   if (is_verbose)
      cout << "  N.B.: uses data from " << gPATH_TO_CLUMPY_DATA << endl;

   int n1D = 22;

   if (flag == "-1D")
      cout << endl << "++++    Tests of 1D modules only    ++++" << endl << endl;
   if (flag == "-2D")
      cout << endl << "++++    Tests of 2D modules only (please be patient...)   ++++" << endl << endl;

   bool is_everything_passed = true;
   int n_failed = 0;
   for (int i = 0; i < n_tests; ++i) {
      bool tmp = false;
      cout << "[" << i + 1 << "/" << n_tests << "]";
      tmp = execute_test(functions_tested[i], filenames_test[i],
                         test_dir, compare_dir, exe_names[i],
                         special_commands[i], is_verbose);

      // Count number of failed tests
      if (!tmp) {
         is_everything_passed = false;
         n_failed += 1;
      }
   }

   if (is_everything_passed) {
      cout << endl << " All tests passed." << endl;
      cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
      // Remove temporary directory
      sys_safeexe(("rmdir " + test_dir).c_str(), is_verbose);
      return EXIT_SUCCESS;
   } else {
      cout << endl << " " <<  n_failed << " out of " << n_tests  << " tests failed. Please compare the test output files in " << test_dir << " with " << compare_dir << "." << endl;
      cout << endl << "++++++++++++++++++++++++++++++++++++" << endl;
      return EXIT_FAILURE;
   }
}

/*! \file clumpy_tests.cc  \brief <b>Test suite for the main CLUMPY executables</b> (runs through all submodules and checks output) */

