#ifndef _PYCLUMPY_PYWRAPPER_CLUMPY_JEANS_CC_
#define _PYCLUMPY_PYWRAPPER_CLUMPY_JEANS_CC_

// PYBIND INCLUDES
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/pytypes.h>
#include <pybind11/stl.h>
namespace py = pybind11;
using namespace py::literals;

// C INCLUDES
#include <string>
#include <vector>

// CLUMPY INCLUDES
#include "../include/jeans_analysis.h"


// Loading jeans data
py::dict py_load_data(string const &filename){
    //Create a temporary gStructJeansData to load the data into
    struct gStructJeansData jeans_data;
    load_jeansdata(filename, jeans_data, false);

    //Copy all array-like data into numpy arrays before assigning them to assure a deep copy
    //sadly string arrays don't seem to work as strings don't have a given size. resorting to next-best option: list
    py::list TypeNames(0);
    for (string item : jeans_data.TypeNames){
        TypeNames.append(item);
    }

    size_t Dec_shape = jeans_data.Dec.size();
    py::array_t<double> Dec(Dec_shape, &jeans_data.Dec[0]);

    size_t RA_shape = jeans_data.RA.size();
    py::array_t<double> RA(RA_shape, &jeans_data.RA[0]);

    size_t MembershipProb_shape = jeans_data.MembershipProb.size();
    py::array_t<double> MembershipProb(MembershipProb_shape, &jeans_data.MembershipProb[0]);

    size_t R_shape = jeans_data.R.size();
    py::array_t<double> R(R_shape, &jeans_data.R[0]);

    size_t RErr_shape = jeans_data.RErr.size();
    py::array_t<double> RErr(RErr_shape, &jeans_data.RErr[0]);

    size_t Val_shape = jeans_data.Val.size();
    py::array_t<double> Val(Val_shape, &jeans_data.Val[0]);

    size_t ValErr_shape = jeans_data.ValErr.size();
    py::array_t<double> ValErr(ValErr_shape, &jeans_data.ValErr[0]);

    //Create the python dictionary to return by giving it all the values it needs
    py::dict jeans_data_dict("FileName"_a       = jeans_data.FileName,
                             "Type"_a           = jeans_data.Type,
                             "TypeNames"_a      = TypeNames,
                             "Dec"_a            = Dec,
                             "Dec_center"_a     = jeans_data.Dec_center,
                             "RA"_a             = RA,
                             "RA_center"_a      = jeans_data.RA_center,
                             "dist"_a           = jeans_data.dist,
                             "MembershipProb"_a = MembershipProb,
                             "R"_a              = R,
                             "RErr"_a           = RErr,
                             "Val"_a            = Val,
                             "ValErr"_a         = ValErr,
                             "Vmean"_a          = jeans_data.Vmean);
    return jeans_data_dict;
}

// Loading jeans config
py::dict py_load_config(string const &filename, double const &eps = 0.05){
    //Create a temporary gStructJeansAnalysis to load the config into
    struct gStructJeansAnalysis jeans_struct;
    load_jeansconfig(filename, jeans_struct, eps);

    //Copy all array-like data into numpy arrays before assigning them to assure a deep copy
    size_t shape = jeans_struct.ParNames.size();
    py::array_t<bool> IsFreePar(shape, &jeans_struct.IsFreePar[0]);

    py::array_t<int> IsLogVal(shape, &jeans_struct.IsLogVal[0]);

    py::array_t<double> RangeLo(shape, &jeans_struct.RangeLo[0]);

    py::array_t<double> RangeUp(shape, &jeans_struct.RangeUp[0]);

    //sadly string arrays don't seem to work as strings don't have a given size. resorting to next-best option: list
    py::list ParNames(0);
    for (string item : jeans_struct.ParNames){
        ParNames.append(item);
    }

    size_t shape_ParPrintOrder = jeans_struct.ParPrintOrder.size();
    py::array_t<int> ParPrintOrder(shape_ParPrintOrder, &jeans_struct.ParPrintOrder[0]);

    py::array_t<double> ParJeans(shape, &jeans_struct.ParJeans[0]);

    //Create the python dictionary to return by giving it all the values it needs
    py::dict jeans_config_dict("Distance"_a     = jeans_struct.Distance,
                             "HaloSize"_a       = jeans_struct.HaloSize,
                             "Longitude"_a      = jeans_struct.Longitude,
                             "Latitude"_a       = jeans_struct.Latitude,
                             "Name"_a           = jeans_struct.Name,
                             "AnalysisEps"_a    = jeans_struct.AnalysisEps,
                             "IsFreePar"_a      = IsFreePar,
                             "IsLogVal"_a       = IsLogVal,
                             "RangeLo"_a        = RangeLo,
                             "RangeUp"_a        = RangeUp,
                             "ParNames"_a       = ParNames,
                             "ParPrintOrder"_a  = ParPrintOrder,
                             "ParJeans"_a       = ParJeans);
    return jeans_config_dict;
}

// Makros to help cast python dictionaries into the jeans-structs; needs a vector<string> required_data to function
#define cast_dict_to_struct_try_catch(Input, Output, Value_name, Value_string, Value_type, Value_type_string, method_string)    \
    try{                                                                                                                        \
        Output.Value_name = Input[Value_string].cast<Value_type>();                                                             \
    }                                                                                                                           \
    catch (py::cast_error&) {                                                                                                   \
        cout<<"\n====> ERROR: "<<method_string<<"() in pyClumpy.Jeans"<<endl;                                                   \
        cout<<"\n             Provided datatype for "<<Value_string<<" is not castable to "<<Value_type_string<<endl;           \
        cout<<"\n             => abort()\n\n";                                                                                  \
        abort();                                                                                                                \
    }                                                                                                                           \
    catch (...) {                                                                                                               \
        if(std::find(required_data.begin(), required_data.end(), Value_string) == required_data.end()){                         \
            cout<<"\n====> ERROR: "<<method_string<<"() in pyClumpy.Jeans"<<endl;                                               \
            cout<<"\n             "<<Value_string<<" is a required item for your attempted call"<<endl;                         \
            cout<<"\n             => abort()\n\n";                                                                              \
            abort();                                                                                                            \
        }                                                                                                                       \
    }                                                                                                                           \

#define cast_dict_to_struct_try_catch_array(Input, Output, Value_name, Value_string, Value_type, Value_type_string, total_number_of_elements, method_string)\
    try{                                                                                                                        \
        vector<Value_type> Values = py::cast<vector<Value_type>>(Input[Value_string]);                                          \
        for (int i = 0; i < total_number_of_elements; i++){                                                                     \
            Output.Value_name[i] = Values[i];                                                                                   \
        }                                                                                                                       \
    }                                                                                                                           \
    catch (py::cast_error&) {                                                                                                   \
        cout<<"\n====> ERROR: "<<method_string<<"() in pyClumpy.Jeans"<<endl;                                                   \
        cout<<"\n             Provided datatype for "<<Value_string<<" is not castable to "<<Value_type_string<<endl;           \
        cout<<"\n             => abort()\n\n";                                                                                  \
        abort();                                                                                                                \
    }                                                                                                                           \
    catch (...) {                                                                                                               \
        if(std::find(required_data.begin(), required_data.end(), Value_string) == required_data.end()){                         \
            cout<<"\n====> ERROR: "<<method_string<<"() in pyClumpy.Jeans"<<endl;                                               \
            cout<<"\n             "<<Value_string<<" is a required item for your attempted call"<<endl;                         \
            cout<<"\n             => abort()\n\n";                                                                              \
            abort();                                                                                                            \
        }                                                                                                                       \
    }                                                                                                                           \

// Function to cast a python dictionary into gStructJeansData
void cast_dict_to_jeans_data(gStructJeansData &jeans_data, py::dict &jeans_data_, vector<string> &required_data){
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, FileName, "FileName", string, "string", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, Type, "Type", int, "int", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, Dec, "Dec", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, Dec_center, "Dec_center", double, "double", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, RA, "RA", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, RA_center, "RA_center", double, "double", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, dist, "dist", double, "double", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, MembershipProb, "MembershipProb", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, R, "R", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, RErr, "RErr", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, Val, "Val", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, ValErr, "ValErr", vector<double>, "vector<double>", "cast_dict_to_jeans_data");
    cast_dict_to_struct_try_catch(jeans_data_, jeans_data, Vmean, "Vmean", double, "double", "cast_dict_to_jeans_data");
}

// Function to cast a python dictionary into gStructJeansAnalysis
void cast_dict_to_jeans_config(gStructJeansAnalysis &jeans_config, py::dict &jeans_config_, vector<string> &required_data){
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, Distance, "Distance", double, "double", "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, HaloSize, "HaloSize", double, "double", "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, Longitude, "Longitude", double, "double", "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, Latitude, "Latitude", double, "double", "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, Name, "Name", string, "string", "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch(jeans_config_, jeans_config, AnalysisEps, "AnalysisEps", double, "double", "cast_dict_to_jeans_config");

    cast_dict_to_struct_try_catch_array(jeans_config_, jeans_config, IsFreePar, "IsFreePar", bool, "bool[26]", jeans_config.ParNames.size(), "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch_array(jeans_config_, jeans_config, IsLogVal, "IsLogVal", int, "int[26]", jeans_config.ParNames.size(), "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch_array(jeans_config_, jeans_config, RangeLo, "RangeLo", double, "double[26]", jeans_config.ParNames.size(), "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch_array(jeans_config_, jeans_config, RangeUp, "RangeUp", double, "double[26]", jeans_config.ParNames.size(), "cast_dict_to_jeans_config");
    cast_dict_to_struct_try_catch_array(jeans_config_, jeans_config, ParJeans, "ParJeans", double, "double[26]", jeans_config.ParNames.size(), "cast_dict_to_jeans_config");
}

double py_log_likelihood_jeans(const py::array_t<double> free_par_jeans, py::dict jeans_data_, py::dict jeans_config_){
    // Load the given jeans_data_ dictionary into a local gStructJeansData

    //Create the gStructJeansData
    struct gStructJeansData jeans_data;

    //Create a vector that saves the required data (the code will only throw errors upon trying to load a value that isn't given, if the value is required as there are plenty of optional values for this calculation)
    vector<string> required_data;
    required_data.push_back("R");
    required_data.push_back("Type");
    required_data.push_back("Val");
    required_data.push_back("ValErr");
    try{
        if(string(py::str(jeans_data_["Type"])) == "2"){
            required_data.push_back("MembershipProb");
            required_data.push_back("Vmean");
        }
    }
    catch (...){
        cout<<"\n====> ERROR: py_log_likelihood_jeans() in pyClumpy.Jeans"<<endl;
        cout<<"\n             Type is a required item in your jeans_data for your attempted call"<<endl;
        cout<<"\n             => abort()\n\n";
        abort();
    }
    cast_dict_to_jeans_data(jeans_data, jeans_data_, required_data);

    //Create the gStructJeansAnalysis
    struct gStructJeansAnalysis jeans_struct;

    //Create a vector that saves the required configurations (the code will only throw errors upon trying to load a value that isn't given, if the value is required as there are plenty of optional values for this calculation)
    vector<string> required_config;
    required_config.push_back("IsFreePar");
    required_config.push_back("RangeLo");
    required_config.push_back("RangeUp");
    required_config.push_back("IsLogVal");
    required_config.push_back("ParJeans");
    cast_dict_to_jeans_config(jeans_struct, jeans_config_, required_config);



    //---Calculates Log-Likelihood for binned (either sigmap or sigmap2) or unbinned analysis
    // from par (relying on given jeans_struct configuration and jeans_data data).

    // Copy parameters proposed by the minimizer

    //Create array of approriate size
    double par_tmp[jeans_struct.ParNames.size()];
    int j = 0;

    //Grab the array of free parameters
    py::buffer_info buf = free_par_jeans.request();
    double * free_par_jeans_ = static_cast<double *>(buf.ptr);

    //Copy either the free parameter given or otherwise the parameter out of the gStructJeansAnalysis into the array
    for (int i = 0; i < jeans_struct.ParNames.size(); ++i) {
        if(jeans_struct.IsFreePar[i]){
            par_tmp[i] = free_par_jeans_[j];
            j++;
        }
        else
            par_tmp[i] = jeans_struct.ParJeans[i];

        // if proposed parameters are nan: skip
        if (std::isnan(par_tmp[i])) {
            cout << "--- skip nan values" << endl;
            return -1.e30;
        }
        // if proposed parameters are out of range: skip
        if (jeans_struct.IsFreePar[i] && (par_tmp[i] < jeans_struct.RangeLo[i] || par_tmp[i] > jeans_struct.RangeUp[i]))
            return -1.e30;
        // Unset log values if needed
        if (jeans_struct.IsLogVal[i])
            par_tmp[i] = pow(10., par_tmp[i]);
    }

    //Compute and return the log likelihood
    return log_likelihood_jeans(par_tmp, jeans_data);
}

double py_chi2(const py::array_t<double> free_par_jeans, py::dict jeans_data_, py::dict jeans_config_){
    //---Calculates Chi2 for binned (either sigmap or sigmap2) or unbinned analysis
    // from free_par_jeans and local jeans_struct configuration and jeans_data data.

    // Calculate the log_likelihood
    double log_likelihood = py_log_likelihood_jeans(free_par_jeans, jeans_data_, jeans_config_);

    // If parameters were invalid in some shape or form, skip
    if (log_likelihood < -.99999e30){
        return 1.e30;
    }
    // Otherwise return the computed Chi2
    else {
        return -2. * log_likelihood;
    }
}

double py_jeans_sigmap2(double R, py::array_t<double> par_jeans, bool is_use_kernel = true){
    // Grab the array of par_jeans and compute sigmap2 on it

    //Grabbing the array
    py::buffer_info buf = par_jeans.request();
    double * tmp_par_jeans = static_cast<double *>(buf.ptr);

    //Compute sigmap2
    return jeans_sigmap2(R, tmp_par_jeans, is_use_kernel);
}

double py_jeans_sigmap2(double R, py::dict &jeans_config_, bool is_use_kernel = true){
    // Grab the underlying array ParJeans and make sure it's copied before doing any operations on it

    //Grabbing the array
    py::array_t<double> par_jeans = py::cast<py::array_t<double>>(jeans_config_["ParJeans"]);
    py::buffer_info buf1 = par_jeans.request();
    double * tmp_par_jeans = static_cast<double *>(buf1.ptr);

    //Create a new vector to store the copied values in
    vector<double> tmp_par_jeans_;

    //Grabbing the array of what values are Log Values
    py::array_t<bool> is_log_val = py::cast<py::array_t<bool>>(jeans_config_["IsLogVal"]);
    py::buffer_info buf2 = is_log_val.request();
    bool * is_log_val_ = static_cast<bool *>(buf2.ptr);

    //Copying and unsetting Log Values
    for(int i = 0; i < par_jeans.shape(0); i++){
        if (is_log_val_[i])
        tmp_par_jeans_.emplace_back(pow(10., tmp_par_jeans[i]));
        else
        tmp_par_jeans_.emplace_back(tmp_par_jeans[i]);
    }

    //Put the copied values into a numpy array to call the overloaded function that does the actual calculation
    py::array_t<double> par_jeans_(*par_jeans.shape(), &tmp_par_jeans_[0]);

    return py_jeans_sigmap2(R, par_jeans_, is_use_kernel);
}

double py_beta_anisotropy(double const &r, py::array_t<double> par_ani){
    // Grab the array of par_ani and compute beta_anisotropy on it

    //Grabbing the array
    py::buffer_info buf = par_ani.request();
    double * tmp_par_ani = static_cast<double *>(buf.ptr);

    //Computing beta_anisotropy
    return beta_anisotropy(r, tmp_par_ani);
}

double py_light_i(double R, py::array_t<double> par_light){
    // Grab the array of par_light and compute beta_anisotropy on it

    //Grabbing the array
    py::buffer_info buf = par_light.request();
    double * tmp_par_light = static_cast<double *>(buf.ptr);

    //Computing beta_anisotropy
    return beta_anisotropy(R, tmp_par_light);
}


#endif