// PYBIND INCLUDES
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/pytypes.h>
#include <pybind11/stl.h>
namespace py = pybind11;
using namespace py::literals;

// C INCLUDES
#include <string>
#include <vector>

// INTERFACE INCLUDES
#include "pywrapper_clumpy.cc"
#include "pywrapper_clumpy_jeans.cc"

// PYBIND MODULE: REGISTER EVERYTHING HERE
PYBIND11_MODULE(_core, m){
// MAIN

    //Submodule for the -g mode of clumpy executable
    py::module_ g = m.def_submodule("galactic_halo", "Module to run for a Galactic halo + list of haloes (optional)");
        g.def("mass_vs_radius"
            , &g0
            , py::arg("input_file_path")
            , "Calculates M_Gal(r) in one dimension.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mass_vs_radius().");
        g.def("massdensity_vs_radius"
            , &g1
            , py::arg("input_file_path")
            , "Calculates rho_Gal(r) (smooth, sub-continuum and total) of a spherically symmetric halo in one dimension.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_massdensity_vs_radius().");
        g.def("mean_vs_integrationangle"
            , &g2
            , py::arg("input_file_path")
            , "Calculates J(containment_angle; l, b) towards a user-defined direction (l, b) in the Galactic halo. Note that a triaxial halo can be studied as well.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_integrationangle().");
        g.def("mean_vs_angulardist"
            , &g3
            , py::arg("input_file_path")
            , "Calculates dJ/dTheta(theta) as a function of the distance the from the centre of a spherically symmetric Galactic halo.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_angulardist().");
        g.def("mean_list_vs_angulardist"
            , &g4
            , py::arg("input_file_path")
            , "Calculates the J(theta) and optionally, the corresponding fluxes, for a fixed, user-defined integration angle containment_angle as a function of the distance theta from the Galactic centre. Any J-factors from haloes defined in the file gLIST_HALOES are overplotted as well.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_list_vs_angulardist().");
        g.def("skymap_mean"
            , &g5
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a smooth distribution.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean().");
        g.def("skymap_mean_list"
            , &g6
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a smooth distribution with additionally drawing from individually resolved subhaloes from a list defined in gLIST_HALOES.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean_list().");
        g.def("skymap_mean_drawn"
            , &g7
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a distribution by drawing from individual randomly drawn resolved Galactic subhaloes from a list defined in gLIST_HALOES into the skymap.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean_drawn().");
        g.def("skymap_mean_drawn_list"
            , &g8
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a distribution by drawing from individual randomly drawn resolved Galactic subhaloes from a list defined in gLIST_HALOES into the skymap with additional user-defined subhaloes from a list defined in gLIST_HALOES.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean_drawn_list().");

        g.def("generateparams_mass_vs_radius"
            , &g0D
            , "Creates a sample parameter file for mass_vs_radius().");
        g.def("generateparams_massdensity_vs_radius"
            , &g1D
            , "Creates a sample parameter file for massdensity_vs_radius().");
        g.def("generateparams_mean_vs_integrationangle"
            , &g2D
            , "Creates a sample parameter file for mean_vs_integrationangle().");
        g.def("generateparams_mean_vs_angulardist"
            , &g3D
            , "Creates a sample parameter file for mean_vs_angulardist().");
        g.def("generateparams_mean_list_vs_angulardist"
            , &g4D
            , "Creates a sample parameter file for mean_list_vs_angulardist().");
        g.def("generateparams_skymap_mean"
            , &g5D
            , "Creates a sample parameter file for skymap_mean().");
        g.def("generateparams_skymap_mean_list"
            , &g6D
            , "Creates a sample parameter file for skymap_mean_list().");
        g.def("generateparams_skymap_mean_drawn"
            , &g7D
            , "Creates a sample parameter file for skymap_mean_drawn().");
        g.def("generateparams_skymap_mean_drawn_list"
            , &g8D
            , "Creates a sample parameter file for skymap_mean_drawn_list().");

        //Deprecated calls for -g
        g.def("g0"
            , &g0
            , "Alias for mass_vs_radius()");
        g.def("g1"
            , &g1
            , "Alias for massdensity_vs_radius()");
        g.def("g2"
            , &g2
            , "Alias for mean_vs_integrationangle()");
        g.def("g3"
            , &g3
            , "Alias for mean_vs_angulardist()");
        g.def("g4"
            , &g4
            , "Alias for mean_list_vs_angulardist()");
        g.def("g5"
            , &g5
            , "Alias for skymap_mean");
        g.def("g6"
            , &g6
            , "Alias for skymap_mean_list");
        g.def("g7"
            , &g7
            , "Alias for skymap_mean_drawn()");
        g.def("g8"
            , &g8
            , "Alias for skymap_mean_drawn_list()");

        g.def("g0D"
            , &g0D
            , "Alias for generateparams_mass_vs_radius()");
        g.def("g1D"
            , &g1D
            , "Alias for generateparams_massdensity_vs_radius()");
        g.def("g2D"
            , &g2D
            , "Alias for generateparams_mean_vs_integrationangle()");
        g.def("g3D"
            , &g3D
            , "Alias for generateparams_mean_vs_angulardist()");
        g.def("g4D"
            , &g4D
            , "Alias for generateparams_mean_list_vs_angulardist()");
        g.def("g5D"
            , &g5D
            , "Alias for generateparams_skymap_mean()");
        g.def("g6D"
            , &g6D
            , "Alias for generateparams_skymap_mean_list()");
        g.def("g7D"
            , &g7D
            , "Alias for generateparams_skymap_mean_drawn()");
        g.def("g8D"
            , &g8D
            , "Alias for skymap_mean_drawn_list()");

    //Submodule for the -h mode of the main-function
    py::module_ h = m.def_submodule("halo_list", "Module to run on (a list of) non-Galactic haloes.");
        h.def("mass_vs_radius"
            , &h0
            , py::arg("input_file_path")
            , "Calculates M_Gal(r) in one dimension.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mass_vs_radius().");
        h.def("massdensity_vs_radius"
            , &h1
            , py::arg("input_file_path")
            , "Calculates rho_Gal(r) (smooth, sub-continuum and total) of a spherically symmetric halo in one dimension.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_massdensity_vs_radius().");
        h.def("mean_vs_integrationangle"
            , &h2
            , py::arg("input_file_path")
            , "Calculates J(containment_angle; l, b) towards a user-defined direction (l, b) in the Galactic halo. Note that a triaxial halo can be studied as well.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_integrationangle().");
        h.def("mean_vs_angulardist"
            , &h3
            , py::arg("input_file_path")
            , "Calculates dJ/dTheta(theta) as a function of the distance the from the centre of a spherically symmetric Galactic halo.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_angulardist().");
        h.def("skymap_mean"
            , &h4
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a smooth distribution.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean().");
        h.def("skymap_mean_drawn"
            , &h5
            , py::arg("input_file_path")
            , "Creates a fits-file containing the J flux and dJ/dTheta per pixel of a distribution by drawing from individual randomly drawn resolved Galactic subhaloes from a list defined in gLIST_HALOES into the skymap.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_skymap_mean_drawn().");
        h.def("containment_angle"
            , &h6
            , "Calculates the integration angle containment_angle = alpha_f for which J_f = f × J_max.\n\nParameters:\n\ninput_file_path\n    The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_containment_angle().");
        h.def("distance_wherepointlike_ok"
            , &h7
            , "Test the distance d for which J_point-like is roughly equal J_full.\n\nParameters:\n\ninput_file_path\n    The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_distance_wherepointlike_ok().");
        h.def("tracer_veldispersion_vs_projradius"
            , &h8
            , "Calculate sigma_p from a file.\n\nParameters:\n\ninput_file_path\n    The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_veldispersion_vs_projradius().");
        h.def("tracer_surfbrightness_vs_projradius"
            , &h9
            , "Calculate I(R) from a file.\n\nParameters:\n\ninput_file_path\n    The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_surfbrightness_vs_projradius().");
        h.def("tracer_velanisotropy_vs_radius"
            , &h10
            , "Calculate beta_ani from a file.\n\nParameters:\n\ninput_file_path\n    The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_velanisotropy_vs_radius().");

        h.def("generateparams_mass_vs_radius"
            , &h0D
            , "Creates a sample parameter file for mass_vs_radius().");
        h.def("generateparams_massdensity_vs_radius"
            , &h1D
            , "Creates a sample parameter file for massdensity_vs_radius().");
        h.def("generateparams_mean_vs_integrationangle"
            , &h2D
            , "Creates a sample parameter file for mean_vs_integrationangle().");
        h.def("generateparams_mean_vs_angulardist"
            , &h3D
            , "Creates a sample parameter file for mean_vs_angulardist().");
        h.def("generateparams_skymap_mean"
            , &h4D
            , "Creates a sample parameter file for skymap_mean().");
        h.def("generateparams_skymap_mean_drawn"
            , &h5D
            , "Creates a sample parameter file for skymap_mean_drawn().");
        h.def("generateparams_containment_angle"
            , &h6D
            , "Creates a sample parameter file for containment_angle().");
        h.def("generateparams_distance_wherepointlike_ok"
            , &h7D
            , "Creates a sample parameter file for distance_wherepointlike_ok().");
        h.def("generateparams_tracer_veldispersion_vs_projradius"
            , &h8D
            , "Creates a sample parameter file for tracer_veldispersion_vs_projradius().");
        h.def("generateparams_tracer_surfbrightness_vs_projradius"
            , &h9D
            , "Creates a sample parameter file for tracer_surfbrightness_vs_projradius().");
        h.def("generateparams_tracer_velanisotropy_vs_radius"
            , &h10D
            , "Creates a sample parameter file for tracer_velanisotropy_vs_radius().");

        //Deprecated calls for -h
        h.def("h0"
            , &h0
            , "Alias for mass_vs_radius()");
        h.def("h1"
            , &h1
            , "Alias for massdensity_vs_radius()");
        h.def("h2"
            , &h2
            , "Alias for mean_vs_integrationangle()");
        h.def("h3"
            , &h3
            , "Alias for mean_vs_angulardist()");
        h.def("h4"
            , &h4
            , "Alias for skymap_mean()");
        h.def("h5"
            , &h5
            , "Alias for skymap_mean_drawn()");
        h.def("h6"
            , &h6
            , "Alias for containment_angle()");
        h.def("h7"
            , &h7
            , "Alias for distance_wherepointlike_ok()");
        h.def("h8"
            , &h8
            , "Alias for tracer_veldispersion_vs_projradius()");
        h.def("h9"
            , &h9
            , "Alias for tracer_surfbrightness_vs_projradius()");
        h.def("h10"
            , &h10
            , "Alias for tracer_velanisotropy_vs_radius()");

        h.def("h0D"
            ,  &h0D
            , "Alias for generateparams_mass_vs_radius()");
        h.def("h1D"
            ,  &h1D
            , "Alias for generateparams_massdensity_vs_radius()");
        h.def("h2D"
            ,  &h2D
            , "Alias for generateparams_mean_vs_integrationangle()");
        h.def("h3D"
            ,  &h3D
            , "Alias for generateparams_mean_vs_angulardist()");
        h.def("h4D"
            ,  &h4D
            , "Alias for generateparams_skymap_mean()");
        h.def("h5D"
            ,  &h5D
            , "Alias for generateparams_skymap_mean_drawn()");
        h.def("h6D"
            ,  &h6D
            , "Alias for generateparams_containment_angle()");
        h.def("h7D"
            ,  &h7D
            , "Alias for generateparams_distance_wherepointlike_ok()");
        h.def("h8D"
            ,  &h8D
            , "Alias for generateparams_tracer_veldispersion_vs_projradius()");
        h.def("h9D"
            ,  &h9D
            , "Alias for generateparams_tracer_surfbrightness_vs_projradius()");
        h.def("h10D"
            , &h10D
            , "Alias for tracer_velanisotropy_vs_radius()");


    //Submodule for the -e mode of clumpy executable
    py::module_ e = m.def_submodule("extragalactic", "Module to run on extragalactic quantities and diffuse extragalactic tracer_surfbrightness_vs_projradius");
        e.def("cosmo_vs_z"
            , &e0
            , py::arg("input_file_path")
            , "Computes the cosmological comoving, angular diameter, and luminosity distances, as well as the Omega and Hubble parameters at z > 0.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_cosmo_vs_z().");
        e.def("hmf_vs_Mz"
            , &e1
            , py::arg("input_file_path")
            , "Computes the halo massfunction.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_hmf_vs_Mz().");
        e.def("c_luminosity_and_boost_vs_M"
            , &e2
            , py::arg("input_file_path")
            , "Computes c_Delta(M_Delta, z), the luminosity L(M_Delta, z) and the boost L_halo-substructre/L_no-substructre over a user-defined mass range of extragalactic halos.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_c_luminosity_and_boost_vs_M().");
        e.def("intensity_multiplier"
            , &e3
            , py::arg("input_file_path")
            , "Computes the intensity multiplier comparing halo computation with substructre against no substructre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_intensity_multiplier().");
        e.def("ebl_absorption_vs_Ez"
            , &e4
            , py::arg("input_file_path")
            , "Computes the gamma-ray EBL absorption exponent, tau(z, E_gamma), and exp(-tau(z, E_gamma)) by a polynomial fit in energy dimension and log-linear interpolation in redshift z to tabulated values from the literature.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_ebl_absorption_vs_Ez().");
        e.def("mean_intensity_in_Mz_ranges"
            , &e5
            , py::arg("input_file_path")
            , "Computes the differential contribution to the gamma-ray or nu intensity from extragalactic DM from different redshift and mass shells, dI/dz(z; Delta M, E_gamma/E_nu), and dI/dM(M; Delta z, E_gamma/E_nu).\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_intensity_in_Mz_ranges().");
        e.def("mean_intensity"
            , &e6
            , py::arg("input_file_path")
            , "Computes the diffuse gamma-ray or nu intensity from extragalactic DM.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_intensity().");

        e.def("generateparams_cosmo_vs_z"
            , &e0D
            , "Creates a sample parameter file for cosmo_vs_z().");
        e.def("generateparams_hmf_vs_Mz"
            , &e1D
            , "Creates a sample parameter file for hmf_vs_Mz().");
        e.def("generateparams_c_luminosity_and_boost_vs_M"
            , &e2D
            , "Creates a sample parameter file for c_luminosity_and_boost_vs_M().");
        e.def("generateparams_intensity_multiplier"
            , &e3D
            , "Creates a sample parameter file for intensity_multiplier().");
        e.def("generateparams_ebl_absorption_vs_Ez"
            , &e4D
            , "Creates a sample parameter file for ebl_absorption_vs_Ez().");
        e.def("generateparams_mean_intensity_in_Mz_ranges"
            , &e5D
            , "Creates a sample parameter file for mean_intensity_in_Mz_ranges().");
        e.def("generateparams_mean_intensity"
            , &e6D
            , "Creates a sample parameter file for mean_intensity().");

        //Deprecated calls for -e
        e.def("e0"
            , &e0
            , "Alias for cosmo_vs_z()");
        e.def("e1"
            , &e1
            , "Alias for hmf_vs_Mz()");
        e.def("e2"
            , &e2
            , "Alias for c_luminosity_and_boost_vs_M()");
        e.def("e3"
            , &e3
            , "Alias for intensity_multiplier()");
        e.def("e4"
            , &e4
            , "Alias for ebl_absorption_vs_Ez()");
        e.def("e5"
            , &e5
            , "Alias for mean_intensity_in_Mz_ranges()");
        e.def("e6"
            , &e6
            , "Alias for mean_intensity()");

        e.def("e0D"
            , &e0D
            , "Alias for generateparams_cosmo_vs_z()");
        e.def("e1D"
            , &e1D
            , "Alias for generateparams_hmf_vs_Mz()");
        e.def("e2D"
            , &e2D
            , "Alias for generateparams_c_luminosity_and_boost_vs_M()");
        e.def("e3D"
            , &e3D
            , "Alias for generateparams_intensity_multiplier()");
        e.def("e4D"
            , &e4D
            , "Alias for generateparams_ebl_absorption_vs_Ez()");
        e.def("e5D"
            , &e5D
            , "Alias for generateparams_mean_intensity_in_Mz_ranges()");
        e.def("e6D"
            , &e6D
            , "Alias for generateparams_mean_intensity()");

    //Submodule for the -s mode of clumpy executable
    py::module_ s = m.def_submodule("statistical", "Module to run on a file containing a list of likely halo parameters (obtained from a statistical analysis). The statistics mode (s) deals with the results of a Jeans + MCMC analysis run on a set of dSph kinematic data. It allows the reconstruction of parameter correlations and confidence intervals of any quantities.");
        s.def("mass_vs_radius"
            , &s0
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed DM halo mass as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mass_vs_radius().");
        s.def("loglikelihood_or_chi2"
            , &s1
            , py::arg("input_file_path")
            , "Calculates the log10 of the input parameters from the file given the data.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_loglikelihood_or_chi2().");
        s.def("corner_plots"
            , &s2
            , py::arg("input_file_path")
            , "Triangle plot showing correlations and marginalised posterior distributions for the mass and DM profile parameters.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_corner_plots().");
        s.def("bestfit_params"
            , &s3
            , py::arg("input_file_path")
            , "Best-fit parameters (best chi2 or log likelihood) from a list of stat. analysis files.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_bestfit_params().");
        s.def("CI_params"
            , &s4
            , py::arg("input_file_path")
            , "CL lower and upper value (first and second line below) from a list of stat. analysis files.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_CI_params().");
        s.def("massdensity_vs_radius"
            , &s5
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed DM halo density as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_massdensity_vs_radius().");
        s.def("mean_vs_integrationangle"
            , &s6
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed J-factors (or D-factors depending on the chosen option) as a function of the integration angle alpha.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_integrationangle().");
        s.def("mean_vs_angulardist"
            , &s7
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed J-factors (or D-factors depending on the chosen option) for a fixed integration angle alpha as a function of theta.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_mean_vs_angulardist().");
        s.def("tracer_veldispersion_vs_projradius"
            , &s8
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed projected velocity-dispersion sigma_p as a function of the distance to the halo centre. This is compared to the kinematic data points used to run the Jeans and MCMC analysis.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_veldispersion_vs_projradius().");
        s.def("tracer_veldispersionsquare_vs_projradius"
            , &s9
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed velocity-squared as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_veldispersionsquare_vs_projradius().");
        s.def("tracer_velanisotropy_vs_radius"
            , &s10
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed anisotropy beta as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_velanisotropy_vs_radius().");
        s.def("tracer_numdensity_vs_radius"
            , &s11
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed luminosity-density nu as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_numdensity_vs_radius().");
        s.def("tracer_surfbrightness_vs_projradius"
            , &s12
            , py::arg("input_file_path")
            , "Median values, 68\% and 95\% confidence intervals of the reconstructed intensity I as a function of the distance to the halo centre.\n\nParameters:\n\ninput_file_path\n   The path to the CLUMPY parameter file. A sample-file may be generated with generateparams_tracer_surfbrightness_vs_projradius().");

        s.def("generateparams_mass_vs_radius"
            , &s0D
            , "Creates a sample parameter file for mass_vs_radius().");
        s.def("generateparams_loglikelihood_or_chi2"
            , &s1D
            , "Creates a sample parameter file for loglikelihood_or_chi2().");
        s.def("generateparams_corner_plots"
            , &s2D
            , "Creates a sample parameter file for corner_plots().");
        s.def("generateparams_bestfit_params"
            , &s3D
            , "Creates a sample parameter file for bestfit_params().");
        s.def("generateparams_CI_params"
            , &s4D
            , "Creates a sample parameter file for CI_params().");
        s.def("generateparams_massdensity_vs_radius"
            , &s5D
            , "Creates a sample parameter file for massdensity_vs_radius().");
        s.def("generateparams_mean_vs_integrationangle"
            , &s6D
            , "Creates a sample parameter file for mean_vs_integrationangle().");
        s.def("generateparams_mean_vs_angulardist"
            , &s7D
            , "Creates a sample parameter file for mean_vs_angulardist().");
        s.def("generateparams_tracer_veldispersion_vs_projradius"
            , &s8D
            , "Creates a sample parameter file for tracer_veldispersion_vs_projradius().");
        s.def("generateparams_tracer_veldispersionsquare_vs_projradius"
            , &s9D
            , "Creates a sample parameter file for tracer_veldispersionsquare_vs_projradius().");
        s.def("generateparams_tracer_velanisotropy_vs_radius"
            , &s10D
            , "Creates a sample parameter file for tracer_velanisotropy_vs_radius().");
        s.def("generateparams_tracer_numdensity_vs_radius"
            , &s11D
            , "Creates a sample parameter file for tracer_numdensity_vs_radius().");
        s.def("generateparams_tracer_surfbrightness_vs_projradius"
            , &s12D
            , "Creates a sample parameter file for tracer_surfbrightness_vs_projradius().");

        //Deprecated calls for -s
        s.def("s0"
            , &s0
            , "Alias for mass_vs_radius()");
        s.def("s1"
            , &s1
            , "Alias for loglikelihood_or_chi2()");
        s.def("s2"
            , &s2
            , "Alias for corner_plots()");
        s.def("s3"
            , &s3
            , "Alias for bestfit_params()");
        s.def("s4"
            , &s4
            , "Alias for CI_params()");
        s.def("s5"
            , &s5
            , "Alias for massdensity_vs_radius()");
        s.def("s6"
            , &s6
            , "Alias for mean_vs_integrationangle()");
        s.def("s7"
            , &s7
            , "Alias for mean_vs_angulardist()");
        s.def("s8"
            , &s8
            , "Alias for tracer_veldispersion_vs_projradius()");
        s.def("s9"
            , &s9
            , "Alias for tracer_veldispersionsquare_vs_projradius()");
        s.def("s10"
            , &s10
            , "Alias for tracer_velanisotropy_vs_radius()");
        s.def("s11"
            , &s11
            , "Alias for tracer_numdensity_vs_radius()");
        s.def("s12"
            , &s12
            , "Alias for tracer_surfbrightness_vs_projradius()");

        s.def("s0D"
            , &s0D
            , "Alias for generateparams_mass_vs_radius()");
        s.def("s1D"
            , &s1D
            , "Alias for generateparams_loglikelihood_or_chi2()");
        s.def("s2D"
            , &s2D
            , "Alias for generateparams_corner_plots()");
        s.def("s3D"
            , &s3D
            , "Alias for generateparams_bestfit_params()");
        s.def("s4D"
            , &s4D
            , "Alias for generateparams_CI_params()");
        s.def("s5D"
            , &s5D
            , "Alias for generateparams_massdensity_vs_radius()");
        s.def("s6D"
            , &s6D
            , "Alias for generateparams_mean_vs_integrationangle()");
        s.def("s7D"
            , &s7D
            , "Alias for generateparams_mean_vs_angulardist()");
        s.def("s8D"
            , &s8D
            , "Alias for generateparams_tracer_veldispersion_vs_projradius()");
        s.def("s9D"
            , &s9D
            , "Alias for generateparams_tracer_veldispersionsquare_vs_projradius()");
        s.def("s10D"
            , &s10D
            , "Alias for generateparams_tracer_velanisotropy_vs_radius()");
        s.def("s11D"
            , &s11D
            , "Alias for generateparams_tracer_numdensity_vs_radius()");
        s.def("s12D"
            , &s12D
            , "Alias for generateparams_tracer_surfbrightness_vs_projradius()");

    //Submodule for the -o mode of clumpy executable
    py::module_ o = m.def_submodule("util_skymaps", "Module to export 2D maps in ASCII and full-sky FITS maps.");
        o.def("content_of_skymap"
            , &o0
            , "writes an overview of what is stored in skymap.fits file and which extensions (ext) and columns (map) can be selected.");
        o.def("export_extensions"
            , &o1
            , "writes the specified extension of skymap.fits into an ASCII file (columns separated by blanks). The conversion preserves all information and provides besides the unique HEALPix pixel number also the coordinates (psi, theta) of the pixel centre.");
        o.def("export_inputpars"
            , &o2
            , "extracts the header keywords from the first extension in the file skymap.fits and saves a CLUMPY parameter file which can be used to reproduce the content of the chosen FITS extension.");
        o.def("remap_to_fullsky"
            , &o3
            , "extracts the second column from the first extension in the file skymap.fits and stores it in a new FITS file as fullsky map with implicit pixel labelling. If skymap.fits contains part-sky maps, pixels outside the field of view are filled with HEALPix’ blind value, -1.6375 * 10^30.");

        o.def("generate_content_of_skymap"
            , &o0D
            , "Creates a sample file for content_of_skymap()");
        o.def("generateparams_export_extensions"
            , &o1D
            , "Creates a sample file for export_extensions()");
        o.def("generateparams_export_inputpars"
            , &o2D
            , "Creates a sample file for export_inputpars()");
        o.def("generateparams_remap_to_fullsky"
            , &o3D
            , "Creates a sample file for remap_to_fullsky()");
        //Deprecated calls for -o
        o.def("o0"
            , &o0
            , "Alias for export_extensions()");
        o.def("o1"
            , &o1
            , "Alias for export_extensions()");
        o.def("o2"
            , &o2
            , "Alias for export_inputpars()");
        o.def("o3"
            , &o3
            , "Alias for remap_to_fullsky()");
        //Deprecated calls for -o
        o.def("o0D"
            , &o0D
            , "Alias for generateparams_export_extensions()");
        o.def("o1D"
            , &o1D
            , "Alias for generateparams_export_extensions()");
        o.def("o2D"
            , &o2D
            , "Alias for generateparams_export_inputpars()");
        o.def("o3D"
            , &o3D
            , "Alias for generateparams_remap_to_fullsky()");

    //Submodule for the -z mode of clumpy executable
    py::module_ z = m.def_submodule("spectrum", "Module to compute gamma-ray or neutrino spectra at source");
        z.def("src_spectra_vs_E", &z0);

        z.def("generateparams_src_spectra_vs_E", &z0D);

        //Deprecated calls for -z
        z.def("z0"
            , &z0
            , "Alias for src_spectra_vs_E()");

        z.def("z0D"
            , &z0D
            , "Alias for generateparams_src_spectra_vs_E()");

// JEANS
    py::module_ jeans = m.def_submodule("Jeans", "Methods needed to run a Jeans-Analysis through Clumpy");
    jeans.def("load_data"   //Name of the function in python
            , &py_load_data //Function from C to be bound
            , py::arg("filename") //Name the arguments for python
            , "Load data provided in the file according to CLUMPY standards.\n\nParameters:\n\nfilename:\n    a string describing the path to the file from which to load the data.\n\nReturns:\n\njeans_data:\n    a dictionary containing all data relevant to a jeans analysis through CLUMPY in CLUMPY standard." //Python doc-string
            );
    jeans.def("load_config"   //Name of the function in python
            , &py_load_config //Function from C to be bound
            , py::arg("filename"), py::arg("eps") = 0.05 //Name the arguments for python and give eps a default value
            , "Load the config provided in the file according to CLUMPY standards.\n\nParameters:\n\nfilename:\n    a string describing the path to the file from which to load the config.\neps:\n    The analysis eps (default 0.05)\n\nReturns:\n\njeans_config:\n    a dictionary containing all configuration information relevant to a jeans analysis through CLUMPY in CLUMPY standard." //Python doc-string
            );
    jeans.def("log_likelihood"   //Name of the function in python
            , &py_log_likelihood_jeans //Function from C to be bound
            , py::arg("free_par_jeans"), py::arg("jeans_data"), py::arg("jeans_config") //Name the arguments for python
            , "Compute the log likelihood of jeans parameters given a dataset through jeans_data.\n\nParameters:\n\nfree_par_jeans:\n    numpy array containing the values of the free parameters of the jeans parameters\njeans_data:\n    a dictionary containing the data the parameters are to be evaluated to. Must be according to CLUMPY standards.\n\nReturns:\n\nlog_likelihood:\n    the log10 of the likelihood of the given configuration and free parameters" //Python doc-string
            );
    jeans.def("chi2"   //Name of the function in python
            , &py_chi2 //Function from C to be bound
            , py::arg("free_par_jeans"), py::arg("jeans_data"), py::arg("jeans_config") //Name the arguments for python
            , "Compute the chi squared of jeans parameters given a dataset through jeans_data.\n\nParameters:\n\nfree_par_jeans:\n    numpy array containing the values of the free parameters of the jeans parameters\njeans_data:\n    a dictionary containing the data the parameters are to be evaluated to. Must be according to CLUMPY standards.\n\nReturns:\n\nchi2:\n    the chi squared of the given configuration and free parameters" //Python doc-string
            );
    jeans.def("sigmap2"   //Name of the function in python
            , py::vectorize(py::overload_cast<double, py::array_t<double>, bool>(&py_jeans_sigmap2)) //Function from C to be bound with resolved overload; vectorized so it can be called with a numpy array in R (and is_use_kernel) and return a numpy array instead of a single value
            , py::arg("R"), py::arg("par_jeans"), py::arg("is_use_kernel") = true //Name the arguments for python
            , "Compute sigma_p squared with given jeans parameters and R.\n\nParameters:\n\nR:\n    float or ndarry(float) of Radius where sigma_p squared should be computed at.\npar_jeans:\n    Jeans parameters for which to compute sigma_p squared. Must be given as is without any logarithmic values.\nis_use_kernel:\n    Whether to use kernel integration; will use numerical integration, if kernel integration is unavailable or set to False. (default True)\n\nReturns:\n\nsigmap2:\n    float or ndarray(float) of sigma_p squared values given the Jeans parameters and R values." //Python doc-string
            );
    jeans.def("sigmap2"   //Name of the function in python
            , py::vectorize(py::overload_cast<double, py::dict&, bool>(&py_jeans_sigmap2))//Function from C to be bound with resolved overload; vectorized so it can be called with a numpy array in R (and is_use_kernel) and return a numpy array instead of a single value
            , py::arg("R"), py::arg("jeans_config"), py::arg("is_use_kernel") = true //Name the arguments for python
            , "Compute sigma_p squared with given jeans parameters and R.\n\nParameters:\n\nR:\n    float or ndarry(float) of Radius where sigma_p squared should be computed at.\npar_jeans:\n    Jeans configuration including all relevant information. Will automatically unset logarithmic values as long as properly defined in IsLogVal.\nis_use_kernel:\n    Whether to use kernel integration; will use numerical integration, if kernel integration is unavailable or set to False. (default True)\n\nReturns:\n\nsigmap2:\n    float or ndarray(float) of sigma_p squared values given the Jeans parameters and R values." //Python doc-string
            );
    jeans.def("beta_anisotropy"
            ,  py::vectorize(&py_beta_anisotropy)
            ,  py::arg("R"), py::arg("par_anisotropy")
            ,  "Returns the anisotropy profile (no unit) at a radius r [kpc] for any anisotropy profile available in gENUM_ANISOTROPYPROFILE (see params.h). Note that physical values for the anisotropy are smaller than 1.\n\nParameters:\n\nr:\n    float or ndarray(float) of radius[kpc] where beta_anisotropy is to be computed at.\npar_anisotropy[0]:\n    Anisotropy at r=0\npar_anisotropy[1]:\n    Anisotropy at r=+infinity\npar_anisotropy[2]:\n    Anisotropy scale radius ra [kpc]\npar_anisotropy[3]:\n    Anisotropy shape parameter eta\npar_anisotropy[4]:\n    Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (kCONSTANT, kBAES, kOSIPKOV; see params.h)\n\nReturns:\n\nbeta_anisotropy:\n    float or ndarray(float) of beta_anisotropy values given the anisotropy parameters and r values.");
    jeans.def("light_i"
            ,  py::vectorize(&py_light_i)
            ,  py::arg("R"), py::arg("par_light")
            ,  "Returns at projected radius R [kpc] the projected (2D) surface density I(R) [Lsol].\n\nParameters:\n\nR:\n    float or ndarray(float) of Projected radius [kpc].\npar_light[0]:\n    Light profile 1st free par (usually normalisation)\npar_light[1]:\n    Light profile 2nd free par (usually scale radius [kpc])\npar_light[2]:\n    Light profile 3rd free par (if used)\npar_light[3]:\n    Light profile 4th free par (if used)\npar_light[4]:\n    Light profile 5th free par (if used)\npar_light[5]:\n    Light card_profile [gENUM_LIGHTPROFILE] (kEXP2D, kEXP3D, kKING2D, kPLUMMER2D, kSERSIC2D, kZHAO3D; see params.h)\npar_light[6]:\n    If not analytical, maximum radius for integration [kpc]\npar_light[7]:\n    If not analytical, relative precision for the integration\n\nReturns:\n\nlight_intensity:\n    float or ndarray(float) of surface density I(R) [Lsol] values given the parameters and R values.");
}
