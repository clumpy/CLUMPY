#ifndef _PYCLUMPY_PYWRAPPER_CLUMPY_CC_
#define _PYCLUMPY_PYWRAPPER_CLUMPY_CC_

// PYBIND INCLUDES
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/pytypes.h>
#include <pybind11/stl.h>
namespace py = pybind11;
using namespace py::literals;

// C INCLUDES
#include <string>
#include <vector>

// CLUMPY INCLUDES
#include "../cli/clumpy.cc"
#include "../include/misc.h"
#include "../include/params.h"


//Parsing python kwargs into C-kwargs
void kwargs_parsing(py::kwargs &kwargs, std::vector<char*> &args_vec){
    bool print_only = true;
    bool display_only = false;
    //The standard for writing root-files should be set to false in python, but perfectly overridable in the parameter-files, so we override the global variable here
    gSIM_IS_WRITE_ROOTFILES = false;
    //Loop over all kwargs
    for (auto item: kwargs){
        //Grab the parameter-name and the value attached
        string param = py::cast<string>(item.first);
        string value = py::cast<string>(item.second);
        //parse the true and false correctly by writing it into its generic 1 and 0 (this might have to be expanded if there are any issues with values)
        if(value == "True") value = "1";
        else if(value == "False") value = "0";

        //Parse the non-generic kwargs
        if(param == "print_only"){
            print_only = (value == "1");
        }
        else if(param == "display_only"){
            display_only = (value == "1");
            if(display_only) print_only = false;
        }
        //Parse the generic kwargs
        else{
            string input = "-";
            input += param + "=" + value;

            char * c_input = new char[input.size() + 1];

            //Copy the string into a character array to be pushed into vector
            for(int i = 0; i < input.size(); i++){
                c_input[i] = input[i];
            }
            c_input[input.size()] = '\0';
            args_vec.push_back(c_input);
        }
    }

    if(print_only || display_only){
        string input = "-";
        if(print_only){
            input += "p";
        }
        else if(display_only){
            input += "d";
        }
        char * c_input = new char[input.size() + 1];

        //Copy the string into a character array to be pushed into vector
        for(int i = 0; i < input.size(); i++){
            c_input[i] = input[i];
        }
        c_input[input.size()] = '\0';
        args_vec.push_back(c_input);
    }
}

//General interface for one-file main-inputs
void sim_parser_1(char * file_path, string mode_string, py::kwargs &kwargs){
    //Create the vector of arguments to be passed
    std::vector<char*> args_vec;
    std::vector<char*>::iterator it;

    //Parse the python kwargs into the C-kwargs
    kwargs_parsing(kwargs, args_vec);

    //input the file
    char i_input[] = "-i";
    it = args_vec.begin();
    args_vec.insert(it, file_path);
    it = args_vec.begin();
    args_vec.insert(it, i_input);

    //input the simulation mode
    char clumpy_path[] = "clumpy";
    char sim_input[mode_string.size()+1];
    for(int i = 0; i < mode_string.size() + 1; i++){
        sim_input[i] = mode_string[i];
    }
    it = args_vec.begin();
    args_vec.insert(it, sim_input);
    it = args_vec.begin();
    args_vec.insert(it, clumpy_path);

    //call the main-method
    main(args_vec.size(), &args_vec[0]);

    //cleanup
    args_vec.clear();
}

//General interface for the parameter files
void sim_parser_3(string mode_string, py::kwargs &kwargs){
    //Create the vector of arguments to be passed
    std::vector<char*> args_vec;
    std::vector<char*>::iterator it;

    //Parse the python kwargs into the C-kwargs
    kwargs_parsing(kwargs, args_vec);

    //input the simulation mode
    char clumpy_path[] = "clumpy";
    char sim_input[mode_string.size()+1];
    for(int i = 0; i < mode_string.size() + 1; i++){
        sim_input[i] = mode_string[i];
    }
    it = args_vec.begin();
    args_vec.insert(it, sim_input);
    it = args_vec.begin();
    args_vec.insert(it, clumpy_path);

    //call the main-method
    main(args_vec.size(), &args_vec[0]);

    //cleanup
    args_vec.clear();
}


//Macros to quicken the definitions of all the generic simulation-mode interfaces
#define sim_mode(mode, mode_string)   void mode(char * file_path, py::kwargs &kwargs){                      \
                                        sim_parser_1(file_path, mode_string, kwargs);                       \
                                    }                                                                       \

#define param_mode(mode, mode_string) void mode(py::kwargs &kwargs){                                        \
                                        sim_parser_3(mode_string, kwargs);                                  \
                                    }                                                                       \

//Definition of the e-modes
sim_mode(e0, "-e0")          //Distances and Omegas
sim_mode(e1, "-e1")          //Mass function (M,z)
sim_mode(e2, "-e2")          //cdelta-mdelta, Lum, Boost
sim_mode(e3, "-e3")          //Intensity multiplier
sim_mode(e4, "-e4")          //EBL absorption tau
sim_mode(e5, "-e5")          //Mean intensity dI/dE(M,z;E)
sim_mode(e6, "-e6")          //Mean intensity dI/dE(E)

param_mode(e0D, "-e0D")      //Distances and Omegas
param_mode(e1D, "-e1D")      //Mass function (M,z)
param_mode(e2D, "-e2D")      //cdelta-mdelta, Lum, Boost
param_mode(e3D, "-e3D")      //Intensity multiplier
param_mode(e4D, "-e4D")      //EBL absorption tau
param_mode(e5D, "-e5D")      //Mean intensity dI/dE(M,z;E)
param_mode(e6D, "-e6D")      //Mean intensity dI/dE(E)

//Definition of the h-modes
sim_mode(h0, "-h0")          //M(r)
sim_mode(h1, "-h1")          //rho_sm+<sub>(r)
sim_mode(h2, "-h2")          //Jsm+<sub>(alpha_int)
sim_mode(h3, "-h3")          //Jsm+<sub>(theta)
sim_mode(h4, "-h4")          //2D-skymap Jsm+<sub>(halo)
sim_mode(h5, "-h5")          //2D-skymap Jsm+sub(halo)
sim_mode(h6, "-h6")          //alpha_int(f*Jtot)
sim_mode(h7, "-h7")          //dist(f*Jpointlike)
sim_mode(h8, "-h8")          //sigma_p(R)
sim_mode(h9, "-h9")          //I(R)
sim_mode(h10, "-h10")        //beta_ani(r)

param_mode(h0D, "-h0D")      //M(r)
param_mode(h1D, "-h1D")      //rho_sm+<sub>(r)
param_mode(h2D, "-h2D")      //Jsm+<sub>(alpha_int)
param_mode(h3D, "-h3D")      //Jsm+<sub>(theta)
param_mode(h4D, "-h4D")      //2D-skymap Jsm+<sub>(halo)
param_mode(h5D, "-h5D")      //2D-skymap Jsm+sub(halo)
param_mode(h6D, "-h6D")      //alpha_int(f*Jtot)
param_mode(h7D, "-h7D")      //dist(f*Jpointlike)
param_mode(h8D, "-h8D")      //sigma_p(R)
param_mode(h9D, "-h9D")      //I(R)
param_mode(h10D, "-h10D")    //beta_ani(r)

//Definition of the g-modes
sim_mode(g0, "-g0")          //M(r)
sim_mode(g1, "-g1")          //rho_sm+<sub>(r)
sim_mode(g2, "-g2")          //Jsm+<sub>(alpha_int)
sim_mode(g3, "-g3")          //Jsm+<sub>(theta)
sim_mode(g4, "-g4")          //Jsm+<sub>+list(theta)
sim_mode(g5, "-g5")          //2D-skymap Jsm+<sub>
sim_mode(g6, "-g6")          //2D-skymap Jsm+<sub>+list
sim_mode(g7, "-g7")          //2D-skymap Jsm+sub
sim_mode(g8, "-g8")          //2D-skymap Jsm+sub+list

param_mode(g0D, "-g0D")      //M(r)
param_mode(g1D, "-g1D")      //rho_sm+<sub>(r)
param_mode(g2D, "-g2D")      //Jsm+<sub>(alpha_int)
param_mode(g3D, "-g3D")      //Jsm+<sub>(theta)
param_mode(g4D, "-g4D")      //Jsm+<sub>+list(theta)
param_mode(g5D, "-g5D")      //2D-skymap Jsm+<sub>
param_mode(g6D, "-g6D")      //2D-skymap Jsm+<sub>+list
param_mode(g7D, "-g7D")      //2D-skymap Jsm+sub
param_mode(g8D, "-g8D")      //2D-skymap Jsm+sub+list

//Definition of the o-modes
sim_mode(o0, "-o0")          // Content of skymap FITS file
sim_mode(o1, "-o1")          // Export skymap FITS extension to ASCII
sim_mode(o2, "-o2")          // Retrieve params that generated FITS
sim_mode(o3, "-o3")          // Cutsky FITS map to whole-sky FITS map

param_mode(o0D, "-o0D")      // Content of skymap FITS file
param_mode(o1D, "-o1D")      // Export skymap FITS extension to ASCII
param_mode(o2D, "-o2D")      // Retrieve params that generated FITS
param_mode(o3D, "-o3D")      // Cutsky FITS map to whole-sky FITS map

//Definition of the s-modes
sim_mode(s0, "-s0")          //CLs M(r)
sim_mode(s1, "-s1")          //PDF logL_or_chi2
sim_mode(s2, "-s2")          //PDF correlations
sim_mode(s3, "-s3")          //Best fit params
sim_mode(s4, "-s4")          //CLs params
sim_mode(s5, "-s5")          //CLs rho(r)
sim_mode(s6, "-s6")          //CLs J(alphaint)
sim_mode(s7, "-s7")          //CLs J(theta)
sim_mode(s8, "-s8")          //CLs sigma_p(R)
sim_mode(s9, "-s9")          //CLs vr2(r)
sim_mode(s10, "-s10")        //CLs beta_ani(r)
sim_mode(s11, "-s11")        //CLs nu(r)
sim_mode(s12, "-s12")        //CLs I(R)

param_mode(s0D, "-s0D")      //CLs M(r)
param_mode(s1D, "-s1D")      //PDF logL_or_chi2
param_mode(s2D, "-s2D")      //PDF correlations
param_mode(s3D, "-s3D")      //Best fit params
param_mode(s4D, "-s4D")      //CLs params
param_mode(s5D, "-s5D")      //CLs rho(r)
param_mode(s6D, "-s6D")      //CLs J(alphaint)
param_mode(s7D, "-s7D")      //CLs J(theta)
param_mode(s8D, "-s8D")      //CLs sigma_p(R)
param_mode(s9D, "-s9D")      //CLs vr2(r)
param_mode(s10D, "-s10D")    //CLs beta_ani(r)
param_mode(s11D, "-s11D")    //CLs nu(r)
param_mode(s12D, "-s12D")    //CLs I(R)

//Definition of the z-modes
sim_mode(z0, "-z0")         //Flux or dPP/dE (jfactor<=0)

param_mode(z0D, "-z0D")       //Flux or dPP/dE (jfactor<=0)

#endif
