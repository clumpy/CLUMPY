.. _rst_licenses:

.. sectionauthor:: XX

.. role::  raw-html(raw)
    :format: html


Licenses
====================

:CLUMPY: The :program:`CLUMPY` library includes a modified version of the :program:`HEALPix v3.31` C++ libraries distributed under the GNU General Public License (`GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>`__). This makes :program:`CLUMPY` available under the same license --- Copyright © V. Bonnivard, A. Charbonnier, C. Combet, M. Hütten, D. Maurin and E. Nezri (2010-2018).

:CLUMPY logo: Copyright © M. Oppermann (2017), courtesy of the author's permission under the `CC BY-NC-ND <https://creativecommons.org/licenses/by-nc-nd/3.0/>`__ license.

:GSL: The GNU Scientific Library (`GSL <http://www.gnu.org/software/gsl>`__) is a numerical library for C and C++ programmers. It is free software under the GNU General Public License.

:CFITSIO:  Copyright (Unpublished-all rights reserved under the copyright laws of the United States), U.S. Government as represented by the Administrator of the National Aeronautics and Space Administration. No copyright is claimed in the United States under Title 17, U.S. Code. Permission to freely use, copy, modify, and distribute this software and its documentation without fee is hereby granted, provided that this copyright notice and disclaimer of warranty appears in all copies --- `CFITSIO <http://heasarc.gsfc.nasa.gov/fitsio/>`__ was developed by Dr. William D. Pence, `Astronomical Data Analysis Software and Systems VIII, ASP Conference Series, Vol. 172 (1999) <http://adsabs.harvard.edu/abs/1999ASPC..172..487P>`__.

:HEALPix: The `HEALPix (Hierarchical Equal Area isoLatitude Pixelisation) <https://healpix.jpl.nasa.gov/>`_ is distributed under the `GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>`__ --- Gorski et al., `ApJ 622, 759 (2005) <http://www.adsabs.harvard.edu/abs/2005ApJ...622..759G>`__.


:ROOT CERN: The `ROOT system <https://root.cern.ch/license>`__ is being made available under the `LGPLv3 <http://www.gnu.org/licenses/lgpl-3.0.en.html>`__, which allows :program:`ROOT` to be used in a wide range of open and closed environments.


:GreAT: The :program:`GreAT` library is available from `GitLab <https://gitlab.in2p3.fr/derome/GreAT>`__ --- Putze and Derome, `PDU 5, 29 (2014) <http://adsabs.harvard.edu/abs/2014PDU.....5...29P>`__.

:Doxygen: The `doxygen system <http://www.stack.nl/~dimitri/doxygen/>`__  is licensed under the GNU General Public License v2 --- Copyright ©  1997-2016 by Dimitri van Heesch.