.. _rst_enumerators_tidalcut:

.. role::  raw-html(raw)
    :format: html

Tidal cut models
--------------------------------------------------------------

Possible keyword choices for the :option:`gDM_FLAG_TIDALCUT` parameter:

.. csv-table::
   :file: tables/gENUM_TIDALCUT.csv_table
   :widths: 10, 70
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso:: :numref:`rst_doc_module_s`, :ref:`rst_doc_module_s`. Input parameter for the :math:`\tt -s0`, :math:`\tt -s2`, :math:`\tt -s4`, :math:`\tt -s5`, and :math:`\tt -s6` modules.