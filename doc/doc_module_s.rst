.. _rst_doc_module_s:

Statistical analysis: :math:`\tt -s`
--------------------------------------

Run on a file containing a list of likely halo parameters (obtained from a statistical analysis). The statistics mode :option:`-s` deals with the results of a Jeans + MCMC analysis run on a set of dSph kinematic data. It allows the reconstruction of parameter correlations and confidence intervals of any quantities.

.. note:: All default parameter files from the following run examples are generated with the :option:`D` flag.

   .. code-block:: console

    $ clumpy -sXD

   Examples below are constructed using the fake MCMC chain in the example file :file:`data/stat_example.dat`.

:math:`\tt -s0`: CLs on :math:`M(r)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Median values, 68% and 95% confidence intervals of the reconstructed DM halo mass as a function of the distance to the halo centre.

.. code-block:: console

    $ clumpy -s0 -i clumpy_params_s0.txt


.. figure:: DocImages/s0D.png
   :align: center
   :figclass: align-center

   :option:`-s0`  output :program:`ROOT` figure


------------


:math:`\tt -s1`: PDF on :math:`\log{\cal L}` or :math:`\chi^2`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s1 -i clumpy_params_s1.txt


.. figure:: DocImages/s1D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-s1`  output :program:`ROOT` figure


------------


:math:`\tt -s2`: PDF correlations plots (parasm)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Triangle plot showing correlations and marginalised posterior distributions for the mass and DM profile parameters. See also :numref:`fig_s2-draco` in the :ref:`rst_gallery`.

.. code-block:: console

    $ clumpy -s2 -i clumpy_params_s2.txt


.. figure:: DocImages/s2D.png
   :align: center
   :figclass: align-center
   :scale: 30%

   :option:`-s2`  output :program:`ROOT` figure



------------


:math:`\tt -s3`: best-fit parameters
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

    $ clumpy -s3 -i clumpy_params_s3.txt

.. code-block:: console

   ....

   >>>>> Best-fit parameters (best chi2 or log likelihood) from a list of stat. analysis files
      [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]

   Name = dSph01
        DMprofile           EINASTO
        Rhos                4.38166e+08
        Rscale              0.0933164
        Rvir                10
        alpha               0.34653
        beta                3
        gamma               1
        Lightprofile        PLUMMER2D
        L                   1
        RLight              0.021
        alpha*              1
        beta*               3
        gamma*              0
        Anisoprofile        CONSTANT
        Beta_Aniso_0        0.738798
        Beta_Aniso_Inf      0
        RAniso              1
        AnisoShapeParam     2
        Log likelihood      -5.24362

      [Best fit params]  clumpy -s3(stat_files switch_stat)


------------

:math:`\tt -s4`: CLs on parameters
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

is interactive:

.. code-block:: console

    $ clumpy -s4 -i clumpy_params_s4.txt

.. code-block:: console

   ....

   >>>>> CL lower and upper value (first and second line below) from a list of stat. analysis files
      [N.B.: a stat. analysis file must be formatted as described in stat_load_list()]

    Select a parameter (from which the CL is calculated):
          rhos (kpc)                            [0]
          rs (kpc)                              [1]
          Rvir (kpc)                            [2]
          alpha                                 [3]
          beta (outer)                          [4]
          gamma (inner)                         [5]
          rho(r)                                [6]
          L  (L_sol)                            [7]
          rL (kpc)                              [8]
          alpha*                                [9]
          beta* (outer)                         [10]
          gamma* (inner)                        [11]
          nu(r) (Lsol/kpc^3)                    [12]
          I(R) (Lsol/kpc^2)                     [13]
          beta_0                                [14]
          beta_infty                            [15]
          ra (kpc)                              [16]
          eta                                   [17]
          beta(r)                               [18]
          M(r) (Msol)                           [19]
          J(alpha_int) (smooth)                 [20]
          sigma_p(R)(km/s) (projected)          [21]
          vr^2(r)(km^2/s^2) (unprojected)       [22]
    .......your choice:


------------

:math:`\tt -s5`: CLs on :math:`\rho(r)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Same as :option:`-s0` for the density.

.. code-block:: console

    $ clumpy -s5 -i clumpy_params_s5.txt


.. figure:: DocImages/s5D.png
   :align: center
   :figclass: align-center
   :scale: 55%

   :option:`-s5`  output :program:`ROOT` figure


------------


:math:`\tt -s6`: CLs on :math:`J(\alpha_{\rm int})`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Same as above for the *J*-factors (*D*-factors depending on the chosen option).

.. code-block:: console

    $ clumpy -s6 -i clumpy_params_s6.txt


.. figure:: DocImages/s6D.png
   :align: center
   :figclass: align-center


   :option:`-s6`  output :program:`ROOT` figure

------------


:math:`\tt -s7`: CLs on :math:`J(\theta)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s7 -i clumpy_params_s7.txt


.. figure:: DocImages/s7D.png
   :align: center
   :figclass: align-center
   :scale: 55%

   :option:`-s7`  output :program:`ROOT` figure

------------


:math:`\tt -s8`: CLs on :math:`\sigma_p(R)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Same as above for the dSph galaxy velocity dispersion :math:`\sigma_{\rm p}`. This is compared to the kinematic data points used to run the Jeans + MCMC analysis.

.. code-block:: console

    $ clumpy -s8 -i clumpy_params_s8.txt


.. figure:: DocImages/s8D.png
   :align: center
   :figclass: align-center

   :option:`-s8`  output :program:`ROOT` figure


------------


:math:`\tt -s9`: CLs on :math:`v_r^2(r)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s9 -i clumpy_params_s9.txt


.. figure:: DocImages/s9D.png
   :align: center
   :figclass: align-center
   :scale: 55%

   :option:`-s9`  output :program:`ROOT` figure

------------


:math:`\tt -s10`: CLs on :math:`\beta_{\rm ani}(r)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s10 -i clumpy_params_s10.txt


.. figure:: DocImages/s10D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-s10`  output :program:`ROOT` figure

------------


:math:`\tt -s11`: CLs on :math:`\nu(r)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s11 -i clumpy_params_s11.txt


.. figure:: DocImages/s11D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-s11`  output :program:`ROOT` figure

------------


:math:`\tt -s12`: CLs on :math:`I(R)`
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -s12 -i clumpy_params_s12.txt


.. figure:: DocImages/s12D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-s12`  output :program:`ROOT` figure

