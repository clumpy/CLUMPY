.. _rst_enumerators_windowfunc:

.. role::  raw-html(raw)
    :format: html

Window functions
--------------------------------------------------------------

Possible keyword choices for the :option:`gSIM_EXTRAGAL_FLAG_WINDOWFUNC` parameter:

.. csv-table::
   :file: tables/gENUM_WINDOWFUNC.csv_table
   :widths: 10, 70
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso:: :numref:`rst_physics_cosmo`, :ref:`rst_physics_cosmo`.