.. _rst_doc_module_e:

Extragalactic: :math:`\tt -e`
--------------------------------------

Run on extragalactic quantities and diffuse extragalactic intensity

.. note:: All default parameter files from the following run examples are generated with the :option:`D` flag.

   .. code-block:: console

    $ clumpy -eXD


:math:`\tt -e0`: Cosmic distances
++++++++++++++++++++++++++++++++++++++++++++++++++++

Computes the cosmological comoving, angular diameter, and luminosity distances, as well as the :math:`\Omega` and Hubble parameters at :math:`z > 0`:

.. code-block:: console

    $ clumpy -e0 -i clumpy_params_e0.txt


.. figure:: DocImages/e0D.png
   :align: center
   :figclass: align-center
   :scale: 54%

   :option:`-e0`  output :program:`ROOT` figure


------------

:math:`\tt -e1`: Halo mass function
++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

    $ clumpy -e1 -i clumpy_params_e1.txt


.. figure:: DocImages/e1D-a.png
   :align: center
   :figclass: align-center
   :scale: 54%



.. figure:: DocImages/e1D-b.png
   :align: center
   :figclass: align-left
   :scale: 53%

   :option:`-e1`  output :program:`ROOT` figures

------------



:math:`\tt -e2`: Luminosity and multi-level boosts
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Computes :math:`c_{\Delta}(M_{\Delta},z)`, the luminosity :math:`\mathcal{L}(M_{\Delta},z)`, and the boost, :math:`\mathcal{L}_{\rm halo\,substructure}/\mathcal{L}_{\rm no\,substructure}`, over a user-defined mass range of extragalactic halos.

.. code-block:: console

    $ clumpy -e2 -i clumpy_params_e2.txt


.. figure:: DocImages/e2D-1.png
   :align: center
   :figclass: align-center
   :scale: 55%

.. figure:: DocImages/e2D-a.png
   :align: center
   :figclass: align-center
   :scale: 54%

   :option:`-e2`  output :program:`ROOT` figures

------------

:math:`\tt -e3`: Intensity multiplier
++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -e3 -i clumpy_params_e3.txt


.. figure:: DocImages/e3D.png
   :align: center
   :figclass: align-center
   :scale: 54%

   :option:`-e3`  output :program:`ROOT` figure


------------

:math:`\tt -e4`: EBL absorption
++++++++++++++++++++++++++++++++++++++++++++++++++++

Computes the :math:`\gamma`-ray EBL absorption exponent, :math:`\tau(z,\,E_\gamma)`, and :math:`e^{-\tau(z,\,E_\gamma)}` by a polynomial fit in energy dimension and log-linear interpolation in redshift :math:`z` to tabulated values from the literature

.. code-block:: console

    $ clumpy -e4 -i clumpy_params_e4txt


.. figure:: DocImages/e4D.png
   :align: center
   :figclass: align-center
   :scale: 54%

.. figure:: DocImages/e4D-3.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-e4`  output :program:`ROOT` figure

------------

:math:`\tt -e5`: Differential intensity contributions
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Computes the differential contribution to the :math:`\gamma`-ray or :math:`\nu` intensity from extragalactic DM from different redshift and mass shells, :math:`\mathrm{d} I/\mathrm{d} z(z;\, \Delta M,\,E_{\gamma,\nu})`, and :math:`\mathrm{d} I/\mathrm{d} M(M;\, \Delta z,\,E_{\gamma,\nu})`.

.. code-block:: console

    $ clumpy -e5 -i clumpy_params_e5.txt


.. figure:: DocImages/e5D.png
   :align: center
   :figclass: align-center
   :scale: 54%

   :option:`-e5`  output :program:`ROOT` figure

------------

:math:`\tt -e6`: Diffuse :math:`\gamma`-ray or :math:`\nu` intensity
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

    $ clumpy -e6 -i clumpy_params_e6.txt


.. figure:: DocImages/e6D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :option:`-e6`  output :program:`ROOT` figure

