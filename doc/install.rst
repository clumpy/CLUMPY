.. _rst_install:

.. role::  raw-html(raw)
    :format: html

Download and Installation
==========================

The :program:`CLUMPY` package is written in C/C++ (but no classes) for UNIX systems (Linux, Mac OSX) and is interfaced with several third-party softwares. Some are mandatory, other optional, and they need to be installed before proceeding to the :program:`CLUMPY` installation.

Here, we first cover the automated (i.e., easy) installation procedure and then discuss what steps
you would have to take if you want to install the dependencies without anaconda or
perform the compilation manually (or both).


Obtain the code
---------------
Clone from git repository and cd into the directory:

   .. code-block:: console

      $ git clone https://gitlab.com/clumpy/CLUMPY.git --depth=1 --branch release4
      $ cd CLUMPY

or visit the `GitLab repository <https://gitlab.com/clumpy/CLUMPY>`__ 
to find older release versions.

If you intend to use the latest code from the `master` branch, omit the `--branch release 4`
from the command above.


Automated installation (recommended)
------------------------------------
The easiest way to obtain all required dependencies is by creating an 
conda environment (link) using the `environment.yml` file provided in the repository.

You will then need to create and activate the environment
to compile the sources. The procedure described here will install `CLUMPY`
into the environment. This means you have to activate the environment whenever you want to use it.

If you want to run the tutorial jupyter notebooks, you will need the `environment.yml` file.
If you only want to run the command-line tools and prefer a smaller environment,
you can get away with the smaller `environment-minimal.yml` file
(in which case the name of the environment also changes to clumpy-minimal).

   .. code-block:: console

      $ conda env create -f environment.yml
      $ conda activate -f clumpy-full

Now you can compile the code using scikit-build:

   .. code-block:: console

      $ pip install .

and the command-line tools as well as the python modules will be available.

You then need to set an environment variable `CLUMPY_DATA` pointing to the location
of the directory containing the `data/` directory. It should be copied to the conda environment into a location similar to
`$CONDA_PREFIX/lib/python3.11/site-packages/share/Clumpy/`.

You can set the environment variable using:

   .. code-block:: console

      $ export CLUMPY_DATA=$CONDA_PREFIX/lib/python3.11/site-packages/share/Clumpy/

The specific path might differ slightly on your system, so make sure it exists and is filled with the content of the `data/` folder.
In particular, the installed python version might differ in the future as we do not pin a version.

If you want to keep the cloned repository, you can also point the variable there:

   .. code-block:: console

      $ export CLUMPY_DATA=$PWD/data


Manual installation
---------------------

Requirements
~~~~~~~~~~~~

CLUMPY relies on third-party software. If not wanting to use a conda environment and/or the automated procedure described above, these need to be pre-installed on your system. Generally, they should be available through the package manager of your distribution.

   1. `GSL (Gnu scientific library) <http://www.gnu.org/software/gsl>`__:
      We recommend installing the package for your distribution with a package manager (or, if needed, install it locally). 
   
   2. `CFITSIO (handle FITS files) <http://heasarc.gsfc.nasa.gov/fitsio/>`__:
      We recommend installing the package for your distribution with a package manager. If it does not exist for your system, `download it <http://heasarc.gsfc.nasa.gov/fitsio/>`__ and install it by hand (see the :ref:`rst_troubleshooting` section if you face problems with a manual :program:`CFITSIO` installation). 

   3. `Healpix CXX <https://healpix.sourceforge.io/doc/html/Healpix_cxx/index.html>`__:
      If available, this is easily installed with the package-manager of your distribution.

   4. `ROOT CERN library <https://root.cern.ch/>`__:
      For displays and random drawing clumps from multivariate distributions.
      This is not required for succesful compilation, but some tools will only work if compiled
      with `ROOT`.
      The easiest way to install it on your system is via your package manager. Alternatively, you may want to compile locally :program:`ROOT`. To do so, `download a version <https://root.cern.ch/releases>`_ and follow the configuration/compilation instructions to `build ROOT <https://root.cern.ch/building-root>`_. In addition, in the latter case, do not forget to define the :envvar:`ROOTSYS` environment variable, e.g., in your :file:`~/.bashrc`).

   5. `pybind11 <https://github.com/pybind/pybind11>`__:
      To compile the optional python bindings.

   .. note :: Depending on your distribution, you will also need to install the development files such as ``gsl-devel`` for the header files to include. For healpix, we specifically require the `_cxx` version.

For example, a minimal setup (excluding root and pybind) on ubuntu 22.04 looks might involve a setup such as:

   .. code-block:: console

      $ apt-get update
      $ apt-get install gcc g++ cmake libcfitsio-dev libhealpix-cxx-dev libgsl-dev pkg-config


After that, you can compile the code using `pip install .` as described above
(provided you have `python` and `pip` available). Alternatively, you can continue with the manual installation procedure below.


Compiling the sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Besides using `scikit-build`, you can also run the respective `cmake` commands yourself.
This works independently of how you installed the dependencies.
The main advantage of this is for developing, where you might not want to trigger a completely
new build whenever you make some small changes.

You will need to have `cmake` and a compiler installed on your system.

From the root of the project (after `cd CLUMPY`), run:

   .. code-block:: console

      $ cmake -S . -B build -DCMAKE_INSTALL_PREFIX=<<install-location>>
      $ cmake --build build
      $ cmake --install build


In the first step, you define where the program should be installed to, a common location might be in a `.local` directory in your users home.
If you wish to install the python-module as well, add the tag `-DUSE_PYCLUMPY=ON`, which enables the build of the python modules.

Since the install location is likely not in the `PATH` of your system, it needs to be added:

   .. code-block:: console
   
     $ export PATH=<<install-location>>/bin:$PATH

(The `$` in from of the second `PATH` is important!)

Similarly, it needs to be part of the `PYTHONPATH` if you want to import it in a python
console / script:

   .. code-block:: console

     $ export PYTHONPATH=<<install-location>>:$PYTHONPATH 

When performing this manual installation, you do not need to set the `CLUMPY_DATA` environment variable as the install location will be compiled into the code.
(This does not work in the recommended install, because the build is performed in a temporary location and later relocated.)
You can still set it and it will overwrite the default location if you want a way to quickly test the influence of changes to
the files in `data/`.

You should now be able to run the code from anywhere just typing

   .. code-block:: console

     $ clumpy


   .. note:: Remember to set the environment variables in a place, where they get reapplied, such as the users `.bashrc`, as they are needed to run the program.



Test the installation
---------------------

All tests are bundled in a single executable:

   .. code-block:: console

      $ clumpy_tests

To pass all tests, the environmental variable `CLUMPY_DATA` must be set.

Alternatively, if you followed the manual installation, you can use the `test` target
created by cmake:

   .. code-block:: console

      $ cd build
      $ make test

There is no functionality-test for the python-module yet. To test whether it is linked correctly, run:

   .. code-block:: console
   
      $ python -c"import pyClumpy"

If it throws an error, double-check that you used the tag ``-DUSE_PYCLUMPY=ON`` in the cmake-step and defined the pythonpath environment-variable in your .bashrc correctly.

Additionaly, you can check the examples provided in ``pyClumpy_demo_notebooks/demo_*``.


Use CLASS to compute different cosmologies
------------------------------------------

For the computation of the linear matter power spectrum, we make use of the software [CLASS](http://www.class-code.net).

Unfortunately, it is not available on `conda-forge`, so if you want to make use of it, you will need to
install it from source.

We do not link against CLASS, but instead call it in a subprocess in some tools.
This means it is not needed at compile time. You can install it later at any time.

If you use CLASS, you will need to set the environment variable `CLASS` that points to the directory containing the class executable.

   .. code-block:: console
   
      $ export CLASS=<<path/to/class>>



Files and directories
---------------------

**Files in** ``CLUMPY/``

+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`CmakeLists.txt`                       | File used by cmake for compilation                                                                     |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`FindROOT.cmake`                       | Called by CmakeLists.txt to find path to :program:`ROOT` installation                                  |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`README.md`                            | README file (for GitLab website)                                                                       |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+
| :file:`LICENSE`                              | A copy of the GNU General Public License v2 under which :program:`CLUMPY` is licensed                  |
+----------------------------------------------+--------------------------------------------------------------------------------------------------------+

**Directories in** ``$CLUMPY/``

.. <http://warp.povusers.org/FunctionParser/fparser.html>`__

+-----------------------------+-------------------------------------------------------------+
| ``data/``                   | Tabulated files (EBL, :math:`P_{\rm lin}(k)`, CLUMPY files) |
+-----------------------------+-------------------------------------------------------------+
| ``doc/``                    | This documentation                                          |
+-----------------------------+-------------------------------------------------------------+
| ``include/*.h``             | :program:`CLUMPY` headers                                   |
+-----------------------------+-------------------------------------------------------------+
| ``src/*.cc``                | :program:`CLUMPY` libraries                                 |
+-----------------------------+-------------------------------------------------------------+
| ``cli/*.cc``                | :program:`CLUMPY` executables                               |
+-----------------------------+-------------------------------------------------------------+
| ``tests/*.cc``              | `:program:`CLUMPY` clumpy_tests` executable                 |
+-----------------------------+-------------------------------------------------------------+
| ``pyClumpy_demo_notebooks`` | Auxiliary :program:`Python` scripts                         |
+---------------------------+---------------------------------------------------------------+


**Example data files in** ``$CLUMPY/data``

+------------------------------------+----------------------------------------------------------+
| ``data/list_generic.txt``          | List of generic spherical DM haloes (option :option:`-h`)|
+------------------------------------+----------------------------------------------------------+
| ``data/list_generic_triaxial.txt`` | List of generic triaxial DM haloes (option :option:`-h`) |
+------------------------------------+----------------------------------------------------------+
| ``data/stat_example.dat``          | Example of statistical-like file (option :option:`-s`)   |
+------------------------------------+----------------------------------------------------------+

+------------------------------------+-------------------------------------------------+
| ``data/list_generic_jeans.txt``    | List of DM haloes for Jeans-related quantities  |
+------------------------------------+-------------------------------------------------+
| ``data/params_jeans.txt``          | User-defined parameters for Jeans analysis      |
+------------------------------------+-------------------------------------------------+
| ``data/data_light.txt``            | Sample of simulated surface brightness data     |
+------------------------------------+-------------------------------------------------+
| ``data/data_sigmap.txt``           | Sample of simulated velocity dispersion data    |
+------------------------------------+-------------------------------------------------+
| ``data/data_vel.txt``              | Sample of simulated line-of-sight velocities    |
+------------------------------------+-------------------------------------------------+

**Auxiliary data files in** ``$CLUMPY/data`` **(do not change)** [#f1]_

+------------------------------------+-------------------------------------------------------------------------+
| ``data/EBL/``                      | Tabulated optical depths :math:`\tau`  (option :option:`-e`)            |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/healpix/``                  | Pixel windows and ring weights for spherical harmonic transformation    |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/pk_precomp/``               | Pre-computed :math:`P_{\rm lin}(k)` for cosmology (option :option:`-e`) |
+------------------------------------+-------------------------------------------------------------------------+
| ``data/PPPC4DMID-spectra/``        | Marco Cirelli's particle physics spectra                                |
+------------------------------------+-------------------------------------------------------------------------+

+------------------------------------+-------------------------------------------------------------------------+
| ``data/tests_ref/``                | Reference files (for :math:`\tt clumpy\_tests`)                         |
+------------------------------------+-------------------------------------------------------------------------+

.. [#f1] You may add your own files to ``data/pk_precomp/``, though.

.. _rst_troubleshooting:

Troubleshooting
---------------------

Cfitsio version mismatch
^^^^^^^^^^^^^^^^^^^^^^^^

You might get a warning such as:
   .. code-block:: console

      WARNING: version mismatch between CFITSIO header (v4.002) and linked library (v4.02).

We do not know how to get rid of it, but as long as the numbers match if you remove leading zeros
or round floats (yes, really!), it should be a false positive produced by the way `cfitsio`
labels its version.

It is annoying nonetheless and on our radar, sorry.

Pybindings python version conflicts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We do not know when exactly this occurs, but in some cases we observed issues between pybind11
and python 3.10 in the compile step.

This should not occur when the conda environment is used.
  
Mac and ROOT
^^^^^^^^^^^^

Some problems can occur at the execution of the code with Mac OSX 10.8.5 if using a :program:`ROOT` version more recent than 5.34.10 (e.g., issues with TF3 with option :option:`-g7`) Please use :program:`ROOT` 5.34.10 or try to update your OS.
  

Undefined reference
^^^^^^^^^^^^^^^^^^^

If you experience the error

  .. code-block:: console
  
     ../lib/libCLPY.so: undefined reference to `curl_global_init'
     ../lib/libCLPY.so: undefined reference to `curl_easy_cleanup'
     ../lib/libCLPY.so: undefined reference to `curl_global_cleanup'
     
     ...
     
  this might be due to the fact that you use a manual installation of :program:`CFITSIO` version > 3.42 and do not have :program:`libcurl` installed on your system. Either (i) use a  :program:`CFITSIO` version <= 3.42 (see `here <https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/>`__), (ii) install the :program:`libcurl` library (see `here <https://curl.haxx.se/download.html>`__) or (iii) install :program:`CFITSIO` via a package manager, which we recommend and which takes care of all dependencies.


Compiling GREAT
^^^^^^^^^^^^^^^

To compile GreAT with c++17 compilers, please replace in the GreAT ``CMakeList.txt`` (not in :program:`CLUMPY`'s one!) the following lines

      .. code-block:: console

        # Check for C++14 support
        include(CheckCXXCompilerFlag)
        check_cxx_compiler_flag("-std=c++14" COMPILER_SUPPORTS_CXX14)
        if(COMPILER_SUPPORTS_CXX14)
           set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
        else()
           message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++14 support needed for ROOT 6. Please use a different C++ compiler or an older ROOT version.")

  by
  
      .. code-block:: console

        # Check for C++17 support
        include(CheckCXXCompilerFlag)
        check_cxx_compiler_flag("-std=c++17" COMPILER_SUPPORTS_CXX17)
        if(COMPILER_SUPPORTS_CXX17)
           set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
        else()
           message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++17 support needed for ROOT 6. Please use a different C++ compiler or an older ROOT version.")

