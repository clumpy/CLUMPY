.. _rst_gallery:

.. sectionauthor:: Moritz Huetten <moritz.huetten@desy.de>

.. role::  raw-html(raw)
    :format: html


Picture gallery
===============

For many examples of :program:`ROOT` pop-up graphics, see also :ref:`rst_doc_modules`.

Figures from publications
--------------------------


.. _fig_halo2D_Charbonnier12:

.. figure:: DocImages/halo2D_Charbonnier12.png
   :align: center
   :figclass: align-center
   :width: 100%

   *J*-factor map of a generic dSph DM halo with substructure in :math:`\gamma`-rays from DM annihilation for different instrumental resolutions. Figure published in Charbonnier et al. :raw-html:`<A href="http://doi.org/10.1111/j.1365-2966.2011.19387.x" target="_blank">(2011)</A>` and :raw-html:`<A href="http://doi.org/10.1016/j.cpc.2011.10.017" target="_blank">(2012)</A>`.

|

.. _fig_ultrafaint:

.. figure:: DocImages/Bonnivard-et-al-2015-ultrafaint.png
   :align: center
   :figclass: align-center
   :width: 100%

   Annihilation *J*-factor estimation of the classical and ultrafaint dSphs. Figure published in :raw-html:`<A href="https://doi.org/10.1093/mnras/stv1601" target="_blank">Bonnivard et al. (2015)</A>`.

|

.. _fig_contamination:

.. figure:: DocImages/Bonnivard-et-al-2016-contamination.png
   :align: center
   :figclass: align-center
   :width: 100%

   MCMC/Jeans re-analysis of the *J*-factor of the Segue I dSph. Figure published in :raw-html:`<A href="http://adsabs.harvard.edu/abs/2016MNRAS.462..223B" target="_blank">Bonnivard et al. (2016)</A>`.

|


.. _fig_skymaps_darkclumps:

.. figure:: DocImages/Skymaps_ModelVarHIGH.png
   :align: center
   :figclass: align-center
   :width: 100%

   Full-sky maps of the :math:`\gamma`-ray emission from Galactic DM (resolved substructures, resolved + unresolved substructures, and total emission). Figure published in :raw-html:`<A href="http://doi.org/10.1088/1475-7516/2016/09/047" target="_blank">H&uuml;tten et al. (2016)</A>`.

|

.. _fig_APS:

.. figure:: DocImages/APS_Huetten2016.png
   :align: center
   :figclass: align-center
   :width: 100%

   Angular power spectrum from Galactic dark matter substructure. Figure published in :raw-html:`<A href="http://doi.org/10.1088/1475-7516/2016/09/047" target="_blank">H&uuml;tten et al. (2016)</A>`.

|

.. _fig_SubsubZoom:

.. figure:: DocImages/SubsubZoom.png
   :align: center
   :figclass: align-center
   :width: 70%

   Galactic subhalo :math:`{\rm d}J/{\rm d}\Omega` 2D profiles with and without sub-substructure. Figure published in :raw-html:`<A href="http://doi.org/10.18452/17766" target="_blank">H&uuml;tten (2017, PhD thesis)</A>`.

|

.. _fig_HMF_comparison:

.. figure:: DocImages/HMF_comparison_threeInRow.png
   :align: center
   :figclass: align-center
   :width: 100%

   Comparison of mass functions in different cosmologies. Figure published in :raw-html:`<A href="http://doi.org/10.1088/1475-7516/2018/02/005" target="_blank">H&uuml;tten et al. (2018)</A>`.

|

Other pictures from :program:`CLUMPY`
--------------------------------------



.. _fig_aladin_perseus:

.. figure:: DocImages/perseus_annihilation_triaxial_aladin.png
   :align: center
   :figclass: align-center
   :width: 100%

   2D triaxial annihilation :math:`{\rm d}J/{\rm d}\Omega` model (arbitrary Euler angles and semiaxes) of the  Perseus cluster w/o resolved subhaloes, based on :raw-html:`<A href="http://dx.doi.org/10.1088/1475-7516/2011/12/011" target="_blank">S&aacute;nchez-Conde et al. (2011)</A>`, displayed over a DSS map with :raw-html:`<a href=http://aladin.u-strasbg.fr target="_blank">Aladin</a>`.

|

.. _fig_ds9_perseus:

.. figure:: DocImages/perseus_annihilation_triaxial_ds9.png
   :align: center
   :figclass: align-center
   :width: 100%

   Same as above, but displayed with :raw-html:`<a href=http://ds9.si.edu target="_blank">ds9</a>`.

|

.. _fig_s2-draco:

.. figure:: DocImages/s2-draco.png
   :align: center
   :figclass: align-center

   MCMC fit of the Draco dSph with 5 parameters (see CLUMPY's :math:`\tt jeansMCMC` :ref:`rst_doc_module_jeansMCMC` and :ref:`rst_doc_module_s` :option:`-s2`).

|


.. _fig_galDMsky_closeup:

.. figure:: DocImages/DMsky_pink.png
   :align: center
   :figclass: align-center
   :width: 100%

   :math:`{\rm d}J/{\rm d}\Omega` skymap of a clumpy Galactic DM halo in  a :math:`20^{\circ}\times 10^{\circ}` field of view close to the Galactic centre.

|




