.. _rst_doc_module_z:

Compute spectra: :math:`\tt -z`
--------------------------------

Simply compute :math:`\gamma`-ray or neutrino spectra at source:

.. code-block:: console

    $ clumpy -z0D
    $ clumpy -z0 -i clumpy_params_z0.txt


.. figure:: DocImages/zD.png
   :align: center
   :figclass: align-center
   :scale: 55%

   :option:`-z0`  output :program:`ROOT` figure


.. seealso:: :numref:`rst_physics_spectra`, :ref:`rst_physics_spectra`, for details.