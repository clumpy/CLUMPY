.. _rst_enumerators_spectra:

.. role::  raw-html(raw)
    :format: html

:math:`\gamma` and :math:`\nu` particle spectra
--------------------------------------------------------------

Possible keyword choices for the :option:`gPP_FLAG_SPECTRUMMODEL` variable:

.. csv-table:: 
   :file: tables/gENUM_PP_SPECTRUMMODEL.csv_table
   :widths: 10, 30
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. note:: 

   When choosing :keyword:`kCIRELLI11_EW`/:keyword:`kCIRELLI11_NOEW`, it is additionally possible/necessary to select one of the following options: 

   - **SM model final state under investigation** via the parameter :option:`gSIM_FLUX_FLAG_FINALSTATE` (see `Keywords for possible final states <enumerators_finalstates.html>`_). Note that this parameter must be set to :option:`gSIM_FLUX_FLAG_FINALSTATE` = :keyword:`kGAMMA` for all other spectral models listed in the table above. :raw-html:`<br><br>`


   - **Neutrino (physics) details, in case** :option:`gSIM_FLUX_FLAG_FINALSTATE` = :keyword:`kNEUTRINO` **is chosen**: Besides the mixing angles of neutrino oscillation (see :numref:`rst_doc_finalstates`), the neutrino species under consideration has to be specified via the :option:`gSIM_FLUX_FLAG_NUFLAVOUR` variable (see `Keywords for neutrino flavours <enumerators_nuflavours.html>`_).

   
.. seealso::
   :numref:`rst_doc_finalstates`.
   
   
   