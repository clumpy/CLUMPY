.. _rst_enumerators_massfunction:

.. role::  raw-html(raw)
    :format: html

Halo mass functions :math:`{\rm d}n/{\rm d}M`
--------------------------------------------------------------

Possible keyword choices for the :option:`gEXTRAGAL_FLAG_MASSFUNCTION` parameter. 

.. csv-table::
   :file: tables/gENUM_MASSFUNCTION.csv_table
   :widths: 10, 70
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso:: :numref:`rst_physics_cosmo`, :ref:`rst_physics_cosmo`.