.. _rst_physics_geometry:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html

Coord. & rotations (geometry.h)
--------------------------------

The observer framework (Solar system) does not coincide with the Galactic (or any DM halo) coordinate systems. The library `geometry.h <doxygen/geometry_8h.html>`_ provides transformation laws to go from the observer system to coordinates centred on DM haloes. Moreover, when dealing with triaxial haloes, we need to specify their orientation (Euler rotation angles).


.. _rst_coordsys:

Coordinate systems
++++++++++++++++++

To avoid confusion, we use the subscripts :math:`(\dots)_\oplus` and :math:`(\dots)_G`  in the following to denote the coordinate frames attached to the Earth (or the Sun if you wish) and to the Galactic centre (GC), respectively. Generally, distances are given in :math:`\rm kpc` and angles in :math:`\rm rad` (except for Euler rotations, for which angles are given in degrees).

.. _fig_geometry:

.. figure:: DocImages/geom.png
   :align: center
   :figclass: align-center
   :scale: 75%

   Coordinates and definitions. :math:`\rm [GW]`: Galactic West, :math:`\rm [GE]`: Galactic East, :math:`\rm [SGP]`: South Galactic  Pole, :math:`\rm [NGP]`: North Galactic  Pole, :math:`\rm [GC]`: Galactic centre. Click to enlarge the figure.

.. note:: In :program:`CLUMPY`, the coordinates :math:`(\psi,\theta)_\oplus` are equivalent to the usual notation of Galactic longitude :math:`l` and latitude :math:`b`, :math:`(\psi,\theta)_\oplus \equiv (l,b)`.

:Coordinates along the line of sight (:abbr:`l.o.s. (line of sight)`):

  The angles :math:`(\alpha,\beta)_\oplus` are defined with respect to the base :math:`(\vec{i}',\vec{j}',\vec{k}')_\oplus` such as the unit vectors of this basis are collinear with the axis :math:`Oxyz` when :math:`\psi=\theta=0`. The coordinates of the point :math:`P` (i.e. :math:`\vec{OP}`, see :numref:`fig_geometry`) in this frame are either :math:`(l,\alpha,\beta)_\oplus` or :math:`(x',y',z')_\oplus`, where

  .. math::

        \vec{OP}_{w.r.t.\, \vec{OS}=(\theta,\psi)} = (x',y',z')_\oplus
          =\big( l\cos\alpha,\, l\sin\alpha \sin\beta,\, l\sin\alpha \cos\beta\big)\,.

  As a result,

      - :math:`\alpha=0` corresponds to  :math:`y'=z'=0`,
      - :math:`\beta=0` corresponds to  :math:`y'=0` and :math:`z'>0`,
      - :math:`\beta=\pi/2` corresponds to  :math:`z'=0` and :math:`y'>0`,
      - :math:`\beta \in [0,2\pi]`.

  Another useful coordinate system is :math:`(x,y,z)_{\rm halo}`, i.e. the coordinate system :math:`(x',y',z')_\oplus` shifted to the halo centre position (located at distance d and assuming the los points at the halo centre).

  .. math::

       (x,y,z)_{\rm halo} = (x',y',z')_\oplus
          =\big( l\cos\alpha-d,\, l\sin\alpha \sin\beta,\, l\sin\alpha \cos\beta\big)\,.

:Galactic coordinates:

  In Galactic coordinates (origin attached to the Earth), we have the corresponding directions for :math:`(\psi,\theta)_\oplus` equals:

      - :math:`(0,0)_\oplus \Rightarrow` Galactic centre (GC),
      - :math:`(0,\pi/2)_\oplus\Rightarrow`  North Galactic Pole (NGP),
      - :math:`(0,-\pi/2)_\oplus\Rightarrow`  South Galactic Pole (SGP),
      - :math:`(\pi/2,0)_\oplus\Rightarrow`  Galactic West direction (GW),
      - :math:`(3\pi/2,0)_\oplus\Rightarrow`  Galactic East direction (GE).


  This choice of angles (appropriate for Galactic coordinates) differs slightly from the standard choice. Indeed, our choice of unit vectors :math:`(\vec{i}',\vec{j}',\vec{k}')_\oplus` are related to the standard spherical coordinate unit vectors by means of :math:`\vec{i}' = + \vec{e}_{r}`, :math:`\vec{j}' = + \vec{e}_{\psi}`, and :math:`\vec{k}' = - \vec{e}_{\theta}`. We can then express the unit vectors :math:`(\vec{i}',\vec{j}',\vec{k}')_\oplus` in the base :math:`(\vec{i},\vec{j},\vec{k})_\oplus`:

  .. math::

      \vec{i}' &= + \cos(\theta) \; \cos(\psi) \;\vec{i} \quad
                 + \cos(\theta) \; \sin(\psi) \; \vec{j} \quad
                 + \sin(\theta) \; \vec{k} \\
      \vec{j}' &= - \sin(\psi) \;\vec{i} \qquad\quad\;\;\;
                 + \cos(\psi) \; \vec{j}\\
      \vec{k}' &= -\sin(\theta) \; \cos(\psi) \; \vec{i} \quad
                 - \sin(\theta) \; \sin(\psi) \; \vec{j} \quad
                 +\cos(\theta) \; \vec{k}\\

:math:`(l,\alpha,\beta)_{\oplus}` to :math:`(x,y,z)_G`
++++++++++++++++++++++++++++++++++++++++++++++++++++++

For the sake of understanding, we recall below the main steps to move from spherical coordinates in the framework attached to the line of sight, to Cartesian coordinates in the Galactic framework. Summary formulae for the global transformation are given in the next paragraph.

   - Step 1: :math:`(l,\alpha,\beta)_{\oplus}` to :math:`(x',y',z')_{\oplus}`. :raw-html:`<br>` This is given in :numref:`rst_coordsys`  above  by the expression for :math:`\vec{OP}`. :raw-html:`<br><br>`

   - Step 2: :math:`(x',y',z')_{\oplus}` to :math:`(x,y,z)_{\oplus}`. :raw-html:`<br>`  This is obtained by the transformation of the unit vectors as in :numref:`rst_coordsys` above. Another way would have been to use the rotation matrices :math:`{\cal R}`, corresponding to the two rotations along the angles :math:`(\psi,\theta)` between the galactic frame attached to the Earth and the :abbr:`l.o.s. (line of sight)` direction:

      .. math::

         \begin{pmatrix}
             x \\
             y \\
             z
         \end{pmatrix} \;=\; {\cal R}^{-1}(\psi)\;\,{\cal R}^{-1}(\theta)\;\,
           \begin{pmatrix}
             x' \\
             y' \\
             z'
         \end{pmatrix}

   - Step 3: :math:`(x,y,z)_{\oplus}` to :math:`(x, y, z)_G`. :raw-html:`<br>` We merely have :math:`(x,y,z)_G = (x-R_\odot,y,z)_{\oplus}`.


**Summary formulae**

   - *Transformation*: from :math:`(l,\alpha,\beta)_\oplus` along the :abbr:`l.o.s. (line of sight)` direction :math:`(\psi,\theta)_\oplus` to :math:`(x,y,z)_G`

     .. math::

         x &= x' \cos(\psi) \cos(\theta) - y' \sin(\psi) - z' \sin(\theta) \cos(\psi) - R_\odot\\
         y &=  x' \sin(\psi) \cos(\theta) + y' \cos(\psi) - z' \sin(\theta) \sin(\psi)\\
         z &=  x' \sin(\theta) + z' \cos(\theta)

     where we have

     .. math::

         x' &= l \cos(\alpha)\\
         y' &= l \sin(\alpha) \sin(\beta)\\
         z' &= l \sin(\alpha) \cos(\beta)

   - *Inverse transformation*: from :math:`(x,y,z)_G` to :math:`(l,\alpha,\beta)_\oplus` along the :abbr:`l.o.s. (line of sight)` direction :math:`(\psi,\theta)_\oplus`

     .. math::

         l      = & \sqrt{x'^2 + y'^2 + z'^2} \\
         \alpha = & \;\cos^{-1}\left(\frac{x'}{l}\right) \\
         \beta  = & \; \cos^{-1}\left(\frac{z'}{\sqrt{x'^{2}+y'^{2}}}\right) \quad {\rm if} \quad y'\geq 0 \\
                  & \; 2\pi - \cos^{-1}\left(\frac{z'}{\sqrt{x'^{2}+y'^{2}}}\right) \quad {\rm if} \quad y'<0

     where we have

     .. math::

         x' &= \quad(x+R_\odot) \cos(\psi) \cos(\theta) + y \sin(\psi) \cos(\theta) + z \sin(\theta)\\
         y' &= -(x+R_\odot) \sin(\psi) + y \cos(\psi)\\
         z' &= -(x+R_\odot) \cos(\psi) \sin(\theta) - y \sin(\psi) \sin(\theta) + z \cos(\theta).

.. _rst_euler_angles:

Halo rotation (Euler angles)
++++++++++++++++++++++++++++

It is useful to define Euler angles for a body whose origin is the centre of the dark matter halo we are looking at. The Euler angles :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler}` allow to define a rotation of the object in its framework, which is useful for triaxial haloes considered in :program:`CLUMPY`. :numref:`fig_euler` below represents the original framework and the rotated framework (see the corresponding :raw-html:`<a href="http://en.wikipedia.org/wiki/Euler_angles" target="_blank">Wikipedia pages</a>` for more details), where

   - :math:`\alpha \in [-180^\circ,180^\circ]` represents a rotation around the z axis (xOy plane),
   - :math:`\beta \in [-90^\circ,90^\circ]` represents a rotation around the N axis (or X axis, obtained after first rotation),
   - :math:`\gamma \in [-180^\circ,180^\circ]` represents a rotation around the Z axis.

  .. _fig_euler:

  .. figure:: DocImages/euler_angles.png
     :align: center
     :figclass: align-center

     Euler angles: original (xyz) and rotated (XYZ) coordinates

**Transformation law**

It is convenient to write the transformation laws in a matrix form. The result is the product of three rotation matrices along three different axes (as depicted in :numref:`fig_euler`):

  .. math::

         \begin{pmatrix}
           X \\
           Y \\
           Z
         \end{pmatrix} =
         \begin{pmatrix}
           \cos\alpha\cos\gamma-\sin\alpha\sin\gamma\cos\beta & \sin\alpha\cos\gamma+\cos\alpha\sin\gamma\cos\beta & \sin\gamma\sin\beta\\
           -\cos\alpha\sin\gamma-\sin\alpha\cos\gamma\cos\beta & -\sin\alpha\sin\gamma+\cos\alpha\cos\gamma\cos\beta & \cos\gamma\sin\beta \\
           \sin\alpha\sin\beta   & -\cos\alpha\sin\beta & \cos\beta
         \end{pmatrix}\;
         \begin{pmatrix}
           x \\
           y \\
           z
         \end{pmatrix}

**Euler angles in the context of triaxial DM halos**

The Euler rotations are used in :program:`CLUMPY` for triaxial DM profiles. The default orientation :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler} = (0,\,0,\,0)` corresponds to:

   - Major axis :math:`a` along the :abbr:`l.o.s. (line of sight)` direction, i.e. :math:`x'` axis in :numref:`fig_geometry` (:math:`b` and :math:`c` in the plane perpendicular to the :abbr:`l.o.s. (line of sight)`);
   - Second axis :math:`b` along the lateral direction w.r.t. the los, i.e. :math:`y'` axis in :numref:`fig_geometry`;
   - Minor axis :math:`c` along the vertical direction w.r.t. the los i.e. :math:`z'` axis in :numref:`fig_geometry`.

  To have a better grasp of the angle orientations, one can run:

  .. code-block:: console

     $ clumpy -h4D; clumpy -h4 -i clumpy_params_h4.txt

  to get a 2D plot of one of a triaxial halo in the :file:`list_generic_triaxial.txt` of example haloes shipped with the code:

  .. code-block:: console

     $ clumpy -h4 -i clumpy_params_h4.txt --gLIST_HALOES=$CLUMPY/data/list_generic_triaxial.txt --gLIST_HALONAME=m5kpc_rs01_g10_1.47_1.22_0.7

  The input variable :option:`gLIST_HALOES` selects a file with halo definitions (here, the variable passed via the command line overwrites the file specified in :file:`clumpy_params_h4.txt`). This file may contain various halo definitions, and the variable :option:`gLIST_HALONAME` selects the halo to compute. See the :ref:`rst_FITS_display` section for different possibilities to view the FITS 2D output map of the halo.

  Exchanging the values of the major, second, and minor axes of the halo definition in :file:`list_generic_triaxial.txt` is directly seen on the orientation of the 2D projection map obtained. The effects of Euler rotation angles (w.r.t. the reference framework) can be checked in a similar way, and the following special cases correspond to:

      - :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler} = (~~~~0~,~~~0~,~~~~0~)`:raw-html:`<br>` :math:`\rightarrow\,` DM halo major axis :math:`a` along the :abbr:`l.o.s. (line of sight)` (see default orientation above); :raw-html:`<br><br>`
      - :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler} = (~~90^\circ,~~~0~,~~~~0~)`:raw-html:`<br>` :math:`\rightarrow\,` Rotation along :math:`Oz'`, i.e. :math:`Ox'` (major axis :math:`a`) moves to :math:`Oy'` (second axis :math:`b`); :raw-html:`<br><br>`
      - :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler} = (~~~~0~,~90^\circ,~~~~0~)`:raw-html:`<br>` :math:`\rightarrow\,` Rotation along :math:`Ox'`, i.e. minor axis :math:`c` (along :math:`Oz'`) moves to :math:`Oy'` (second axis :math:`b`); :raw-html:`<br><br>`
      - :math:`(\alpha,\,\beta,\,\gamma)_{\rm Euler} = (-90^\circ,~\!90^\circ,-90^\circ)`:raw-html:`<br>` :math:`\rightarrow\,` Rotation along :math:`Oy'`, i.e. major axis :math:`a` (along :math:`Ox'`) moves to :math:`Oz'` (minor axis :math:`c`).

.. note::
   The orientation of the DM halo axes in the plane perpendicular to the :abbr:`l.o.s. (line of sight)` only matters for 2D maps. For the 1D *J*-factor integration, the halo is integrated on a regular conic section (solid angle) that intersects the projected profile as a circle, so that the result is invariant under rotation in this plane.



