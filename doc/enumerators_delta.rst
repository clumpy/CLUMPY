.. _rst_enumerators_delta:

.. role::  raw-html(raw)
    :format: html

Overdensity :math:`\Delta(z)` descriptions
--------------------------------------------------------------

Possible keyword choices for the :option:`gCOSMO_FLAG_DELTA_REF` parameter. 

.. csv-table::
   :file: tables/gENUM_DELTA_REF.csv_table
   :widths: 10, 90
   :align: left


:raw-html:`<br>&nbsp;<br>`

.. note:: :math:`\Delta_{\rm crit}` is the constant overdensity factor set via the :option:`gCOSMO_DELTA0` input parameter. See also :numref:`rst_physics_cosmo`, :ref:`rst_physics_cosmo`.
