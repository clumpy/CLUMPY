﻿pyClumpy API
============

.. currentmodule:: pyClumpy

.. autosummary::
   :toctree:
   :recursive:

   Jeans
   append
   export
   extragalactic
   galactic_halo
   haloes
   spectra
   statistical