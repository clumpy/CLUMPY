.. _rst_physics_integr_los:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html


Integrations (integr_los.h)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the end, all calculations amount to multi-dimensional integrations repeated a great many number of times. Because the quantities to integrate are mostly power-laws of monotonic functions, we can optimise the integration for speed. Particularly sophisticated optimisations have been developed for the line-of-sight integration over extended ("resolved") haloes. This is the core of the `integr_los.h  <doxygen/integr__los_8h.html>`__ library and explained in this section.



Formal equations
+++++++++++++++++

For many quantities calculated along a given line of sight (:abbr:`l.o.s. (line of sight)`) over a solid angle :math:`\Delta\Omega` (*J*-factor for the smooth and clumpy component, number of clumps in a :abbr:`FOV (field of view)`, etc.), we have to perform the following integration :math:`I_{l.o.s.}` on the generic function :math:`f` (valid for spherical or triaxial haloes):

.. math::

    I_{l.o.s.}(\psi,\theta, \Delta\Omega) = \int_{\Delta\Omega} {\rm d}\Omega\int_{l.o.s.} f(l,\psi,\theta) \,{\rm d}l\,,

where the :abbr:`l.o.s. (line of sight)` is defined by the angles :math:`(\psi,\theta)` w.r.t. the Galactic centre direction, as shown in :numref:`fig_geometry`. The integration :math:`\int_{\Delta\Omega}` over the solid angle is described by the two angles :math:`(\alpha,\beta)`. In practice, :math:`(l,\alpha,\beta)` are spherical coordinates over which the integration is performed, and for which the origin of the framework is Earth location (also see :numref:`fig_geometry`). Hence, using

.. math::

    \int_{\Delta\Omega} {\rm d}\Omega= \int_{0}^{2\pi} d\beta \int_{0}^{\alpha_{\rm int}} \sin(\alpha) {\rm d}\alpha\, ,


where :math:`\Delta\Omega` and :math:`\alpha_{\rm int}` are related by

.. math::

   \Delta\Omega = 2\pi\times[1-\cos(\alpha_{\rm int})] \, ,

we can rewrite :math:`I_{l.o.s.}` to be

.. math::  I_{l.o.s.}(\psi,\theta, \Delta\Omega) = \int_{0}^{2\pi} F_\beta \,{\rm d}\beta\,,
   :label: eq_1

with

.. math:: F_\beta = \int_{0}^{\alpha_{\rm int}} F_{(\beta,\alpha)} \,{\rm d}\alpha
   :label: eq_2

and

.. math:: F_{(\beta,\alpha)} = \sin(\alpha) \int_{l_{\rm min}}^{l_{\rm max}} f(l,\beta,\alpha; \psi,\theta) \,{\rm d}l\,.
   :label: eq_3

In practice, this amounts to performing three integrations: over :math:`\beta` in :eq:`eq_1`, over :math:`\alpha` in :eq:`eq_2`, and over :math:`l` in :eq:`eq_3`. :program:`CLUMPY` relies on a Simpson integration scheme in linear or logarithmic adaptive steps, and to optimise the computation time, the integration along :math:`l` is broken down into several parts, as described in the following:.

Strategy and tricks
++++++++++++++++++++

The DM halo density profile can be very cuspy at its centre, and thus it is hard to integrate over the :abbr:`l.o.s. (line of sight)` with a regular integration step. We can take advantage of the quasi-sphericity of the clumps to tackle this problem. In
any case, integrating a clump or the smooth DM halo requires, most of the time, a similar strategy.

.. note::
 For the smooth component, when we integrate over the solid angle, the distance :math:`l_{\rm max}` (distance from Earth to the Galaxy DM halo boundary) slightly changes. However, for instance, for :math:`\Delta\Omega<10^{-3}`, the change is smaller than a few :math:`\rm pc`: as in practice there is no clear boundary of the DM halo, this is unimportant, and for simplicity, we keep the same :math:`l_{\rm max}` for the integration over :math:`(\alpha,\beta)`.

**General integration over** :math:`\alpha` **and** :math:`\beta`

   The integration range is usually small on these parameters and a standard adaptive Simpson routine using linear steps is used. Indeed, the integral is not varying much over this range, unless, e.g., we wish to integrate slightly offset from the density peak centre, in which case the linear integration scheme used on these variable is less efficient. But still, the calculation converges, and we did not make any attempt to improve the situation in this very small :abbr:`l.o.s. (line of sight)` region.


**Strategy for the l.o.s. integration**

  DM profiles are decreasing power-law functions of their radius :math:`r` (see :ref:`rst_physics_profiles`). To discuss our strategy, we shall consider the following symbolic integration:

   .. math::

      [F_{(\beta,\alpha)}=] \,F \, \equiv \int_{0}^{l_{\rm max}} f(l,\phi) \,{\rm d}l

  Note that tackling the integration over the range :math:`[l_{min}-l_{max}]` instead (for instance for a clump), implies more  if/else statement in the code, but are irrelevant for the discussion below. For the sake of brevity, we define the quantities (see also figure below):

      - :abbr:`l.o.s. (line of sight)`: line of sight;
      - :abbr:`l.o.i. (line of integration)`: line of integration (for a given :math:`\alpha,\beta` direction around the :abbr:`l.o.s. (line of sight)`);
      - :abbr:`F.O.I. (field of integration)`: field of integration (usually corresponds to the solid angle defined by the instrument resolution);
      - :math:`r_{\rm trick}`: largest radius (measured from the clump centre) for which :math:`\rho(r<r_{\rm trick})` still falls in the :abbr:`F.O.I. (field of integration)`


   - **Special case 1**: If :math:`\phi_{l.o.i.}>\pi/2` (where :math:`\phi_{l.o.i.}` is the angle between the GC and the :abbr:`l.o.i. (line of integration)`) and if this is not a clump, :math:`\rho^2` monotonically decreases with :math:`l`. A mere log-step integration suffices:

      .. math:: F = \int_{\epsilon}^{l_{\rm max}} l\;\times\; f(l,\phi) \,{\rm d}\ln l

   - **Special case 2**:  If :math:`\phi_{l.o.i.}<\pi/2` (or if we integrate a clump anywhere in the Galaxy), :math:`\rho^2` is no longer monotonic.  Based on a simple geometric argument (halos are quasi-sphericals), the density peaks, for the Galactic halo, at :math:`l_{\rho_{\rm max}} = \cos(\phi)/R_\odot`. Hence, the integral can be broken down in two parts, both of them being now monotonic. An offset applied to :math:`l` allows to take advantage of the Simpson log-step integral (as the integrand is a almost power-law decreasing function from both sides of :math:`l_{\rho_{\rm max}}`):

      .. math:: F = I_1 + I_2\,,

     with

      .. math::

         I_1 = \int_{\epsilon}^{l_{\rm max}-l_{\rho_{\rm max}}} l'\;\times\; f_{\rm of\!fset}(l',\phi) \,{\rm d}\ln l'
           \quad {\rm having} \quad l' = l_{\rho_{\rm max}}-l

     and

      .. math::
           I_2 = \int_{\epsilon}^{l_{\rho_{\rm max}}} l'\;\times\; f_{\rm of\!fset}(l',\phi) \,{\rm d}\ln l'
           \quad {\rm having} \quad l' = l-l_{\rho_{\rm max}}\,.

     This simple and efficient strategy is explained in the :numref:`fig_geom_integr_smooth` below (it is for a clump not coincident with the GC, but the spirit is the same would the DM smooth component only be considered). The two-part split at :math:`l_{\rho_{\rm max}}` is seen on the :abbr:`l.o.i. (line of integration)` in dotted blue on the left:

      .. _fig_geom_integr_smooth:

      .. figure:: DocImages/geom_integr_smooth.png
         :align: center
         :figclass: align-center
         :width: 75%

         Integration strategy along the :abbr:`l.o.s. (line of sight)` Click to enlarge the figure.

  **Remark 1**: If the integration region encompasses the position of the DM density peak, we treat the central part as a point-like contribution. We define :math:`r_{\rm trick}` to be the maximum radius for which we are still in the solid angle of the integration and for which the point-like criterion is met (i.e. :math:`r_{\rm trick}\leq f \;\times; l_{\rm cl}`, where :math:`f\sim 10^{-3}`, in which case :math:`l` can be considered constant and factors out of the integration). The rest is a question of geometry to get correctly the various quantities to test and skip the region within :math:`r_{\rm trick}` to avoid double-counting.

  **Remark 2**: If the field of integration crosses the clump centre (this is rare, but not impossible), the Simpson routine again is stuck. There is no simple and clean fix to this problem. For the time being we simply integrate on a slightly smaller field of integration to get rid of this stiff point.


**Back to** :math:`\alpha` **and** :math:`\beta` **for a clump out of the l.o.s.**

   When the :abbr:`l.o.s. (line of sight)` falls completely out of the clump (but chunks of it are still encompassed in the integration angle), most of the integration angles :math:`\alpha` and :math:`\beta` contribute to zero and the simpson routine gets stuck. :numref:`fig_geom_int_clump` shows this situation (projected on the sky).

   .. _fig_geom_int_clump:

   .. figure:: DocImages/geom_int_clump.png
      :align: center
      :figclass: align-center
      :width: 60%

      Integration strategy for a clump out of the :abbr:`l.o.s. (line of sight)`: face-on view

   - Strategy for :math:`\alpha`:

      The centre of the :abbr:`F.O.I. (field of integration)` corresponds to :math:`\alpha=0`. We replace this lower integration boundary by :math:`\alpha_{\rm min} = \psi_{l.o.s.,\,\rm cl_{centre}} - \alpha_{\rm cl}` where :math:`\alpha_{\rm cl} = \sin^{-1}(R_{\rm cl}/l_{\rm cl})` is the angular size of the clump in the sky (this obviously equals 0 is the centre of the :abbr:`F.O.I. (field of integration)` touches exactly the clump boundary).

   - Strategy for :math:`\beta`:

      We calculate the minimal integration range required :math:`\Delta\beta`, which from :numref:`fig_geom_int_clump`, is given by

        .. math:: \Delta\beta = \min(\beta_{\rm view},\beta_{\rm intersect})\,.

      On the right-hand side, the first angle is the half angle which encompasses the clump as seen from a point at a distance :math:`l_{\rm cl}` on the :abbr:`l.o.s. (line of sight)`. This angle is indeed maximal when the distance on the :abbr:`l.o.s. (line of sight)` equals the distance of the clump, i.e. when the distance between the :abbr:`l.o.s. (line of sight)` and the clump-centre, denoted :math:`d_{(l.o.s.)-(\rm cl)}` is minimal:

        .. math:: \beta_{\rm view} = \sin^{-1}(R_{\rm cl}/d_{(l.o.s.)-(\rm cl)})\,.

      The second angle is where the :abbr:`F.O.I. (field of integration)` intersects the clump boundary. It is given by the Al-Kashi theorem

        .. math::

           \beta_{\rm intersect} = \cos^{-1}\left(
             \frac{R_{\rm cl}^2 + R_{\rm F.O.I.}^2 - d_{(l.o.s.)-(\rm cl)}^2}{2 \; R_{\rm cl} \times R_{\rm F.O.I.}}
             \right)

      where :math:`R_{\rm F.O.I.}  = \sin(\alpha_{\rm int}) \times l_{\rm cl}`.

      We finally have (symbolic notation)

        .. math::
          \int_{0}^{2\pi} F_\beta\, {\rm d}\beta = 2 \int_{\beta_{\rm ref}}^{\beta_{\rm ref}+\beta_{\rm integr}} F_\beta\, {\rm d}\beta
              = 2 \int_{\beta_{\rm ref}-\beta_{\rm integr}}^{\beta_{\rm ref}} F_\beta\, {\rm d}\beta\,,

      where :math:`\beta_{\rm ref}` is the angle between :math:`\beta=0` (i.e. the :math:`z'` axis, see :numref:`fig_geometry`) and the centre of the clump.

|
      
.. container:: twocol

   .. container:: leftside

      :numref:`fig_callgrind_h5` finally shows a `gprof2dot <https://github.com/jrfonseca/gprof2dot>`__  caller flowchart for the :option:`-h5` module example from the :ref:`rst_quick_start` (for every called function, it is shown the time spent in this function and all its children, in parentheses the time spent in the function alone, and the total number of function calls). Unsurprisingly, most of the run time is spent in integrating (:math:`\tt simpson\_log\_adapt`  and :math:`\tt trapeze\_log\_refine`, see `integr.h <doxygen/integr_8h.html>`__ for more details about CLUMPY's integrators). Note how these integrals, being mostly line-of-sight integrals, are called by the `integr_los.h  <doxygen/integr__los_8h.html>`__ core function :math:`\tt los\_integral\_mix()` optimising the calculation.

   .. container:: rightside

      .. _fig_callgrind_h5:

      .. figure:: DocImages/callgrind_h5.png
         :scale: 16%
         :align: right
         
         
         Caller flowchart
      