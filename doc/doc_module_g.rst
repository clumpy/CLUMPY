.. _rst_doc_module_g:

Galactic (Milky Way): :math:`\tt -g`
------------------------------------

Run for a  Galactic halo + list of haloes (optional)

.. note:: All default parameter files from the following run examples are generated with the :option:`D` flag.

   .. code-block:: console

    $ clumpy -gXD


:math:`\tt -g0`: mass profile (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`M_{\rm Gal.}(r)` in one dimension:

.. code-block:: console

    $ clumpy -g0 -i clumpy_params_g0.txt


.. figure:: DocImages/g0D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Mass profile of the Galactic DM halo.

Note that a triaxial halo can be studied as well.

------------

.. _rst_g1:

:math:`\tt -g1`: density profiles (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`\rho_{\rm Gal.}(r)` (smooth, sub-continuum and total) of a  **spherically symmetric** halo in one dimension:

.. code-block:: console

    $ clumpy -g1 -i clumpy_params_g1.txt


.. figure:: DocImages/g1D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Density profile of the Galactic DM halo. This is :numref:`fig_g1D` already created in the :ref:`rst_quick_start`.

------------

:math:`\tt -g2`: *J*-factor profiles (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`J(\alpha_{\rm int};\,l,b)` towards a user-defined direction :math:`(l,b)` in the Galactic halo. Note that a triaxial halo can be studied as well.

.. code-block:: console

    $ clumpy -g2 -i clumpy_params_g2.txt


.. figure:: DocImages/g2D-1.png
   :align: center
   :figclass: align-center
   :scale: 65%

   *J*-factor (of smooth, sub-continuum and total components) towards the Galactic anti-center as a function of the integration radius :math:`\alpha_{\rm int}` of the search cone :math:`\Delta\Omega`.

.. figure:: DocImages/g2D-2.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Flux (here: integrated in energy) towards the Galactic anti-center as a function of the integration radius :math:`\alpha_{\rm int}`.

------------

:math:`\tt -g3`: :math:`{\rm d}J/{\rm d}\Omega` (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Calculates :math:`{\rm d}J/{\rm d}\Omega(\theta)` as a function of the distance :math:`\theta` from the centre of a  **spherically symmetric** Galactic halo.

.. code-block:: console

    $ clumpy -g3 -i clumpy_params_g3.txt


.. figure:: DocImages/g3D-1.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :math:`{\rm d}J/{\rm d}\Omega` (of smooth, sub-continuum and total components) over the distance :math:`\theta` from the Galactic centre.


Like in the :option:`-g2` module, also the corresponding :math:`\gamma`-ray and neutrino intensity can be calculated if enabled in the parameter file:

.. figure:: DocImages/g3D-3.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :math:`{\rm d}\Phi/{\rm d}\Omega` in :math:`\gamma`-rays and neutrinos over the distance :math:`\theta` from the Galactic centre.


.. As a legacy from the previous code releases, also the *J*-factors  for a fixed, user-defined integration angle :math:`\alpha_{\rm int}` as a function of the :math:`\theta` are output as well. Note the difference between above and below figures:

.. .. figure:: DocImages/g3D-2.png
..    :align: center
..    :figclass: align-center
..    :scale: 65%

..    :math:`J(\theta;\,\alpha_{\rm int})` (of smooth, sub-continuum and total components) over the distance :math:`\theta` from the Galactic centre.


.. .. figure:: DocImages/g3D-4.png
..    :align: center
..    :figclass: align-center
..    :scale: 65%

..    Corresponding fluxes :math:`\Phi` in :math:`\gamma`-rays and neutrinos over the distance :math:`\theta` from the Galactic centre.


------------

:math:`\tt -g4`: :math:`J_{\alpha_{\rm int}}(\theta)` (1D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


Returns the :math:`J(\theta)` and optionally, the corresponding *fluxes*, for a fixed, user-defined integration angle :math:`\alpha_{\rm int}` as a function of the distance :math:`\theta`  from the Galactic centre. Any *J*-factors  :math:`J_{\rm list}(\alpha_{\rm int})` from haloes defined in the file :option:`gLIST_HALOES` are overplotted as well:

.. code-block:: console

    $ clumpy -g4 -i clumpy_params_g4.txt

.. figure:: DocImages/g4D-1.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Galactic :math:`J(\theta;\,\alpha_{\rm int})` (of smooth, sub-continuum and total components) over the distance :math:`\theta` from the Galactic centre.


.. figure:: DocImages/g4D-2.png
   :align: center
   :figclass: align-center
   :scale: 65%

   Corresponding Galactic DM fluxes :math:`\Phi` in :math:`\gamma`-rays and neutrinos over the distance :math:`\theta` from the Galactic centre.


------------

:math:`\tt -g5`: :math:`\langle J_{\rm pixel}\rangle` and :math:`\langle {\rm d}J/{\rm d}\Omega\rangle` (2D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. code-block:: console

    $ clumpy -g5 -i clumpy_params_g5.txt


.. figure:: DocImages/g5D.png
   :align: center
   :figclass: align-center
   :scale: 65%

   :math:`4^{\circ}\times 4^{\circ}` skymap of :math:`J(\rm pixel)` towards the Galactic anti-center without resolving individual Galactic subhaloes.

Corresponding flux- and intensity maps for all :option:`-g5` to :option:`-g8`  are calculated  with the :option:`gSIM_IS_WRITE_FLUXMAPS = True` enabled.

------------

:math:`\tt -g6`: g5 + list haloes (2D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Same as :option:`-g5`, but allows to draw individual resolved haloes from a list defined in :option:`gLIST_HALOES` into the skymap.

.. code-block:: console

    $ clumpy -g6 -i clumpy_params_g6.txt


.. figure:: DocImages/g6D.png
   :align: center
   :figclass: align-center
   :scale: 60%

   :math:`90^{\circ}\times 45^{\circ}` skymap of :math:`J(\rm pixel)` in cartesian  (left) and Hammer-Aitoff (right) projection towards the Galactic centre without resolving Galactic subhaloes, but including a user-defined halo at :math:`(l,b) =(20^{\circ},20^{\circ})`. Note the user-drawn halo at :math:`(l,b) =(20^{\circ},20^{\circ})` faintly visible in the right panel.



.. note:: Only Galactic objects with redshift :math:`z\ll 1` can be added in these maps in the current release.

------------

:math:`\tt -g7`: :math:`J_{\rm pixel}` and :math:`{\rm d}J/{\rm d}\Omega` + drawn subhaloes (2D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Same as :option:`-g5`, but allows to draw individual *randomly drawn* resolved Galactic subhaloes from a list defined in :option:`gLIST_HALOES` into the skymap.

.. code-block:: console

    $ clumpy -g7 -i clumpy_params_g7.txt


.. figure:: DocImages/g7D.png
   :align: center
   :figclass: align-center
   :scale: 60%

   :math:`4^{\circ}\times 4^{\circ}` skymap of :math:`J(\rm pixel)` in cartesian projection towards the Galactic anti-center with resolved and unresolved Galactic subhaloes. The left panel shows the components (emission from smooth Galactic halo, unresolved halos, cross-product, and resolved halos), the right panel the sum.

In this option run, for the selected :option:`gSIM_USER_RSE` threshold, 54 subhaloes are resolved. If more than 20 subhaloes are resolved in a run, the following interactive :program:`ROOT` population study plots are generated:

.. figure:: DocImages/g7D-popstudy.png
   :align: center
   :figclass: align-center
   :scale: 73%

   Plots to study the proerties of the resolved subhalo population in the default :option:`-g7` run.

------------

:math:`\tt -g8`: g7 + list haloes (2D)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The :option:`-g8` module does the same as :option:`-g7`, however, allows to additionally draw user-defined haloes from a list, stored in a file called by the :option:`gLIST_HALOES` parameter:

.. code-block:: console

    $ clumpy -g8 -i clumpy_params_g8.txt

.. _fig_g8D:

.. figure:: DocImages/g8D.png
  :align: center
  :figclass: align-center
  :scale: 60%

  :math:`4^{\circ}\times 4^{\circ}` skymap of :math:`J(\rm pixel)` in cartesian projection towards the Galactic anti-centre with resolved and unresolved Galactic subhaloes plus the example halo ``rs10_gamma10`` read from the definition file :file:`$CLUMPY/data/list_generic.txt`.

.. note ::

    You need the :envvar:`CLUMPY` environmental variable pointing to your installation directory in order to find :file:`list_generic.txt`.