.. _rst_doc_module_o:

Export/append to skymaps file: :math:`\tt -o`
--------------------------------------------

Export 2D maps in ASCII and full-sky FITS maps.


:math:`\tt -o0`: Content of FITS files
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

        $ clumpy -o0 -i clumpy_o0.txt

lists the content (extensions) of :file:`skymap.fits`.


:math:`\tt -o1`: Conversion to ASCII files
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

        $ clumpy -o1 -i clumpy_o1.txt

writes the first extension of :file:`skymap.fits` into an ASCII file (columns separated by blanks). The conversion preserves all information and provides besides the unique :program:`HEALPix` pixel number also the coordinates :math:`(\psi,\theta)` of the pixel centre.

--------------------------

:math:`\tt -o2`: Recreate an input parameter file from a  :program:`FITS` header
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

        $ clumpy -o2 -i clumpy_o2.txt

extracts the header keywords from the first extension in the file :file:`skymap.fits` and saves a :program:`CLUMPY` parameter file which can be used to reproduce the content of the chosen :program:`FITS` extension.

--------------------------

:math:`\tt -o3`: Part-sky to full-sky  :program:`FITS` maps
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


.. code-block:: console

        $ clumpy -o3 -i clumpy_o3.txt

extracts the second column from the first extension in the file :file:`skymap.fits` and stores it in a new :program:`FITS` file as fullsky map with implicit pixel labelling. If :file:`skymap.fits` contains part-sky maps, pixels outside the field of view are filled with :program:`HEALPix`' blind value, :math:`-1.6375\times 10^{30}`.

.. warning::

   Blowing up small-field-of-view high-resolution skymaps to fullsky maps (mostly filled with :program:`HEALPix`' blind value) may produce huge (>GB) files.

--------------------------


.. hint:: Using any of the :option:`-o1`, :option:`-o2`, or :option:`-o3` modules without specifying an extension or column, prints the content of the :program:`FITS` file. E.g., using the result from the :ref:`rst_quick_start`:

    .. code-block:: console

        $  clumpy -o0 -i clumpy_o0.txt

   prints

   .. code-block:: console

       ...

       >>>>> read FITS headers from file $CLUMPY_DATA/jfactor_map_from_h5.fits
          => FITS file contains 3 extensions:

             Extension 1 :
               name of this extension: JFACTOR
               physical maps in this extension: 5
                  map 0: PIXEL                [Healpix Pixel indices]
                  map 1: Jtot                 Units: GeV^2 cm^-5
                  map 2: Jsmooth              Units: GeV^2 cm^-5
                  map 3: Jsub                 Units: GeV^2 cm^-5
                  map 4: Jcrossp              Units: GeV^2 cm^-5
                  map 5: Jdrawn               Units: GeV^2 cm^-5
               n_side of the maps: 1024
               ordering scheme of the maps: 1
               number of pixels (n_pix) in the maps: 960

             Extension 2 :
               name of this extension: JFACTOR_PER_SR
               physical maps in this extension: 5
                  map 0: PIXEL                [Healpix Pixel indices]
                  map 1: Jtot_per_sr          Units: GeV^2 cm^-5 sr^-1
                  map 2: Jsmooth_per_sr       Units: GeV^2 cm^-5 sr^-1
                  map 3: Jsub_per_sr          Units: GeV^2 cm^-5 sr^-1
                  map 4: Jcrossp_per_sr       Units: GeV^2 cm^-5 sr^-1
                  map 5: Jdrawn_per_sr        Units: GeV^2 cm^-5 sr^-1
               n_side of the maps: 1024
               ordering scheme of the maps: 1
               number of pixels (n_pix) in the maps: 960

         N.B.: for the -o3 option, the size of the whole-sky FITS files
               will be about ~ GB (with lots of BLIND values), for NSIDE ~ 8000!
