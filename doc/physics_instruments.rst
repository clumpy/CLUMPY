.. _rst_physics_instruments:

.. sectionauthor:: Moritz Huetten <moritz.huetten@desy.de>

.. role::  raw-html(raw)
    :format: html

Detector-related interfaces
---------------------------


2D output for gammalib
~~~~~~~~~~~~~~~~~~~~~~

:program:`CLUMPY` results can be easily fed into the :program:`gammalib/ctools` framework:


.. code-block:: console

   $ python

.. code-block:: python

    >>> from gammalib import *
    >>> signalmodels = GModels("DM-model-from-CLUMPY.xml")
    >>> print signalmodels[0]

where the file :file:`DM-model-from-CLUMPY.xml` contains a DM model separated into a spatial and spectral part:

.. code-block:: xml

   <?xml version="1.0" standalone="no"?>
   <source_library title="source library">
           <source name="Perseus" type="DiffuseSource">
           <spectrum type="FileFunction" file="spectrum.dat">
               <parameter scale="1.0" name="Normalization" min="0.0" max="1000.0" value="1.0" free="0"/>
          </spectrum>
           <spatialModel type="SpatialMap" file="jfactor-map-image.fits" normalize="1">
               <parameter scale="1" name="Prefactor" min="0.001" max="1000.0" value="1" free="0"/>
           </spatialModel>
           </source>
   </source_library>



The :option:`-z` module (see :numref:`rst_doc_module_z`) provides already the spectral ASCII file :file:`spectrum.dat` in the correct format required by :program:`gammalib`. 


While :program:`gammalib` is technically supposed to be able to read full-sky :program:`HEALPix` :program:`FITS` maps, we have experienced that piping :program:`CLUMPY` 's high-resolution :program:`HEALPix` maps into :program:`gammalib` is inconveniently slow. Therefore, we provide with the ``pyClumpy_demo_notebooks/makeFitsImage.py`` script a tool to convert a :program:`HEALPix` :program:`FITS` maps into a :program:`FITS` image :file:`jfactor-map-image.fits`, which can be also read by :program:`gammalib`.


Therefore, take the output file ``annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits`` from the :ref:`rst_quick_start`. Similar to converting columns of the output file to full-sky maps or ASCII (see :numref:`rst_FITS_display`), a single column can also be converted into a :program:`FITS` *image*, with the help of the ``makeFitsImage.py`` :program:`Python` script located in the folder ``pyClumpy_demo_notebooks/``:

.. code-block:: console

    $ python pyClumpy_demo_notebooks/makeFitsImage.py -i output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits -e 3 -c 3
    
This command reads the third column (``Intensity_gamma``) of the third extension (``INTEGRATED_FLUXES``) of the input file and creates an output file ``annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024-INTEGRATED_FLUXES-Intensity_gamma-image.fits``. This file contains a :program:`FITS` image in cartesian projection, which can be viewed with common `FITS` viewers (e.g., :program:`ds9`, :program:`fv`,...). 

.. note:: :raw-html:`<b><a href=http://github.com/healpy/healpy/ target="_blank">healpy</a></b>`  and :raw-html:`<b><a href=http://www.astropy.org/ target="_blank">astropy</a></b>` are required to run the ``makeFitsImage.py`` script.

Here we provide a simple snippet of how to display the output :program:`FITS` image using :program:`Python` :program:`astropy`:

.. code-block:: python

    >>> import matplotlib.pyplot as plt
    >>> from matplotlib.colors import LogNorm
    >>> from astropy.wcs import WCS
    >>> from astropy.io import fits
    
    >>> filename = 'annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024-INTEGRATED_FLUXES-Intensity_gamma-image.fits'
    >>> hdu = fits.open(filename)[0]

    >>> ax = plt.subplot(projection=WCS(hdu.header))
    WARNING: FITSFixedWarning: CDELTSTD= 0. / log-std. deviation of subhalo scattering
    keyword looks very much like CDELTia but isn't. [astropy.wcs.wcs]
    >>> im = ax.imshow(hdu.data, norm=LogNorm(vmax = hdu.data.max()/10), origin='lower')
    >>> ax.set_xlabel(hdu.header['CTYPE1'])
    >>> ax.set_ylabel(hdu.header['CTYPE2'])

    >>> cbar = plt.gcf().colorbar(im, ax=ax, orientation="vertical")
    >>> cbar.set_label(hdu.header['BUNIT'])
    
    >>> plt.show()
    
 
.. _fig_h5D-pythonplot-intensity-fitsimage:

.. figure:: DocImages/h5D-pythonplot-intensity-fitsimage.png
   :align: center
   :figclass: align-center

   Intensity skymap from the default :option:`-h5` module, transformed to an :program:`FITS` image with the ``pyClumpy_demo_notebooks/makeFitsImage.py`` script and displayed with :program:`astropy`.

.. important ::

   The :option:`normalize="1"` in the above example of :file:`DM-model-from-CLUMPY.xml` is crucial. It  means that the spatial map is normalised to :math:`\int_{S^2} {\rm map}\,{\rm d}\Omega = 1` and the file :file:`spectrum.dat` must contain the total flux within the extension of the spatial model. This is needed, because **the converted FITS image does not preserve equi-areal pixels and cannot be used to calculate *J*-factors or fluxes by summing up pixel values.** 
    
   However, in the header of an transformed  :program:`CLUMPY` :program:`FITS` *J*-factor image, we store the precise total *J*-factor of the entire map based on the original :program:`HEALPix` map under the :program:`FITS` keyword :keyword:`FLUX_TOT`. When generating :file:`spectrum.dat`, this value must be used as the :option:`gSIM_JFACTOR` value. The same applies for the redshift of the source, which has to be taken into account when generating the spectrum:
   
   .. code-block:: console
   
      $ python pyClumpy_demo_notebooks/makeFitsImage.py -i output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024.fits -e 1 -c 1

      $ JTOT=`python pyClumpy_demo_notebooks/printKeywordValue.py -i output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024-JFACTOR-Jtot-image.fits -k FLUX_TOT`
      $ REDSHIFT=`python pyClumpy_demo_notebooks/printKeywordValue.py -i output/annihil_rs01_gamma052D_FOVdiameter2.0deg_nside1024-JFACTOR-Jtot-image.fits -k REDSHIFT`

      $ clumpy -z0D
      $ clumpy -z0 -i clumpy_params_z.txt  --gSIM_JFACTOR=$JTOT --gSIM_REDSHIFT=$REDSHIFT --gSIM_XPOWER=0

   Make sure the option :option:`--gSIM_XPOWER=0` is set.

   .. code-block:: console
   
      ...
      
      ... output written in: output/spectra_CIRELLI11_EW_GAMMA_m100.txt
                             output/dnde_CIRELLI11_EW_GAMMA_m100.txt
      ... output [use ROOT TBrowser] written in: output/spectra_CIRELLI11_EW_GAMMA_m100.root [this line only appears if ROOT is linked]
      _______________________


   Use the file :file:`output/spectra_CIRELLI11_EW_GAMMA_m100.txt` for your spectral file. Possibly you have to remove the header line ``#  E [MeV]   Flux [# cm^{-2} s^{-1}  MeV^{-1}]`` to make it being accepted by :program:`gammalib`.

----------------------

2D Gaussian smoothing
~~~~~~~~~~~~~~~~~~~~~

Using the :program:`HEALPix` library routines, :program:`CLUMPY` now provides the options of smoothing the output *J*-factor skymaps with a Gaussian beam and of calculating the angular power spectrum (APS) of the maps. Up to two smoothing kernels may be specified (e.g., one for a :math:`\gamma`-ray intrument and one of a neutrino observatory), both being applied on the same output map originally computed by :program:`CLUMPY`. In order to avoid smoothing the edges of part-sky or masked maps, :program:`CLUMPY` appropriately extends the original grid when smoothing is requested by the user. The APS calculation is limited to the Galactic mode ./bin/clumpy  -g and is performed independently for the total *J*-factor  profile of the halo :math:`J_{\rm tot}`, the smooth halo contribution :math:`J_{\rm sm}`, and the substructures only (if present).

Executing

.. code-block:: console

    $ clumpy -g8D
    ...
    
    $ clumpy -g8 -i clumpy_params_g8.txt --gSIM_GAUSSBEAM_GAMMA_FWHM_DEG=0.2

gives (compare to :numref:`fig_g8D`):

.. figure:: DocImages/g8D-smoothed.png
   :align: center
   :figclass: align-center
   :scale: 60%

.. seealso ::

   :download:`here for a Python Jupyter-notebook example of how display the APS output <DocData/plotClumpyAPS.ipynb>`


   