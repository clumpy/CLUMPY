set( GREAT_ENV "$ENV{GREAT}" )
if( EXISTS ${GREAT_ENV} )
   message( "-- Looking for GreAT... - Found in ${GREAT_ENV}" )
   set( GREAT_INCLUDES ${GREAT_ENV}/fparser4.5.1 ${GREAT_ENV}/Manager/include ${GREAT_ENV}/MCMC/include )
   set( GREAT_LIBRARIES "-L${GREAT_ENV}/lib -lgreat" )
   include_directories( ${GREAT_INCLUDES}  )
   link_directories( ${GREAT_LIBRARIES} )
   return()
else()
    message( FATAL "-- Looking for GreAT...  - Not found:" )
    message( FATAL "    N.B.: if you want to compile and use src/jeansMCMC.cc, install GreAT, see instructions at https://clumpy.gitlab.io/CLUMPY/install.html)." )
endif()

