# RPATH handling. This makes setting LD_LIBRARY_PATH unecessary
# as it will compile the path to the libraries into the libraries / executables
include(GNUInstallDirs)
file(RELATIVE_PATH libdir_relative
    ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}
    ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}
)
if(APPLE)
    list(APPEND CMAKE_INSTALL_RPATH @loader_path)
    list(APPEND CMAKE_INSTALL_RPATH @loader_path/${libdir_relative})
    list(APPEND CMAKE_INSTALL_RPATH @executable_path/${libdir_relative})
else()
    list(APPEND CMAKE_INSTALL_RPATH $ORIGIN)
    list(APPEND CMAKE_INSTALL_RPATH $ORIGIN/${libdir_relative})
endif()

# If installing via pip/skbuild the executables and libs will not
# necessarily end up next to each other 
if(SKBUILD AND NOT CONDA_BUILD)
    find_package(Python REQUIRED)
    list(APPEND CMAKE_INSTALL_RPATH "${Python_SITELIB}/${CMAKE_INSTALL_LIBDIR}")
endif()

set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
