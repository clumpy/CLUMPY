# Modern cfitsio supports cmake, try finding it via config
MESSAGE(STATUS "Try to find cfitsio using find_package")
find_package(cfitsio CONFIG)
if(cfitsio_FOUND)
    MESSAGE(STATUS "Found cfitsio via cmake config in ${cfitsio_library}")
    # Linking to cfitsio fails on macos without setting properties
    list(GET cfitsio_LINK_LIBRARIES 0 cfitsio_library)
    add_library(cfitsio SHARED IMPORTED)
    set_target_properties(cfitsio PROPERTIES IMPORTED_LOCATION ${cfitsio_library})
    return()
endif(cfitsio_FOUND)

# if not, try again via pkg-config
MESSAGE(STATUS "Try to find cfitsio using PkgConfig instead")
find_package(PkgConfig REQUIRED)
pkg_search_module(cfitsio REQUIRED cfitsio)
if(cfitsio_FOUND)
    MESSAGE(STATUS "Found cfitsio ${cfitsio_VERSION} via pkg-config in ${cfitsio_LINK_LIBRARIES}")
    # Linking to cfitsio fails on macos without setting properties
    list(GET cfitsio_LINK_LIBRARIES 0 cfitsio_library)
    add_library(cfitsio SHARED IMPORTED)
    set_target_properties(cfitsio PROPERTIES IMPORTED_LOCATION ${cfitsio_library})
    return()
endif(cfitsio_FOUND)

message(FATAL "cfitsio not found")
