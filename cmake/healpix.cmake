find_package(PkgConfig REQUIRED)
pkg_search_module(healpix_cxx REQUIRED healpix_cxx)

cmake_policy(SET CMP0111 NEW)
list(GET healpix_cxx_LINK_LIBRARIES 0 healpix_cxx_library)
message(STATUS "Found healpix_cxx in ${healpix_cxx_library}")

add_library(healpix_cxx SHARED IMPORTED)
set_target_properties(healpix_cxx PROPERTIES IMPORTED_LOCATION ${healpix_cxx_library})
target_include_directories(healpix_cxx INTERFACE ${healpix_cxx_INCLUDE_DIRS})
target_link_libraries(healpix_cxx INTERFACE ${healpix_cxx_LIBRARIES})
