{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **pyClumpy.Jeans Demo**\n",
    "In this notebook we will provide a demonstration on how to work the data output from the ``pyClumpy.Jeans`` module for statistical Jeans-Analysis. For more information on how such an analysis is done we refer to the ``CLUMPY`` [documentation](https://clumpy.gitlab.io/CLUMPY/physics_jeans.html) on the topic. Note that here we are using python analysis tools [``iMinuit``](https://github.com/scikit-hep/iminuit) and [``pyMC``](https://github.com/pymc-devs/pymc) instead of the C analysis tools refered to there."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First will be an example on how to run a simple Minuit minimisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from iminuit import Minuit\n",
    "import pyClumpy.Jeans as jeans\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We provide internal load-methods for both data and config of CLUMPY-style files which return dictionaries of all data; alternatively you may (at your own risk) create these dictionaries yourself through any method you choose"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = os.environ['CLUMPY_DATA']\n",
    "jeans_data = jeans.load_data(data_path + \"/data_sigmap.txt\")\n",
    "jeans_config = jeans.load_config(data_path + \"/params_jeans.txt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating the cost-function for the chi2-minimisation is as simple as using a lambda-function on our provided chi2-method giving it the previously loaded data- and config-dictionaries as permanent parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cost_function = lambda params : jeans.chi2(params, jeans_data, jeans_config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating the minimiser with Miniuit then only needs the start-values for our free parameters which are conviently stored in the jeans_config"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minimiser = Minuit(cost_function, jeans_config[\"ParJeans\"][jeans_config[\"IsFreePar\"]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parsing the limits from the provided lower and upper ranges of our free parameters and assigning them to the minimiser"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "limits = np.array([(jeans_config[\"RangeLo\"][i], jeans_config[\"RangeUp\"][i])     for i in range(0, np.size(jeans_config[\"ParJeans\"]))])\n",
    "limits = limits[jeans_config[\"IsFreePar\"]]\n",
    "\n",
    "minimiser.limits = limits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Minuit can then run several different minimisations on this data and configuration; here we chose the migrad-minimisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minimiser.migrad()\n",
    "minimiser.hesse()\n",
    "\n",
    "print(\"Results:\")\n",
    "names = np.array(jeans_config[\"ParNames\"])\n",
    "for i in range(0, np.shape(names[jeans_config[\"IsFreePar\"]])[0]):\n",
    "    name = names[jeans_config[\"IsFreePar\"]][i]\n",
    "    if jeans_config[\"IsLogVal\"][jeans_config[\"IsFreePar\"]][i] == True:\n",
    "        name = \"Log(\" + name + \")\"\n",
    "    print(name, \": \", minimiser.values[i], \" +- \", minimiser.errors[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now to plot the resulting sigmap-values we must feed the sigmap2 method a numpy-array of R-values and either ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "R_scale = np.logspace(-2.3, -1, 1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "a) the full and updated jeans parameters (values of which are provided in the jeans_config; the free parameters need to be updated and the logarithmic values need to be un-set)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_params = jeans_config[\"ParJeans\"]\n",
    "full_params[jeans_config[\"IsFreePar\"]] = minimiser.values\n",
    "full_params[jeans_config[\"IsLogVal\"]] = 10**full_params[jeans_config[\"IsLogVal\"]]\n",
    "data = np.sqrt(jeans.sigmap2(R_scale, full_params))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "b) the jeans_config itself with the free parameters updated to its post-minimisation values. No need to un-set the logarithmic values as this is done on CLUMPYs end automatically"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "jeans_config[\"ParJeans\"][jeans_config[\"IsFreePar\"]] = minimiser.values\n",
    "data = np.sqrt(jeans.sigmap2(R_scale, jeans_config))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(R_scale, data, label = \"Median\")\n",
    "plt.errorbar(jeans_data[\"R\"], jeans_data[\"Val\"], xerr = jeans_data[\"RErr\"], yerr = jeans_data[\"ValErr\"], label = \"Data\", fmt =\"x\")\n",
    "\n",
    "plt.xscale(\"log\")\n",
    "plt.xlabel(\"R [kpc]\")\n",
    "\n",
    "plt.ylabel(\"σ_p [km/s]\")\n",
    "\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Secondly will be an example how to run a MCMC-chain using pyMC and the helperscript provided with Clumpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pymc as pm\n",
    "from jeans_MCMC import *\n",
    "import pyClumpy.Jeans as jeans\n",
    "# Additional required packages are pytensor, numpy and matplotlib,\n",
    "# but they are handled in the helperscript, so no need to load them here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_path = os.environ['CLUMPY_DATA']\n",
    "jeans_data = jeans.load_data(data_path + \"/data_sigmap.txt\")\n",
    "jeans_config = jeans.load_config(data_path + \"/params_jeans.txt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "jeans_Model_ = jeans_Model(jeans_data, jeans_config)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pm.model_to_graphviz(jeans_Model_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with jeans_Model_:\n",
    "    idata = pm.sample(draws = 10000, tune = 1000, chains = 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "idata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(idata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualisation using `arviz` built-in functionality. \n",
    "\n",
    "Example using the corner library to be added soon"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import arviz as az \n",
    "\n",
    "az.style.use(\"arviz-doc\")\n",
    "ax = az.plot_pair(\n",
    "    idata,\n",
    "    kind=[\"kde\", \"hexbin\"],\n",
    "    kde_kwargs={\"fill_last\": False},\n",
    "    marginals=True,\n",
    "    point_estimate=\"median\",\n",
    "    figsize=(10, 10),\n",
    "    textsize=15,\n",
    "    colorbar=True,\n",
    "    gridsize=20,\n",
    ")\n",
    "fig = ax.ravel()[0].figure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
