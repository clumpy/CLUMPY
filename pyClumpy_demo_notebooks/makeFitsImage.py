import logging
import sys

import astropy
import click
import healpy as hp
import numpy as np
from astropy.io import fits
from rich.logging import RichHandler


def setup_logging(logfile=None, verbose=False):
    """
    Basic logging setup allowing logs to be saved in
    a file on top of the command line output.
    Adapted from
    https://calmcode.io/logging/rich.html
    """

    level = logging.INFO
    if verbose is True:
        level = logging.DEBUG

    log = logging.getLogger()
    log.level = level

    shell_handler = RichHandler()
    shell_formatter = logging.Formatter(fmt="%(message)s")
    shell_handler.setFormatter(shell_formatter)
    log.addHandler(shell_handler)

    if logfile is not None:
        file_handler = logging.FileHandler(logfile)
        file_formatter = logging.Formatter(
            fmt="%(asctime)s|%(levelname)s|%(name)s|\n%(message)s",
            datefmt="%Y-%m-%dT%H:%M:%S",
        )
        file_handler.setFormatter(file_formatter)
        log.addHandler(file_handler)

    return log


def check_extension_header(header):
    """
    Test whether all required header keywords are present.
    This ignores keywords, that have defaults defined in the script
    TODO: Probably check those aswell, but dont raise, only warn
    """
    # Only keywords, that do not have default values in the script
    required_keywords = {
        "EXTNAME",
        "PSI_0",
        "THETA_0",
        "SIZE_Y",
        "COORDSYS",
        "NSIDE",
        "HPX_TYPE",
    }
    missing_keys = required_keywords.difference(set(header.keys()))
    if missing_keys:
        raise KeyError(f"Missing keys {missing_keys} from extension header")


def check_header_1(header):
    """
    Test whether all required header keywords are present.
    This ignores keywords, that have defaults defined in the script
    TODO: Probably check those aswell, but dont raise, only warn
    """
    # Only keywords, that do not have default values in the script
    required_keywords = {
        "TTYPE1",
        "INDXSCHM",
    }
    missing_keys = required_keywords.difference(set(header.keys()))
    if missing_keys:
        raise KeyError(f"Missing keys {missing_keys} from header_1")


def check_fullsky(header):
    """
    Check whether the input map contains full-sky or partial sky data.
    """
    # detect map_type of input file
    if header["TTYPE1"][:5] == "PIXEL":
        return False
    else:
        scheme = header["INDXSCHM"]
        if scheme[:8] == "IMPLICIT":
            return True
        elif scheme[:8] == "EXPLICIT":
            return False
        else:
            raise ValueError(f'FITS keyword "INDXSCHM" must be either "IMPLICIT" or "EXPLICIT", but is {scheme[:8]}')


@click.command()
@click.argument(
    "input-file",
    type=click.Path(exists=True, dir_okay=False),
)
@click.argument(
    "output-file",
    type=click.Path(dir_okay=False),
)
@click.option(
    "-e",
    "--extension-idx",
    default=1,
    help="Extension to be used. Run CLUMPY -o0 -i <input-file> to see available extensions",
)
@click.option(
    "-c",
    "--column-idx",
    default=1,
    help="Column Index to be used. Run CLUMPY -o0 -i <input-file> to see available columns",
)
@click.option(
    "-s",
    "--coordsys-out",
    default="Same",
    type=click.Choice(["Same", "G", "C", "E"]),
    help="Coordinate system to use in the output file. Has to be supported by healpy",
)
@click.option(
    "-o0",
    "--overwrite",
    is_flag=True,
    help="Overwrite existing extension in output file",
)
@click.option("-v", "--verbose", help="Verbose log output", is_flag=True)
@click.option(
    "-l",
    "--log-file",
    help="File to save log output to",
    type=click.Path(dir_okay=False),
)
def main(
    input_file,
    output_file,
    extension_idx,
    column_idx,
    coordsys_out,
    overwrite,
    verbose,
    log_file,
):
    """ "
    Create a fits output file containing a healpix map of data in the input file specified
    by the extension and column index (Default 1/1)
    The important thing handled by this script is the rotation of the original CLUMPY map
    to proper healpix indices:
    CLUMPY produces a map centered at the galactic center
    with the actual coordinates hidded in the extension metadata whereas the resulting
    map contains the data at the correct pixels enabling interoperability with
    other tools such as gammapy.

    """
    hp_version = hp.__version__
    astropy_version = astropy.__version__
    log = setup_logging(verbose=verbose, logfile=log_file)
    log.info(f"Healpy Version: {hp_version}\nAstropy Version: {astropy_version}")
    log.warning(
        "Attention: Conversion from HEALPix format to FITS image causes degradation.\n All information in the FITS header corresponds to the original HEALPix data."
    )

    #  read header:
    with fits.open(input_file) as hdulist_in:
        input_header = hdulist_in[extension_idx].header
        # TODO: What is in that exactly and why not use the other header?
        header_1 = hdulist_in[1].header
        tbdata = hdulist_in[extension_idx].data
        columns = hdulist_in[extension_idx].columns

    # Make sure all keywords are there
    check_extension_header(input_header)
    check_header_1(header_1)

    # Depending on whether its fullsky or partial sky data, indexing slightly differs
    fullsky = check_fullsky(header_1)
    if fullsky:
        log.debug("Full sky map detected")
        column = columns[column_idx - 1]
    else:
        log.debug("Partial sky map detected")
        column = columns[column_idx]  # zeroth column is PIXEL
    log.info(f"Converting column {column} from extension {input_header['EXTNAME']}")

    npix_fov = len(tbdata[column.name])
    sum_data = tbdata[column.name]
    sum_data = sum_data[sum_data > 0]
    map_sum = sum_data.sum(dtype=np.float64)

    map_mean = input_header.get("MEAN")
    if map_mean is None:
        log.info("Calculate mean from data because its not in the header")
        map_mean = tbdata[column.name].mean(dtype=np.float64)
    log.info(f"Map mean is {map_mean}")
    log.info(f"Map sum is {map_sum}")

    #  read map:
    dtype_hpx = input_header["HPX_TYPE"]
    if dtype_hpx == "FLOAT32":
        dtype_map = np.float32
    elif dtype_hpx == "FLOAT64":
        dtype_map = np.float64
    else:
        raise ValueError('Healpix datatype (keyword "HPX_TYPE" in input file header) must be FLOAT32 or FLOAT64.')
    log.info("Read input map into healpy...")
    if fullsky:
        # TODO: The original code did not specify field and hdu here.
        # That has to be wrong, right? But is this correct or would field be column_idx then?
        map_in = hp.read_map(input_file, field=column_idx - 1, hdu=extension_idx, dtype=dtype_map)
    else:
        map_in = hp.read_map(
            input_file,
            partial=True,
            field=column_idx - 1,
            hdu=extension_idx,
            dtype=dtype_map,
        )

    # To check whether something went wrong loading the data, e.g. screwed up the hdu index
    log.debug(f"Mean of in: {map_in[map_in>0].mean()}")
    log.debug(f"Max of in: {map_in.max()}")
    log.debug(f"Sum of in: {map_in[map_in>0].sum()}")

    sky_fraction = input_header.get("F_SKY", None)
    if not sky_fraction:
        log.info("Calculating fraction of sky covered by F.o.V. because its not in the header")
        npix = len(map_in)
        sky_fraction = float(npix_fov) / float(npix)

    # Actual calculations
    map_center_psi_deg = input_header["PSI_0"]
    map_center_theta_deg = input_header["THETA_0"]
    map_diam_theta_deg = input_header["SIZE_Y"]
    map_diam_theta_orth_deg = input_header.get("SIZE_X", map_diam_theta_deg)
    nside = input_header["NSIDE"]

    # TODO: magic numbers anyone?
    if map_diam_theta_orth_deg < 342.85:
        map_diam_theta_orth_deg *= 1.05
    if map_diam_theta_deg < 171.42:
        map_diam_theta_deg *= 1.05

    resol_deg = np.rad2deg(hp.nside2resol(nside))

    # make sure that npix_x, npix_y are odd numbers:
    # TODO: Why is it times 2? It is already the diameter
    npix_x_check = (2 * map_diam_theta_orth_deg // resol_deg) + 1
    npix_y_check = (2 * map_diam_theta_deg // resol_deg) + 1

    # TODO: What is this for? Looks super random especially since 1997 != 1999
    if npix_x_check > 1999:
        npix_x_check = 1997
        log.warning(f"n_pix_x_check is greater than 1999 ({npix_x_check}) and therefore set to 1997")
    if npix_y_check > 1999:
        npix_y_check = 1997
        log.warning(f"n_pix_y_check is greater than 1999 ({npix_y_check}) and therefore set to 1997")

    coordsys_in = input_header["COORDSYS"][:5]
    assert coordsys_in in [
        "G",
        "C",
        "E",
    ], f"Input coordinate system not recognized. Must be either G, C, or E, but is {coordsys_in}"
    if coordsys_out == "Same":
        coordsys_out = coordsys_in

    # If out != in, healpy needs to perform a rotation and the center changes
    # In that case, the cartview function wants a tuple of in and out coordinate systems
    if coordsys_out == coordsys_in:
        coord = coordsys_in
    else:
        coord = (coordsys_in, coordsys_out)
        log.debug(coord)
        rotator = hp.Rotator(coord=coord, deg=False)
        # TODO: Can healpy work with astropy units? Like could we give it a Quantity with unit degree here (and adapt code at other places as well)?
        map_center_theta, map_center_psi = rotator(
            np.pi / 2 - np.deg2rad(map_center_theta_deg),
            np.deg2rad(map_center_psi_deg),
        )
        map_center_psi_deg = np.rad2deg(map_center_psi) % 360
        map_center_theta_deg = 90.0 - np.rad2deg(map_center_theta)

    #  get projected map
    map_out = hp.cartview(
        map_in,
        rot=[map_center_psi_deg, map_center_theta_deg],
        lonra=[-map_diam_theta_orth_deg / 2, map_diam_theta_orth_deg / 2],
        latra=[-map_diam_theta_deg / 2, map_diam_theta_deg / 2],
        xsize=npix_x_check,
        flip="astro",
        coord=coord,
        return_projected_map=True,
    )
    # TODO: We already have a masked map here. Can we not perform the mean/... operations on that?
    # How does this affect fits.write?
    map_out = map_out.data

    # To check whether something went wrong loading the data, e.g. screwed up the hdu index
    log.debug(f"Mean of out: {map_out[map_out>0].mean()}")
    log.debug(f"Max of out: {map_out.max()}")
    log.debug(f"Sum of out: {map_out[map_out>0].sum()}")

    npix_y, npix_x = map_out.shape
    if npix_x_check != npix_x:
        log.warning(f"Attention!  npix_x_check={npix_x_check} != npix_x = {npix_x}")
    if npix_y_check != npix_y:
        log.warning(f"Attention!  npix_y_check={npix_y_check} != npix_y = {npix_y}")

    delta_x = map_diam_theta_orth_deg / npix_x
    delta_y = map_diam_theta_deg / npix_y
    log.info(f"Resolution of map ( delta_x , delta_y ): ({delta_x}, {delta_y}) degrees")

    # write out total J-Factor/flux in map:
    # For J-factor/flux values per pixel, renormalize
    # to new (approximate) cartesian pixel size
    units = input_header.get("TUNIT" + str(column_idx + 1), "not specified")
    if "sr^-1" in units:
        totalflux = map_mean * 4.0 * np.pi * sky_fraction
    else:
        totalflux = map_sum
        pix_area_hp = hp.nside2pixarea(nside, degrees=True)
        pix_area_cart = delta_x * delta_y
        norm = pix_area_cart / pix_area_hp
        map_out *= norm
        log.info(f"Values re-normalized to new pixel sizes using norm factor of {norm}.")
    log.info(f"Total flux: {totalflux}/{units}")

    # replace 1e-40 values by HEALPIX blind value
    # TODO: gammapy does not like this, it prefers 0. Maybe that can be fixed using the masked data aswell?
    hpx_blindval = -1.6375e30
    map_out[map_out < 4e-40] = hpx_blindval

    # create fits header:
    if coordsys_out == "G":
        coordlon = "GLON-CAR"
        coordlat = "GLAT-CAR"
    elif coordsys_out == "C":
        coordlon = "RA---CAR"
        coordlat = "DEC--CAR"
    elif coordsys_out == "E":
        coordlon = "ELON-CAR"
        coordlat = "ELAT-CAR"

    # DAFUQ?
    reference_header = input_header[41:]
    reference_header.update(
        {
            "EXTNAME": ("Skymap"),
            "CDELT1": (-delta_x, "pixel size (approx.) in longitude-dir. [deg]"),
            "CDELT2": (delta_y, "pixel size (approx.) in latitude-dir. [deg]"),
            "CRPIX1": ((npix_x + 1) / 2, "central pixel in longitude direction"),
            "CRPIX2": ((npix_y + 1) / 2, "central pixel in latitude direction"),
            "CRVAL1": (
                map_center_psi_deg,
                "longitude coordinate of map center [deg]",
            ),
            "CRVAL2": (
                map_center_theta_deg,
                "latitude coordinate of map center [deg]",
            ),
            "CTYPE1": (coordlon, "longitude coord. system (cartesian projection)"),
            "CTYPE2": (coordlat, "latitude coord. system (cartesian projection)"),
            "CUNIT1": ("deg", "longitude axis unit"),
            "CUNIT2": ("deg", "latitude axis unit"),
            "NAXIS": 2,
            "NAXIS1": (npix_x, "number of pixels in longitude direction"),
            "NAXIS2": (npix_y, "number of pixels in latitude direction"),
            "BUNIT": (units, "pixel value unit"),
            "F_SKY": (sky_fraction, "fraction of sky covered by FOV"),
            "MEAN": (map_mean, "mean value in FOV (same unit as BUNIT)"),
            "FLUX_TOT": (totalflux, "total integrated J-Factor (flux) in FOV"),
            "NSIDE": (nside, "HEALPix resolution of original image"),
            "AUTHOR": "file created by Clumpy, makeFitsImage.py",
            "TRANSPAR": (hpx_blindval, "transparency value"),
        }
    )

    fits.writeto(output_file, map_out, reference_header, overwrite=overwrite)
    log.info("Output file written to: " + output_file)


if __name__ == "__main__":
    main()
