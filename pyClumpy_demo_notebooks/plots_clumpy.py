import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
from astropy import units as u
from matplotlib import cm

def names_and_units(data, excluded_columns, x_label):
    names = data.colnames
    units = np.empty(np.size(names), dtype = "object")
    i = 0
    first = True
    main_unit = None
    for col in data.itercols():
        if col.unit is not None:
            units[i] = col.unit.to_string()
            if first and not np.any(excluded_columns == names[i]) and not x_label == names[i]:
                main_unit = col.unit
                first = False
        if names[i] == x_label:
            x_data = col
        i+=1
    return names, units, x_data, main_unit

def obj_parse(data, names):
    i = 0
    obj_names = []
    for col in data.itercols():
        if names[i] == "obj_name":
            obj_names = col
        i+=1
    if np.size(obj_names) == 0:
        obj_slices = np.array([slice(None, None)])
        unique_names = np.array([""])
    else:
        unique_names = np.unique(obj_names)
        n_obj = np.size(unique_names)
        obj_slices = np.empty(n_obj, dtype = slice)
        for i in range(0, n_obj):
            indices = np.argwhere(obj_names == unique_names[i])
            obj_slices[i] = slice(indices[0][0], indices[-1][0])
    return obj_slices, unique_names

def find_labels(names, x_label, excluded_columns):
    seperated_names = np.empty(np.size(names), dtype = np.ndarray)
    for i in range(0, np.size(names)):
        seperated_names[i] = np.array(names[i].split("_"))
    y_label = ""
    names_labels = np.copy(names)

    mask = np.empty(np.shape(names), dtype = bool)
    for i in range(0, np.size(names)):
        mask[i] = not (names[i] == x_label) and not (names[i] in excluded_columns)
    
    longest_common = seperated_names[mask][0]
    for seperated_name in seperated_names[mask][1:]:
        longest_common_ = np.array([], dtype = str)
        for common in longest_common:
            if common in seperated_name:
                longest_common_ = np.append(longest_common_, common)
        longest_common = longest_common_

    for common in longest_common:
        y_label += "_" + common
    if np.size(y_label) > 0:
        y_label = y_label[1:]

    for i in range(0, np.size(names)):
        names_labels[i] = ""
        for seperated in seperated_names[i]:
            check_x = not (names[i] == x_label)
            check_longest_common = True
            for common in longest_common:
                if common in seperated:
                    check_longest_common = False
            if check_x and check_longest_common:
                names_labels[i] += "_" + seperated
        if np.size(names_labels[i])>0:
            names_labels[i] = names_labels[i][1:]

    return names_labels, y_label

def plot_1D(data, x_label, x_scale = "log", y_scale = "log", main_unit = None, excluded_columns = None, y_min = None):
    if excluded_columns == None:
        excluded_columns = "obj_name"
    else:
        excluded_columns = np.append(excluded_columns, "obj_name")
    names, units, x_data, main_unit_ = names_and_units(data, excluded_columns, x_label)
    if main_unit == None:
        main_unit = main_unit_
    obj_slices, obj_names = obj_parse(data, names)
    names_labels, y_label = find_labels(names, x_label, excluded_columns)
    n_data = np.size(names)
    n_obj = np.size(obj_names)
    if n_obj == 0:
        n_obj = 1

    cmap = cm.get_cmap("gist_rainbow")
    # cmap = cm.get_cmap("gnuplot")
    formats = ["-", "--", "-.", ":"]
    for j in range(0, n_obj):
        n=0
        for i in range(0, n_data):
            if not (names[i] == x_label) and not np.any(excluded_columns == names[i]):
                if n_obj == 1:
                    name = names_labels[i]
                    y_data = data[names[i]]
                    color = cmap(1.0 * i/(n_data))
                else:
                    name = f"{obj_names[j]} {names_labels[i]}"
                    y_data = data[names[i]][obj_slices[j]]
                    color = cmap(1.0 * j/n_obj)
                if y_min:
                    y_data[y_data < y_min] = None
                if main_unit is not None:
                    y_data = y_data.to(main_unit)
                if n <= 3:
                    linestyle = formats[n]
                else:
                    linestyle = formats[3]
                n+=1
                plt.plot(x_data[obj_slices[j]], y_data, label = name, color = color, linestyle = linestyle)
    x_unit = x_data.unit
    if x_unit is not None:
        x_label = f"{x_label} [{x_unit.to_string()}]"
    plt.xlabel(x_label)
    if main_unit is not None:
        plt.ylabel(f"{y_label} [{main_unit.to_string()}]")
    else:
        plt.ylabel(y_label)
    plt.xscale(x_scale)
    plt.yscale(y_scale)
    if len(names)>2: # If we are plotting more than 1 column against x we need a legend (otherwise the entire label is on the y-axis and no legend-entry is done anyway)
        plt.legend()