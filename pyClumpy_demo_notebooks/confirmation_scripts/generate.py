# Going to test only the modes that don't have a fatal parameter problem (they are in generate_fits.py)
import pyClumpy.galactic_halo as g
import pyClumpy.halo_list as h
import pyClumpy.extragalactic as e
import pyClumpy.spectrum as z

g.g0("clumpy_params_g0.txt", gSIM_OUTPUT_DIR = "build")
g.g1("clumpy_params_g1.txt", gSIM_OUTPUT_DIR = "build")


h.h0("clumpy_params_h0.txt", gSIM_OUTPUT_DIR = "build")
h.h1("clumpy_params_h1.txt", gSIM_OUTPUT_DIR = "build")
# h.h6("clumpy_params_h6.txt", gSIM_OUTPUT_DIR = "build") # No outputfile
# h.h7("clumpy_params_h7.txt", gSIM_OUTPUT_DIR = "build") # No outputfile
h.h8("clumpy_params_h8.txt", gSIM_OUTPUT_DIR = "build")
h.h9("clumpy_params_h9.txt", gSIM_OUTPUT_DIR = "build")
h.h10("clumpy_params_h10.txt", gSIM_OUTPUT_DIR = "build")


e.e0("clumpy_params_e0.txt", gSIM_OUTPUT_DIR = "build")
e.e1("clumpy_params_e1.txt", gSIM_OUTPUT_DIR = "build")
e.e2("clumpy_params_e2.txt", gSIM_OUTPUT_DIR = "build")
e.e3("clumpy_params_e3.txt", gSIM_OUTPUT_DIR = "build")
e.e4("clumpy_params_e4.txt", gSIM_OUTPUT_DIR = "build")
e.e5("clumpy_params_e5.txt", gSIM_OUTPUT_DIR = "build")
e.e6("clumpy_params_e6.txt", gSIM_OUTPUT_DIR = "build")


o.o0("clumpy_params_o0.txt", gSIM_OUTPUT_DIR = "build")
o.o1("clumpy_params_o1.txt", gSIM_OUTPUT_DIR = "build")
o.o2("clumpy_params_o2.txt", gSIM_OUTPUT_DIR = "build")
o.o3("clumpy_params_o3.txt", gSIM_OUTPUT_DIR = "build")

z.z0("clumpy_params_z0.txt", gSIM_OUTPUT_DIR = "build")