clumpy -g0D
clumpy -g0 -i=clumpy_params_g0.txt
clumpy -g1D
clumpy -g1 -i=clumpy_params_g1.txt
clumpy -g2D
clumpy -g2 -i=clumpy_params_g2.txt
clumpy -g3D
clumpy -g3 -i=clumpy_params_g3.txt
clumpy -g4D
clumpy -g4 -i=clumpy_params_g4.txt
clumpy -g5D
clumpy -g5 -i=clumpy_params_g5.txt
clumpy -g6D
clumpy -g6 -i=clumpy_params_g6.txt
clumpy -g7D
clumpy -g7 -i=clumpy_params_g7.txt
clumpy -g8D
clumpy -g8 -i=clumpy_params_g8.txt


clumpy -h0D
clumpy -h0 -i=clumpy_params_h0.txt
clumpy -h1D
clumpy -h1 -i=clumpy_params_h1.txt
clumpy -h2D
clumpy -h2 -i=clumpy_params_h2.txt
clumpy -h3D
clumpy -h3 -i=clumpy_params_h3.txt
clumpy -h4D
clumpy -h4 -i=clumpy_params_h4.txt
clumpy -h5D
clumpy -h5 -i=clumpy_params_h5.txt
# clumpy -h6D                           # No outputfile
# clumpy -h6 -i=clumpy_params_h6.txt    # No outputfile
# clumpy -h7D                           # No outputfile
# clumpy -h7 -i=clumpy_params_h7.txt    # No outputfile
clumpy -h8D
clumpy -h8 -i=clumpy_params_h8.txt
clumpy -h9D
clumpy -h9 -i=clumpy_params_h9.txt
clumpy -h10D
clumpy -h10 -i=clumpy_params_h10.txt


clumpy -e0D
clumpy -e0 -i=clumpy_params_e0.txt
clumpy -e1D
clumpy -e1 -i=clumpy_params_e1.txt
clumpy -e2D
clumpy -e2 -i=clumpy_params_e2.txt
clumpy -e3D
clumpy -e3 -i=clumpy_params_e3.txt
clumpy -e4D
clumpy -e4 -i=clumpy_params_e4.txt
clumpy -e5D
clumpy -e5 -i=clumpy_params_e5.txt
clumpy -e6D
clumpy -e6 -i=clumpy_params_e6.txt


clumpy -zD
clumpy -z -i=clumpy_params_z.txt