import os
from pathlib import Path
import numpy as np
from tqdm import tqdm

python_path = Path("build/")
C_path = Path("output/")

python_files = list(python_path.iterdir())
C_files = list(C_path.iterdir())

python_names = np.array([], dtype = str)
python_lines = {}
for file in python_files:
    if (file.name[-5:] == ".root") or (file.name[-5:] == ".fits"):
        continue
    python_names = np.append(python_names, file.name)
    python_lines[file.name] = ""
    with file.open("r") as file_handle:
        for line in file_handle:
            python_lines[file.name] += line

C_names = np.array([], dtype = str)
C_lines = {}
for file in C_files:
    if (file.name[-5:] == ".root") or (file.name[-5:] == ".fits"):
        continue
    C_names = np.append(C_names, file.name)
    C_lines[file.name] = ""
    with file.open("r") as file_handle:
        for line in file_handle:
            C_lines[file.name] += line

for name in python_names:
    if name not in C_names:
        exit(f"Python generated file \"{name}\" not found in command-line generated files, generate ALL files you want to check or clear your build and output directory before running the checks.")
    print(f"Checking {name}:")
    not_correct = False
    for i in tqdm(range(0, len(python_lines[name]))):
        if not (python_lines[name][i] == C_lines[name][i]):
            not_correct = True
            # print(f"Mismatch found in file \"{name}\", character number \"{i}\"")
            # print(f"Python: {python_lines[name][i]}; C: {C_lines[name][i]}")
    if not_correct:
        print(f"Mismatch(es) found in file \"{name}\"")