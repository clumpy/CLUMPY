import pymc as pm
# print(f"Running on PyMC v{pm.__version__}")
import pytensor.tensor as at
import pyClumpy.Jeans as jeans
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import arviz as az 

def jeans_Model(jeans_data, jeans_config):
    """
    Create a pyMC model for Jeans analysis parameters based on the data and config provided by CLUMPY through the pyClumpy.Jeans.load_data and pyClumpy.Jeans.load_config.

    Parameters
    ----------
    jeans_data:
        a dictionary containing the data of the object according to CLUMPY's standards
    jeans_config:
        a dictionary containing the configuration of your analysis according to CLUMPY's standards
    """
    # define an aesara Op to wrap our likelihood function in a way that pymc will accept it
    class LogLike(at.Op):
        """
        Specify what type of object will be passed and returned to the Op when it is
        called. In our case we will be passing it a vector of values (the parameters
        that define our model) and returning a single "scalar" value (the
        log-likelihood)
        """

        itypes = [at.dvector]  # expects a vector of parameter values when called
        otypes = [at.dscalar]  # outputs a single scalar value (the log likelihood)

        def __init__(self, custom_function):
            """
            Initialise the Op with various things that our log-likelihood function
            requires. Below are the things that are needed in this particular
            example.

            Parameters
            ----------
            custom_function:
                The log-likelihood (or whatever) function we've defined
            """

            # add inputs as class attributes
            self.likelihood = custom_function

        def perform(self, node, inputs, outputs):
            # the method that is used when calling the Op
            (params) = inputs  # this will contain my variables
            # call the log-likelihood function
            logl = self.likelihood(params)
            outputs[0][0] = np.array(logl)  # output the log-likelihood

    # define the custom log likelihood function
    custom_log_likelihood = lambda params : jeans.log_likelihood(params, jeans_data, jeans_config)
    # wrap our custom log likelihood function into our custom aesera Op
    log_l = LogLike(custom_log_likelihood)

    # define the names and upper/lower limits of the analysis
    free_params_names = np.array(jeans_config["ParNames"])[jeans_config["IsFreePar"]]
    lower = jeans_config["RangeLo"][jeans_config["IsFreePar"]]
    upper = jeans_config["RangeUp"][jeans_config["IsFreePar"]]

    # define the base model with the free parameters as the only adjustable parameters
    myModel = pm.Model(coords={"free_param": free_params_names})

    with myModel:
        # create the tensor containing the free parameters through a Uniform distribution from the lower to upper limits
        free_params = pm.Uniform("free_params", lower = lower, upper = upper, dims = "free_param")
        free_params = at.as_tensor(free_params)
        # define the potential of our model through our custom likelihood giving it the tensor of free parameters
        pm.Potential("likelihood", log_l(free_params))
    
    return myModel

if __name__ == "__main__":
    jeans_data = jeans.load_data("../../data/data_sigmap.txt")
    jeans_config = jeans.load_config("../../data/params_jeans.txt")

    jeans_Model = jeans_Model(jeans_data, jeans_config)
    with jeans_Model:
        idata = pm.sample(draws = 10000, tune = 1000, chains = 4)

    az.style.use("arviz-doc")
    ax = az.plot_pair(
        idata,
        kind=["kde", "hexbin"],
        kde_kwargs={"fill_last": False},
        marginals=True,
        point_estimate="median",
        figsize=(10, 10),
        textsize=15,
        colorbar=True,
        gridsize=20,
    )
    fig = ax.ravel()[0].figure
    fig.savefig("./jeans_MCMC.pdf")        
