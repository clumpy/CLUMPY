{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "34253cf4-0a3c-43fb-b20f-c5dea9c6037e",
   "metadata": {},
   "source": [
    "# **pyClumpy Demo - Galactic mode (-g)**\n",
    "In this notebook we demonstrate how to use the `Galactic module` (i.e. Milky Way) of ``CLUMPY`` (`-g` option when running from the command line) in python through the ``pyClumpy`` package. All options of the ``CLUMPY`` command line calls are available in python (see also the [documentation](https://clumpy.gitlab.io/CLUMPY/v3.1.1/doc_module_g.html))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "955fc975-eddf-4ee6-9779-3178a181a858",
   "metadata": {},
   "outputs": [],
   "source": [
    "from plots_clumpy import plot_1D\n",
    "from astropy.table import Table"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc3442e9-9936-4e1e-a0d0-0b35d68139cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyClumpy.galactic_halo as g"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18cf7531-54d4-4d32-b5d8-695ca8525cc6",
   "metadata": {},
   "source": [
    "Every example consists of two calls of ``pyClumpy``. One call of ``x.generateparams_*()`` creating a default/mockup parameter-file in the ``CLUMPY`` standard and another call of ``x.*(\"clumpy_params_g0.txt\")`` for the actual calculation. Any parameter set in the parameter-files may be overwritten by passing it in the function call as demonstrated with the ``gSIM_OUTPUTDIR`` option."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67b867e2-0cb2-470a-af71-f550b70f21be",
   "metadata": {},
   "source": [
    "NB: for Clumpy users familiar with the command-line calls numbers (`-g1`, `-g2`, etc.),  all the calls can also be done in the traditional (but deprecated) forms\n",
    "\n",
    "    g.gXD()\n",
    "\n",
    "and\n",
    "\n",
    "    g.gX(\"clumpy_params_gX.txt\")\n",
    "\n",
    "where `X` is the option number."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bdc5e23-63f8-49b2-b653-485d506dd8c6",
   "metadata": {},
   "source": [
    "### Mass profile (1D)\n",
    "To calculate *M*<sub>Gal.</sub>(*r*) in one dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63dea3fb-33f1-4766-b270-06f68977a621",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "g.generateparams_mass_vs_radius()\n",
    "g.mass_vs_radius(\"clumpy_params_g0.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7236a8d-96b5-413b-8e66-0c6538e000a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Mr.ecsv\")\n",
    "plot_1D(data, \"r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60565fba-ecd6-4363-91b3-3c8b282ccce5",
   "metadata": {},
   "source": [
    "### Density profiles (1D)\n",
    "To calculate *ρ*<sub>Gal.</sub>(*r*) (smooth, sub-continuum and total) of a **spherically symmetric** halo in one dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0266996-8aa5-45b1-8c88-d538297802d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_massdensity_vs_radius()\n",
    "g.massdensity_vs_radius(\"clumpy_params_g1.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c45a99d4-d9fd-4dd5-bbc1-80f146ffe6e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.rhor.ecsv\")\n",
    "plot_1D(data, \"r\", excluded_columns = [\"rho_subs_share\", \"rho_n_cl(r)_share\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a63fd109-e8df-4ff6-9f85-209eeead821a",
   "metadata": {},
   "source": [
    "### *J*-Factor profiles (1D)\n",
    "To calculate *J*(*α*<sub>int</sub>; *l, b*) towards a user-defined direction (*l, b*) in the Galactic halo. Note that a triaxial halo can be studied as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "05538671-f03b-4471-a52e-20cb393d1d50",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_mean_vs_integrationangle()\n",
    "g.mean_vs_integrationangle(\"clumpy_params_g2.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d4df279-77f6-4091-83e0-f48401cba819",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Jalphaint.ecsv\")\n",
    "plot_1D(data, \"alpha_int\", excluded_columns = [\"J_alpha_int_subs_share\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21e4d1ac-3079-4cbc-995a-68dd61a8baae",
   "metadata": {},
   "source": [
    "### d*J*/d*Ω*(*θ*) (1D)\n",
    "To calculate d*J*/d*Ω*(*θ*) as a function of the distance *θ* from the centre of a spherically symmetric Galactic halo."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "418df159-d7b8-4353-8130-68bd471b9c0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_mean_vs_angulardist()\n",
    "g.mean_vs_angulardist(\"clumpy_params_g3.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd0ab71c-1586-4ed0-9a40-ba2eab079528",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Jtheta.ecsv\")\n",
    "plot_1D(data, \"theta\", excluded_columns = [\"J_subs_share\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06885b93-9071-46b4-9daa-7974a279e417",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Jthetadiff.ecsv\")\n",
    "plot_1D(data, \"theta\", excluded_columns = [\"dJ_subs_share\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "291f0993-c3c4-48ec-a7d2-0a2a7f5d54e0",
   "metadata": {},
   "source": [
    "### *J*<sub>*α*<sub>int</sub></sub>(*θ*) (1D)\n",
    "Returns the *J*(*θ*) and optionally, the corresponding fluxes, for a fixed, user-defined integration angle *α*<sub>int</sub> as a function of the distance  from the Galactic centre. Any J-factors *J*<sub>list</sub>(*α*<sub>int</sub>) from haloes defined in the file `gLIST_HALOES` are output as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1014ffd5-ee08-46de-a815-064bfa82f9b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_mean_list_vs_angulardist()\n",
    "g.mean_list_vs_angulardist(\"clumpy_params_g4.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20feb5da-2a3e-4e6e-a5c0-2619cfcd04d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Jtheta.ecsv\")\n",
    "plot_1D(data, \"theta\", excluded_columns = [\"J_subs_share\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d7a773e-6f0d-41f1-a484-233e4c7615dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Table.read(\"build/gal.Jthetadiff.ecsv\")\n",
    "plot_1D(data, \"theta\", excluded_columns = [\"dJ_subs_share\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c771e2e3-e1fd-4a31-9931-5b38230de5ea",
   "metadata": {},
   "source": [
    "### 〈*J*<sub>pixel</sub>〉 and 〈d*J*/d*Ω*〉 (2D)\n",
    "Returns a skymap of *J*(pixel) and d*J*/d*Ω* towards the Galactic anti-center without resolving individual Galactic subhaloes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa18fa2f-b5fc-43bb-a063-5ee3a8a86d24",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_skymap_mean()\n",
    "g.skymap_mean(\"clumpy_params_g5.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "076664e7-51cc-43a3-a2a0-53cc9b7c9be9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Note:</b> The default call above generates a 4 deg x 4 deg map, towards the Galactic anticenter, with a healpix resolution Nside=1024. \n",
    "\n",
    "On the command line, running: `clumpy -o3 -i clumpy_o3.txt will show you what is inside the selected fits file.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9cead1e-3dd2-4948-ad67-f2291665c47a",
   "metadata": {},
   "source": [
    "Below, we show how to visualise the first map (Jtot) of the first extension of that FITS file, using `healpy` functionalities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2292e73b-f017-4c4b-8433-73548cdd4fa0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import healpy as hp\n",
    "from astropy.io import fits\n",
    "from matplotlib import pyplot as plt\n",
    "import numpy as np\n",
    "outfile = 'build/annihil_gal2D_LOS180_0_FOV4x4_nside1024.fits'\n",
    "ext = 1 # J-factors\n",
    "\n",
    "col = 1 # Jtot\n",
    "data = hp.read_map(outfile, partial=True, hdu=ext, field=col-1)\n",
    "\n",
    "hdulist = fits.open(outfile)\n",
    "dtheta = hdulist[ext].header['SIZE_Y']\n",
    "dtheta_orth = hdulist[ext].header['SIZE_X']\n",
    "title = hdulist[ext].header['TTYPE'+str(col+1)]\n",
    "units = hdulist[ext].header['TUNIT'+str(col+1)]\n",
    "hdulist.close()\n",
    "\n",
    "hp.cartview(data, lonra=[180-dtheta_orth/2,180+dtheta_orth/2], latra=[-dtheta/2,dtheta/2], \n",
    "                         norm='hist', title=title, unit=units, hold=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a667acc-6b2d-4dbc-a83a-0ef8408eeec2",
   "metadata": {},
   "source": [
    "### 〈*J*<sub>pixel</sub>〉 and 〈d*J*/d*Ω*〉 + list haloes (2D) \n",
    "Returns a skymap of *J*(pixel) and d*J*/d*Ω* towards the Galactic anti-center without resolving individual Galactic subhaloes with additional resolved haloes from a list defined in `gLIST_HALOES`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3afd0f6c-8279-411f-96f5-20530a36d7e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_skymap_mean_list()\n",
    "g.skymap_mean_list(\"clumpy_params_g6.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8647043-2d33-44dc-8a8c-25febc0d5a5d",
   "metadata": {},
   "source": [
    "### *J*<sub>pixel</sub> and d*J*/d*Ω* + drawn subhaloes (2D)\n",
    "Returns a skymap of *J*(pixel) and d*J*/d*Ω* towards the Galactic anti-center with randomly drawn resolved Galactic subhaloes from a list defined in `gLIST_HALOES`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0117f44-2cd0-45e7-b145-c3066feb0fd9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Warning:</b> For some users, the cell below will hang and not provide any output. This comes from the `skymap_J_clumps` call and currently is under investigation. Running these lines in a `ipython` console works, so this seems to be a jupyter-related matter.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c148794-cfcb-4818-b5e2-4e0752c40630",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_skymap_mean_drawn()\n",
    "g.skymap_mean_drawn(\"clumpy_params_g7.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e02a208-ae3f-4b77-a573-b2c9b8b8ae5f",
   "metadata": {},
   "source": [
    "### *J*<sub>pixel</sub> and d*J*/d*Ω* + drawn subhaloes + list haloes (2D)\n",
    "Returns a skymap of *J*(pixel) and d*J*/d*Ω* towards the Galactic anti-center with randomly drawn resolved Galactic subhaloes plus additional external haloes stored in a file called by `gLIST_HALOES`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73b9b270-882d-4a8f-a14e-49ed7348533f",
   "metadata": {},
   "outputs": [],
   "source": [
    "g.generateparams_skymap_mean_drawn_list()\n",
    "g.skymap_mean_drawn_list(\"clumpy_params_g8.txt\", gSIM_OUTPUT_DIR = \"build\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
