#ifndef _CLUMPY_INLINES_H_
#define _CLUMPY_INLINES_H_

#define C_SPEED                    (2.99792458e8)                               //!< Speed of light in vacuum \f$m s^{-1}\f$
#define HUBBLE_LENGTHxh_Mpc        (2.99792458e3)                               //!< Hubble length \f$c\,h/H_0\f$ in [\f$h^{-1} \; Mpc \f$]
#define NEWTON_CONSTANT_SI         (6.67408e-11)                                //!< Gravitational constant in SI units (PDG2015 and CODATA 2014)
#define RHO_CRITperh2_MSOLperKPC3  (277.533)                                    //!< Critical density of the universe in units of \f$h^2\,M_\odot\,\,kpc^{-3}\f$
#define DELTA_COLLAPSE             (1.6865)                                     //!<
#define HALO_MMAX                  (1e17)                                       //!< [Msol], maximum halo mass usable in the code
#define HALO_MMIN                  (1e-15)                                      //!< [Msol], minimum halo mass usable in the code
#define ZMIN_EXTRAGAL              (1e-4)                                       //!< Redshift threshold below which no cosmological effects (EBL absorption, geometry) are considered. \f$z=10^{-4}\f$ corresponds to \f$440\,\rm kpc\f$.

#define SIGMA_THOMSON              (0.6652458734)                               //!< Thomson cross section \f$sigma_{T}=8\pi r_e^2/3\f$ \f$barn\f$ (PDG 2014)
#define MASS_E                     (510.998928)                                 //!< Electron mass \f$keV\f$ (PDG 2014)
#define RADIATION_CONSTANT         (1.2437628e-4)                               //!< Radiation constant \f$\frac{\pi^2\,k_B^4}{15\,c^5\,hbar^3}\f$ in units of \f$h^2\,M_\odot\,kpc^{-3}\,K^{-4}\f$

#define M_to_KPC                   (3.240779289444365e-20)                      //!< Conversion \f$m\f$ to \f$Kpc\f$ (PDG 2013)
#define KM_to_MPC                  (3.240779289444365e-20)                      //!< Conversion \f$km\f$ to \f$Mpc\f$ (PDG 2013)
#define CM_to_KPC                  (3.240779289444365e-22)                      //!< Conversion \f$cm\f$ to \f$kpc\f$ (PDG 2013)
#define CM_to_MPC                  (3.240779289444365e-25)                      //!< Conversion \f$cm\f$ to \f$Mpc\f$ (PDG 2013)
#define KPC_to_KM                  (3.0856775814913676e+16)                     //!< Conversion \f$kpc\f$ to \f$km\f$ (PDG 2013)
#define KPC_to_MPC                 (1.e-3)
#define KPC3_to_MPC3               (1.e-9)

#define GEV_to_MSOL                (8.9648764e-58)                              //!< Conversion \f$GeV\f$ to \f$M_{\odot}\f$ (PDG 2013)
#define KG_to_MSOL                 (5.0289162e-31)                              //!< Conversion \f$kg\f$ to \f$M_{\odot}\f$  (PDG 2013)
#define M3perKG_to_KM2KPCperMSOL   (6.4442894e+04)                              //!< Conversion \f$m^{3}\,\,kg^{-1}\f$ to \f$km^{2}\,\,kpc\,\,M_{\odot}^{-1}\f$  (PDG 2013)
#define GEVperCM2_to_MSOLperKPC2   (8.5358230e-15)                              //!< Conversion \f$GeV\,\,cm^{-2}\f$ to \f$M_{\odot}\,\,kpc^{-3}\f$ (for D-factor)
#define GEVperCM3_to_MSOLperKPC3   (2.6338797e+07)                              //!< Conversion \f$GeV\,\,cm^{-3}\f$ to \f$M_{\odot}\,\,kpc^{-3}\f$ (for DM density)
#define GEV2perCM5_to_MSOL2perKPC5 (2.2482330e-07)                              //!< Conversion \f$GeV^{2}\,\,cm^{-5}\f$ to \f$M_{\odot}^{2}\,\,kpc^{-5}\f$ (for J-factor)
#define BARN_to_CM2                (1.e-24)

#define SIGN(a,b)                  ((b) >= 0.0 ? fabs(a) : -fabs(a))            //!< Returns +a if b>=0, -a otherwise.
#define PI                         (3.14159265358979312)                        //!< What do you think it is?
#define MY_SIN(x)                  ((fabs(x)==PI) ? 0. : sin(x))                //!< Ensures sin(x)=0 for x=PI
#define MY_TAN(x)                  ((fabs(x)==PI) ? 0. : tan(x))                //!< Ensures tan(x)=0 for x=PI
#define MY_COS(x)                  ((fabs(x)==PI/2.||fabs(x)==3.*PI/2.)?0.:cos(x)) //!< Ensures cos(x)=0 for x=PI/2 or x=3PI/2
#define DEG_to_RAD                 (0.017453292519943295)                       //!< Conversion deg to rad (\f$\pi/180\f$ )
#define RAD_to_DEG                 (57.29577951308232)                          //!< Conversion rad to deg (\f$180/\pi\f$ )
#define SR_to_DEG2                 (3282.80635)                                 //!< Conversion steradian to square degrees
#define SR_to_ARCMIN2              (1.1818102e7)                                //!< Conversion steradian to square arcminutes

#define SMALL_NUMBER               (1e-8)                                       //!< Some small number
#define MAX_ITER                   (1000)                                       //!< Maximum number of iterations for root finder

#define HPX_BLIND_VALUE            (-1.6375e+30)                                //!< Value to mark blind/masked values.

#define SHFT2(a,b,c)               {(a)=(b);(b)=(c);}                           //!< Shift \a a, \a b, and \a c to save the housekeeping involved when moving around 3 points.
#define SHFT3(a,b,c,d)             {(a)=(b);(b)=(c);(c)=(d);}                   //!< Shift \a a, \a b, \a c, and \a d to save the housekeeping involved when moving around 4 points.


#define COLOR_RESET                "\033[0m"                                    //!< Reset original terminal font color

#define COLOR_BLACK                "\033[0;30m"                                 //!< Regular Black console color
#define COLOR_RED                  "\033[0;31m"                                 //!< Regular Red console color
#define COLOR_GREEN                "\033[0;32m"                                 //!< Regular Green console color
#define COLOR_YELLOW               "\033[0;33m"                                 //!< Regular Yellow console color
#define COLOR_BLUE                 "\033[0;34m"                                 //!< Regular Blue console color
#define COLOR_PURPLE               "\033[0;35m"                                 //!< Regular Purple console color
#define COLOR_CYAN                 "\033[0;36m"                                 //!< Regular Cyan console color
#define COLOR_WHITE                "\033[0;37m"                                 //!< Regular White console color

#define COLOR_BBLACK               "\033[1;30m"                                 //!< Bold Black console color
#define COLOR_BRED                 "\033[1;31m"                                 //!< Bold Red console color
#define COLOR_BGREEN               "\033[1;32m"                                 //!< Bold Green console color
#define COLOR_BYELLOW              "\033[1;33m"                                 //!< Bold Yellow console color
#define COLOR_BBLUE                "\033[1;34m"                                 //!< Bold Blue console color
#define COLOR_BPURPLE              "\033[1;35m"                                 //!< Bold Purple console color
#define COLOR_BCYAN                "\033[1;36m"                                 //!< Bold Cyan console color
#define COLOR_BWHITE               "\033[1;37m"                                 //!< Bold White console color

#define COLOR_IBLACK               "\033[0;90m"                                 //!< High intensity Black console color
#define COLOR_IRED                 "\033[0;91m"                                 //!< High intensity Red console color
#define COLOR_IGREEN               "\033[0;92m"                                 //!< High intensity Green console color
#define COLOR_IYELLOW              "\033[0;93m"                                 //!< High intensity Yellow console color
#define COLOR_IBLUE                "\033[0;94m"                                 //!< High intensity Blue console color
#define COLOR_IPURPLE              "\033[0;95m"                                 //!< High intensity Purple console color
#define COLOR_ICYAN                "\033[0;96m"                                 //!< High intensity Cyan console color
#define COLOR_IWHITE               "\033[0;97m"                                 //!< High intensity White console color

#define COLOR_BIBLACK              "\033[1;90m"                                 //!< Bold High intensity Black console color
#define COLOR_BIRED                "\033[1;91m"                                 //!< Bold High intensity Red console color
#define COLOR_BIGREEN              "\033[1;92m"                                 //!< Bold High intensity Green console color
#define COLOR_BIYELLOW             "\033[1;93m"                                 //!< Bold High intensity Yellow console color
#define COLOR_BIBLUE               "\033[1;94m"                                 //!< Bold High intensity Blue console color
#define COLOR_BIPURPLE             "\033[1;95m"                                 //!< Bold High intensity Purple console color
#define COLOR_BICYAN               "\033[1;96m"                                 //!< Bold High intensity Cyan console color
#define COLOR_BIWHITE              "\033[1;97m"                                 //!< Bold High intensity White console color

#define COLOR_UIBLACK              "\033[4;30m"                                 //!< Underline regular Black console color
#define COLOR_UIRED                "\033[4;31m"                                 //!< Underline regular Red console color
#define COLOR_UIGREEN              "\033[4;32m"                                 //!< Underline regular Green console color
#define COLOR_UIYELLOW             "\033[4;33m"                                 //!< Underline regular Yellow console color
#define COLOR_UIBLUE               "\033[4;34m"                                 //!< Underline regular Blue console color
#define COLOR_UIPURPLE             "\033[4;35m"                                 //!< Underline regular Purple console color
#define COLOR_UICYAN               "\033[4;36m"                                 //!< Underline regular Cyan console color
#define COLOR_UIWHITE              "\033[4;37m"                                 //!< Underline regular White console color

#endif

/*! \file inlines.h
   \brief Inline variables/macros (constants, conversions)
*/
