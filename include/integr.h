#ifndef _CLUMPY_INTEGR_H_
#define _CLUMPY_INTEGR_H_

void simpson_lin_adapt(void fn(double &, double *, double &), double &xmin, double &xmax, double par[], double &s, double const &eps, bool is_verbose = false); //!< Returns the 1D integral of fn using Simpson rule in linear steps. Adaptative steps stop when the precision 'eps' is reached.
void simpson_log_adapt(void fn(double &, double *, double &), double &xmin, double &xmax, double par[], double &s, double const &eps, bool is_verbose = false); //!< Returns 1D integral of fn using Simpson rule in logarithmic steps. Adaptative steps stop when the precision 'eps' is reached.
void simpson_log(void fn(double &, double *, double &), double &xmin, double &xmax, double par[], double &s, int nstep); //!<Returns 1D integral of fn with 'nsteps' log steps with Simpson rule.
void trapeze_log_refine(void fn(double &, double *, double &), double &xmin, double &xmax, double par[], double &s, int n); //!< Used for step adaptation in simpson_log_adapt.
void trapeze_lin_refine(void fn(double &, double *, double &), double &xmin, double &xmax, double par[], double &s, int n); //!< Used for step adaptation in simpson_lin_adapt.

#endif

/*! \file integr.h
  \brief Integration routines (for lin. and log. step)

 <CENTER>________________________________________</CENTER>\n

<b>I. \anchor simpson  Simpson-based routines</b>\n

These routines are adapted/modified from <a href=http://nr.com/ target="_blank">Numerical Recipes</a>:
    -# <b>Simpson integration with linear steps: \c simpson_lin_adapt()</b> \n
     \f[
       I = \int_{x_{min}}^{x_{max}} f(x,par[]) \;dx
     \f]
    Integrates \c *fn(\c double\&, \c double*,  \c double&), between \f$x_{min}\f$
    and \f$x_{max}\f$, until the precision \f$ \Delta s/s < \epsilon \f$ is reached
    (aborts if fails).
    \param[in]  fn              Function to integrate
    \param[in]  xmin            Lower integration boundary
    \param[in]  xmax            Upper integration boundary
    \param[in]  par[]           Parameters used by fn()
    \param[out] s               Result of the integration
    \param[in]  eps             Relative precision
    \param[in]  is_verbose[opt] If true, print refinement level reached (default=false)
\n\n\n
    -# <b>Simpson integration with logarithmic step: \c simpson_log_adapt()</b> (or simpson_log() for integration with a fixed number of steps)\n
     \f[
       I = \int_{x_{min}}^{x_{max}} x\cdot f(x,par[]) \;d(\ln x)
     \f]
    Integrates \c *fn(\c double\&, \c double*,  \c double&), between \f$x_{min}\f$
    and \f$x_{max}\f$, until the precision \f$ \Delta s/s < \epsilon \f$ is reached
    (aborts if fails).
    The difference with \c simpson_lin_adapt() is that the doubling of steps is based on a logarithmic step.
    As many functions to integrate are power-law, this performs better than
    the former.
    \param[in]  fn              Function to integrate
    \param[in]  xmin            Lower integration boundary
    \param[in]  xmax            Upper integration boundary
    \param[in]  par[]           Parameters used by fn()
    \param[out] s               Result of the integration
    \param[in]  eps             Relative precision
    \param[in]  is_verbose[opt] If true, print refinement level reached (default=false)

\n

 <b> N.B.:</b> <em>The static functions \c trapeze_lin_refine() and \c trapeze_log_refine() must never be called directly!</em>
*/
