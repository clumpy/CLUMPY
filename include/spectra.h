#ifndef _CLUMPY_SPECTRA_H_
#define _CLUMPY_SPECTRA_H_

double dNdE(double const &e_gev, double par_spec[6]);                           //!<
double dNdE_BERGSTROM98(double const &e_gev, double par[1]);                    //!<
double dNdE_BRINGMANN08(double const &e_gev, double par[1]);                    //!<
double dNdE_CIRELLI11(double const &e_gev, double par[3]);                      //!<
double dNdE_CIRELLI11_interp(vector<vector<vector<double> > > &matrix_PPPC,
                             vector<double> &vec_mdm, vector<double> &vec_logx,
                             double const &e_gev, double const &mdm_gev,
                             int channel);                                      //!<
void   dNdE_CIRELLI11_load(string const &f_name,
                           vector<vector<vector<double> > > &matrix_PPPC,
                           vector<double> &vec_mdm, vector<double> &vec_logx);  //!<
double dNdE_TASITSIOMI02(double const &e_gev, double par[1]);                   //!<

double dPPdE(double &e_gev, double par_spec[6]);                                //!<
void   dPPdE(double &e_gev, double par_spec[6], double &res);                   //!<
double dPPdE_integrated(double par_spec[6], double &e1_gev, double &e2_gev,
                        double const &eps);                                     //!<

double flux(double par_spec[6], double &e_gev, double const &jfact_pp_unit = -1,
            bool is_integrated = false);                                        //!<
void   fluxes_plot(double par_spec[6], double e1_gev, double e2_gev, int n_e,
                   double const &jfact_pp_unit = -1);                           //!<

string legend_for_spectrum(double par_spec[6], bool is_finalstate,
                           bool is_z = true);                                   //!<
double opt_depth(double const &e_gev, double const &z, int flag);               //!<
double exp_minus_opt_depth(double const &e_gev, double const &z, int flag,
                           double const sys_uncertainty = 0.);                  //!<
void   nu_oscillationmatrix(double nu_pij[gN_NUFLAVOUR][gN_NUFLAVOUR],
                            bool is_verbose = true);                            //!<

#endif

/*! \file spectra.h
  \brief Spectra (\f$\gamma\f$-ray and \f$\nu\f$) from DM annihilation or decay
*/
