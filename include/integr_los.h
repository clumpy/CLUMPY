#ifndef _CLUMPY_INTEGR_LOS_H_
#define _CLUMPY_INTEGR_LOS_H_

double los_integral(double par[10], int switch_f, double const &psi_los, double const &theta_los, double const &lmin, double const &lmax, double const &eps, bool is_verbose = false); //!< Returns the integral, for a given l.o.s. direction (psi_los, theta_los), of the function selected by switch_f between lmin and lmax over the aperture gSIM_ALPHAINT, until the relative precision eps is reached.
double los_integral_mix(double par[21], int switch_f, double const &psi_los, double const &theta_los, double const &lmin, double const &lmax, double const &eps, bool is_verbose = false);  //!< Same as los_integral but the function to integrate may be a combinaition of two independent functions, e.g. \f$\rho_1\times\rho_2\f$.
void   fn_beta_alpha(double &alpha, double par[36], double &res); //!< Integral along radial direction \f$l\f$ \f$ f_{\beta\alpha} = \sin(\alpha) \int_{l_{\rm min}}^{l_{\rm max}} f(l,\beta,\alpha; \psi,\theta) dl.\f$. Becomes the integrand of fn_beta.
void   fn_beta(double &beta, double par[36], double &res); //!< Integral of fn_alpha_beta \f$ f_\beta = \int_{0}^{\alpha_{\rm int}} f_{\beta\alpha} d\alpha\f$.
void   integrand_l(double &l, double par[36], double &res); //!< Value of the integrand (related to the DM density) at position \f$(l,\alpha,\beta)\f$.
void   integrand_lrel(double &lrel, double par[36], double &res);//!< Same as integrand_l, but the distance is now measured from the density maximum along the line-of-sight.

#endif

/*! \file integr_los.h
  \brief Integrates along a l.o.s. (line of sight) any function \f$f(l,\Omega)\f$: \f$\;\; I_{l.o.s} \equiv \int_{\Delta\Omega}\int_{l_{min}}^{l_{max}} f (l,\Omega) \,dld\Omega \,\,\;\;\f$
*/
