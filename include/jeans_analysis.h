#ifndef _CLUMPY_JEANSANALYSIS_H_
#define _CLUMPY_JEANSANALYSIS_H_

struct gStructJeansAnalysis {     //!< Structure gathering parameters of a Jeans analysis minimization.
   double Distance;               //!< Distance to the object [kpc]
   double HaloSize;               //!< Size of the dark matter halo [kpc]
   double Longitude;              //!< Longitude of the object [deg]
   double Latitude;               //!< Latitude of the object [deg]
   string Name;                   //!< Name of the object
   double AnalysisEps;            //!< Precision sought for integration
   bool   IsFreePar[26] = {false};//!< Free (true) or fixed (false) parameter
   int    IsLogVal[26] = {0};     //!< Whether parameters are log values (if in fit)
   double RangeLo[26];            //!< Lower range on parameters (if fit, otherwise unused)
   double RangeUp[26];            //!< Upper range on parameters (if fit, otherwise unused)
   vector<string> ParNames = {
      "rhos(Msol/kpc3)", "rs(kpc)", "alpha", "beta", "gamma", "profile", "Rvir(kpc)",
      "L(Lsol)", "rL(kpc)", "alpha*", "beta*", "gamma*", "lightprofile", "R", "eps",
      "beta0", "betainf", "raniso(kpc)", "eta", "anisoprofile",
      "rhos_gas(Msol/kpc3)",  "rs_gas(kpc)", "alpha_gas", "beta_gas", "gamma_gas", "gasprofile"
   };                             //!< Names of the parameters (ordered as in ParJeans[])
   vector<int> ParPrintOrder = {0, 1, 6, 5, 2, 3, 4, 12, 7, 8, 9, 10, 11, 19, 15, 16, 17, 18, 25, 20, 21, 22, 23, 24};
   double ParJeans[26];           //!< All parameters required for a Jeans analysis (ordered as in NamesJeansParams[])
                                  //    ParJeans[0]:  Dark matter profile normalisation [Msol/kpc^3]
                                  //    ParJeans[1]:  Dark matter profile scale radius [kpc]
                                  //    ParJeans[2]:  Dark matter shape parameter #1
                                  //    ParJeans[3]:  Dark matter shape parameter #2
                                  //    ParJeans[4]:  Dark matter shape parameter #3
                                  //    ParJeans[5]:  Dark matter card_profile [gENUM_PROFILE] (see params.h)
                                  //    ParJeans[6]:  Dark matter maximum radius for integration [kpc]
                                  //    ParJeans[7]:  Light profile normalisation [Lsol]
                                  //    ParJeans[8]:  Light profile scale radius [kpc]
                                  //    ParJeans[9]:  Light profile shape parameter #1
                                  //    ParJeans[10]: Light profile shape parameter #2
                                  //    ParJeans[11]: Light profile shape parameter #3
                                  //    ParJeans[12]: Light card_profile [gENUM_LIGHTPROFILE] (see params.h)
                                  //    ParJeans[13]: R: projected radius considered for calculations of projected quantities [kpc]
                                  //    ParJeans[14]: eps: precision sought for integration
                                  //    ParJeans[15]: Anisotropy parameter #1
                                  //    ParJeans[16]: Anisotropy parameter #2
                                  //    ParJeans[17]: Anisotropy parameter #3
                                  //    ParJeans[18]: Anisotropy parameter #4
                                  //    ParJeans[19]: Anisotropy card_profile [gENUM_ANISOTROPYPROFILE] (see params.h)
                                  //    ParJeans[20]: Gas profile normalisation [Msol/kpc^3] => Gas mass unused if par_jeans[20]<=0.
                                  //    ParJeans[21]: Gas profile scale radius [kpc]
                                  //    ParJeans[22]: Gas shape parameter #1
                                  //    ParJeans[23]: Gas shape parameter #2
                                  //    ParJeans[24]: Gas shape parameter #3
                                  //    ParJeans[25]: Gas card_profile [gENUM_PROFILE] (see params.h)
};

struct gStructJeansData {         //!< Structure storing data used for a Jeans analysis
   string         FileName;       //!< Name of Data file
   int            Type;           //!< Data type: 0=Sigmap2 (vel.disp.square), 1=Sigmap (vel.disp), 2=Vel (velocity), or 3=SB (surface brightness, aka light profile)
   vector<string> TypeNames = {"Sigmap2", "Sigmap", "Vel", "SB"}; //!< Names enabled for for data types
   vector<double> Dec;            //!< Declination of the datas point \f$[deg]\f$
   double         Dec_center;     //!< Declination of the center of the object \f$[deg]\f$
   vector<double> RA;             //!< Right ascension of the data points \f$[deg]\f$
   double         RA_center;      //!< Right ascension of the center of the object \f$[deg]\f$
   double         dist;           //!< Distance to the center of the object \f$[kpc]\f$
   vector<double> MembershipProb; //!< Membership probability of the data point (only used for unbinned analysis)
   vector<double> R;              //!< Radius of the data point \f$[kpc]\f$
   vector<double> RErr;           //!< Error on R \f$[kpc]\f$
   vector<double> Val;            //!< Values at R for Sigmap2 \f$[km^{2}/s^{2}]\f$, Sigma \f$[km/s]\f$, Vel \f$[km/s]\f$, or SB \f$[kpc^{-2}]\f$
   vector<double> ValErr;         //!< Errors on Val
   double         Vmean = 0.;     //!< Mean velocity (used for Vel data only): to calculate/update with update_meanvelocity()
};

double beta_anisotropy(double const &r, double const par_ani[5]);
double beta_anisotropy_fr(double const &s, double const par_ani[5], double const &eps);
double beta_anisotropy_kernel(double const &r, double const par_ani[5], double const &R);
double beta_anisotropy_BAES(double const &r, double const par[4]);
double beta_anisotropy_CONSTANT(double const &r, double const par[1]);
double beta_anisotropy_OSIPKOV(double const &r, double const par[1]);
string beta_anisotropy_legend(double const par_ani[5], bool is_with_formula = false);

void   jeans_isigmap2_integrand(double &y_or_r, double par_jeans[27], double &res);
void   jeans_isigmap2_integrand_log(double &y_or_r, double par_jeans[27], double &res);
void   jeans_isigmap2_integrand_numerical(double &y, double par_jeans[26], double &res);
void   jeans_isigmap2_integrand_withkernel(double &y, double par_jeans[26], double &res);
double jeans_nuvr2(double &r, double par_jeans[26]);
void   jeans_nuvr2_integrand(double &s, double par_jeans[26], double &res);
void   jeans_nuvr2_integrand_log(double &s, double par_jeans[26], double &res);
double jeans_sigmap2(double &R, double par_jeans[26], bool is_use_kernel = true);

double light_i(double &R, double par_light[8]);
void   light_i_integrand(double &y, double par_light[8], double &res);
double light_i_norm(double par_light[8]);
void   light_i_norm_integrand(double &R, double par_light[8], double &res);
double light_nu(double &r, double par_light[8]);
void   light_nu_integrand(double &Y, double par_light[8], double &res);
double light_profile(double &rR_or_yY, double par_light[8], int switch_qty, double rmax_integr = -1.);
string light_profile_legend(double par_light[6], bool is_with_formula = false);
double light_profile_EXP2D(double &R_or_r, double par[2], bool is_deproject);
double light_profile_EXP3D(double &r_or_R, double par[2], bool is_project);
double light_profile_KING2D(double &R_or_r, double par[3], bool is_deproject);
double light_profile_PLUMMER2D(double &R_or_r, double par[2], bool is_deproject);
double light_profile_SERSIC2D(double &R_or_r, double par[3], bool is_deproject, double const &rmax_integr = 1.e10, double const &eps = 1.e-2);
double light_profile_ZHAO3D(double &r_or_R, double par[5], bool is_project, double const &rmax_integr = 1.e10, double const &eps = 1.e-2);


void   halo_jeans(vector<double> const &x, int switch_y);
void   halo_loadlist4jeans(string const &file_halos, vector<struct gStructHalo> &list_jeans_struct, vector<struct gStructJeansData> &list_jeansdata, int switch_y, bool is_clear = true);

void   load_jeansconfig(string const &filename, struct gStructJeansAnalysis &jeans_struct, double const& eps);
void   load_jeansdata(string const &filename, struct gStructJeansData &jeans_data, bool is_verbose);

double log_likelihood_jeans(double par_jeans[26], gStructJeansData &jeans_data);

double mass(double const &r, double const par_jeans[26]);

void   print_jeansanalysis_object(struct gStructJeansAnalysis const &jeans_struct);
void   print_jeansanalysis_setup(struct gStructJeansAnalysis const &jeans_struct);
void   print_pargas(double const par_gas[6]);
void   print_parjeans(double const par_jeans[26]);
void   print_parlight(double const par_light[8]);
void   printf_parjeans_header(FILE *fp, struct gStructJeansAnalysis const &jeans_struct, struct gStructJeansData const &jeans_data);
void   printf_parjeans_result(FILE *fp, struct gStructJeansAnalysis const &jeans_struct, double const &chi2min);

void   update_meanvelocity(struct gStructJeansData &jeans_data, bool is_verbose);

#endif

/*! \file jeans_analysis.h
  \brief Jeans analysis to get DM profiles from stars (light profile and velocity dispersion)


<b><A NAME="jeans_func"> \anchor jeans_func  Implementation in the code and functions</A></b>\n

 1. <b>Light profile quantities</b>
    - <i>Native 2D profiles (surface brightness)</i>: \c light_profile_EXP2D(), \c light_profile_KING2D(),
      \c light_profile_PLUMMER2D(), and \c light_profile_SERSIC2D().
       \param[in]    R_or_r                Projected distance (\f$R\f$) or distance (\f$r\f$) from halo centre [kpc]
       \param[in]    par[0-2]              Light: \f$I(R)\f$ +  scale radius [kpc] + shape #1 (SERSIC2D) or second scale radius (KING)
       \param[in]    is_deproject          Whether to return projected (native) or deprojected \f$\nu(r)\f$
       \param[in]    rmax                  Integration boundary [used only if SERSIC2D and is_deproject==true]
       \param[in]    eps                   Integration precision [used only if SERSIC2D and is_deproject==true]
       \returns      \f$I^{\rm XXX}(R)\f$ in [\f$L_\odot\f$] or \f$\nu^{\rm XXX}(r)\f$ in [\f$kpc^{-3}\f$], where \f${\rm XXX} =\f$ EXP2D, KING2D, PLUMMER2D,
                     or SERSIC2D
    \n\n\n
    - <i>Native 3D profiles (density profile)</i>: \c light_profile_EXP3D(), and \c light_profile_ZHAO3D().
       \param[in]    r_or_R                Distance (\f$r\f$) or projected distance (\f$R\f$) from halo centre [kpc]
       \param[in]    par[0-4]              Light: \f$\nu(r)\f$ + scale radius [kpc] + shape #(1,2,3) [only for ZHAO3D]
       \param[in]    is_project            Whether to return deprojected (native) or projected \f$I(R)\f$
       \param[in]    rmax                  Integration boundary [used only if ZHAO3D and is_project==true]
       \param[in]    eps                   Integration precision [used only if ZHAO3D and is_project==true]
       \returns      \f$\nu^{\rm XXX}(r)\f$ in [\f$kpc^{-3}\f$] or \f$I^{\rm XXX}(R)\f$ in [\f$L_\odot\f$], where \f${\rm XXX} =\f$ EXP3D, or ZHAO3D
    \n\n\n
    - light_profile() returns, depending on a switch, surface brightness \f$I(R)\f$
       (or integrand to calculate it), or density profile \f$f\nu(r)\f$ (or integrand to calculate it),
       for any light profile available in \c gENUM_LIGHTPROFILE (see params.h).
       \param[in]    rR_or_yY              Distance (depends on \c par_light[7]) if 3D: \f$r\f$ or \f$y=\sqrt{r^2-R^2}\f$; if 2D: \f$R\f$ or \f$Y=\sqrt{R^2-r^2}\f$  [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]          Radius \f$R\f$ (resp. \f$r\f$) [kpc] considered for projection (resp. deprojection)
       \param[in]    par_light[7]          Switch quantity to calculate:
                                              - if 1, surface brightness \f$I(R)=\Sigma(R)\f$ [\f$L_\odot\f$]
                                              - if 2, density profile \f$\nu(r)=\rho(r)\f$ [\f$kpc^{-3}\f$]
                                              - if 3, integrand to calculate \f$I(R)\f$ from \f$\nu(r)\f$, i.e. \f$2 \nu(y)\f$ with \f$y = \sqrt{r^2-R^2}\f$]
                                              - if 4, integrand to calculate \f$\nu(r)\f$ from \f$I(R)\f$, i.e.  \f$-\frac{1}{\pi R} \frac{dI(R)}{dR}\f$
       \param[in]    par_light[8]          Maximum radius for integration (if \c par_light[7]==3 or 4) [kpc]
       \param[in]    par_light[9]          Relative precision sought for integration (if \c par_light[7]==3 or 4)
       \returns      \f$I(R)\f$ in [\f$L_\odot\f$], or \f$\nu(r)\f$ in [\f$kpc^{-3}\f$], or \f$2\nu(y)\f$ in [\f$kpc^{-3}\f$],
                      or \f$-\frac{1}{\pi R}\frac{dI(R)}{dR}\f$ in [\f$L_\odot kpc^{-2}\f$]
    \n\n\n
    - light_i() returns the surface density brightness \f$I(R)\f$ calling light_profile().
       \param[in]    R                     Projected radius [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  Maximum radius for integration (if \c par_light[7]==3 or 4) [kpc]
       \param[in]    eps                   If not analytical, relative precision sought for the integration
       \returns      Surface density brightness \f$I(R)\f$ in [\f$L_\odot\f$]
    \n\n\n
    - light_i_integrand() calculates integrand of surface density brightness calling light_profile().
       \param[in]    y                     Variable on which integration is performed (at projected radius \f$R\f$): \f$y = \sqrt{r^2-R^2}\f$ [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$R\f$ [kpc] considered for projection
       \param[out]   res                   Integrand \f$2\nu(y)\f$ of \f$I(R) = 2 \int_0^\infty nu(y) dy\f$ in [\f$kpc^{-3}\f$]
    \n\n\n
    - light_i_norm() returns \f$\int_0^{r_{\rm max}} 2\pi \, R \, I(R) dR\f$, i.e. normalisation of surface brightness
                     (useful, e.g., to ensure \f$I(R)\f$ is a probability).
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  Maximum radius for integration [kpc]
       \param[in]    eps                   Relative precision sought for the integration
       \returns     \f$\int_0^{r_{\rm max}} 2\pi \, R \, I(R) dR\f$ in [\f$L_\odot kpc^2\f$]
    \n\n\n
    - light_i_norm_integrand() calculates \f$2\pi R I(R)\f$, i.e. integrand used in light_i_norm().
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$R\f$ [kpc] considered for projection
       \param[in]    par_light[7]   Maximum radius for integration [kpc]
       \param[out]   res                   Calculates \f$2\pi R I(R)\f$ in [\f$L_\odot kpc^2\f$]
    \n\n\n
    - light_nu() returns the 3D density profile \f$\nu(r)\f$ calling light_profile().
       \param[in]    r                     Radius [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    rmax                  If not analytical, maximum radius for integration [kpc]
       \param[in]    eps                   If not analytical, relative precision sought for the integration
       \returns      Stellar density \f$\nu(r)\f$ in [\f$kpc^{-3}\f$]
    \n\n\n
    - light_nu_integrand() calculates integrand of 3D density profile calling light_profile().
       \param[in]    Y                     Variable on which integration is performed (at radius \f$r\f$): \f$y = \sqrt{r^2-R^2}\f$ [kpc]
       \param[in]    par_light[0-5]        Light: par#1 (usually normalisation) + par#2 (usually scale radius [kpc]) + par#(3,4,5) [if used] + card_profile (see \c gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_light[6]   Radius \f$r\f$ [kpc] considered for deprojection
       \param[out]   res                   Integrand \f$-\frac{1}{\pi R} \frac{dI(R)}{dR}\f$ of \f$\nu(r) = -\frac{1}{\pi}\int_0^\infty \frac{dI(R)}{dR}\frac{dY}{R}\f$
                                           in [\f$L_\odot kpc^{-2}\f$]
    \n\n\n\n

 2. <b>Anisotropy \f$\beta_{\rm ani}(r)\f$ (no unit)</b>

    Anisotropy-related functions are of two types, which are listed and detailed below:\n
    -# <i>Anisotropy parametrisations</i> (see \c gENUM_ANISOTROPYPROFILE in \c params.h)\n
       - \c beta_anisotropy_BAES(): \f$\displaystyle\beta_{\rm ani}^{\rm BAES}(r)=\displaystyle\frac{\beta_0 + \beta_\infty (r/r_a)^\eta}{1+(r/r_a)^\eta}\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy \f$\beta_0\f$ at \f$r=0\f$
          \param[in]    par[1]                Anisotropy \f$\beta_\infty\f$ at \f$r=\infty\f$
          \param[in]    par[2]                Anisotropy scale radius \f$r_a\f$ [kpc]
          \param[in]    par[3]                Anisotropy shape parameter \f$\eta\f$
       \n\n
       - \c beta_anisotropy_CONSTANT(): \f$\displaystyle\beta_{\rm ani}^{\rm CONSTANT}(r)=\beta_0\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy \f$\beta_0\f$
       \n\n
       - \c beta_anisotropy_OSIPKOV(): \f$\displaystyle\beta_{\rm ani}^{\rm OSIPKOV}(r)=\displaystyle\frac{r^2}{r^2+r_a^2}\f$
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par[0]                Anisotropy scale radius \f$r_a\f$ [kpc]
    \n\n\n
    -# <i>Functions depending on the anisotropy profile</i>: they rely on a generic call with 5
    following anisotropy parameters, where the fifth one is the keyword selecting the parametrisation to consider.
       \param[in]    par_ani[0]            Anisotropy \f$\beta_0\f$      [unused for OSIPKOV]
       \param[in]    par_ani[1]            Anisotropy \f$\beta_\infty\f$ [unused for CONSTANT and OSIPKOV]
       \param[in]    par_ani[2]            Anisotropy scale radius \f$r_a\f$ [kpc]  [unused for CONSTANT]
       \param[in]    par_ani[3]            Anisotropy shape parameter \f$\eta\f$    [unused for CONSTANT and OSIPKOV]
       \param[in]    par_ani[5]            Anisotropy card_profile (see \c gENUM_ANISOTROPYPROFILE in \c params.h)

      \n\n

       - beta_anisotropy() for a generic call to any of the above anisotropy profiles \c beta_anisotropy_XXX().
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par_ani[0..5]         Anisotropy \f$\beta_0\f$      [unused for OSIPKOV]
          \returns      \f$\beta_{\rm ani}^{\rm XXX}(r)\f$ with \f${\rm XXX}\f$ any anistotropy profile
                                            from \c gENUM_ANISOTROPYPROFILE (see params.h)
       \n\n\n
       - beta_anisotropy_fr() calculates Eq.(2) above, i.e. \f$f(r)\f$ -- used in the calculation of
          \f$\nu(r)\, \bar{v_r^2}(r)\f$ --, for any of the above anisotropy profile.
          \param[in]    r                     Distance from halo centre [kpc]
          \param[in]    par_ani[0..5]  Anisotropy parameters
          \param[in]    eps                   Relative precision sought for the integration
          \returns    \f$f(r)= f_{r_1} \exp\left[\int_{r_1}^r \frac{2}{t}\beta_{\rm ani}(t)\, dt \right]\f$,
                             which is required for the calculation of \f$\nu(r)\, \bar{v_r^2}(r)\f$,
                             for any anistotropy profile from \c gENUM_ANISOTROPYPROFILE (see params.h).
       \n\n\n
       - beta_anisotropy_kernel() calculates the kernel \f${\cal K}(u,u_a)\f$ used for the calculation of
            \f$\sigma_p^2(R)\f$, see Eq.(4). Note that the Kernel does not exist for all anisotropy profiles
            (see, e.g., Appendix of Mamon & Lokas, MNRAS 363, 705 (2005)).
          \param[in]    r                     Radius from halo centre [kpc]
          \param[in]    par_ani[0..5]         Anisotropy parameters
          \param[in]    R                     Projected radius from halo centre [kpc]
          \returns    \f${\cal K}(u,u_a)\f$ (no unit) for anisotropy profiles for which it exists, otherwise abort()
       \n\n\n


 3. <b>Solution of the 'un'-projected and projected Jeans equation \f$\nu(r)\bar{v_r^2}(r)\f$ and \f$\sigma_p^2(R)\f$</b>\n
     All the function related to the solution of the Jeans equation rely on the same number of parameters (namely 20, related
     to the DM profile, the light profile, and the anisotropy profile). They are gathered below, using
     \f$f(r)=\f$beta_anisotropy_fr(), \f$f(r)=\f$beta_anisotropy_kernel(), light_nu() in [\f$kpc^{-3}\f$],
     and the DM halo and gas (if any) mass \f$M(s)\f$ in [\f$M_\odot\f$]:
    \n\n
    - jeans_nuvr2() returns \f$\displaystyle \frac{\nu(r)\bar{v_r^2}(r)}{G} = \frac{1}{f(r)} \int_r^\infty f(s) \nu(s) \frac{M(s)}{s^2}\, ds\f$
      in [\f$M_\odot kpc^{-4}\f$];
    \n
    - jeans_nuvr2_integrand() calculates \f$f(s)\,\nu(s)\,M(s) s^{-2}\f$ in [\f$M_\odot kpc^{-5}\f$],
     the integrand used in jeans_nuvr2() to get \f$\displaystyle\frac{\nu(r)\bar{v_r^2}(r)}{G}\f$;
    \n
    - jeans_nuvr2_integrand_log() calculates \f$s\times\f$ jeans_nuvr2_integrand() in [\f$M_\odot kpc^{-4}\f$].
       It is used to find radii where to cut the integrations of jeans_nuvr2_integrand() in different sub-parts
       (integrations are in log-scale).

       \param[in]    r_or_s              Radius from halo centre [kpc]
       \param[in]    par_jeans[0]        Dark matter profile normalisation \f$[M_\odot\,\,kpc^{-3}]\f$
       \param[in]    par_jeans[1]        Dark matter profile scale radius [kpc]
       \param[in]    par_jeans[2]        Dark matter shape parameter #1
       \param[in]    par_jeans[3]        Dark matter shape parameter #2
       \param[in]    par_jeans[4]        Dark matter shape parameter #3
       \param[in]    par_jeans[5]        Dark matter card_profile (see \c enum gENUM_PROFILE in \c params.h)
       \param[in]    par_jeans[6]        Dark matter maximum radius for integration [kpc]
       \param[in]    par_jeans[7]        Light profile normalisation
       \param[in]    par_jeans[8]        Light profile scale radius [kpc]
       \param[in]    par_jeans[9]        Light profile shape parameter #1
       \param[in]    par_jeans[10]       Light profile shape parameter #2
       \param[in]    par_jeans[11]       Light profile shape parameter #3
       \param[in]    par_jeans[12]       Light card_profile (see \c enum gENUM_LIGHTPROFILE in \c params.h)
       \param[in]    par_jeans[13]       Projected radius \f$R\f$ considered for calculations of projected quantities [kpc]
       \param[in]    par_jeans[14]       Relative precision sought for the integration
       \param[in]    par_jeans[15]       Anisotropy parameter #1
       \param[in]    par_jeans[16]       Anisotropy parameter #2
       \param[in]    par_jeans[17]       Anisotropy parameter #3
       \param[in]    par_jeans[18]       Anisotropy parameter #4
       \param[in]    par_jeans[19]       Anisotropy card_profile (see \c gENUM_ANISOTROPYPROFILE in \c params.h)
       \param[in]    par_jeans[20]       Gas profile normalisation \f$[M_\odot\,\,kpc^{-3}]\f$ (no gas if par_jeans[20]<=0)
       \param[in]    par_jeans[21]       Gas profile scale radius [kpc]
       \param[in]    par_jeans[22]       Gas shape parameter #1
       \param[in]    par_jeans[23]       Gas shape parameter #2
       \param[in]    par_jeans[24]       Gas shape parameter #3
       \param[in]    par_jeans[25]       Gas card_profile  (see \c enum gENUM_PROFILE in \c params.h)
       \param[out]   res                 See description in the above defined functions


\n


\remark It is sometimes useful to print for checks the content of the parameters used
        in the various functions, and we have here specifically for the Jeans analysis
        print_pargas(), print_parjeans(), and print_parlight()... in addition to the
        usual print_pardpdv(), print_parhost(), print_parmix(), print_parsubs(), and
        print_partot() for the halo parameters (in \c janalysis.h).


 4. <b>Log-likelihood for minimization relevant functions in \c CLUMPY</b> \n

    - load_jeansconfig(): fills from a file the parameters to use in a Jeans
      analysis (free and fixed parameters, priors, etc.). An example file is
      <A href="../data/params_jeans.txt" target="_blank">data/params_jeans.txt</A>.
       \param[in]   file_name       Name of the file frow which to load parameters
       \param[out]  jeans_struct  Structure in which to store Jeans analysis parameters
    \n\n
    - set_parjeans(): sets par_jeans[] (used in clumpy_jeans modules) from the structure
      content loaded by the above function.
       \param[in]   jeans_struct  Structure in which to store Jeans analysis parameters
    \n\n
    - log_likelihood_jeans_binned() and log_likelihood_jeans_unbinned()
       \param[in]   par_jeans[26]   Jeans parameters
       \param[in]   stat_halo       Structure in which to store Jeans analysis parameters
    \n\n\n

 5. <b>Series of functions used for displays of Jeans-related quantities from a list loaded with \c halo_loadlist4jeans(), \c load_jeansdata()
   - <em>Load list of files (data for jeans analysis) or single file</em>: must always be called first, to stores
       a list of halo properties in a vector.
      - \c halo_loadlist4jeans(): must always be called first in case of a Jeans analysis, to store the list of halo properties (for Jeans analysis, e.g. <A href="../data/list_generic_jeans.txt" target="_blank">list_generic_jeans.txt</A>).
          \param[in]  file_halos          Path to the list-formatted file to be read (\c string).
          \param[out] list_halos          Fill <c> vector<struct gStructJeansAnalysis> list_halos</c> to store halo properties
          \param[in]  switch_y            If data files are present, switch to determine which data to select:
                                             - 0 \f$\Rightarrow\f$ Kinematic data (e.g. \f$\sigma_{p}\f$)
                                             - 1 \f$\Rightarrow\f$ Surface brightness data \f$I(R)\f$
          \param[in]  is_clear[opt=true]     If multiple calls, keep only last file read (\c true), or merge all halos found (\c false)
      - \c load_jeansdata(): loads a Jeans analysis data file (Kinematics or Surface Brightness), stored either in a \c gStructJeansData or a \c gStructHalo structure.
           \param[in]  data_file          Jeans analysis data file to be read
           \param[in,out]  data_halo      Structure to store the data. It can be either a \c gStructJeansData or a \c gStructHalo structure.
           \param[in]  is_verbose         Chatter or not...
   - \c halo_jeans(): 1D Jeans analysis-related profiles \f$\sigma_p(R)\f$, \f$I(R)\f$ and \f$\beta_{ani}(r)\f$.
       \param[in]  x                      Vector of x-axis values (see \c switch_y)
       \param[in]  param_file             CLUMPY parameter file (e.g. <A href="../clumpy_params.txt" target="_blank">clumpy_params.txt</A>): then loads \c gLIST_HALOES_JEANS file
       \param[in]  switch_y               Switch to select y(x)
                                             - 0 \f$\Rightarrow y(x)=\sigma_p(R)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[km\,\,s^{-1}]\f$
                                             - 1 \f$\Rightarrow y(x)=I(R)\f$ with \f$[x]=[kpc]\f$ and \f$[y]=[L_\odot\,\,kpc^{-2}]\f$
                                             - 2 \f$\Rightarrow y(x)=\beta_{ani}(r)\f$ with \f$[x]=[kpc]\f$

*/

