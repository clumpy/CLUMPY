[![pipeline status](https://gitlab.com/clumpy/CLUMPY/badges/master/pipeline.svg)](https://gitlab.com/clumpy/CLUMPY/commits/master) [![coverage report](https://gitlab.com/clumpy/CLUMPY/badges/master/coverage.svg)](https://gitlab.com/clumpy/CLUMPY/commits/master)

# For detailed information including ways to install the program without the use of anaconda, see the [online documentation](http://lpsc.in2p3.fr/clumpy/) & [CLUMPY publications](http://adsabs.harvard.edu/abs/2012CoPhC.183..656C,2016CoPhC.200..336B,2018arXiv180608639H)

##  I. WHAT IS CLUMPY?


**Physics goal**   
CLUMPY addresses the following physics problems of dark matter annihilation/decay signals for all targets in the sky:   
   - *astrophysical J-factors* from spherical or triaxial DM distribution in the Galaxy (smooth, mean and/or drawn sub-haloes) and/or for nearby haloes;   
   - *&gamma;-ray and &nu; fluxes* for galactic and/or extragalactic contributions (combining the astrophysical factor and the particle physics factor);
   - *Jeans analyses* to reconstruct DM profiles from kinematic data (&chi;2 analysis or MCMC analysis) and calculate the associated J-factors.   

**History (of development)**  
- V1 or V11.09 [2011/09] -- *CLUMPY: A code for &gamma;-ray signals from dark matter structures*; [Charbonnier, Combet, & Maurin, CPC 183, 656 (2012)](http://cdsads.u-strasbg.fr/abs//2012CoPhC.183..656C).  
- V2 or V15.06 [2015/06] -- *CLUMPY v2: Jeans analysis, &gamma;-ray and &nu; fluxes from dark matter (sub-)structures;* [Bonnivard et al., CPC 200, 336 (2016)](http://cdsads.u-strasbg.fr/abs/2016CoPhC.200..336B).  
- V3 or V18.06 [2018/06] -- *CLUMPY v3: &gamma;-ray and &nu; signals from dark matter at all scales;* [Hütten, Combet, & Maurin (2018)](http://adsabs.harvard.edu/abs/2018arXiv180608639H).   

**Developers/contacts**   
For any question, or if you believe that you have a module that would be a bonus for a future CLUMPY release, feel free to [contact us](mailto:clumpy@lpsc.in2p3.fr) to discuss the matter:
- [Céline Combet](mailto:celine.combet@lpsc.in2p3.fr) (DM distributions, clump drawing, extragalactic module)
- [Moritz Hütten](mailto:mhuetten@mpp.mpg.de) (HEALPix/FITS implementation, APS, I/O interfaces, extragalactic module)
- [David Maurin](mailto:dmaurin@lpsc.in2p3.fr) (project coordinator)

*Past contributors*: Vincent Bonnivard (Jeans analysis module in v2), Aldée Charbonnier (wrote unreleased v0), and Emmanuel Nezri (particle physics module in v2).


## II. INSTALLATION AND COMPILATION

The CLUMPY package is written in C/C++ (but no classes) and is interfaced with several third-party softwares. Some are mandatory (other optionals) and must be installed before proceeding to the CLUMPY installation.

*NOTE*
We are in the process of preparing a new release, that includes reworking the installation procedure.
While we generally recommend to use the latest release version (3.1) for producing scientific results, this 
means, that the newer simplified installation is not available and the steps below might not apply.

Please consult the [online documentation](https://clumpy.gitlab.io/CLUMPY) if you are looking for
instructions on how to use older versions.

The instructions below only apply to the code on the master branch(!).

We assume the availability of general development tools such as Make, cmake, compilers and pkg-config
and do not include them in the environment files.
If you do not have them available for whatever reason, take a look at the
[.gitlab-ci.yml](.gitlab-ci.yml).

### Clone the git repository

Retrieve the code:
```
$ git clone https://gitlab.com/clumpy/CLUMPY.git --depth=1
```

and change the working directory:
```
cd CLUMPY
```

### Simplified install (recommended for new users)

As a pure user of the software, the easiest way is to use [anaconda](https://anaconda.org/) (or one of its derivatives such as mamba) and create an
environment for CLUMPY using one of the provided environment files.
You will then need to activate your environment when you want to use CLUMPY as they are separated from the system installations.

If you want to run the provided jupyter notebooks, you will need the `environment.yml` file.
If you only want to run the command-line tools and prefer a smaller environment,
you can get away with the smaller `environment-minimal.yml` file (in which case the name of the environment also changes to clumpy-minimal).

```
$ conda env create -f environment.yml
```

Next, switch to the newly created environment:

```
$ conda activate -f clumpy-full
```

Now you can compile the code:
```
pip install .
```

and the command-line tools as well as the python modules should be available.

You then need to set an environment variable `CLUMPY_DATA` pointing to the location of the directory containing the `data/` directory.
It should be copied to the conda environment into a location similar to
`$CONDA_PREFIX/lib/python3.11/site-packages/share/Clumpy/`.

You can set the environment variable using:
```
export CLUMPY_DATA=$CONDA_PREFIX/lib/python3.11/site-packages/share/Clumpy/
```

The specific path might differ slightly on your system, so make sure it exists and is filled with the content in `data/`.
In particular, the installed python version might differ in the future as we do not pin a version.

If you want to keep the repository, you can also point the variable there:
```
export CLUMPY_DATA=$PWD/data
```


### Test the installation

All tests are bundled in a single executable:
```
$ clumpy_tests
```

There is no functionality-test for the python-module yet. To test whether it is linked correctly, run:
```
python -c"import pyClumpy"
```

Additionaly, you can check the examples provided in ``demo_notebooks/pyClumpy*_Tutorial/``.


## Known Issues

### Cfitsio version mismatch

You might get a warning such as:
```
WARNING: version mismatch between CFITSIO header (v4.002) and linked library (v4.02).
```

We do not know how to get rid of it, but as long as the numbers match if you remove leading zeros
or round floats (yes, really!), it should be a false positive.

It is annoying nonetheless and on our radar, sorry.

### Pybindings python version conflicts

We do not know when exactly this occurs, but in some cases we observed issues between pybind11
and python 3.10 in the compile step.

This should not occur when the conda environment is used.


## III. License

CLUMPY is licensed under the [GNU General Public License (GPLv2)](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).


