# export file, so other projects can use this via `find_package`
include(CMakeFindDependencyMacro)

# we need to find the external depedencies here
# essentially we need to repeat all the `find_package` calls from the build
# here as `find_dependency`
find_package(OpenMP QUIET)
find_dependency(GSL)
include("${CMAKE_CURRENT_LIST_DIR}/cfitsio.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/healpix.cmake")

#cmakdefine
find_package(ROOT CONFIG QUIET)

# Include the auto-generated targets file
include("${CMAKE_CURRENT_LIST_DIR}/ClumpyTargets.cmake")
